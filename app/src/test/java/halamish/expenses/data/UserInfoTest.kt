package halamish.expenses.data

import com.google.firebase.Timestamp
import org.junit.Assert.assertEquals
import org.junit.Test

class UserInfoTest {
  @Test
  fun when_copy_shouldCopyAllData() {
    val info = UserInfo(
      uid = "abcd",
      email = "reemh@reem.com",
      uploadedToFirestore = true,
      logoutOnAutoLogin = true,
      projectName = "reem_project",
      deleted = true,
      edit = Timestamp.now()
    )
    val copy = info.copy()
    assertEquals(info, copy)
  }
}