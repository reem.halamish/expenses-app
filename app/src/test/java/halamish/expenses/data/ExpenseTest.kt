package halamish.expenses.data

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.threeten.bp.LocalDate
import kotlin.random.Random

class ExpenseTest {

    fun randomExpense() = Random.run {
        ExpenseEvent(
            id = nextInt(5).toString(),
            amount = nextDouble(100.0),
            date = LocalDate.of(nextInt(1990, 2020), nextInt(1, 11), nextInt(1, 25)),
            categoryId = nextInt().toString(),
        )
    }

    @Test
    fun queryMultipleValuesSameKey() {
        val list = mutableListOf<Pair<String, ExpenseEvent>>()
        val comparator: Comparator<Pair<String, ExpenseEvent>> = compareBy { it.first }
        for (i in 0 until 100) {
            var pair: Pair<String, ExpenseEvent>
            do {
                pair = randomExpense().let { it.id to it }
            } while (list.contains(pair))

            val unknownPosition = list.binarySearch(pair, comparator)
            if (unknownPosition >= 0) { // item with this key is in already
                if (list[unknownPosition] != pair) {
                    list.add(unknownPosition, pair)
                } else {
                    println(list)
                    println(pair)
                    fail("positive index + value already in")
                }
            } else {
                val position = -(unknownPosition + 1)
                list.add(position, pair)
            }
            assertEquals(list.sortedWith(comparator), list)
        }
    }
}