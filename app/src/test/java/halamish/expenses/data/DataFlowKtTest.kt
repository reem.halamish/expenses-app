package halamish.expenses.data

import org.junit.Test

import org.junit.Assert.*

class DataFlowKtTest {

    data class Human(val name: String? = null, val age: Int? = null)

    @Test
    fun mergePartialValues() {
        assertEquals(Human("reem", 26), mergeLeftRight(Human(name = "reem"), Human(age = 26)))
    }

    @Test
    fun mergePreferRight() {
        val left = Human("reem")
        val right = Human("adi", 22)
        val expected = Human("adi", 22)
        assertEquals(expected, mergeLeftRight(left, right, preferRightOnCollisions = true))
    }



    @Test
    fun mergePreferLeft() {
        val left = Human("reem")
        val right = Human("adi", 22)
        val expected = Human("reem", 22)
        assertEquals(expected, mergeLeftRight(left, right, preferRightOnCollisions = false))
    }
}