package halamish.expenses.data

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class ProjectDetailsTest {
  @Test
  fun when_callingClone_should_haveDifferentValue() {
    val first = ProjectDetails()
    val second = first.clone()
    assertEquals(first, second)
    second.currencyCode = "$"
    assertNotEquals(first, second)
  }
}