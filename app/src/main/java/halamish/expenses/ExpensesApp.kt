package halamish.expenses

import android.app.Application
import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.jakewharton.threetenabp.AndroidThreeTen
import extensions.android.IO
import halamish.expenses.logger.FileLoggerTree
import halamish.expenses.logger.Logger
import halamish.expenses.repo.CrashlyticsTree
import halamish.expenses.v2.PhoneDataHolderSpImpl
import halamish.expenses.v2.initializeV2
import halamish.expenses.v2.withLanguage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import timber.log.Timber
import timber.log.Timber.DebugTree
import java.util.*


@Suppress("unused") // used by manifest
class ExpensesApp : Application() {

    private val logger = Logger("ExpensesApp")

    // BEFORE onCreate()
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base?.withLanguage(PhoneDataHolderSpImpl(base).language))
    }

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        FirebaseAnalytics.getInstance(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(CustomDebugTree())
        }
        Timber.plant(CrashlyticsTree())
        Timber.plant(FileLoggerTree(this, CoroutineScope(Dispatchers.IO(Logger("FileLogger")))))

        val timestamp = Instant.now().atZone(ZoneId.systemDefault())
        logger.d("~~~ App started: ${BuildConfig.APPLICATION_ID} v${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE}) at $timestamp ~~~")
        initializeV2(this)
    }

    private class CustomDebugTree : DebugTree() {
        // remove all the "$collect$inlined$$suspend"
        override fun createStackElementTag(element: StackTraceElement): String? {
            return super.createStackElementTag(element)?.split("$")?.firstOrNull()
        }
    }
}


/**

todo:

- currency format:                 val string = NumberFormat.getCurrencyInstance().apply { setCurrency(Currency.getInstance(currency.currencyCode)) }.format(1500.3)

- Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored

- NOT WORKING log custom events ("tap save expense", "swiped to delete category") to analytics
currently using this - https://stackoverflow.com/a/49872548


- BUDGETS: add option to have budget for all / 1-category. allow changing budgets. allow accumulatings. show budget(s?) in the left drawer.

 */