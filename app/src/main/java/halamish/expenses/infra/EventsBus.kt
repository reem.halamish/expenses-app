package halamish.expenses.infra

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.coroutineScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import halamish.expenses.BuildConfig
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.io.Serializable

@Deprecated(message = "use direct communication instead")
class EventsBus<T: Serializable>(private val identifier: String,
                                 private val tClass: Class<T>,
                                 private val localBroadcastManager: LocalBroadcastManager) {

    interface ObservationCancelable {
        fun cancelObservation()
    }

    companion object {

        inline fun <reified Events : Serializable> withIdentifier(identifier: String) = EventsBus(
            identifier,
            Events::class.java,
            LocalBroadcastManager.getInstance(appRedux.app)
        )

        inline fun <reified Events: Serializable> from(fragmentCls: Class<out Fragment>)
                = withIdentifier<Events>("${fragmentCls.simpleName}_${Events::class.java.name}")
    }


    fun send(event: T) {
        if (BuildConfig.DEBUG) println("EventBus: event $event")
        localBroadcastManager.sendBroadcast(Intent(identifier).apply { putExtra("eventData", event) })
    }

    fun observeWhileHasScreen(lifecycle: Lifecycle, block: suspend (T) -> Unit) {
        val receiver = createReceiver(lifecycle, block)
        lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            fun startObserving() = localBroadcastManager.registerReceiver(receiver, IntentFilter(identifier))

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            fun stopObserving() = localBroadcastManager.unregisterReceiver(receiver)
        })
    }

    fun observeUntilDestroyed(lifecycle: Lifecycle, block: suspend (T) -> Unit) {
        val receiver = createReceiver(lifecycle, block)
        lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
            fun startObserving() = localBroadcastManager.registerReceiver(receiver, IntentFilter(identifier))

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun stopObserving() = localBroadcastManager.unregisterReceiver(receiver)
        })
    }

    // it's the user's responsibility to cancelObservation when finished listening to events!
    fun observe(block: suspend (T) -> Unit): ObservationCancelable {
        val receiver = createReceiver(lifecycle = null, block = block)
        localBroadcastManager.registerReceiver(receiver, IntentFilter(identifier))
        return object : ObservationCancelable {
            override fun cancelObservation() {
                localBroadcastManager.unregisterReceiver(receiver)
            }
        }
    }

    private fun createReceiver(lifecycle: Lifecycle? = null, block: suspend (T) -> Unit) = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent ?: return
            if (intent.action != identifier) return
            val event = intent.getSerializableExtra("eventData") ?: return
            if (!tClass.isInstance(event)) return
            @Suppress("UNCHECKED_CAST")
            event as T
            val scope = lifecycle?.coroutineScope ?: MainScope()
            scope.launch { block(event) }
        }
    }
}