package halamish.expenses.infra

interface ExceptionWithMetadata {
  val metadata: Map<String, String>
}