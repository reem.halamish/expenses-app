package halamish.expenses.infra

object ShortId {
    fun create(length: Int = 7): String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789"
        return (1..length)
                .map { allowedChars.random() }
                .joinToString("")
    }
}