package halamish.expenses.infra

import android.app.Application
import halamish.expenses.R
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

interface DateFormatter {
    fun format(date: LocalDate): String
}

class DateFormatterForDaysImpl(
    private val app: Application
): DateFormatter {
    private val dateFormatterPreviousYears =
        DateTimeFormatter.ofPattern("EEEE, MMMM dd yyyy") // "Saturday, July 14 2018"
    private val dateFormatterThisYear =
        DateTimeFormatter.ofPattern("EEEE, MMMM dd") // "Saturday, July 14"
    private val importantDates by lazy {
        mapOf(
            LocalDate.now() to app.getString(R.string.dateFormatterForDays_today),
            LocalDate.now()
                .minusDays(1) to app.getString(R.string.dateFormatterForDays_yesterday)
        )
    }

    override fun format(date: LocalDate): String {
        return importantDates[date]
            ?: takeIf { date.year == LocalDate.now().year }?.let { dateFormatterThisYear.format(date) }
            ?: dateFormatterPreviousYears.format(date)
    }
}
class DateFormatterForMonthsImpl : DateFormatter {
    private val dateFormatter =
        DateTimeFormatter.ofPattern("MMMM yyyy") // "July 2018"

    override fun format(date: LocalDate): String {
        return dateFormatter.format(date)
    }
}