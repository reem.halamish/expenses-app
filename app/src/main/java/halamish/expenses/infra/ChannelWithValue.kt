package halamish.expenses.infra

import halamish.expenses.ui.offerIfPossible
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

@ObsoleteCoroutinesApi
@Deprecated("use StateFlow")
class ChannelWithValue<T>(initialValue: T) {
    private val channel = ConflatedBroadcastChannel(initialValue)
    var value: T = initialValue
        set(value) {
            field = value
            channel.offerIfPossible(value)
        }

    fun listen() = channel.openSubscription()

    fun close() = channel.close()
}