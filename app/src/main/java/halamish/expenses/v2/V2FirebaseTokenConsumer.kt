package halamish.expenses.v2

import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import extensions.android.awaitComplete
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class V2FirebaseTokenConsumer(
  onNewToken: suspend (String) -> Unit,
  val scope: CoroutineScope = appRedux.scope, val eventBus: EventBus = appRedux.eventBus
) {

  init {
    scope.launch {
      val token = Firebase.messaging.token.awaitComplete()
      eventBus.send(NewFcmTokenEvent(token)) // we also emit this from `onTokenChanged()`
    }

    eventBus.onEveryEvent<NewFcmTokenEvent>(scope) {
      onNewToken(token)
    }
  }
}