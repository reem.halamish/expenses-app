@file:Suppress("KDocUnresolvedReference")

package halamish.expenses.v2

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.LocaleList
import androidx.annotation.RequiresApi
import extensions.android.toIterable
import halamish.expenses.BuildConfig
import halamish.expenses.v2.PhoneLanguage.*
import timber.log.Timber
import java.util.*


// https://stackoverflow.com/a/40849142/3339597
// for all methods here

fun Companion.canClearLanguage(): Boolean {
    return BuildConfig.DEBUG
//    // one day...
//    val mainLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//        (defaultLocaleList as LocaleList)[0]
//    } else defaultLocale
//    // if main locale is from a supported language, we're good to clear
//    return PhoneLanguage.values().any { it.locale.language == mainLocale.language }
}

private fun firstMatchingLanguage(context: Context) = PhoneLanguage.values().firstOrNull { it.hasMatch(context) }

fun PhoneData.needsLanguageSetup(context: Context): Boolean {
    if (language != null) return false // no further setup is needed
    return firstMatchingLanguage(context) == null // needs setup iff no matching language is organically set
}

fun PhoneData.isRtl(context: Context): Boolean {
    val language = this.language ?: firstMatchingLanguage(context)
    return when (language) {
        ENGLISH -> false
        HEBREW -> true
        null -> {
            Timber.w("unknown language, forcing LTR")
            false
        }
    }
}

private fun PhoneLanguage.hasMatch(context: Context): Boolean {
    val languageCode = this.locale.language
    val configuration = context.resources.configuration

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return configuration.locales.toIterable().any { languageCode == it.language }
    }

    @Suppress("DEPRECATION")
    val hasMatch = languageCode == configuration.locale.language
    @Suppress("DEPRECATION")
    Timber.d("phone locale: %s, language I'm checking: %s, hasMatch: %b", configuration.locale.language.toString(), languageCode, hasMatch)

    return hasMatch
}

fun Context.withLanguage(phoneLanguage: PhoneLanguage? = appRedux.state.value.phoneData.language): Context {
    // record defaults if not already recorded
    defaultLocale
    defaultLocaleList

    val context = this

    val configuration = when {
        phoneLanguage == null -> nullableConfigurationWithClearedLanguage()
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> {
            nullableConfigurationWithLanguageVersionNPlus(phoneLanguage)
        }
        else -> nullableConfigurationWithLanguagePreVersionN(phoneLanguage)
    }
    return if (configuration != null) context.createConfigurationContext(configuration) else context
}

/** @return null if [Context.resources.configuration] was not changed when producing cleared-language] */
private fun Context.nullableConfigurationWithClearedLanguage(): Configuration? {
    val context = this
    val res: Resources = context.resources
    val configuration = Configuration(res.configuration)

    if (defaultLocale != Locale.getDefault()) {
        Locale.setDefault(defaultLocale)
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val defaultList = defaultLocaleList as LocaleList
        if (defaultList != LocaleList.getDefault()) {
            LocaleList.setDefault(defaultList)
        }
    }

    var wasChanged = false
    @Suppress("DEPRECATION")
    if (defaultLocale != configuration.locale) {
        configuration.setLocale(defaultLocale)
        wasChanged = true
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val defaultList = defaultLocaleList as LocaleList
        if (defaultList != configuration.locales) {
            LocaleList.setDefault(defaultList)
            wasChanged = true
        }
    }

    return configuration.takeIf { wasChanged }
}

/** @return null if [Context.getResources.configuration] was not changed when producing language] */
private fun Context.nullableConfigurationWithLanguagePreVersionN(phoneLanguage: PhoneLanguage): Configuration? {
    val context = this
    val res: Resources = context.resources
    val configuration = Configuration(res.configuration)

    if (phoneLanguage.hasMatch(context)) return null // all good
    Locale.setDefault(phoneLanguage.locale)
    configuration.setLocale(phoneLanguage.locale)
    return configuration
}

/** @return null if [Context.getResources.configuration] was not changed when producing language] */
@RequiresApi(Build.VERSION_CODES.N)
private fun Context.nullableConfigurationWithLanguageVersionNPlus(phoneLanguage: PhoneLanguage): Configuration? {
    val context = this
    val res: Resources = context.resources
    val configuration = Configuration(res.configuration)

    if (phoneLanguage.hasMatch(context)) return null // all good

    val localeMutableList = mutableListOf<Locale>()


    for (i in 0 until configuration.locales.size()) {
        val locale = configuration.locales.get(i) ?: continue
        localeMutableList += locale
    }

    val existingLocale = localeMutableList.firstOrNull { it.language == phoneLanguage.locale.language }
    if (existingLocale != null) { // yay matching locale. let's use it
        localeMutableList.remove(existingLocale)
        localeMutableList.add(0, existingLocale) // add as first
    } else {
        localeMutableList.add(0, phoneLanguage.locale) // no matching locale found, use our custom one
    }
    val mostImportantLocale = localeMutableList.firstOrNull() ?: return null // no locales, can't operate

    Locale.setDefault(mostImportantLocale)
    configuration.setLocale(mostImportantLocale)

    val localeList = LocaleList(*localeMutableList.toTypedArray())
    LocaleList.setDefault(localeList)
    configuration.setLocales(localeList)
    return configuration
}

private val defaultLocale by lazy { Locale.getDefault() }
private val defaultLocaleList by lazy {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        LocaleList.getDefault()
    }
    else Unit
}
