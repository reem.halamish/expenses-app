package halamish.expenses.v2

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class UpdateProjectFcmTokenHook(
    scope: CoroutineScope = appRedux.scope,
    eventBus: EventBus = appRedux.eventBus,
    stateFlow: StateFlow<V2State> = appRedux.state,
    deviceId: String = appRedux.services.deviceId,
) {


    // update FCM token
    // NOTICE: null just means "no data yet".
    // there is no such a thing as "google completely removed FCM for this device" - they can only change the FCM
    init {
        scope.launch {
            stateFlow.map { listOf(it.fcmToken, it.project.fcmTokensByDeviceIds[deviceId]) }
                .distinctUntilChanged()
                .filter { it[0] != it[1] }
                .mapNotNull { it[0] }
                .collect { fcmToken ->
                    eventBus.sendAwaitingNext(UpdateProjectDetailsRunnable("update fcm token to [${ fcmToken.take(4)}...]") {
                        copy(fcmTokensByDeviceIds = fcmTokensByDeviceIds + (deviceId to fcmToken))
                    })
                }
        }
    }
}
