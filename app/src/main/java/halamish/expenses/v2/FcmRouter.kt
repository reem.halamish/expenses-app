package halamish.expenses.v2

import extensions.android.fromJson
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.repo.EmptyPkData
import halamish.expenses.repo.FcmRequestDataType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import timber.log.Timber

class FcmRouter(
    val eventBus: EventBus = appRedux.eventBus,
    val scope: CoroutineScope = appRedux.scope,
    val repo: V2FirestoreRepo = appRedux.services.firestoreRepo,
    val onFcmMessageReceived: () -> Unit
) {

    fun interface Handler {
        fun handle(actionType: String, data: Map<String, String>)
    }

    private val handlerByType = mutableMapOf(
        "created" to expenseCreatedHandler,
        "expense_created" to expenseCreatedHandler,
        FcmRequestDataType.NEW_USER_IN_PROJECT.name to newUserInProjectHandler,
        FcmRequestDataType.USER_REMOVED_FROM_PROJECT.name to userRemovedFromProjectHandler,
        "user_kicked_out" to userKickedFromProjectHandler,
        "logout_and_delete_local" to requestLogoutAndDeleteLocalStateHandler,
    )

    init {
        eventBus.onEveryEvent<GotFcmMessage>(scope) {
            onFcmMessageReceived()
            val type = msg["action_type"] ?: return@onEveryEvent
            handlerByType[type]?.handle(type, msg)
        }
    }
}

@Suppress("UNUSED_VARIABLE") // documenting the fcm's fields
private val newUserInProjectHandler = FcmRouter.Handler { actionType, data ->
    Timber.d("running newUserInProjectHandler: actionType:$actionType, data:$data")
    val projectId = data["project_id"] ?: return@Handler
    val newUserId = data["user_id"] ?: return@Handler
    val newUserEmail: String? = data["email"]
    val newUserDisplayName: String? = data["display_name"]
    // todo: if user is not me, send push notification :)
}

@Suppress("UNUSED_VARIABLE") // documenting the fcm's fields
private val userRemovedFromProjectHandler = FcmRouter.Handler { actionType, data ->
    Timber.d("running userRemovedFromProjectHandler: actionType:$actionType, data:$data")
    val projectId = data["project_id"] ?: return@Handler
    val removedUserId = data["user_id"] ?: return@Handler
    // todo: if user is me, send push notification
}

// todo: remove this?
private val userKickedFromProjectHandler = FcmRouter.Handler { actionType, data ->
    Timber.d("running userLoggedOutHandler: actionType:$actionType, data:$data")
    val projectIdKickedOut = data["original_project_id"] ?: return@Handler
    val creatorName = data["creator_name"]
    val creatorEmail = data["creator_email"]
    val creatorDescription = creatorName ?: creatorEmail ?: return@Handler

    appRedux.scope.launch {
        appRedux.eventBus.sendAwaitingResponse(V2UserLoginVerifier.RecheckUserValidation)
        val state = appRedux.state.first { it.userInfo != null && it.user is LoggedInUser && it.verificationState == V2VerificationState.ALL_GOOD }
        if (state.project.id != projectIdKickedOut) {
            // indeed kickout!
            appRedux.eventBus.send(FcmPushKickedFromProject(creatorDescription, projectId = projectIdKickedOut))
        }
    }
}

private val expenseCreatedHandler =
    FcmRouter.Handler { actionType: String, data: Map<String, String> ->
        Timber.d("running expenseCreatedHandler: actionType:$actionType")
        // val fullObjectPath = msg.data["full_path"] ?: return
        // val objectId = msg.data["object_id"] ?: return
        val serializedObject = data["object_data"] ?: return@Handler
        val subCollection = data["subcollection"] ?: return@Handler
        val projectId = data["project_id"] ?: return@Handler
        val myDeviceId = appRedux.services.deviceId

        // client guard: if got a push with project I'm no longer associated with, ignore
        if (projectId != appRedux.state.value.project.id) return@Handler

        // if this device was the trigger cause, ignore
        serializedObject.fromJson(EmptyPkData::class.java)?.let { pkData ->
            if (pkData.lastEditBy == myDeviceId) {
                Timber.w("change originated from me, ignoring.")
                return@Handler
            }
        }

        when (subCollection) {
            V2FirestoreRepo.subCollectionOneTimeExpenses -> {
                serializedObject.fromJson(OneTimeExpenseRecord::class.java)?.let { expense ->
                    appRedux.eventBus.send(FcmPushOneTimeExpenseCreated(expense))
                }
            }
            V2FirestoreRepo.subCollectionExpenseRules -> {
                serializedObject.fromJson(RecurringExpenseRule::class.java)?.let {
                    appRedux.eventBus.send(FcmPushRuleCreated(it))
                }
            }
            else -> {
                Timber.w("ignoring, subCollection not supported: $subCollection")
            }
        }
    }

private val requestLogoutAndDeleteLocalStateHandler = FcmRouter.Handler { actionType, data ->
    Timber.d("running requestLogoutAndClearHandler: actionType:$actionType, data: $data")
    val projectId = data["project_id"] ?: return@Handler
    val userId = data["user_id"] ?: return@Handler
    appRedux.scope.launch {
        val appState = withTimeoutOrNull(10_000) { appRedux.state.first { it.user is LoggedInUser || it.verificationState == V2VerificationState.ALL_GOOD } }
        val currentProject = appState?.project ?: appRedux.state.value.project
        if (currentProject.id != projectId) {
            Timber.w("project id mismatch, ignoring. fcm($projectId) local(${currentProject.id})")
            return@launch
        }
        val userInfo = appState?.userInfo
        if (userInfo != null && (userInfo.uid != userId)) {
            Timber.w("user info mismatch, ignoring. fcm($userId $userInfo) local(${userInfo})")
            return@launch
        }

        Timber.i("project match + user match (or local user), deleting user data")
        appRedux.handlers.deleteAllUserDataHandler.deleteAllUserData(useCase = "fcm push")
    }
}
