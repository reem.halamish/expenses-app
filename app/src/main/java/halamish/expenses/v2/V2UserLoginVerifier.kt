package halamish.expenses.v2

import android.os.Build
import androidx.annotation.Keep
import com.google.firebase.firestore.FieldValue
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.infra.ExceptionWithMetadata
import halamish.expenses.infra.ShortId
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.dataStore.V2DataStore
import halamish.expenses.repo.handlers.VerifyDeletedUserHandler
import halamish.expenses.repo.signUp
import halamish.expenses.v2.V2FirestoreRepo.Companion.collectionProjects
import halamish.expenses.v2.V2FirestoreRepo.Companion.collectionUsers
import halamish.expenses.v2.V2FirestoreRepo.Companion.downloadProject
import halamish.expenses.v2.V2FirestoreRepo.Companion.downloadUser
import halamish.expenses.v2.V2FirestoreRepo.Companion.uploadProject
import halamish.expenses.v2.V2FirestoreRepo.Companion.uploadUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import timber.log.Timber
import java.util.Date

val SESSION_START by lazy { Date() }
private val DEVICE_MODEL_DESCRIPTION by lazy {
  val device = Build.DEVICE
  val manufacturer = Build.MANUFACTURER
  if (device.startsWith(manufacturer, ignoreCase = true))
    device
  else "$manufacturer $device"
}

class V2UserLoginVerifier(
  private val onVerificationStateChanged: suspend (V2VerificationState) -> Unit,
  private val getCurrentFcmToken: () -> String? = { appRedux.state.value.fcmToken },
  scope: CoroutineScope = appRedux.scope,
  private val deviceId: String = appRedux.services.deviceId,
  private val eventBus: EventBus = appRedux.eventBus,
  private val state: StateFlow<V2State> = appRedux.state,
  private val firestoreRepo: V2FirestoreRepo = appRedux.services.firestoreRepo,
  private val analytics: AnalyticsLogger = appRedux.services.analytics,
  private val verifyDeletedUserHandler: VerifyDeletedUserHandler = appRedux.handlers.verifyDeletedUserHandler,
  private val dataStore: V2DataStore,
) {

  @Keep
  object RecheckUserValidation : V2Event(), HasResponse {
    override fun toString() = "RecheckUserValidation"
  }

  @Keep
  data class DeleteDeviceIdFromFirestoreBeforeLogout(val deleteFromUser: Boolean=true, val deleteFromProject: Boolean=true): V2Event(), HasResponse


  private val pingToCheckAgain = MutableStateFlow(1)

  init {
      eventBus.onEveryEvent<RecheckUserValidation>(scope) {
        onVerificationStateChanged(V2VerificationState.VERIFYING)
        pingToCheckAgain.value = pingToCheckAgain.value + 1
      }
    eventBus.onEveryEvent<DeleteDeviceIdFromFirestoreBeforeLogout>(scope) {
      val projectId = state.value.project.id
      val userId = state.value.userInfo?.uid
      if (deleteFromProject)
        doRemoveDeviceIdFromFirestoreProject(projectId)
      if (deleteFromUser && userId != null)
        doRemoveDeviceIdFromFirestoreUser(userId)
      eventBus.sendResponseFor(this)
    }
  }

  init {
    scope.launch {
      var lastValue: User?
      var curValue: User? = null
      state.map { it.user }
        .distinctUntilChanged()
        .flatMapLatest { user -> pingToCheckAgain.map { user } }
        .collect handleUser@{ user ->
          lastValue = curValue
          curValue = user
          Timber.d("lastValue: $lastValue, curValue: $curValue")

          when (user) {
            is LoggedInUser -> {
              /*
              1. fetch user data
              2. fetch project data
              3. make sure they match
              4. handle problems:
                  - (A) new user / deleted user (no firestore data)
                  - (B) user not in project (kicked out)
                  - (C) user.projectId != project.userId
               */
              onVerificationStateChanged(V2VerificationState.VERIFYING)
              dataStore.setBlocked(
                uploads=true,
                downloads=true,
                localChanges=true,
                resetTimestampHolder = false
              )
              dataStore.setFirestoreMightHaveModeData(false)

              val userId = user.userId
              val userInFirestore = firestoreRepo.downloadUser(userId)
              if (userInFirestore != null) {
                Timber.d("user exists in firestore: $userInFirestore, saving locally")
                dataStore.setUserFromFirestore(userInFirestore, "downloaded from firestore")
              }


              // to be used later (in cases we will need to upload a new clean project to firestore)
              val newProjectId = ShortId.create()
              val newProjectToUpload: ProjectDetails = dataStore.getCurrentProject().clone().copy(
                id = newProjectId,
                users = mapOf(userId to (user.email ?: "")).toMutableMap(),
                creator = userId,
                devices = mapOf(deviceId to SESSION_START),
                userIdByDeviceId = mapOf(deviceId to userId)
              )

              // handle no user
              if (userInFirestore == null) {
                Timber.i("empty user object in firestore! first login?")

                if (!user.manualLogin) { // auto login: verify (maybe old phone and user was deleted meanwhile?)
                  if (verifyDeletedUserHandler.wasRequestedDelete(user.userId) == true) {
                      Timber.e("auto-login for deleted user, will stop login and delete all local data. user:$user")
                    onVerificationStateChanged(V2VerificationState.ERROR_FOUND)
                    dataStore.deleteAllLocalData(createNewProject=true, reason = "local data is a deleted user, clear local")
                    dataStore.createDefaultCategories()
                    eventBus.sendAwaitingNext(RequestSignOutEvent)
                    return@handleUser
                  }
                }

                if (user.manualLogin) {
                  Timber.i("indeed first login")
                  verifyDeletedUserHandler.markUserAsNew(user.userId)
                }

                analytics.signUp(userId, method = user.methods.firstOrNull() ?: "unknown")

                Timber.d("creating new project-id: $newProjectId, will reference this new project both from firestore and locally")
                val newUser = UserInfo(
                  uid = userId,
                  email = user.email,
                  projectName = newProjectId,
                  devices = mapOf(deviceId to true)
                )

                firestoreRepo.uploadUser(newUser)
                firestoreRepo.uploadProject(newProjectToUpload)
                dataStore.setUserFromFirestore(newUser, "first time creating userInfo (both locally and in firestore)")
                dataStore.setProjectFromFirestore(newProjectToUpload, "first time uploading project - create project with new ID")
                onVerificationStateChanged(V2VerificationState.VERIFIED_UPLOADING_DATA)
                dataStore.setBlocked(uploads=false, resetTimestampHolder=true)
                dataStore.uploadExpensesCollectionsToFirestore()
                dataStore.setBlocked(downloads=false,localChanges=false)
                onVerificationStateChanged(V2VerificationState.ALL_GOOD)
                return@handleUser
              }

              // fix user data if needed
              // notice: no need to download (reflect) those changes as they don't matter for current session runtime, only for book-keeping
              if (deviceId !in userInFirestore.devices) {
                firestoreRepo.editDocFields(collectionUsers, userId, mapOf(
                  UserInfo::devices.name.dotDeviceId() to true,
                  UserInfo::edit.name to FieldValue.serverTimestamp(),
                  UserInfo::lastEditBy.name to deviceId,
                ))
              }

              // next: check project data
              val originalProjectInFirestore = firestoreRepo.downloadProject(userInFirestore.projectName)
              val projectInFirestore = if (originalProjectInFirestore != null) {
                Timber.d("project exists in firestore: $originalProjectInFirestore")
                originalProjectInFirestore
              } else {
                Timber.d("no project in firestore, uploading local project + referencing the user and setting data locally")
                firestoreRepo.uploadProject(newProjectToUpload)
                firestoreRepo.editDocFields(collectionUsers, userId, mapOf(
                  UserInfo::projectName.name to newProjectToUpload.id,
                  UserInfo::edit.name to FieldValue.serverTimestamp(),
                  UserInfo::lastEditBy.name to deviceId,
                ))
                newProjectToUpload
              }

              if (projectInFirestore.id != dataStore.getCurrentProject().id) {
                val deleteMsg = "project id differ - can't trust local, deleting all local data and will later download all expenses from firestore's data"
                Timber.w(deleteMsg)
                Timber.w(RuntimeExceptionWithMetadata(deleteMsg, metadata = mapOf(
                  "err_project_local" to dataStore.getCurrentProject().id,
                  "err_project_firestore" to projectInFirestore.id
                )))
                dataStore.deleteAllLocalData(createNewProject = false, reason = deleteMsg)
                dataStore.setUserFromFirestore(userInFirestore, reason = deleteMsg)
                dataStore.setProjectFromFirestore(projectInFirestore, reason = deleteMsg)
              }

              // handle kicked-out user: create a new project for them
              if (userId !in projectInFirestore.users && userId != projectInFirestore.creator) {
                val msg = "noticed user kicked-out from collaborating. will delete all local data and create a new project"
                Timber.w(msg)
                Timber.w(RuntimeExceptionWithMetadata(msg, metadata = mapOf(
                  "err_project_firestore" to projectInFirestore.id
                )))
                doRemoveDeviceIdFromFirestoreProject(projectInFirestore.id)
                newProjectToUpload.settings += "creation_reason" to "kickout:${projectInFirestore.id}"
                firestoreRepo.uploadProject(newProjectToUpload)
                firestoreRepo.editDocFields(
                  collectionUsers, userId, mapOf(
                    UserInfo::projectName.name to newProjectToUpload.id,
                    UserInfo::edit.name to FieldValue.serverTimestamp(),
                    UserInfo::lastEditBy.name to deviceId,
                  )
                )
                dataStore.deleteAllLocalData(createNewProject = false, reason = "user kicked out from project, delete local data")
                dataStore.setProjectFromFirestore(newProjectToUpload, "new project for a kicked-out collaborator user")
                dataStore.setUserFromFirestore(firestoreRepo.downloadUser(userId) ?: userInFirestore, "refreshing user info after creating post-kickout new project")
                onVerificationStateChanged(V2VerificationState.VERIFIED_UPLOADING_DATA)
                dataStore.setBlocked(uploads = false, resetTimestampHolder = true)
                dataStore.createDefaultCategories()
                dataStore.uploadExpensesCollectionsToFirestore()
                Timber.d("done handling kick-out, user now has their own project, all good :)")
                dataStore.setBlocked(downloads = false, localChanges = false)
                onVerificationStateChanged(V2VerificationState.ALL_GOOD)
                // todo: if we show UI rn, pop in a bottom sheet. else: push notification. text:
                //  "you were removed from ...'s project, so we created a whole new project for you!"
                return@handleUser
              }

              Timber.d("User has a valid project")
              dataStore.setProjectFromFirestore(projectInFirestore, "setting from firestore")
              val needsUploadExpenses = originalProjectInFirestore == null
              val firestoreMightHaveMoreData = user.manualLogin // on manual login, maybe the user was reconnected to an ancient project with a lot of data

              // fix user project if needed
              // notice: will also download (reflect) those changes as they are used locally as well
              val updates = mutableMapOf<String, Any>()
              val defaultUpdates = mapOf(
                ProjectDetails::devices.name.dot(deviceId) to SESSION_START,
                        ProjectDetails::edit.name to FieldValue.serverTimestamp(),
                        ProjectDetails::lastEditBy.name to deviceId,
              )
              with (projectInFirestore) {
                if (creator.isEmpty()) {
                  updates += ProjectDetails::creator.name to userId
                }
                if (users[userId] != (user.email ?: "")) {
                  updates += ProjectDetails::users.name.dot(userId) to (user.email ?: "")
                }
                if (userIdByDeviceId[deviceId] != userId) {
                  updates += ProjectDetails::userIdByDeviceId.name.dot(deviceId) to userId
                }
                val fcmToken = getCurrentFcmToken()
                if (fcmTokensByDeviceIds[deviceId] != fcmToken && fcmToken != null) {
                  updates += ProjectDetails::fcmTokensByDeviceIds.name.dot(deviceId) to fcmToken
                }
                if (deviceModelByDeviceId[deviceId] != DEVICE_MODEL_DESCRIPTION) {
                  updates += ProjectDetails::deviceModelByDeviceId.name.dot(deviceId) to DEVICE_MODEL_DESCRIPTION
                }
              }
              if (updates.isNotEmpty()) {
                updates += defaultUpdates
                firestoreRepo.editDocFields(collectionProjects, projectInFirestore.id, updates)
                firestoreRepo.downloadProject(projectInFirestore.id)?.let {
                  dataStore.setProjectFromFirestore(it, reason = "downloaded after updating fields on firebase: ${updates.keys}")
                }
              } else {
                // just update "last seen" so other users would know. no need to download (reflect) locally
                firestoreRepo.editDocFields(collectionProjects, projectInFirestore.id, defaultUpdates)
              }

              if (needsUploadExpenses) {
                onVerificationStateChanged(V2VerificationState.VERIFIED_UPLOADING_DATA)
                dataStore.setBlocked(uploads=false, resetTimestampHolder=true)
                dataStore.uploadExpensesCollectionsToFirestore()
              }

              if (firestoreMightHaveMoreData) {
                dataStore.setFirestoreMightHaveModeData(true)
              }
              dataStore.setBlocked(uploads=false,downloads=false,localChanges=false)
              onVerificationStateChanged(V2VerificationState.ALL_GOOD)
              return@handleUser
            }
            NotLoggedIn -> {
              onVerificationStateChanged(V2VerificationState.VERIFYING)
              dataStore.setFirestoreMightHaveModeData(false)

              when (val lastUser = lastValue) {
                is LoggedInUser -> {
                  Timber.i("LoginVerifier: User logout detected (was ${lastUser.userId})")
                }
                NotLoggedIn, null -> Unit // nothing to log - user was not logged in, they are still not logged in.
              }

              dataStore.ensureProjectCreatedProperlyLocally()

              Timber.d("anonymous user is ready. all good")
              dataStore.setBlocked(
                uploads=true,
                downloads=true,
                localChanges=false,
                resetTimestampHolder = false
              )
              onVerificationStateChanged(V2VerificationState.ALL_GOOD)
            }
          }
        }
    }
  }

  private suspend fun doRemoveDeviceIdFromFirestoreProject(projectId: String) {
    firestoreRepo.editDocFields(collectionProjects, projectId,
      mapOf(
        ProjectDetails::history.name.dotTimestamp() to "removed deviceId:$deviceId",
        ProjectDetails::devices.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::deviceModelByDeviceId.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::deviceNameByDeviceId.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::excludeNotifyDeviceId.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::fcmTokensByDeviceIds.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::userIdByDeviceId.name.dotDeviceId() to FieldValue.delete(),
        ProjectDetails::edit.name to FieldValue.serverTimestamp(),
      )
    )
  }

  private suspend fun doRemoveDeviceIdFromFirestoreUser(userId: String) {
    firestoreRepo.editDocFields(
      collectionUsers, userId,
      mapOf(
        UserInfo::devices.name.dotDeviceId() to FieldValue.delete(),
        UserInfo::lastEditBy.name to deviceId,
        UserInfo::edit.name to FieldValue.serverTimestamp(),
        )
    )
  }

  private fun String.dot(innerField: String) = "${this}.${innerField}"
  private fun String.dotDeviceId(): String = dot("deviceId")
  private fun String.dotTimestamp(): String = dot(Instant.now().epochSecond.toString())

  private class RuntimeExceptionWithMetadata(msg: String,
    override val metadata: Map<String, String>
  ): RuntimeException(msg), ExceptionWithMetadata
}
