package halamish.expenses.v2

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Keep
import halamish.expenses.ui.spWithName
import kotlinx.coroutines.CoroutineScope
import java.util.*


@Keep
enum class PhoneLanguage(
    val locale: Locale
    ) {
    ENGLISH(Locale.US),
    HEBREW(Locale("iw", "IL"));
    companion object
}

data class PhoneData(
    val deviceId: String = "",
    val language: PhoneLanguage? = null
)

interface PhoneDataHolder {
    @Keep
    data class LanguageChanged(val newLanguage: PhoneLanguage): V2Event(), HasResponse
    @Keep
    object LanguageCleared: V2Event(), HasResponse
    val language: PhoneLanguage?
}

class PhoneDataHolderSpImpl(
    val context: Context = appRedux.app,
    val sp: SharedPreferences = context.spWithName("phone_data"),
    ): PhoneDataHolder {


    override var language: PhoneLanguage?
        get() = sp.phoneLanguage
        set(value) {
            sp.phoneLanguage = value
        }

    private var SharedPreferences.phoneLanguage: PhoneLanguage?
        get() = getString(KEY_LANGUAGE, null)?.let { spValue -> PhoneLanguage.values().firstOrNull { it.name == spValue } }
        set(value) {
            edit().apply {
                if (value == null) remove(KEY_LANGUAGE)
                else putString(KEY_LANGUAGE, value.name)
            }.apply()
        }

    companion object {
        private const val KEY_LANGUAGE = "phone_language"
    }
}

class PhoneDataHolderWrapperImpl(
    val spHolder: PhoneDataHolderSpImpl = PhoneDataHolderSpImpl(appRedux.app),
    val scope: CoroutineScope = appRedux.scope,
    val eventBus: EventBus = appRedux.eventBus,
    onLanguageChanged: (PhoneLanguage?) -> Unit
) {


    init {
        onLanguageChanged(spHolder.language)

        eventBus.onEveryEvent<PhoneDataHolder.LanguageChanged>(scope) {
            spHolder.language = newLanguage
            onLanguageChanged(newLanguage)
            eventBus.sendResponseFor(this)
        }
        eventBus.onEveryEvent<PhoneDataHolder.LanguageCleared>(scope) {
            spHolder.language = null
            onLanguageChanged(null)
            eventBus.sendResponseFor(this)
        }
    }
}
