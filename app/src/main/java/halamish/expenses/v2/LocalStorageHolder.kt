package halamish.expenses.v2

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Keep
import halamish.expenses.BuildConfig
import halamish.expenses.ui.spWithName
import halamish.expenses.v2.NightModeRepo.*
import kotlinx.coroutines.CoroutineScope
import org.threeten.bp.Instant

class LocalStorageHolder(
    val context: Context = appRedux.app,
    val sp: SharedPreferences = context.spWithName("local_storage_data"),
    val scope: CoroutineScope = appRedux.scope,
    val eventBus: EventBus = appRedux.eventBus,
    onLocalStorageChanged: (description: String, newMap: Map<String, String>) -> Unit
) {

    @Keep
    data class ChangeValue(val key: String, val newValue: String?) : V2Event(), HasResponse

    private val map = mutableMapOf<String, String>()

    init {
        sp.all.entries.forEach { entry ->
            (entry.value as? String)?.let { value -> map[entry.key] = value }
        }
        onLocalStorageChanged("loaded local storage map", map.toMap())

        eventBus.onEveryEvent<ChangeValue>(scope) {
            if (newValue != null) {
                map[key] = newValue
                sp.edit().putString(key, newValue).apply()
            } else {
                map.remove(key)
                sp.edit().remove(key).apply()
            }
            onLocalStorageChanged("local storage: new value($newValue) for key($key)", map.toMap())
            eventBus.sendResponseFor(this)
        }

        eventBus.onEveryEvent<RequestDeleteLocalStorageData>(scope) {
            // here hook to delete local data if needed. current keys shouldn't be deleted but maybe future keys will
            eventBus.sendResponseFor(this)
        }
    }

    init {
        if (KEY_APP_INSTALL_VERSION_CODE !in map) {
            eventBus.send(ChangeValue(KEY_APP_INSTALL_VERSION_CODE, BuildConfig.VERSION_CODE.toString()))
        }
        if (KEY_APP_INSTALL_VERSION_NAME !in map) {
            eventBus.send(ChangeValue(KEY_APP_INSTALL_VERSION_NAME, BuildConfig.VERSION_NAME))
        }
    }
}


private const val KEY_SHOW_ADD_CATEGORIES_BUTTON = "KEY_SHOW_ADD_CATEGORIES_BUTTON"
private const val KEY_APP_INSTALL_VERSION_NAME = "KEY_APP_INSTALL_VERSION_NAME"
private const val KEY_APP_INSTALL_VERSION_CODE = "KEY_APP_INSTALL_VERSION_CODE"
private const val APP_INSTALL_VERSION_CODE_LOWEST = 26 // 2.2.0 (25) still didn't have this
private const val KEY_LAST_TIME_GOOGLE_PLAY_FEEDBACK_SHOWN = "KEY_LAST_TIME_GOOGLE_PLAY_FEEDBACK_SHOWN"
private const val KEY_NIGHT_MODE = "KEY_NIGHT_MODE"
private const val KEY_LAST_NEW_FEATURE_SHOWN = "KEY_LAST_NEW_FEATURE_SHOWN"




// ~~~ outer API ~~~ //
@Suppress("unused") // maybe one day it will be used
val LocalStorage.installVersionCode: Int
    get() = map[KEY_APP_INSTALL_VERSION_CODE]?.toInt()
        ?: APP_INSTALL_VERSION_CODE_LOWEST

val LocalStorage.showingAddCategoryButton: Boolean
    get() = map[KEY_SHOW_ADD_CATEGORIES_BUTTON] != "false"
fun LocalStorage.Companion.updateForShowingAddCategoryButton(showing: Boolean)
= LocalStorageHolder.ChangeValue(KEY_SHOW_ADD_CATEGORIES_BUTTON, if (showing) null else "false")

val LocalStorage.lastTimeGooglePlayFeedbackShown: Instant?
    get() = map[KEY_LAST_TIME_GOOGLE_PLAY_FEEDBACK_SHOWN]?.toLongOrNull()?.let { Instant.ofEpochMilli(it) }
fun LocalStorage.Companion.updateForLastTimeGooglePlayFeedbackShown(shown: Instant)
= LocalStorageHolder.ChangeValue(KEY_LAST_TIME_GOOGLE_PLAY_FEEDBACK_SHOWN, shown.toEpochMilli().toString())

val LocalStorage.nightMode: NightMode
    get() = map[KEY_NIGHT_MODE]?.let { NightMode.valueOf(it) } ?: NightMode.FOLLOW_PHONE
fun LocalStorage.Companion.updateForNightMode(nightMode: NightMode)
        = LocalStorageHolder.ChangeValue(KEY_NIGHT_MODE, nightMode.name)

val LocalStorage.lastNewFeatureShownVersionCode: Int?
    get() = map[KEY_LAST_NEW_FEATURE_SHOWN]?.toIntOrNull()
fun LocalStorage.Companion.updateForLastNewFeatureShownVersionCode(versionCode: Int)
 = LocalStorageHolder.ChangeValue(KEY_LAST_NEW_FEATURE_SHOWN, versionCode.toString())

// ~~~ END outer API ~~~ //