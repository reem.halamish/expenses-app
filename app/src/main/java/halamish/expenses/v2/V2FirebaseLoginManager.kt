package halamish.expenses.v2

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import extensions.android.awaitComplete
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.CustomAppKeys
import halamish.expenses.repo.logIn
import kotlinx.coroutines.CoroutineScope
import timber.log.Timber

class V2FirebaseLoginManager constructor(
  scope: CoroutineScope = appRedux.scope,
  eventBus: EventBus = appRedux.eventBus,
  analytics: AnalyticsLogger = appRedux.services.analytics,
  private val customAppKeys: CustomAppKeys = appRedux.services.customAppKeys,
  private val auth: FirebaseAuth = FirebaseAuth.getInstance(),
  val onNewUser: (newUser: User) -> Unit,
  val onRequestedManualLogin: () -> Unit,
  ) {

  private var didTriggerManualLogin = false

  private var lastNewUserCalled: User? = null
  private fun cacheCallOnNewUser(user: User) {
    if (user == lastNewUserCalled) return
    lastNewUserCalled = user
    onNewUser(user)
  }

  private val FirebaseUser?.toUser: User
    get() =
      if (this != null) LoggedInUser(
        userId = uid,
        isAnonymous = isAnonymous,
        email = email?.takeIf { it.isNotEmpty() }
          ?: "$uid@anonymous.login".takeIf { isAnonymous }
        ,
        displayName = displayName,
        methods = this.providerData.mapNotNull { it.providerId }.toSet(),
        manualLogin = didTriggerManualLogin,
      )
      else NotLoggedIn

  private fun overrideCustomKeyUserId(user: FirebaseUser?) {
    customAppKeys.setUserId(user?.uid ?: "")
  }

  init {
    overrideCustomKeyUserId(auth.currentUser)
    cacheCallOnNewUser(auth.currentUser.toUser)
    auth.addAuthStateListener { currentAuth ->
      Timber.d("auth state changed! ${currentAuth.currentUser}")
      overrideCustomKeyUserId(currentAuth.currentUser)
      cacheCallOnNewUser(currentAuth.currentUser.toUser)
    }
    eventBus.onEveryEvent<RequestGoogleSignInIntent>(scope) {
      didTriggerManualLogin = true
      onRequestedManualLogin()
    }
    eventBus.onEveryEvent<RequestAnonymousLogin>(scope) {
      didTriggerManualLogin = true
      onRequestedManualLogin()
      makeSureUserLoggedIn()
      eventBus.sendResponseFor(this)
    }
    eventBus.onEveryEvent<GoogleAccountLoginEvent>(scope) {
      Timber.d("will connect google account to firebase and login")
      val credential = GoogleAuthProvider.getCredential(account.idToken, null)
      val currentUser = auth.currentUser
      if (currentUser != null) {
        // link to existing user
        currentUser.linkWithCredential(credential).addOnSuccessListener {
          auth.currentUser?.uid?.let { userId ->
            analytics.logIn(userId, method = "google")
            eventBus.send(DidLogInEvent(userId))
          }
        }
      } else {
        auth.signInWithCredential(credential).addOnSuccessListener {
          auth.currentUser?.uid?.let { userId ->
            analytics.logIn(userId, method = "google")
            eventBus.send(DidLogInEvent(userId))
          }
        }
      }
    }
    eventBus.onEveryEvent<DidSignOutEvent>(scope) {
      auth.signOut()
      eventBus.send(DidLogOutEvent)
      didTriggerManualLogin = false
    }
    eventBus.onEveryEvent<RequestDeleteAuthUser>(scope) {
      val user = auth.currentUser ?: run {
        eventBus.send(DeleteAuthUserResult(success = false, exception = NoConnectedUser()))
        return@onEveryEvent
      }
      user.delete()
        .addOnSuccessListener {
          eventBus.send(DeleteAuthUserResult(success = true, exception = null))
        }
        .addOnFailureListener { exception ->
          Timber.e(exception, "exception while trying to delete auth user")
          eventBus.send(DeleteAuthUserResult(success = false, exception))
        }
    }
    eventBus.onEveryEvent<DeleteAuthUserResult>(scope) {
      eventBus.send(RequestSignOutEvent)
    }
  }

  private suspend fun makeSureUserLoggedIn() {
    auth.currentUser ?: run {
      auth.signInAnonymously().awaitComplete()
    }
  }
}

private class NoConnectedUser: RuntimeException("No connected user")