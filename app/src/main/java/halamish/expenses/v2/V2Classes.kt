package halamish.expenses.v2

import android.app.Application
import androidx.annotation.Keep
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.android.play.core.review.testing.FakeReviewManager
import halamish.expenses.BuildConfig
import halamish.expenses.app.AppLifecycleState
import halamish.expenses.app.AppLifecycleStateImpl
import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.backup.BackupAndRestoreServiceImpl
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.ExpensesDbItem
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.data.UserInfo
import halamish.expenses.infra.DateFormatter
import halamish.expenses.infra.DateFormatterForDaysImpl
import halamish.expenses.infra.DateFormatterForMonthsImpl
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.AvailableCategoriesColorsProvider
import halamish.expenses.repo.CategoriesColorsProvider
import halamish.expenses.repo.CustomAppKeys
import halamish.expenses.repo.CustomAppKeysImpl
import halamish.expenses.repo.FirebaseAnalyticsLogger
import halamish.expenses.repo.config.AppConfiguration
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.repo.dagger.DaggerNetworkComponent
import halamish.expenses.repo.dataRepo.DynamicLinksCreator
import halamish.expenses.repo.dataRepo.DynamicLinksCreatorImpl
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandlerImpl
import halamish.expenses.repo.dataStore.V2FirestoreTimestampHolder
import halamish.expenses.repo.dataStore.V2FirestoreTimestampHolderImpl
import halamish.expenses.repo.handlers.DeleteAllUserDataHandler
import halamish.expenses.repo.handlers.DeleteAllUserDataHandlerImpl
import halamish.expenses.repo.handlers.LocalDataDeleteHandlerImpl
import halamish.expenses.repo.handlers.UserLogoutHandler
import halamish.expenses.repo.handlers.UserLogoutHandlerImpl
import halamish.expenses.repo.handlers.VerifyDeletedUserHandler
import halamish.expenses.repo.handlers.VerifyDeletedUserHandlerImpl
import halamish.expenses.repo.icons.CurrencySymbolServer
import halamish.expenses.repo.icons.IconsRepo
import halamish.expenses.ui.popup.PopupRequest
import halamish.expenses.ui.popup.PopupsCreator
import halamish.expenses.ui.popup.PopupsCreatorImpl
import halamish.expenses.ui.spWithName
import halamish.expenses.v2.V2VerificationState.ALL_GOOD
import halamish.expenses.v2.V2VerificationState.NOT_STARTED
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import timber.log.Timber


interface V2App {
  val app: Application
  val scope: CoroutineScope
  val state: StateFlow<V2State>
  val eventBus: EventBus
  val services: V2Services
  val handlers: V2Handlers
}

// services - exposed to handlers
interface V2Services {
  val firestoreRepo: V2FirestoreRepo
  val firestoreTimestampHolder: V2FirestoreTimestampHolder
  val customAppKeys: CustomAppKeys
  val analytics: AnalyticsLogger
  val categoriesColorsProvider: CategoriesColorsProvider
  val currencySymbolServer: CurrencySymbolServer
  val currencyFormatter: () -> CurrencyFormatter
  val icons: IconsRepo
  val dynamicLinks: DynamicLinksCreator
  val deviceId: String
  val dateFormatterForDays: DateFormatter
  val dateFormatterForMonths: DateFormatter
  val projectUsersInvitationsHandler: ProjectUsersInvitationsHandler
  val googlePlayAppReviewManager: ReviewManager
  val nightModeRepo: NightModeRepo
  val backupAndRestore: BackupAndRestoreService
  val appLifecycle: AppLifecycleState
}

// handlers - exposed to UI layer
interface V2Handlers {
  val verifyDeletedUserHandler: VerifyDeletedUserHandler
  val deleteAllUserDataHandler: DeleteAllUserDataHandler
  val logoutHandler: UserLogoutHandler
  val popups: PopupsCreator
}

class V2AppImpl(
  override val app: Application,
  initialState: V2State = V2State(),
) : V2App {

  override val scope =
    CoroutineScope(SupervisorJob() + Dispatchers.Main.immediate + CoroutineExceptionHandler { context, exception ->
      Timber.e(exception, "Exception in main scope. context: $context")
    })

  private val deviceIdHolder = V2MobileDeviceId(app)

  override val state = MutableStateFlow(initialState.copy(phoneData = initialState.phoneData.copy(deviceId = deviceIdHolder.id)))

  override val eventBus = EventBusImpl(scope)

  override val services = object : V2Services {
    override val deviceId: String get() = deviceIdHolder.id
    override val customAppKeys = CustomAppKeysImpl()
    override val firestoreRepo = V2FirestoreRepoImpl(eventBus, deviceId)
    override val firestoreTimestampHolder = V2FirestoreTimestampHolderImpl(app)
    override val analytics = FirebaseAnalyticsLogger(app)
    override val categoriesColorsProvider = AvailableCategoriesColorsProvider(app)
    override val dynamicLinks = DynamicLinksCreatorImpl(app)

    private val daggerNetworkComponent by lazy { DaggerNetworkComponent.create() }
    override val icons by lazy { daggerNetworkComponent.iconsRepo() }
    override val currencySymbolServer by lazy { daggerNetworkComponent.currencySymbolServer() }
    override val currencyFormatter: () -> CurrencyFormatter
      get() = { CurrencyFormatter.get() }
    override val dateFormatterForDays = DateFormatterForDaysImpl(app)
    override val dateFormatterForMonths = DateFormatterForMonthsImpl()
    override val projectUsersInvitationsHandler = ProjectUsersInvitationsHandlerImpl(state, eventBus, firestoreRepo, firestoreTimestampHolder, deviceId)
    override val googlePlayAppReviewManager: ReviewManager by lazy {
      if (BuildConfig.DEBUG) FakeReviewManager(appRedux.app)
      else ReviewManagerFactory.create(appRedux.app)
    }
    override val nightModeRepo = NightModeRepoImpl(scope, state, eventBus)
    override val backupAndRestore = BackupAndRestoreServiceImpl(
      context = app,
      sp = app.spWithName("expenses_backup_snapshots_sp"),
      getCurrentUserId = { state.value.userInfo?.uid },
      getCurrentData = { state.value.data },
      eventBus = eventBus,
    )
    override val appLifecycle =  AppLifecycleStateImpl()
  }

  override val handlers = object : V2Handlers {
    val localDataDeleteHandler = LocalDataDeleteHandlerImpl(eventBus)

    override val deleteAllUserDataHandler = DeleteAllUserDataHandlerImpl(
      appState = state,
      eventBus = eventBus,
      firestoreRepo = services.firestoreRepo,
      localDataDeleteHandler = localDataDeleteHandler,
      backup = services.backupAndRestore,
    )
    override val logoutHandler = UserLogoutHandlerImpl(
      isUserConnected = state.map { when (it.user) {
        NotLoggedIn -> false
        is LoggedInUser -> true
      } },
      awaitUserVerified = { state.first { it.verificationState == ALL_GOOD }},
      eventBus = eventBus,
      localDataDeleteHandler = localDataDeleteHandler
    )

    override val verifyDeletedUserHandler = VerifyDeletedUserHandlerImpl(firestoreRepo = services.firestoreRepo)

    override val popups = PopupsCreatorImpl
  }

  fun update(desc: String, updater: V2State.() -> V2State) {
    val old = state.value
    val new = updater(old)
    if (old == new) {
      Timber.d("update($desc): nothing to update")
      return
    }
    Timber.i("update($desc)")
    state.value = new
  }
}

@Keep
enum class V2VerificationState { NOT_STARTED, VERIFYING, VERIFIED_UPLOADING_DATA, ALL_GOOD, ERROR_FOUND }

data class UserAuthState(val googleConnected: Boolean = false, val firebaseConnected: Boolean = false)
val UserAuthState.loading get() = googleConnected && !firebaseConnected

data class LocalStorage(internal val map: Map<String, String> = emptyMap()) { companion object }

data class V2State(
  val phoneData: PhoneData = PhoneData(),
  val auth: UserAuthState = UserAuthState(),
  val localStorage: LocalStorage = LocalStorage(),
  val user: User = NotLoggedIn,
  val userInfo: UserInfo? = null,
  val verificationState: V2VerificationState = NOT_STARTED,
  val project: ProjectDetails = ProjectDetails.EMPTY_INSTANCE,
  val fcmToken: String? = null,
  val config: AppConfiguration = AppConfiguration(),
  val downloadRequests: Set<String> = emptySet(),
  val popupRequests: List<PopupRequest> = emptyList(), // order is important - FIFO
  val data: ExpensesData = ExpensesData(),
) {
  val deviceId get() = phoneData.deviceId
}

data class ExpensesData(
  val project: ProjectDetails = ProjectDetails.EMPTY_INSTANCE,
  val categories: Map<String, ExpenseCategory> = emptyMap(),
  val oneTime: Map<String, OneTimeExpenseRecord> = emptyMap(),
  val rules: Map<String, RecurringExpenseRule> = emptyMap(),
) {
  val allExpensesDbItems: Map<String, ExpensesDbItem> by lazy { oneTime + rules }
}

sealed class User
object NotLoggedIn : User()
data class LoggedInUser(
  val userId: String,
  val email: String? = null,
  val isAnonymous: Boolean,
  val displayName: String? = null,
  val methods: Set<String> = emptySet(),
  val manualLogin: Boolean // true iff user interaction was the trigger to log-in
) :
  User()
