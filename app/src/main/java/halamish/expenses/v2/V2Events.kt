package halamish.expenses.v2

import android.content.Intent
import androidx.annotation.Keep
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.crashlytics.FirebaseCrashlytics
import halamish.expenses.data.*
import halamish.expenses.ui.popup.PopupRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import timber.log.Timber

interface EventBus {
  fun send(event: V2Event)
  fun register(): SharedFlow<V2Event>
}

inline fun <reified T : V2Event> EventBus.onEveryEvent(
  scope: CoroutineScope,
  crossinline onEvent: suspend T.() -> Unit
) {
  scope.launch {
    register().filterIsInstance<T>()
      .collect { onEvent(it) }
  }
}

inline fun <reified T> EventBus.sendResponseFor(event: T)
  where T : V2Event, T : HasResponse {
  send(V2EventResponse(event))
}

suspend inline fun <reified T, reified S> EventBus.sendAwaitingNext(event: T): S
  where
  T : V2Event,
  T : NextEvent<S>,
  S : V2Event {
  return register()
    .onSubscription { send(event) }
    .filterIsInstance<S>()
    .first()
}

suspend inline fun <reified T> EventBus.sendAwaitingResponse(event: T): V2EventResponse<T>
  where  T : V2Event, T : HasResponse {
  return register()
    .onSubscription { send(event) }
    .filterIsInstance<V2EventResponse<T>>()
    .filter { event == it.originalEvent }
    .first()
}


class EventBusImpl(private val scope: CoroutineScope) : EventBus {
  private val events = MutableSharedFlow<V2Event>(extraBufferCapacity = 16)
  override fun send(event: V2Event) {
    Timber.i("will send event $event")
    scope.launch {
      val result = withTimeoutOrNull(3_000) { events.emit(event) }
      if (result == null) {
        val exception = RuntimeException("event bus is blocked!")
        FirebaseCrashlytics.getInstance().recordException(exception)
        Timber.e(exception, "event bus is blocked")
      }
    }
  }

  override fun register() = events
}


// EVENTS:

sealed class V2Event

interface NextEvent<T : V2Event>
interface HasResponse

@Keep
data class V2EventResponse<T : V2Event>(
  val originalEvent: T
) : V2Event()


@Keep
object RequestSignOutEvent : V2Event(), NextEvent<DidSignOutEvent>

@Keep
object DidSignOutEvent : V2Event(), NextEvent<DidLogOutEvent>

@Keep
object DidLogOutEvent : V2Event()

@Keep
object RequestAnonymousLogin : V2Event(), HasResponse

@Keep
object RequestGoogleSignInIntent : V2Event(), NextEvent<GeneratedGoogleSignInIntent>

@Keep
data class GeneratedGoogleSignInIntent(
  val intent: Intent,
) : V2Event()

@Keep
data class GoogleSignInActivityResultEvent(
  val resultCode: Int, val data: Intent?
) : V2Event()

@Keep
data class GoogleAccountLoginEvent(
  val account: GoogleSignInAccount
) : V2Event(), NextEvent<DidLogInEvent>

@Keep
data class DidLogInEvent(val userId: String) : V2Event()

@Keep
data class RequestDeleteLocalExpensesData(val reason: String) : V2Event(), HasResponse

@Keep
data class RequestDeleteLocalStorageData(val reason: String) : V2Event(), HasResponse

@Keep
object RequestMigrateFromV1OldSpData : V2Event(), NextEvent<MigrateFromOldDataResult>

@Keep
object RequestDeleteV1OldSpData : V2Event(), HasResponse

@Keep
data class MigrateFromOldDataResult(
  val categories: Int = 0, // # of migrated data
  val expenseRules: Int = 0, // # of migrated data
  val oneTimeExpenses: Int = 0, // # of migrated data
) : V2Event() {
  val hasData: Boolean get() = categories + expenseRules + oneTimeExpenses > 0
}

@Keep
object RequestCreateDefaultCategories : V2Event(), HasResponse

@Keep
object RequestDeleteAuthUser : V2Event(), NextEvent<DeleteAuthUserResult>

@Keep
data class DeleteAuthUserResult(val success: Boolean, val exception: Exception?) : V2Event(), NextEvent<RequestSignOutEvent>

@Keep
data class UpdateUserInfo(val description: String, val updater: UserInfo.() -> UserInfo) : V2Event(),
  NextEvent<DidUpdateUserInfo>

@Keep
data class DidUpdateUserInfo(val success: Boolean, val original: UpdateUserInfo, val newValue: UserInfo?) : V2Event()

@Keep
class UpdateProjectDetailsRunnable(
  val description: String,
  val updater: ProjectDetails.() -> ProjectDetails
) : V2Event(),
  NextEvent<DidUpdateProjectDetailsRunnable> {
  // todo remove
  @Deprecated("use a constructor Project.() -> Project")
  constructor(@Suppress("UNUSED_PARAMETER") deprecated: Unit, description: String, selfUpdater: ProjectDetails.() -> Unit) :
    this(description, updater = { clone().apply(selfUpdater) })

  override fun toString(): String {
    return "UpdateProjectDetailsRunnable($description)"
  }
}

data class DidUpdateProjectDetailsRunnable(val success: Boolean, val newProjectDetails: ProjectDetails, val originalEvent: UpdateProjectDetailsRunnable) : V2Event()

@Keep
data class UpdateExpenseCategory(val newCategory: ExpenseCategory) : V2Event(), HasResponse

@Keep
data class OverrideExpenseCategories(val newCategories: Collection<ExpenseCategory>) : V2Event(),
  HasResponse

@Keep
data class RequestDeleteSingleCategory(val categoryId: String) : V2Event(), HasResponse

@Keep
data class UpdateExpenseRule(val newRule: RecurringExpenseRule) : V2Event(), HasResponse

@Keep
data class OverrideExpenseRules(val newRules: Collection<RecurringExpenseRule>) : V2Event(),
  HasResponse

// ignoreIfAbsent
@Keep
data class RequestDeleteSingleExpenseRule(val ruleId: String) : V2Event(), HasResponse

@Keep
data class UpdateOneTimeExpense(val newExpense: OneTimeExpenseRecord) : V2Event(), HasResponse

@Keep
data class OverrideOneTimeExpenses(val newExpenses: Collection<OneTimeExpenseRecord>) : V2Event(),
  HasResponse

// ignoreIfAbsent
@Keep
data class RequestDeleteSingleOneTimeExpense(val expenseId: String) : V2Event(), HasResponse

@Keep
data class NewFcmTokenEvent(val token: String) : V2Event()

@Keep
data class FcmPushOneTimeExpenseCreated(val expense: OneTimeExpenseRecord) : V2Event()

@Keep
data class FcmPushRuleCreated(val rule: RecurringExpenseRule) : V2Event()

@Keep
data class FcmPushKickedFromProject(val ownerDescription: String, val projectId: String) : V2Event()

@Keep
data class GotFcmMessage(val msg: Map<String, String>): V2Event()

@Keep
data class NewPopupRequestCreated(val request: PopupRequest): V2Event()

@Keep
data class PopupRequestWasHandled(val request: PopupRequest): V2Event()
