@file:Suppress("unused")

package halamish.expenses.v2

import androidx.annotation.Keep
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.Source
import extensions.android.awaitComplete
import extensions.android.fromJson
import extensions.android.toJson
import halamish.expenses.BuildConfig
import halamish.expenses.data.BackendRequest
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.repo.*
import halamish.expenses.v2.V2FirestoreRepo.*
import halamish.expenses.v2.V2FirestoreRepo.Companion.collectionFcmRequests
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.emptyFlow
import org.threeten.bp.Instant
import timber.log.Timber
import java.io.Serializable
import java.util.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface V2FirestoreRepo {
  enum class Source { SERVER, CACHE }
  suspend fun <T: Serializable> singleDownload(collection: String, documentId: String, clazz: Class<T>): T?
  suspend fun upload(collection: String, document: PkData, oldDocument: PkData? = null)
  suspend fun delete(collection: String, documentPk: String) // just marking as delete
  suspend fun deleteDocumentForReal(collection: String, documentId: String) // REALLY DELETE ALL DATA, be carefull with this
  suspend fun editDocFields(collection: String, documentPk: String, documentFields: Map<String, Any>)
  suspend fun requestFcm(fcmRequest: FcmRequestData)
  fun <T: Serializable> observeSingleDoc(collection: String, documentId: String, clazz: Class<T>): Flow<T?>
  suspend fun <T: Serializable> downloadCollectionOnce(collection: String, clazz: Class<T>): CollectionEmit<T>
  suspend fun <T: Serializable> downloadCollectionOnceWithFilters(collection: String, clazz: Class<T>, equalsFilters: Map<String, Any>): CollectionEmit<T>
  fun <T: Serializable> observeCollection(collection: String, clazz: Class<T>): Flow<CollectionEmit<out T>> // HOT FLOW!!

  data class CollectionDiffEmit<T>(val values: Collection<T>, val deleted: Set<String>, val fromServer: Boolean)
  data class CollectionEmit<T>(val values: Collection<T>, val fromServer: Boolean)

  data class Filtering(val fieldsToBeEqual: Map<String, Any> = emptyMap())

  fun <T: Serializable> observeDiffsInCollectionGreaterThenTimestamp(
    collection: String,
    timestamp: Long,
    clazz: Class<T>
  ): Flow<CollectionDiffEmit<T>> // HOT FLOW!!

  @Keep sealed class FirestoreEvent {
    @Keep data class FirestoreSnapshotDownloaded(val path: String, val filter: String, val size: Int): FirestoreEvent()
    @Keep data class FirestoreQueryObserverWasPlaced(val path: String): FirestoreEvent()
    @Keep data class FirestoreQuerySnapshotEmit(val path: String, val size: Int): FirestoreEvent()
    @Keep data class FirestoreItemUploaded(val path: String): FirestoreEvent()
    @Keep data class FirestoreDocumentDeleted(val path: String): FirestoreEvent()
  }

  companion object {
    const val collectionUsers = "users"
    const val collectionProjects = "expenses"
    const val collectionFcmRequests = "fcm_requests"
    const val collectionBackendRequests = "backend_requests"
    const val subCollectionCategories = "categories"
    const val subCollectionExpenseRules = "expense_rules"
    const val subCollectionOneTimeExpenses = "one_time_expenses"

    fun subCollectionProjectCategories(projectId: String) =
      "$collectionProjects/$projectId/$subCollectionCategories"

    fun subCollectionProjectExpenseRules(projectId: String) =
      "$collectionProjects/$projectId/$subCollectionExpenseRules"

    fun subCollectionProjectOneTimeExpenses(projectId: String) =
      "$collectionProjects/$projectId/$subCollectionOneTimeExpenses"

    suspend fun V2FirestoreRepo.downloadUser(userId: String) =
      singleDownload(collectionUsers, userId, UserInfo::class.java)

    suspend fun V2FirestoreRepo.uploadUser(userInfo: UserInfo) =
      upload(collectionUsers, userInfo, oldDocument = null)

    suspend fun V2FirestoreRepo.downloadProject(projectId: String) =
      singleDownload(collectionProjects, projectId, ProjectDetails::class.java)

    suspend fun V2FirestoreRepo.uploadProject(project: ProjectDetails) =
      upload(collectionProjects, project, oldDocument = null)

    suspend fun V2FirestoreRepo.doBackendRequest(request: BackendRequest) = upload(
      collectionBackendRequests, request, oldDocument = null)

    suspend fun V2FirestoreRepo.updateBackendRequest(request: BackendRequest, oldRequest: BackendRequest) = upload(
      collectionBackendRequests, request, oldDocument = oldRequest)
  }
}


@Suppress("EXPERIMENTAL_API_USAGE")
class V2FirestoreRepoImpl(
  val eventBus: EventBus,
  val deviceId: String,
  val firestore: FirebaseFirestore = FirebaseFirestore.getInstance(),
  val env: String? = calculateEnv()
) : V2FirestoreRepo {

  // using gson to make sure we pass through the real constructor and nullable fields will get initiated
  private fun <T: Serializable> DocumentSnapshot.toObjectEnsuringInitiation(clazz: Class<T>): T?
  = toObject(clazz)?.toJson()?.fromJson(clazz)

  override suspend fun <T: Serializable> singleDownload(
    collection: String,
    documentId: String,
    clazz: Class<T>
  ): T? {
    if (collection.isEmpty() || documentId.isEmpty()) {
      Timber.e("can't continue, empty data. collection = [${collection}], documentId = [${documentId}], clazz = [${clazz}]")
      return null
    }
    getCollection(collection)
      .document(documentId)
      .get()
      .awaitComplete()
      .let { snapshot ->
        Timber.d(FirestoreEvent.FirestoreSnapshotDownloaded(snapshot.reference.path, filter = "---", size=1).toString())
        return if (snapshot.exists())
          snapshot.toObjectEnsuringInitiation(clazz)
        else null
      }
  }

  override suspend fun upload(collection: String, document: PkData, oldDocument: PkData?) {
    Timber.d("will upload doc to ${getCollection(collection).path}/${document.pk()}")

    getCollection(collection)
      .document(document.pk())
      .set(
        document
          .withCreationTimestamp(oldDocument)
          .withClearedEditTimestamp()
          .withEditedBy(deviceId),
        SetOptions.merge()
      )
      .awaitComplete()
    Timber.d(FirestoreEvent.FirestoreItemUploaded("$collection/${document.pk()}").toString())

    calculateDeleteChangesMap(document, oldDocument)
      .takeIf { it.isNotEmpty() }
      ?.let { map ->
        getCollection(collection)
          .document(document.pk())
          .update(map)
          .awaitComplete()
        Timber.d(FirestoreEvent.FirestoreItemUploaded("$collection/${document.pk()}--deletions(${map.keys})").toString())
      }
  }

  private fun calculateDeleteChangesMap(newDoc: PkData, oldDoc: PkData?): Map<String, FieldValue> {
    oldDoc?.pk()?.let { if (it != newDoc.pk()) return emptyMap() }
    val changesMap = mutableMapOf<String, FieldValue>()
    newDoc.allNullableFields()
      .filter { it.name != PkData::created.name }
      .filter { it.name != PkData::edit.name }
      .filter { it.get() == null }
      .filter { newDocField -> oldDoc?.allNullableFields()?.firstOrNull { it.name == newDocField.name }?.get() != null }
      .forEach { changesMap += (it.name to FieldValue.delete()) }

    if (oldDoc != null) {
      newDoc.allMapTypeFields()
        .forEach { kProp ->
          val propName = kProp.name
          val mapInOld = oldDoc.allMapTypeFields().firstOrNull { it.name == kProp.name }?.get() ?: return@forEach
          val mapInNew = kProp.get()
          val missingKeys = mapInOld.keys - mapInNew.keys
          missingKeys.forEach { key -> changesMap += ("$propName.$key" to FieldValue.delete()) }
        }
    }
    return changesMap
  }

  override suspend fun delete(collection: String, documentPk: String) {
    getCollection(collection)
      .document(documentPk)
      .update(
        mapOf(
          PkData::deleted.name to true,
          PkData::lastEditBy.name to deviceId,
          PkData::edit.name to FieldValue.serverTimestamp()
        )
      )
      .awaitComplete()
    Timber.d(FirestoreEvent.FirestoreItemUploaded("$collection/$documentPk").toString())
  }

  override suspend fun deleteDocumentForReal(collection: String, documentId: String) {
    val ref = getCollection(collection).document(documentId)
    ref.delete().awaitComplete()
    Timber.d(FirestoreEvent.FirestoreDocumentDeleted(ref.path).toString())
  }

  override suspend fun editDocFields(
    collection: String,
    documentPk: String,
    documentFields: Map<String, Any>
  ) {
    getCollection(collection)
      .document(documentPk)
      .update(documentFields)
      .awaitComplete()
    Timber.d(FirestoreEvent.FirestoreItemUploaded("$collection/$documentPk").toString())
  }

  override suspend fun requestFcm(fcmRequest: FcmRequestData) {
    val docRef = getCollection(collectionFcmRequests).document(fcmRequest.id)
    docRef.set(fcmRequest)
    Timber.d(FirestoreEvent.FirestoreItemUploaded(docRef.path).toString())
  }

  override fun <T: Serializable> observeSingleDoc(
    collection: String,
    documentId: String,
    clazz: Class<T>
  ): Flow<T?> {
    if (collection.isEmpty() || documentId.isEmpty()) {
      Timber.e("can't continue, empty data. collection = [${collection}], documentId = [${documentId}], clazz = [${clazz}]")
      return emptyFlow()
    }
    return callbackFlow {
      val listener: EventListener<DocumentSnapshot> = EventListener { value, error ->
        Timber.d(FirestoreEvent.FirestoreQuerySnapshotEmit("$collection/$documentId", size = 1).toString())
        if (error != null) cancel("error with snapshot-listener", error)
        else if (value == null || !value.exists()) trySendBlocking(null)
        else trySendBlocking(value.toObjectEnsuringInitiation(clazz))
      }

      val cookie =
        getCollection(collection)
          .document(documentId)
          .addSnapshotListener(listener)

      Timber.d(FirestoreEvent.FirestoreQueryObserverWasPlaced("$collection/$documentId").toString())

      awaitClose { cookie.remove() }
    }
  }

  override suspend fun <T: Serializable> downloadCollectionOnce(collection: String, clazz: Class<T>): CollectionEmit<T> {
    if (collection.isEmpty()) throw IllegalArgumentException("empty collection")
    return suspendCoroutine { continuation ->
      val path = getCollection(collection).path
      val task = getCollection(collection).get(Source.SERVER)
      doDownloadCollection(path, filter = "---", clazz, task, continuation)
    }
  }

  override suspend fun <T : Serializable> downloadCollectionOnceWithFilters(collection: String, clazz: Class<T>, equalsFilters: Map<String, Any>): CollectionEmit<T> {
    return suspendCoroutine { continuation ->
      val path = getCollection(collection).path

      var task: Query = getCollection(collection)
      equalsFilters.entries.forEach { entry ->
        task = task.whereEqualTo(entry.key, entry.value)
      }
      doDownloadCollection(path, filter = equalsFilters.toString(), clazz, task.get(Source.SERVER), continuation)
    }
  }

  private fun <T: Serializable> doDownloadCollection(
    path: String,
    filter: String,
    clazz: Class<T>,
    task: Task<QuerySnapshot>,
    continuation: Continuation<CollectionEmit<T>>
  ) {
    task.addOnSuccessListener { snapshot ->
      Timber.d(FirestoreEvent.FirestoreSnapshotDownloaded(path, filter, size=snapshot?.documents?.size ?: 0).toString())

      if (snapshot == null) {
        continuation.resumeWithException(IllegalStateException("collection at path($path) filter($filter) 1time snapshot is null"))
      } else {
        val documents = snapshot.documents.mapNotNull { it.toObjectEnsuringInitiation(clazz) }
        val fromServer = snapshot.metadata.isFromCache.not()
        Timber.d("collection $path class ${clazz.simpleName}: got one-time download with ${documents.size} documents (fromServer: $fromServer)")
        continuation.resume(CollectionEmit(values = documents, fromServer = fromServer))
      }
    }.addOnFailureListener { exception ->
      continuation.resumeWithException(exception)
    }
  }

  override fun <T: Serializable> observeCollection(collection: String, clazz: Class<T>): Flow<CollectionEmit<out T>> {
    if (collection.isEmpty()) throw IllegalArgumentException("empty collection")
    return callbackFlow {
      val path = getCollection(collection).path
      val cookie = getCollection(collection)
        .addSnapshotListener { snapshot, error ->
          Timber.d(FirestoreEvent.FirestoreQuerySnapshotEmit(collection, size = snapshot?.documents?.size ?: 0).toString())
          when {
            error != null -> cancel("collection $path: error with snapshot-listener", error)
            snapshot == null -> Timber.w("collection $path: empty snapshot with no exception")
            else -> {
              val fromServer = snapshot.metadata.isFromCache.not()
              val documents = snapshot.documents.mapNotNull { it.toObjectEnsuringInitiation(clazz) }
              Timber.d("collection $path class ${clazz.simpleName}: got snapshot with ${documents.size} documents")
              trySendBlocking(CollectionEmit(documents,fromServer))
            }
          }
        }

      Timber.d(FirestoreEvent.FirestoreQueryObserverWasPlaced(collection).toString())

      awaitClose { cookie.remove() }
    }
  }

  override fun <T: Serializable> observeDiffsInCollectionGreaterThenTimestamp(
    collection: String,
    timestamp: Long,
    clazz: Class<T>
  ): Flow<CollectionDiffEmit<T>> {
    if (collection.isEmpty()) throw IllegalArgumentException("empty collection")
    return callbackFlow {
      val path = getCollection(collection).path
      val cookie = getCollection(collection)
        .whereGreaterThanOrEqualTo(PkData::edit.name, Date(timestamp))
        .addSnapshotListener { snapshot, error ->
          Timber.d(FirestoreEvent.FirestoreQuerySnapshotEmit(collection, size = snapshot?.documents?.size ?: 0).toString())

          when {
            error != null -> cancel("collection $path: error with snapshot-listener", error)
            snapshot == null -> Timber.w("collection $path: empty snapshot with no exception")
            else -> {
              val fromServer = snapshot.metadata.isFromCache.not()
              val documents: Collection<T> = snapshot.documents.mapNotNull { it.toObjectEnsuringInitiation(clazz) }
              val deleted: Set<String> = snapshot.documentChanges
                .filter { it.type == DocumentChange.Type.REMOVED }
                .mapNotNull { it.document.id }
                .toSet()
              Timber.d(
                "collection $path class ${clazz.simpleName}: got DIFF snapshot with ${documents.size} documents + ${deleted.size} deleted docs (timestamp: ${
                  Instant.ofEpochMilli(
                    timestamp
                  )
                })"
              )
              trySendBlocking(CollectionDiffEmit(documents, deleted, fromServer))
            }
          }
        }

      Timber.d(FirestoreEvent.FirestoreQueryObserverWasPlaced(collection).toString())

      awaitClose { cookie.remove() }
    }
  }

  private fun getCollection(collection: String): CollectionReference {
    return if (env != null) firestore.collection("env").document(env).collection(collection)
    else firestore.collection(collection)
  }
}

private fun calculateEnv(): String? {
  if (!BuildConfig.DEBUG) return null
  @Suppress("KotlinConstantConditions") // BuildConfig complains it is always "debug" (but we can build it in different variants)
  return when (val buildType = BuildConfig.BUILD_TYPE) {
    "debug", "beta", "rc" -> buildType
    else -> null
  }
}
