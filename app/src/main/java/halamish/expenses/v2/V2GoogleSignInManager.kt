package halamish.expenses.v2

import android.app.Activity
import android.app.Application
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.coroutines.CoroutineScope
import timber.log.Timber


private val options: GoogleSignInOptions by lazy {
  GoogleSignInOptions.Builder()
    .requestEmail()
    .requestProfile()
    .requestIdToken("818240945539-uapeh1d1474841hob9hgf0rjjm0a855s.apps.googleusercontent.com")
    .build()
}


class V2GoogleSignInManager constructor(
  app: Application = appRedux.app,
  scope: CoroutineScope = appRedux.scope,
  eventBus: EventBus = appRedux.eventBus,
  client: GoogleSignInClient = GoogleSignIn.getClient(app, options),
  val onConnectionChanged: (nowConnected: Boolean) -> Unit,
) {

  init {
    GoogleSignIn.getLastSignedInAccount(app).let { account ->
      if (account == null) {
        onConnectionChanged(false)
        return@let
      }
      if (account.isExpired) {
        client.silentSignIn().addOnSuccessListener {
          onConnectionChanged(true)
          eventBus.send(GoogleAccountLoginEvent(account))
        }
      } else {
        onConnectionChanged(true)
      }
    }
  }

  init {
    eventBus.onEveryEvent<GoogleSignInActivityResultEvent>(scope) {
      onSignInActivityResult(
        eventBus,
        resultCode,
        data
      )
    }
    eventBus.onEveryEvent<RequestSignOutEvent>(scope) {
      client.signOut().addOnSuccessListener {
        onConnectionChanged(false)
        eventBus.send(DidSignOutEvent)
      }
    }
    eventBus.onEveryEvent<RequestGoogleSignInIntent>(scope) {
      eventBus.send(
        GeneratedGoogleSignInIntent(client.signInIntent)
      )
    }
  }


  private fun onSignInActivityResult(eventBus: EventBus, resultCode: Int, data: Intent?) {
    if (resultCode == Activity.RESULT_CANCELED || data == null) {
      Timber.w("no data, Google sign in will probably fail")
    }

    val task = GoogleSignIn.getSignedInAccountFromIntent(data)
    try {
      // Google Sign In was successful
      val account = task.getResult(ApiException::class.java)
      onConnectionChanged(true)
      eventBus.send(GoogleAccountLoginEvent(account))
    } catch (e: ApiException) {
      // Google Sign In failed
      Timber.e(e, "Google sign in failed")
      onConnectionChanged(false)
    }
  }
}