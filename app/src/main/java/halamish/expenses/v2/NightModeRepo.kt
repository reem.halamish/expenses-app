package halamish.expenses.v2

import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatDelegate.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch


interface NightModeRepo {
    @Keep // their names are used in both saving to local SP and in sync with R.string.*** keys
    enum class NightMode { ALWAYS_DAY, ALWAYS_NIGHT, FOLLOW_PHONE }

    fun state(): Flow<NightMode>
    suspend fun setNew(nightMode: NightMode)
}

class NightModeRepoImpl(
    scope: CoroutineScope,
    private val appStateFlow: Flow<V2State>,
    private val eventBus: EventBus,
) : NightModeRepo {

    init {
        scope.launch {
            calculateNightModeFlow().collect {
                setDefaultNightMode(it.toAppCompatNightModeValue())
            }
        }
    }

    override fun state() = calculateNightModeFlow()

    override suspend fun setNew(nightMode: NightModeRepo.NightMode) {
        eventBus.sendAwaitingResponse(LocalStorage.updateForNightMode(nightMode))
    }

    private fun calculateNightModeFlow() = appStateFlow.map { it.localStorage.nightMode }.distinctUntilChanged()
}

private fun NightModeRepo.NightMode.toAppCompatNightModeValue() = when (this) {
    NightModeRepo.NightMode.ALWAYS_DAY -> MODE_NIGHT_NO
    NightModeRepo.NightMode.ALWAYS_NIGHT -> MODE_NIGHT_YES
    NightModeRepo.NightMode.FOLLOW_PHONE -> MODE_NIGHT_FOLLOW_SYSTEM
}