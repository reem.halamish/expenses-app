package halamish.expenses.v2

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.work.WorkManager
import halamish.expenses.backup.DeleteOldBackupsWorker
import halamish.expenses.backup.NightlyBackupWorker
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.logger.DeleteOldLoggerFilesWorker
import halamish.expenses.repo.PushNotificationSender
import halamish.expenses.repo.config.AppConfiguration
import halamish.expenses.repo.config.ConfigValuesGetter
import halamish.expenses.repo.dataStore.V2DataMigrationFromV1Sp
import halamish.expenses.repo.dataStore.V2DataStore
import halamish.expenses.ui.feedback.FeedbackInitiator
import halamish.expenses.ui.popup.PopupRequestsManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


val appRedux: V2App get() = appReduxImpl
private lateinit var appReduxImpl: V2AppImpl

fun initializeV2(app: Application) {
    if (::appReduxImpl.isInitialized) return
    appReduxImpl = V2AppImpl(app)
    PhoneDataHolderWrapperImpl(onLanguageChanged = { phoneLanguage ->
        appReduxImpl.update("new language: $phoneLanguage") {
            copy(
                phoneData = phoneData.copy(
                    language = phoneLanguage
                )
            )
        }
    })
    LocalStorageHolder(onLocalStorageChanged = { description, map ->
        appReduxImpl.update(description) { copy(localStorage = LocalStorage(map)) }
    })
    V2FirebaseTokenConsumer(
        onNewToken = {
            appReduxImpl.update("new FCM token: $it") { copy(fcmToken = it) }
        }
    )
    ConfigValuesGetter(onGotConfig = { appConfiguration: AppConfiguration ->
        appReduxImpl.update("new AppConfiguration (new size: ${appConfiguration.amount})") {
            copy(config = appConfiguration)
        }
    })
    val dataStore = V2DataStore(
        onProjectDetailsChanged = { description, projectDetails: ProjectDetails ->
            appReduxImpl.update("new ProjectDetails(${projectDetails.id}): $description") {
                copy(
                    project = projectDetails,
                    data = data.copy(project = projectDetails)
                )
            }
        },
        onUserInfoChanged = { description, info: UserInfo? ->
            appReduxImpl.update("${if (info != null) "new" else "delete"} UserInfo: $description") {
                copy(
                    userInfo = info
                )
            }
        },
        onCategoriesChange = {
            appReduxImpl.update("categories changed (new size:${it.size})") {
                copy(data = data.copy(categories = it))
            }
        },
        onExpenseRulesChange = {
            appReduxImpl.update("expense rules changed (new size:${it.size})") {
                copy(data = data.copy(rules = it))
            }
        },
        onOneTimeExpensesChange = {
            appReduxImpl.update("oneTimes changed (new size:${it.size})") {
                copy(data = data.copy(oneTime = it))
            }
        })

    V2GoogleSignInManager(onConnectionChanged = {
        appReduxImpl.update("google connection changed, isConnected:$it") {
            copy(auth = auth.copy(googleConnected = it))
        }
    })
    V2FirebaseLoginManager(
        onNewUser = {
            appReduxImpl.update("new user $it") {
                copy(
                    user = it,
                    auth = auth.copy(firebaseConnected = it is LoggedInUser),
                    verificationState = V2VerificationState.NOT_STARTED
                )
            }
        },
        onRequestedManualLogin = {
            appReduxImpl.update("requested manual login") { copy(downloadRequests = downloadRequests + "requested manual login") }
        },
    )
    V2UserLoginVerifier(
        onVerificationStateChanged = {
            appReduxImpl.update("verification changed to $it") { copy(verificationState = it) }
        },
        dataStore = dataStore,
    )
    FcmRouter(
        onFcmMessageReceived = {
            appReduxImpl.update("got FCM msg") { copy(downloadRequests = downloadRequests + "incoming FCM msg") }
        })
    V2DataMigrationFromV1Sp()
    UpdateProjectFcmTokenHook()
    V2ProjectCurrencyUpdator()
    PushNotificationSender()
    PopupRequestsManager(
        onCreatedNewPopupRequest = {
            appReduxImpl.update("new popup request: ${it.description}") {
                copy(
                    popupRequests = popupRequests + it
                )
            }
        },
        onHandledPopupRequest = {
            appReduxImpl.update("handled popup request: ${it.description}") {
                copy(
                    popupRequests = popupRequests - it
                )
            }
        }
    )
    FeedbackInitiator()
    CustomAppKeysUpdater(appReduxImpl.state, appReduxImpl.scope, appReduxImpl.services.customAppKeys)

    appReduxImpl.scope.launch(Dispatchers.Default) {
        // await until a visible screen to avoid loop on "app started in bg by workManager to run workers --> app changes the workers"
        appReduxImpl.services.appLifecycle.lifecycleState.first { it == Lifecycle.State.RESUMED }
        NightlyBackupWorker.setupNightly(WorkManager.getInstance(app))
        DeleteOldBackupsWorker.setupNightly(WorkManager.getInstance(app))
        DeleteOldLoggerFilesWorker.setupNightly(WorkManager.getInstance(app))
    }
}
