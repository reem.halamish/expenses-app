package halamish.expenses.v2

import android.app.Application
import android.content.SharedPreferences
import halamish.expenses.infra.ShortId
import halamish.expenses.ui.spWithName

class V2MobileDeviceId(
  private val sp: SharedPreferences = appRedux.app.spWithName("device_id")
) {
  constructor(app: Application) : this(app.spWithName("device_id"))

  val id: String = loadId() ?: createId()

  private fun loadId(): String? = sp.getString("phone_id", null)

  private fun createId(): String {
    val newId = ShortId.create()
    sp.edit().putString("phone_id", newId).apply()
    return newId
  }
}