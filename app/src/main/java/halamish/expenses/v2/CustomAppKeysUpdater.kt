package halamish.expenses.v2

import halamish.expenses.repo.CustomAppKeys
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CustomAppKeysUpdater(
  private val appStateFlow: StateFlow<V2State>,
  scope: CoroutineScope,
  private val customAppKeys: CustomAppKeys,
) {

  init {

    scope.launch {
      appStateFlow.collect { state ->
        customAppKeys.setValue("user_id", state.userInfo?.uid ?: "")
        customAppKeys.setValue("verification_state", state.verificationState.name)
        customAppKeys.setValue("project_id", state.project.id)
        customAppKeys.setValue("device_id", state.deviceId)
      }
    }
  }
}