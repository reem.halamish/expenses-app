package halamish.expenses.v2

import halamish.expenses.repo.icons.CurrencySymbolServer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class V2ProjectCurrencyUpdator(
  scope: CoroutineScope = appRedux.scope,
  private val state: StateFlow<V2State> = appRedux.state,
  private val eventBus: EventBus = appRedux.eventBus,
  private val currencySymbolServer: CurrencySymbolServer = appRedux.services.currencySymbolServer,

  onCurrencyChanged: suspend (currencyCode: String) -> Unit = {
    eventBus.sendAwaitingNext(UpdateProjectDetailsRunnable("new currency code: $it") {
      apply { currencyCode = it }
    })
  },
) {

  init {
    scope.launch {
      state
        .map { it.project }
        .distinctUntilChangedBy { listOf(it.currencyCode, it.id) }
        .filter { it.currencyCode == null }
        .collect {
          val response = withContext(Dispatchers.IO) {
            currencySymbolServer.getUserCurrency()
          }
          onCurrencyChanged(response.currencyCode)
        }
    }
  }
}