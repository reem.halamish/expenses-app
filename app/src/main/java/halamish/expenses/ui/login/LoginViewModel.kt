package halamish.expenses.ui.login

import android.content.Intent
import androidx.lifecycle.viewModelScope
import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.v2.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class LoginViewModel(
  val app: V2App = appRedux
) : ProtoViewModel() {

  val loggedIn: Flow<Boolean>
    get() = appRedux.state.map { it.user }.map { it is LoggedInUser }.distinctUntilChanged()

  suspend fun googleLoginIntent(): Intent {
    val ackEvent = app.eventBus.sendAwaitingNext(RequestGoogleSignInIntent)
    return ackEvent.intent
  }

  suspend fun anonymousLogin() {
    app.eventBus.sendAwaitingResponse(RequestAnonymousLogin)
  }

  fun onGoogleSignInActivityResult(resultCode: Int, data: Intent?) {
    viewModelScope.launch {
      app.services.backupAndRestore.recordSnapshot(BackupAndRestoreService.Reason.BEFORE_GOOGLE_LOGIN)
      app.eventBus.send(GoogleSignInActivityResultEvent(resultCode, data))
    }
  }
}
