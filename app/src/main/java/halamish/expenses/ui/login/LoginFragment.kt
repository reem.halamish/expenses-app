package halamish.expenses.ui.login


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import halamish.expenses.BuildConfig
import halamish.expenses.R
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


open class LoginFragment : Fragment() {
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        view.btn_google_connect.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                val intent = viewModel.googleLoginIntent()
                requireActivity().startActivityFromFragment(
                    this@LoginFragment,
                    intent,
                    REQUEST_GOOGLE_LOGIN
                )
            }
        }

        if (showAnonymousLogin) {
            view.tvAnonymousLogin.isVisible = true
            view.tvAnonymousLogin.paint?.isUnderlineText = true
            view.tvAnonymousLogin.setOnClickListener {
                viewLifecycleOwner.lifecycleScope.launch {
                    viewModel.anonymousLogin()
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            viewModel.loggedIn.filter { it }.first()
            runLoginAnimation()
            onLoggedIn()
        }
    }

    private suspend fun runLoginAnimation() {
        val animationDurationMs = 200L
        val afterAnimationDurationMs = 2000L
        val root = requireView().fragment_login_root
        val viewsToDisappear = mutableListOf<View>()
        val viewsToAppear = mutableListOf<View>()
        root.children.forEach { view ->
            if (view.id == R.id.signInSuccess) {
                viewsToAppear += view
            } else {
                viewsToDisappear += view
            }
        }
        viewsToDisappear
            .filter { it.visibility == View.VISIBLE }
            .forEach { view ->
                view.animate()
                    .alpha(0f)
                    .setDuration(animationDurationMs)
                    .withEndAction { view.visibility = View.GONE }
                    .start()
            }
        delay(animationDurationMs)

        viewsToAppear
            .filter { it.visibility != View.VISIBLE }
            .forEach { view ->
                view.visibility = View.VISIBLE
                view.alpha = 0f
                view.animate()
                    .alpha(1f)
                    .setDuration(animationDurationMs)
                    .start()
            }

        delay(animationDurationMs)
        delay(afterAnimationDurationMs)
    }


    // TODO convert one day
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != REQUEST_GOOGLE_LOGIN) return super.onActivityResult(
            requestCode,
            resultCode,
            data
        )

        viewModel.onGoogleSignInActivityResult(resultCode, data)
    }

    open fun onLoggedIn() {}

    open val showAnonymousLogin get() = BuildConfig.DEBUG

    companion object {
        private const val REQUEST_GOOGLE_LOGIN = 1
    }
}
