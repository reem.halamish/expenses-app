package halamish.expenses.ui.expensesHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.google.android.material.shape.CornerFamily
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
import halamish.expenses.R
import halamish.expenses.ui.dpToPx
import halamish.expenses.ui.expenseManipulation.ExpenseManipulationActivity
import kotlinx.android.synthetic.main.fragment_view_expenses_history.view.*
import kotlinx.android.synthetic.main.fragment_view_expenses_history_item_date.view.*
import kotlinx.android.synthetic.main.fragment_view_expenses_history_item_expense.view.*
import kotlinx.coroutines.launch

typealias UiItem = ViewExpensesHistoryViewModel.UiItem
typealias UiExpense = ViewExpensesHistoryViewModel.UiItem.Expense

class ViewExpensesHistoryFragment : Fragment(R.layout.fragment_view_expenses_history) {

  private val viewModel by viewModels<ViewExpensesHistoryViewModel>()
  private val adapter = Adapter { onItemClicked(it) }

  private fun onItemClicked(expense: UiExpense) {
    ExpenseManipulationActivity.startEditExpense(requireContext(), expense.dbId, canDelete = true)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    viewLifecycleOwner.lifecycleScope.launch {
      view.recycler.adapter = adapter
      viewModel.items.collect { items ->
        adapter.submitList(items)
        view.recycler.isVisible = items.isNotEmpty()
        view.expenseHistoryNoData.isVisible = items.isEmpty()
        view.recycler.setFastScrollEnabled(items.size > 20)
      }
    }
  }

  class DateViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val dateTextView: TextView = view.dateText
  }

  class ExpenseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val card: MaterialCardView = view.historyExpenseCardView
    val categoryImage: ImageView = view.historyExpenseCategoryImage
    val bottomSeparator: View = view.historyExpenseBottomSeparator
    val amount: TextView = view.historyExpenseAmountTextView
    val title: TextView = view.historyExpenseTitleTextView
    val subtitle: TextView = view.historyExpenseSubtitleTextView
    val repeat: View = view.historyExpenseImageRepeat
  }

  class SpaceViewHolder(view: View): RecyclerView.ViewHolder(view)

  class Adapter(val onItemClick: (UiExpense) -> Unit) :
    ListAdapter<UiItem, RecyclerView.ViewHolder>(UiItem.Callback), FastScrollRecyclerView.SectionedAdapter {
    override fun getItemViewType(position: Int) = getItem(position).viewType
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when (viewType) {
      UiItem.VIEW_TYPE_DATE -> {
        DateViewHolder(
          LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_view_expenses_history_item_date, parent, false)
        )
      }
      UiItem.VIEW_TYPE_EXPENSE -> {
        ExpenseViewHolder(
          LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_view_expenses_history_item_expense, parent, false)
        )
      }
      UiItem.VIEW_TYPE_SPACE -> {
        SpaceViewHolder(
          LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_view_expenses_history_item_space, parent, false)
        )
      }
      else -> throw IllegalArgumentException()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
      when (val item = getItem(position)) {
        is ViewExpensesHistoryViewModel.UiItem.Space -> {
          holder as SpaceViewHolder
          holder.itemView.minimumHeight = item.heightDp.dpToPx()
        }
        is ViewExpensesHistoryViewModel.UiItem.Date -> {
          holder as DateViewHolder
          holder.dateTextView.text = item.display
        }
        is ViewExpensesHistoryViewModel.UiItem.Expense -> {
          holder as ExpenseViewHolder
          holder.card.setCornerRadiusBy(item)
          holder.card.setOnClickListener { onItemClick(item) }
          holder.bottomSeparator.isVisible = !item.isLastInList
          holder.amount.text = item.amount
          holder.title.text = item.title
          holder.subtitle.text = item.subtitle
          holder.subtitle.isVisible = item.subtitle != null
          holder.repeat.isVisible = item.repeat
          Glide.with(holder.itemView.context)
            .load(item.categoryImg)
            .into(holder.categoryImage)
        }
      }
    }

    override fun getSectionName(position: Int): String = getItem(position).scrollSection


    private fun MaterialCardView.setCornerRadiusBy(item: UiExpense) {
      shapeAppearanceModel =
        shapeAppearanceModel.toBuilder()
          .setAllCorners(CornerFamily.ROUNDED, 0f)
          .apply {
            if (item.isFirstInList) {
              setTopLeftCornerSize(16.dpToPx().toFloat())
              setTopRightCornerSize(16.dpToPx().toFloat())
            }
            if (item.isLastInList) {
              setBottomLeftCornerSize(16.dpToPx().toFloat())
              setBottomRightCornerSize(16.dpToPx().toFloat())
            }
          }
          .build()
    }
  }
}