package halamish.expenses.ui.expensesHistory

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.ExpenseEvent
import halamish.expenses.data.granulateExpenses
import halamish.expenses.infra.DateFormatter
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.of
import halamish.expenses.repo.icons.icon8Url
import halamish.expenses.v2.V2State
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDate

class ViewExpensesHistoryViewModel(
  state: StateFlow<V2State> = appRedux.state,
  private val daysFormatter: DateFormatter = appRedux.services.dateFormatterForDays,
  private val monthsFormatter: DateFormatter = appRedux.services.dateFormatterForMonths,
) : ViewModel() {
  sealed class UiItem(val viewType: Int) {
    companion object {
      const val VIEW_TYPE_DATE = 1
      const val VIEW_TYPE_EXPENSE = 2
      const val VIEW_TYPE_SPACE = 3
      val Callback = object : DiffUtil.ItemCallback<UiItem>() {
        override fun areItemsTheSame(oldItem: UiItem, newItem: UiItem) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: UiItem, newItem: UiItem) = oldItem == newItem
      }
    }

    abstract val id: String
    abstract val scrollSection: String

    data class Space(override val id: String,
                     override val scrollSection: String,
                     val heightDp: Int) : UiItem(VIEW_TYPE_SPACE)
    data class Date(override val id: String,
                    override val scrollSection: String, val display: String) : UiItem(VIEW_TYPE_DATE)
    data class Expense(
      override val id: String,
      val dbId: String,
      override val scrollSection: String,
      val categoryImg: String,
      val amount: String,
      val title: String,
      val subtitle: String? = null,
      val repeat: Boolean = false,
      val isFirstInList: Boolean = false,
      val isLastInList: Boolean = false,
    ) : UiItem(VIEW_TYPE_EXPENSE)
  }

  val items = state.map { it.data }.distinctUntilChanged().map { data ->
    withContext(Dispatchers.Default) {
      fun ExpenseEvent.category() = data.categories[categoryId] ?: ExpenseCategory(id = categoryId)

      val currencyFormatter = CurrencyFormatter.of(data.project)

      val events = granulateExpenses(
        rules = data.rules.values, oneTimeRecords = data.oneTime.values,
        endDate = LocalDate.now()
      )
      val perDay = events.groupBy { it.date }
      val sortedKeys = perDay.keys.sortedDescending()
      val finalList = mutableListOf<UiItem>()
      if (sortedKeys.isNotEmpty()) {
        finalList += UiItem.Space(id="top-space", heightDp = 1, scrollSection = ".")
      }
      sortedKeys.forEach { date ->
        val monthString = monthsFormatter.format(date)
        val itemsForDate = perDay[date] ?: return@forEach
        finalList += UiItem.Date(id = date.toString(), display = daysFormatter.format(date), scrollSection = monthString)
        itemsForDate.forEach { expenseEvent ->

          finalList += UiItem.Expense(
            id = expenseEvent.id,
            dbId = expenseEvent.dbId,
            scrollSection = monthString,
            categoryImg = icon8Url(expenseEvent.category().icon),
            amount = currencyFormatter.format(expenseEvent.amount),
            title = expenseEvent.category().displayName.capitalize(),
            subtitle = expenseEvent.comments?.takeIf { it.isNotEmpty() }?.capitalize(),
            repeat = expenseEvent.recurring != null,
            isFirstInList = expenseEvent == itemsForDate.first(),
            isLastInList = expenseEvent == itemsForDate.last(),
          )
        }
      }
      if (sortedKeys.isNotEmpty()) {
        // leave a space below the FABs
        finalList += UiItem.Space(id="bottom-space", heightDp = 8, scrollSection = "",)
      }
      return@withContext finalList
    }
  }
}
