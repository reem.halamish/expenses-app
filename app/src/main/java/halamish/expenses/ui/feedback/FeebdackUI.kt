package halamish.expenses.ui.feedback

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import extensions.android.doOnceAt
import halamish.expenses.R
import halamish.expenses.ui.popup.PopupRequest
import halamish.expenses.ui.popup.PopupRequestCreator
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.temporal.ChronoUnit
import timber.log.Timber

class FeedbackInitiator(
    scope: CoroutineScope = appRedux.scope,
    flow: Flow<V2State> = appRedux.state,
    eventBus: EventBus = appRedux.eventBus,
) {

    init {
        val createdEventNowFlow = MutableStateFlow(false)
        val feedbackEnabledFlow = flow.map { state ->
            ((state.data.oneTime.size + state.data.rules.size) >= state.config.minimumExpensesCreateBeforeRequestingFeedback)
                    && state.localStorage.lastTimeGooglePlayFeedbackShown.let { it == null || it.until(Instant.now(), ChronoUnit.DAYS) >= 30
            }
        }

        eventBus.onEveryEvent<UpdateOneTimeExpense>(scope) {
            createdEventNowFlow.value = true
        }
        eventBus.onEveryEvent<UpdateExpenseRule>(scope) {
            createdEventNowFlow.value = true
        }

        scope.launch {
            createdEventNowFlow.combine(feedbackEnabledFlow) { created, enabled -> created && enabled }
                .distinctUntilChanged()
                .filter { it }
                .collect {
                    eventBus.send(NewPopupRequestCreated(PopupRequest.ReviewInGooglePlay))
                }
        }

        eventBus.onEveryEvent<PopupRequestWasHandled>(scope) {
            if (request == PopupRequest.ReviewInGooglePlay) {
                Timber.d("handled $request, marking timestamp")
                eventBus.send(LocalStorage.updateForLastTimeGooglePlayFeedbackShown(Instant.now()))
            }
        }
    }
}


// custom UI for feedback dialog. not used - we request feedback directly from google play's UI.
// keeping this for a day when we might want specific feedback for specific features.
@Deprecated("We use google play UI as feedback screen", replaceWith = ReplaceWith("<no replacement>"))
private fun createPopupFeedbackRequest() = PopupRequest.Descriptor(
    description = "user's feedback popup",
    creator = FeedbackPopupCreator
)

object FeedbackPopupCreator: PopupRequestCreator {
    override fun showPopup(
        context: Context,
        lifecycleOwner: LifecycleOwner,
        data: Any?,
        onDone: () -> Unit
    ): DialogInterface {
        val dialog = AlertDialog.Builder(context)
            .setView(R.layout.dialog_user_feedback)
            .create()

        dialog.show()
        lifecycleOwner.doOnceAt(Lifecycle.Event.ON_DESTROY) { dialog.cancel() }
        dialog.setOnDismissListener { onDone() }

        // todo:
//        dialog.findViewById<>()
        // disable submit button until getting feedback value on the rating bar
        // until then: clicks on the submit button will trigger ripple BG on the rating bar
        // on 4 or 5 stars, say "yay! rate us on playstore please?"
        // on 3 - 1 stars, say "Sorry to hear that, do you like to tell us why?"
        // on click exit, just exit.
        // on dialog dismiss / cancel - update the last shown to be now (or maybe on dialog shown?)
        // on submit with input text or in sending user to app store, dismiss the dialg and send the data to firebase.

        return dialog
    }
}