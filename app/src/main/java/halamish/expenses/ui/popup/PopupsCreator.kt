package halamish.expenses.ui.popup

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import halamish.expenses.R
import halamish.expenses.databinding.DialogConfirmDestructiveOperationBinding
import halamish.expenses.databinding.DialogInfromativeBinding
import halamish.expenses.repo.confirmDestructiveActionPopup
import halamish.expenses.repo.infromativePopup
import halamish.expenses.ui.popup.PopupsCreator.CancellationPolicy.CANCEL_ON_TAP_OUTSIDE
import halamish.expenses.ui.popup.PopupsCreator.CancellationPolicy.CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE
import halamish.expenses.ui.popup.PopupsCreator.CancellationPolicy.CANT_CANCEL
import halamish.expenses.v2.appRedux

interface PopupsCreator {

  fun showInformativePopup(
    analyticsScreenName: String,
    context: Context,
    lifecycle: Lifecycle,
    cancelPolicy: CancellationPolicy = CANCEL_ON_TAP_OUTSIDE,
    messageText: CharSequence,
    buttonText: String,
    onButtonClick: () -> Unit = {},
  )

  fun showDestructivePopup(
    analyticsScreenName: String,
    context: Context,
    lifecycle: Lifecycle,
    cancelPolicy: CancellationPolicy = CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE,
    messageText: CharSequence,
    destructiveButtonText: String,
    secondaryButtonText: String,
    onDestructiveButtonClick: () -> Unit,
    onSecondaryButtonClick: () -> Unit,
  )

  enum class CancellationPolicy {
    CANT_CANCEL,
    CANCEL_ON_TAP_OUTSIDE,
    CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE,
  }
}

object PopupsCreatorImpl : PopupsCreator {


  override fun showInformativePopup(
    analyticsScreenName: String,
    context: Context,
    lifecycle: Lifecycle,
    cancelPolicy: PopupsCreator.CancellationPolicy,
    messageText: CharSequence,
    buttonText: String,
    onButtonClick: () -> Unit
  ) {

    val analytics = appRedux.services.analytics

    val view =
      LayoutInflater.from(context).inflate(R.layout.dialog_infromative, null)
    val binding = DialogInfromativeBinding.bind(view)
    val dialog = AlertDialog.Builder(context).setView(view)
      .setCancelable(cancelPolicy.cancelOnTapOutside)
      .create()

    dialog.show()
    analytics.infromativePopup(analyticsScreenName, "shown")

    binding.viewTitle.text = messageText
    binding.btnConfirm.text = buttonText
    binding.btnConfirm.setOnClickListener {
      analytics.infromativePopup(analyticsScreenName, "click_confirm")
      dialog.dismiss()
      onButtonClick()
    }

    lifecycle.addObserver(object : DefaultLifecycleObserver {
      override fun onPause(owner: LifecycleOwner) {
        if (cancelPolicy.cancelOnLifecyclePause && dialog.isShowing) {
          analytics.infromativePopup(analyticsScreenName, "cancel_automatic")
          dialog.cancel()
        }
      }

      override fun onDestroy(owner: LifecycleOwner) {
        if (dialog.isShowing) {
          analytics.infromativePopup(analyticsScreenName, "cancel_screen_killed")
          dialog.dismiss()
        }
      }
    })
  }

  override fun showDestructivePopup(
    analyticsScreenName: String,
    context: Context,
    lifecycle: Lifecycle,
    cancelPolicy: PopupsCreator.CancellationPolicy,
    messageText: CharSequence,
    destructiveButtonText: String,
    secondaryButtonText: String,
    onDestructiveButtonClick: () -> Unit,
    onSecondaryButtonClick: () -> Unit
  ) {

    val analytics = appRedux.services.analytics

    val view =
      LayoutInflater.from(context).inflate(R.layout.dialog_confirm_destructive_operation, null)
    val binding = DialogConfirmDestructiveOperationBinding.bind(view)
    val dialog = AlertDialog.Builder(context).setView(view)
      .setCancelable(cancelPolicy.cancelOnTapOutside)
      .create()

    dialog.show()
    analytics.confirmDestructiveActionPopup(analyticsScreenName, "shown")

    binding.viewTitle.text = messageText
    binding.btnConfirmDelete.text = destructiveButtonText
    binding.btnConfirmDelete.setOnClickListener {
      analytics.confirmDestructiveActionPopup(analyticsScreenName, "click_destructive")
      dialog.dismiss()
      onDestructiveButtonClick()
    }
    binding.btnDecline.text = secondaryButtonText
    binding.btnDecline.setOnClickListener {
      analytics.confirmDestructiveActionPopup(analyticsScreenName, "click_secondary")
      dialog.dismiss()
      onSecondaryButtonClick()
    }


    lifecycle.addObserver(object : DefaultLifecycleObserver {
      override fun onPause(owner: LifecycleOwner) {
        if (cancelPolicy.cancelOnLifecyclePause && dialog.isShowing) {
          analytics.confirmDestructiveActionPopup(analyticsScreenName, "cancel_automatic")
          dialog.cancel()
        }
      }

      override fun onDestroy(owner: LifecycleOwner) {
        if (dialog.isShowing) {
          analytics.confirmDestructiveActionPopup(analyticsScreenName, "cancel_screen_killed")
          dialog.dismiss()
        }
      }
    })
  }
}


private val PopupsCreator.CancellationPolicy.cancelOnTapOutside
  get() = when (this) {
    CANT_CANCEL -> false
    CANCEL_ON_TAP_OUTSIDE,
    CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE -> true
  }

private val PopupsCreator.CancellationPolicy.cancelOnLifecyclePause
  get() = when (this) {
    CANT_CANCEL,
    CANCEL_ON_TAP_OUTSIDE -> false

    CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE -> true
  }
