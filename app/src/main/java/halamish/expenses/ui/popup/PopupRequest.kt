package halamish.expenses.ui.popup

import android.content.Context
import android.content.DialogInterface
import androidx.lifecycle.LifecycleOwner
import halamish.expenses.infra.ShortId
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope

sealed class PopupRequest {
    abstract val description: String
    override fun toString() = "PopupRequest($description)"

    object ReviewInGooglePlay: PopupRequest() {
        override val description = "ReviewInGooglePlay"
    }

    data class Descriptor(
        val popupId: String = ShortId.create(),
        override val description: String,
        val creator: PopupRequestCreator,
        val data: Any? = null // must implement proper .equals()
    ): PopupRequest()
}

// also must support empty-args constrcutor
interface PopupRequestCreator {
    // data - the data from the request descriptor
    fun showPopup(context: Context, lifecycleOwner: LifecycleOwner, data: Any?, onDone: () -> Unit): DialogInterface
}

class PopupRequestsManager(
    scope: CoroutineScope = appRedux.scope,
    eventBus: EventBus = appRedux.eventBus,
    val onCreatedNewPopupRequest: (PopupRequest) -> Unit,
    val onHandledPopupRequest: (PopupRequest) -> Unit
) {

    init {
        eventBus.onEveryEvent<NewPopupRequestCreated>(scope) {
            onCreatedNewPopupRequest(request)
        }
        eventBus.onEveryEvent<PopupRequestWasHandled>(scope) {
            onHandledPopupRequest(request)
        }
    }
}