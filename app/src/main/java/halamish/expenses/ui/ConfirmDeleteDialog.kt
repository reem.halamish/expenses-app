package halamish.expenses.ui

import android.content.Context
import androidx.appcompat.app.AlertDialog
import halamish.expenses.R

fun ConfirmDeleteDialog(context: Context, onConfirmed: () -> Unit) {
  AlertDialog.Builder(context)
    .setMessage(R.string.title_confirm_delete)
    .setPositiveButton(R.string.cta_confirm_delete_do) { d, _ ->
      d.dismiss()
      onConfirmed()
    }
    .setNegativeButton(R.string.cta_confirm_delete_cancel) { d, _ ->
      d.cancel()
    }
    .setCancelable(true)
    .show()
}