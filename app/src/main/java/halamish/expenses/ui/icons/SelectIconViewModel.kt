package halamish.expenses.ui.icons

import androidx.lifecycle.LiveData
import halamish.expenses.repo.icons.IconData
import halamish.expenses.repo.icons.IconsRepo
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.ui.toLiveData
import halamish.expenses.v2.appRedux
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class SelectIconViewModel(val repo: IconsRepo = appRedux.services.icons) : ProtoViewModel() {
    sealed class ViewState {
        object Empty : ViewState()
        data class Typing(val lastTerm: String) : ViewState()
        data class ApiCallRequested(val lastTerm: String) : ViewState()
        data class ListReady(val term: String, val icons: List<IconData>) : ViewState()
    }

    private companion object {
        const val DELAY_UNTIL_API_CALL_MS = 400L
    }


    private fun mapTextChanges(): ObservableTransformer<String, ViewState> = ObservableTransformer {strings ->
        Observable.merge(Observable.just(ViewState.Empty),
            strings.switchMap<ViewState> { term ->
                if (term.isEmpty())
                    return@switchMap Observable.just(ViewState.Empty)

                Observable.merge<ViewState>(Observable.just(ViewState.Typing(term)),
                    Observable.timer(DELAY_UNTIL_API_CALL_MS, TimeUnit.MILLISECONDS).switchMap<ViewState> {
                        Observable.merge<ViewState>(Observable.just(ViewState.ApiCallRequested(term)),
                            repo.icons(term)
                                .onErrorReturnItem(emptyList())
                                .map { ViewState.ListReady(term, it) }.toObservable()) })
            })
    }

    private val text = PublishSubject.create<String>()
    private val hotIconsDownloadState = text.compose(mapTextChanges())
        .map {
            println("SelectIconVM: state = $it")
            it
        }
        .replay(1)

    private val downloadDisposable = hotIconsDownloadState.connect()

    fun getState(): LiveData<ViewState> {
        return hotIconsDownloadState.toLiveData()
    }

    fun searchTermChanged(newTerm: String) = text.onNext(newTerm)

    override fun onCleared() {
        downloadDisposable.dispose()
    }
}