package halamish.expenses.ui.icons

import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.widget.textChanges
import halamish.expenses.R
import halamish.expenses.infra.EventsBus
import halamish.expenses.repo.icons.IconData
import halamish.expenses.ui.dpToPx
import halamish.expenses.ui.f
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_select_icon.*
import kotlinx.android.synthetic.main.fragment_select_icon.view.*
import java.io.Serializable

class SelectIconFragment: Fragment() {
    data class SelectIconEvent(val data: IconData): Serializable

    private val viewModel by viewModels<SelectIconViewModel>()

    private val adapter = SelectIconAdapter()

    private val bag = CompositeDisposable()

    private val initialSearchTerm: String?
        get() = arguments?.getString("search_term", null)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.getState().observe(this) { state ->
            println("SelectIconFragment: state = $state")

            when (state) {
                is SelectIconViewModel.ViewState.Empty -> {
                    btn_clear_edt.isVisible = false
                    adapter.submitList(emptyList())
                    api_progress.isVisible = false
                    tv_info.isVisible = false
                }

                is SelectIconViewModel.ViewState.Typing -> {
                    btn_clear_edt.isVisible = true
                    adapter.submitList(emptyList())
                    api_progress.isVisible = true
                    tv_info.isVisible = true
                    tv_info.text = getString(R.string.looking_for_icons_proto).replace("[[TERM]]", state.lastTerm)
                }

                is SelectIconViewModel.ViewState.ApiCallRequested -> {
                    btn_clear_edt.isVisible = true
                    adapter.submitList(emptyList())
                    api_progress.isVisible = true
                    tv_info.isVisible = true
                    tv_info.text = getString(R.string.looking_for_icons_proto).replace("[[TERM]]", state.lastTerm)
                }

                is SelectIconViewModel.ViewState.ListReady -> {
                    btn_clear_edt.isVisible = true
                    adapter.submitList(state.icons)
                    api_progress.isVisible = false
                    tv_info.isVisible = state.icons.isEmpty()
                    tv_info.text = getString(R.string.no_icons_for_search_proto).replace("[[TERM]]", state.term)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_icon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bag += view.edt_term.textChanges()
            .subscribe { viewModel.searchTermChanged(it.toString()) }

        bag += view.btn_clear_edt.clicks()
            .subscribe { edt_term.setText("") }

        eventsBus().observeUntilDestroyed(lifecycle) {
            findNavController().popBackStack() // exit after user has selected an iconName
        }

        view.post {
            view.recycler.adapter = adapter
            view.recycler.layoutManager = FlexboxLayoutManager(view.context).apply {
                justifyContent = JustifyContent.SPACE_BETWEEN
            }
        }

        view.setOnClickListener {
            view.requestFocus()
        }

        view.edt_term.setOnFocusChangeListener { edt, hasFocus ->
            TransitionManager.beginDelayedTransition(view as ViewGroup)
            edt.translationZ = if (hasFocus) 2.dpToPx().f else 0f
        }

        initialSearchTerm?.let(view.edt_term::setText)
    }

    override fun onDestroyView() {
        bag.clear()
        super.onDestroyView()
    }

    companion object {
        @JvmStatic fun eventsBus(): EventsBus<SelectIconEvent> {
            return EventsBus.from(SelectIconFragment::class.java)
        }
    }
}