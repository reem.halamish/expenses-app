package halamish.expenses.ui.icons

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import halamish.expenses.R
import halamish.expenses.repo.icons.IconData
import halamish.expenses.repo.icons.pngUrl
import halamish.expenses.ui.prototypes.ProtoViewHolder
import kotlinx.android.synthetic.main.item_select_icon.view.*

class SelectIconAdapter :
  ListAdapter<IconData, ProtoViewHolder>(Callback) {
  private companion object {
    val Callback = object : DiffUtil.ItemCallback<IconData>() {
      override fun areItemsTheSame(oldItem: IconData, newItem: IconData) = oldItem == newItem
      override fun areContentsTheSame(oldItem: IconData, newItem: IconData) =
        oldItem == newItem
    }
  }

  private val eventsBus by lazy { SelectIconFragment.eventsBus() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ProtoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_select_icon, parent, false))
            .apply {
                itemView.btn_icon.setOnClickListener {
                    eventsBus.send(SelectIconFragment.SelectIconEvent(getItem(adapterPosition)))
                }
            }

    override fun onBindViewHolder(holder: ProtoViewHolder, position: Int) {
        Glide.with(holder.context)
            .load(getItem(position).pngUrl)
            .placeholder(R.drawable.ic_cloud_download_grey300_24dp)
            .error(R.drawable.ic_warning_red400_24dp)
            .into(holder.itemView.btn_icon) // {
//                holder.itemView.btn_icon.setColorFilter(colors[position % colors.size])
//            }
    }
}