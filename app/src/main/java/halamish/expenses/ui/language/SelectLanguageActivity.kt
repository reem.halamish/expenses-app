package halamish.expenses.ui.language

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import halamish.expenses.R
import halamish.expenses.ui.prototypes.ProtoActivity
import halamish.expenses.ui.showWithCancelOnPause
import halamish.expenses.ui.theme.AppTheme
import halamish.expenses.v2.PhoneLanguage
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import timber.log.Timber

class SelectLanguageActivity : ProtoActivity() {

  override val screenName = "SelectLanguageActivity"

  val viewModel by viewModels<SelectLanguageViewModelImpl>()

  override val languageConfig = LanguageConfig(
    recreateOnLanguageChange = false,
    startLanguageSelectionIfMissing = false
  )

  val fragment = SelectLanguageBottomSheet()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_select_language)

    supportFragmentManager.beginTransaction().apply {
      supportFragmentManager.fragments.filter { it.isAdded }.forEach { fragment ->
        remove(fragment)
      }
      fragment.show(this, null)
      fragment.dialog?.setOnDismissListener {
        Timber.d("will finish")
        finish()
      }
    }

    lifecycleScope.launch {
      viewModel.closeSignal.take(1).collect { finish() }
    }
  }

  class SelectLanguageBottomSheet : BottomSheetDialogFragment() {
    val viewModel by activityViewModels<SelectLanguageViewModelImpl>()

    override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
    ): View = ComposeView(inflater.context)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      view as ComposeView
//            val fragment = this
      if (requireActivity().shouldBlock()) {
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnKeyListener { _, keyCode, event: KeyEvent ->
          val isBackClick = keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP
          if (!isBackClick) return@setOnKeyListener false
          Timber.d("detected BACK. will show toast")
          val msg = getString(R.string.flow_selectLanguage_toast_backDisabled)
          // SnackBar is rendered on top of the BACK|MENU keys. so using Toast instead
          Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
          return@setOnKeyListener true
        }
      }
      dialog?.setOnDismissListener { viewModel.onFragmentDialogDismissed() }

      view.setContent {
        AppTheme {
          BottomSheetContent(
            onEngligh = {
              viewModel.onSelected(PhoneLanguage.ENGLISH)
              dialog?.dismiss()
            },
            onHebrew = {
              viewModel.onSelected(PhoneLanguage.HEBREW)
              dialog?.dismiss()
            },
            onClear = if (viewModel.canClear) {
            {
              viewModel.clearLanguage()
              dialog?.dismiss()
            }
          } else null,
            onMore = {
              showVolunteerDialog()
            },
          )
        }
      }
    }

    private fun showVolunteerDialog() {
      MaterialAlertDialogBuilder(requireContext())
        .setTitle(R.string.flow_selectLanguage_popupDifferentLanguage_title)
        .setMessage(R.string.flow_selectLanguage_popupDifferentLanguage_msg)
        .setPositiveButton(R.string.flow_selectLanguage_popupDifferentLanguage_buttonPrimary) { dialog, what ->
          dialog.dismiss()
          context?.sendTranslationVolunteerEmail()
        }
        .setNegativeButton(R.string.flow_selectLanguage_popupDifferentLanguage_buttonSecondary) { dialog, what ->
          dialog.cancel()
        }
        .showWithCancelOnPause(viewLifecycleOwner.lifecycle)
    }
  }

  companion object {
    const val KEY_BLOCK = "BLOCK"
    private fun Activity.shouldBlock() = intent.getBooleanExtra(KEY_BLOCK, true)

    private fun Context.sendTranslationVolunteerEmail() {
      val devEmail = getString(R.string.settings_developer_email)
      startActivity(
        Intent.createChooser(
          Intent(
            Intent.ACTION_SENDTO,
            Uri.fromParts("mailto", devEmail, null)
          ).apply {
            putExtra(
              Intent.EXTRA_SUBJECT,
              "Trnaslation volunteer to ${getString(R.string.app_name)} android app"
            )
            putExtra(Intent.EXTRA_EMAIL, arrayOf(devEmail))
            putExtra(
              Intent.EXTRA_TEXT,
              "Hi! I heard you want help translating your app, I can help translating to ______. Let's talk :)"
            )

          }, getString(R.string.flow_selectLanguage_popupDifferentLanguage_title)
        )
      )
    }
  }
}

@Composable
private fun BottomSheetContent(
  onEngligh: () -> Unit,
  onHebrew: () -> Unit,
  onClear: (() -> Unit)?,
  onMore: () -> Unit,
) {
  Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = CenterHorizontally) {
    Spacer(modifier = Modifier.height(32.dp))
    Text(
      text = stringResource(id = R.string.flow_selectLanguage_title),
      style = MaterialTheme.typography.h5,
    )
    Spacer(modifier = Modifier.height(32.dp))
    Button(onEngligh) {
      Text(stringResource(id = R.string.language_consts_langName_english))
    }
    OutlinedButton(onHebrew, colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color.Transparent)) {
      Text(stringResource(id = R.string.language_consts_langName_hebrew))
    }

    if (onClear != null) {
      Spacer(modifier = Modifier.height(16.dp))
      TextButton(onClear) {
        Text(stringResource(id = R.string.flow_selectLanguage_option_clear))
      }
    }

    Spacer(modifier = Modifier.height(24.dp))
    Text(text = stringResource(id = R.string.flow_selectLanguage_helpTranslating),
      style = MaterialTheme.typography.caption,
      textDecoration = TextDecoration.Underline,
      modifier = Modifier.clickable { onMore() })

    Spacer(modifier = Modifier.height(24.dp))

  }
}

@Composable
@Preview(uiMode = UI_MODE_NIGHT_YES)
private fun BottomSheetContent_Preview() {
  AppTheme {
    BottomSheetContent({}, {}, {}, {})
  }
}