package halamish.expenses.ui.language

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import halamish.expenses.v2.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

interface SelectLanguageViewModel {
    val canClear: Boolean
    val languageFlow: Flow<PhoneLanguage?>
    fun onSelected(language: PhoneLanguage)
    fun clearLanguage()
    fun onFragmentDialogDismissed()
    val closeSignal: Flow<Unit>
}

class SelectLanguageViewModelImpl(
    stateFlow: StateFlow<V2State> = appRedux.state,
    val eventBus: EventBus = appRedux.eventBus
): ViewModel(), SelectLanguageViewModel {

    override val languageFlow = stateFlow.map { it.phoneData.language }
    override fun onSelected(language: PhoneLanguage) {
        viewModelScope.launch {
            eventBus.sendAwaitingResponse(PhoneDataHolder.LanguageChanged(language))
        }
    }
    override fun clearLanguage(){
        viewModelScope.launch {
            eventBus.sendAwaitingResponse(PhoneDataHolder.LanguageCleared)
        }
    }

    override fun onFragmentDialogDismissed() {
        viewModelScope.launch {
            // await animation (one day: can we get this value from somewhere?)
            delay(200)
            closeSignal.emit(Unit)
        }
    }

    override val canClear: Boolean = PhoneLanguage.canClearLanguage()

    override val closeSignal = MutableSharedFlow<Unit>()
}