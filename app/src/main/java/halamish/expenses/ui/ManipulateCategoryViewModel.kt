@file:Suppress("EXPERIMENTAL_API_USAGE")

package halamish.expenses.ui

import android.content.Context
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.ExpensesDbItem
import halamish.expenses.data.cleanedId
import halamish.expenses.infra.ShortId
import halamish.expenses.repo.CategoriesColorsProvider
import halamish.expenses.ui.icons.SelectIconFragment
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class ManipulateCategoryViewModel(
    private val categoryId: String?,
    private val colorsProvider: CategoriesColorsProvider = appRedux.services.categoriesColorsProvider,
    private val categories: Map<String, ExpenseCategory> = appRedux.state.value.data.categories,
    private val allExpenses: Map<String, ExpensesDbItem> = appRedux.state.value.data.allExpensesDbItems,
    private val eventBus: EventBus = appRedux.eventBus,
    private val scope: CoroutineScope? = appRedux.scope,
)
    : ProtoViewModel() {

    val existingCategory = categoryId?.let { categories[categoryId] }
    val canDeleteCategory = existingCategory != null

    fun availableColorValues(context: Context): IntArray
         = colorsProvider.colorValues(context).toIntArray()

    private val chosenColorResource: MutableLiveData<Int> = MutableLiveData(existingCategory?.resColor() ?: colorsProvider.colorsRes.random())
    fun chosenColorValue(context: Context) = chosenColorResource.value!!.let { ContextCompat.getColor(context, it) }
    fun setChosenColor(@ColorInt color: Int) {
        chosenColorResource.value = colorsProvider.resolveStoredColor(categoryHardcodedColor = color)
    }
    fun chosenColorLiveData(contextProvider: () -> Context?) = chosenColorResource.map {
        val context = contextProvider() ?: return@map null
        ContextCompat.getColor(context, it!!)
    }

    val iconName = mutableLiveDataOf(existingCategory?.icon ?: ExpenseCategory().icon)
    private val iconChangeObserver = SelectIconFragment.eventsBus().observe { selctIconEvent ->
        iconName.value = selctIconEvent.data.commonName
    }

    override fun onCleared() {
        super.onCleared()
        iconChangeObserver.cancelObservation()
    }

    private fun newCategoryId(displayName: String): String {
        val cleanedName = displayName.cleanedId()
            .lowercase(Locale.getDefault()).take(10)
        val baseId = "c_${ShortId.create()}"
        return if (cleanedName.isEmpty()) baseId
        else "${cleanedName}_${baseId}"
    }

    fun saveCategory(displayName: String): ExpenseCategory {
        val category = ExpenseCategory(
            id = categoryId ?: newCategoryId(displayName),
            displayName = displayName,
            color = colorsProvider.calculateHardcodedColor(resColor = chosenColorResource.value!!),
            icon = iconName.value ?: ExpenseCategory().icon
        )

        (scope ?: viewModelScope).launch {
            eventBus.sendAwaitingResponse(UpdateExpenseCategory(category))
        }
        return category
    }

    fun needsVerificationBeforeDeleteCategory(): Boolean {
        return calculateAttachedExpensesAmount() > 0
    }

    fun calculateAttachedExpensesAmount(): Int {
        val categoryId = existingCategory?.id ?: return 0
        return allExpenses.filter { it.value.categoryId == categoryId }.size
    }

    fun deleteCategory() {
        val categoryId = existingCategory?.id ?: return run {
            Timber.e("can't delete - category doesn't exist yet")
        }
        (scope ?: viewModelScope).launch {
            eventBus.sendAwaitingResponse(RequestDeleteSingleCategory(categoryId))
        }
    }

    class Factory(
        val context: Context,
        val categoryId: String?
    ): ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return ManipulateCategoryViewModel(categoryId) as T
        }
    }

    companion object {
        @JvmStatic fun get(fragment: Fragment): ManipulateCategoryViewModel {
            val categoryId = ManipulateCategoryFragment.categoryIdToEdit(fragment.arguments)
            return ViewModelProvider(fragment, Factory(fragment.requireContext(), categoryId)).get(ManipulateCategoryViewModel::class.java)
        }
    }
}