package halamish.expenses.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import halamish.expenses.R

class ArrowPointingDownView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

  companion object {
    val ARROW_START_HORIZONTAL_FRACTION = 0.55f // 0.5f places the arrow start in the middle
    val TIP_OFFSET_HORIZONTAL_FRACTION = 0.5f // 0.5f places the tip exactly in the middle
    val ARROW_TIP_DP_VERTICAL_LEFT by lazy { 24.dpToPx().toFloat() }
    val ARROW_TIP_DP_HORIZONTAL_LEFT by lazy { 16.dpToPx().toFloat() }
    val ARROW_TIP_DP_VERTICAL_RIGHT by lazy { 28.dpToPx().toFloat() }
    val ARROW_TIP_DP_HORIZONTAL_RIGHT by lazy { 8.dpToPx().toFloat() }
  }

  private val paint = Paint().apply {
    color = ContextCompat.getColor(context, R.color.boldOnSurface)
    strokeWidth = 2.dpToPx().toFloat()
    style = Paint.Style.STROKE
  }

  val paths = mutableListOf<Path>()

  override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
    paths.clear()
    if (w == 0 || h == 0) {
      return
    }

    val wf = w.toFloat()
    val hf = h.toFloat()
    val arrowStartX = wf * ARROW_START_HORIZONTAL_FRACTION
    val tipX = wf * TIP_OFFSET_HORIZONTAL_FRACTION

    val mainPath = Path().apply {
      moveTo(arrowStartX, 0f)
      lineTo(tipX, hf)
    }

    paths += mainPath

    // left tip
    paths += Path().apply {
      moveTo(tipX, hf)
      lineTo(tipX - ARROW_TIP_DP_HORIZONTAL_LEFT, hf - ARROW_TIP_DP_VERTICAL_LEFT)
    }
    // right tip
    paths += Path().apply {
      moveTo(tipX, hf)
      lineTo(tipX + ARROW_TIP_DP_HORIZONTAL_RIGHT, hf - ARROW_TIP_DP_VERTICAL_RIGHT)
    }
  }

  override fun onDraw(canvas: Canvas?) {
    canvas ?: return

    val isRtl = layoutDirection == LAYOUT_DIRECTION_RTL
    if (isRtl) canvas.scale(-1f, 1f, width / 2f, height / 2f)
    for (path in paths) canvas.drawPath(path, paint)
  }
}