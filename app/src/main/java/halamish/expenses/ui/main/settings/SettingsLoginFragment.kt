package halamish.expenses.ui.main.settings

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.ViewCompat.LAYOUT_DIRECTION_LTR
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import extensions.android.awaitLayout
import halamish.expenses.ui.login.LoginFragment
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class SettingsLoginFragment : LoginFragment() {

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    viewLifecycleOwner.lifecycleScope.launch {
      animate(view, entrance = true)
    }
  }

  override fun onLoggedIn() {
    viewLifecycleOwner.lifecycleScope.launch {
      animate(requireView(), entrance = false)
      findNavController().popBackStack()
    }
  }

  private suspend fun animate(view: View, entrance: Boolean) {

    view.awaitLayout()
    val width = view.width.toFloat()
    val direction = ViewCompat.getLayoutDirection(view)
    val isLtr = direction == LAYOUT_DIRECTION_LTR

    val translated = if (isLtr) width else -width

    view.translationX = if (entrance) translated else 0f

    return suspendCancellableCoroutine { continuation ->
      view.animate()
        .translationX(if (entrance) 0f else translated)
        .setDuration(200L)
        .withEndAction { continuation.resume(Unit) }
        .start()

      continuation.invokeOnCancellation { view.animate().cancel() }
    }
  }
}
