package halamish.expenses.ui.main.backup

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import halamish.expenses.R
import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class BackupFragment : Fragment(R.layout.fragment_backup) {
  private val viewModel by viewModels<BackupFragmentViewModel>()

  private val adapter by lazy { Adapter(onSnapshotClick = {
    val job = viewModel.onRestoreToSnapshot(it)
    job.invokeOnCompletion {
      appRedux.handlers.popups.showInformativePopup(
        analyticsScreenName = "backups_fragment",
        context = requireContext(),
        lifecycle = viewLifecycleOwner.lifecycle,
        messageText = getString(R.string.feature_backup_dialogSuccess_title),
        buttonText = getString(R.string.feature_backup_dialogSuccess_buttonAck),
      )
    }
  }) }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    view as ViewGroup
    val scope = viewLifecycleOwner.lifecycleScope

    val recycler = view.findViewById<RecyclerView>(R.id.backup_recycler)
    val noData = view.findViewById<View>(R.id.backup_noData)
    val subtitleView = view.findViewById<TextView>(R.id.backup_subtitle)
    val buttonCreateSnapshot = view.findViewById<View>(R.id.backup_createBackup)
    val buttonDeleteAll = view.findViewById<View>(R.id.backup_deleteAll)

    recycler.layoutManager =
      LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    recycler.adapter = adapter

    scope.launch {
      viewModel.subtitle.collect { title ->
        subtitleView.text = title
      }
    }
    scope.launch {
      viewModel.snapshots.collect { snapshots ->
        adapter.submitList(snapshots)
      }
    }

    scope.launch {
      var first = true
      viewModel.hasData.collect { hasData ->
        if (!first) {
          TransitionManager.beginDelayedTransition(view)
        }
        noData.isVisible = !hasData
        recycler.isVisible = hasData
        buttonDeleteAll.isVisible = hasData
        first = false
      }
    }

    buttonCreateSnapshot.setOnClickListener {
      scope.launch {
        viewModel.onCreateBackup().join()
        delay(200)
        recycler.layoutManager?.startSmoothScroll(recyclerScroller())
      }
    }

    buttonDeleteAll.setOnClickListener {
      appRedux.handlers.popups.showDestructivePopup(
        analyticsScreenName = "backups_fragment",
        context = requireContext(),
        lifecycle = viewLifecycleOwner.lifecycle,
        messageText = getString(R.string.feature_backup_dialogDeleteAll_title),
        destructiveButtonText = getString(R.string.feature_backup_dialogDeleteAll_confirm),
        secondaryButtonText = getString(R.string.feature_backup_dialogDeleteAll_cancel),
        onDestructiveButtonClick = { viewModel.onDeleteAll() },
        onSecondaryButtonClick = {},
      )
    }
  }

  private fun recyclerScroller(context: Context = requireContext(), position: Int = 0) = object : LinearSmoothScroller(context) {
    init { targetPosition = position }
    override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
      // increase duration by a factor (a.k.a decrease scroll speed by a factor)
      return super.calculateSpeedPerPixel(displayMetrics) * 3f
    }
  }

  private class BackupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val title: TextView = itemView.findViewById(R.id.backup_item_title)
    val description: TextView = itemView.findViewById(R.id.backup_item_description)
    val button: View = itemView.findViewById(R.id.backup_item_buttonRestore)
  }

  private class Adapter(private val onSnapshotClick: (BackupAndRestoreService.Snapshot) -> Unit) :
    ListAdapter<BackupFragmentViewModel.SnapshotUi, BackupViewHolder>(BackupDiffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BackupViewHolder(
      LayoutInflater.from(parent.context)
        .inflate(R.layout.fragment_backup_item_snapshot, parent, false)
    )

    override fun onBindViewHolder(holder: BackupViewHolder, position: Int) {
      val snapshotUi = getItem(position)
      holder.title.text = snapshotUi.title
      holder.description.text = snapshotUi.subtitle
      holder.button.setOnClickListener { onSnapshotClick(snapshotUi.snapshot) }
    }
  }
}

private object BackupDiffCallback : DiffUtil.ItemCallback<BackupFragmentViewModel.SnapshotUi>() {
  override fun areItemsTheSame(
    oldItem:  BackupFragmentViewModel.SnapshotUi,
    newItem:  BackupFragmentViewModel.SnapshotUi
  ) = oldItem.id == newItem.id

  override fun areContentsTheSame(
    oldItem:  BackupFragmentViewModel.SnapshotUi,
    newItem:  BackupFragmentViewModel.SnapshotUi
  ) = oldItem == newItem

}