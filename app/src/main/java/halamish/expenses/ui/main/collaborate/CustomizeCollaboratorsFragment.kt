package halamish.expenses.ui.main.collaborate

import android.app.Dialog
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import halamish.expenses.R
import halamish.expenses.ui.hideKeyboard
import halamish.expenses.ui.main.collaborate.CustomizeCollaboratorsViewModel.DeviceData
import halamish.expenses.ui.main.collaborate.CustomizeCollaboratorsViewModel.Event
import halamish.expenses.ui.main.collaborate.CustomizeCollaboratorsViewModel.UserData


class CustomizeCollaboratorsFragment: Fragment(R.layout.fragment_customize_collaborators) {
    private val viewModel by viewModels<CustomizeCollaboratorsViewModelImpl>()
    private val adapter = Adapter(
        onCustomizeDeviceNameButtonClicked = { deviceId, model, currentName: String? -> openCustomDeviceNameDialog(deviceId, model, currentName) },
        onRemoveUserButtonClicked = { userId -> viewModel.intercept(Event.DidRequestRemoveUser(userId)) },
        onInvitationButtonClicked = {
            findNavController().navigate(R.id.action_customizeCollaboratorsFragment_to_createInvitationLinkFragment)
        }
    )

    private fun openCustomDeviceNameDialog(deviceId: String, model: String?, currentName: String?) {
        val hadText = !currentName.isNullOrEmpty()

        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.fragment_customize_collaborators_popup_edit_device)
        dialog.setCancelable(true)
        val editText = dialog.findViewById<EditText>(R.id.dialogEditText)
        val buttonOk = dialog.findViewById<TextView>(R.id.dialogMainButton)
        val buttonCancel = dialog.findViewById<TextView>(R.id.dialogSecondaryButton)

        editText.setText(currentName ?: "")
        editText.hint = model ?: requireContext().getString(R.string.flow_customize_collaborators_popup_edit_device_name_hint)

        fun dismissDialog(){
            hideKeyboard()
            dialog.dismiss()
        }

        fun setMainButton(){
            val text = editText.text.toString()
            if (text.isEmpty()) {
                if (hadText) {
                    buttonOk.setText(R.string.flow_customize_collaborators_popup_edit_device_name_cta_clear)
                    buttonOk.isEnabled = true
                } else {
                    buttonOk.setText(R.string.flow_customize_collaborators_popup_edit_device_name_cta_confirm)
                    buttonOk.isEnabled = false
                }
                buttonOk.setOnClickListener {
                    dismissDialog()
                    viewModel.intercept(Event.DidClearCustomName(deviceId))
                }
            } else {
                buttonOk.setText(R.string.flow_customize_collaborators_popup_edit_device_name_cta_confirm)
                buttonOk.isEnabled = true
                buttonOk.setOnClickListener {
                    dismissDialog()
                    viewModel.intercept(Event.DidSetCustomName(deviceId, text))
                }
            }
        }

        setMainButton()
        editText.addTextChangedListener(afterTextChanged = {
            setMainButton()
        })

        buttonCancel.setOnClickListener {
            dismissDialog()
        }


        dialog.show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recycler = view.findViewById<RecyclerView>(R.id.recyclerView)
        recycler.adapter = adapter

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.dataFlow.collect { viewData ->
                if (viewData.shouldGoBack) {
                    findNavController().popBackStack()
                }

                val adapterItems = mutableListOf<AdapterItem>()
                // devices
                adapterItems += AdapterItem.Headline(headlineId = "Devices", text = requireContext().getString(R.string.flow_customize_collaborators_headline_devices))
                viewData.devices.forEach { deviceData ->
                    adapterItems += AdapterItem.Separator("sep_before_${deviceData.id}")
                adapterItems += AdapterItem.Device(device = deviceData)
                }
                // users
                adapterItems += AdapterItem.Headline(headlineId = "Users", text = requireContext().getString(R.string.flow_customize_collaborators_headline_users))
                viewData.users.forEach { userData ->
                    adapterItems += AdapterItem.Separator("sep_before_${userData.id}")
                    adapterItems += AdapterItem.User(user = userData)
                }
                if (viewData.canInvite) {
                    adapterItems += AdapterItem.InvitationButton(buttonId = "button_after_users_list")
                }

                adapter.submitList(adapterItems)
            }
        }
    }
}

private sealed class AdapterItem(val id: String, val typeId: Int) {
    data class User(val user: UserData): AdapterItem(user.id, 1)
    data class Device(val device: DeviceData): AdapterItem(device.id, 2)
    data class Headline(val headlineId: String, val text: String): AdapterItem(headlineId, 3)
    data class Separator(val sepId: String): AdapterItem(sepId, 4)
    data class InvitationButton(val buttonId: String): AdapterItem(buttonId, 5)
}

private val callback = object : DiffUtil.ItemCallback<AdapterItem>(){
    override fun areItemsTheSame(oldItem: AdapterItem, newItem: AdapterItem) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: AdapterItem, newItem: AdapterItem) = oldItem == newItem
}


private class Adapter(
    private val onCustomizeDeviceNameButtonClicked: (deviceId: String, model: String, currentName: String?) -> Unit,
    private val onRemoveUserButtonClicked: (userId: String) -> Unit,
    private val onInvitationButtonClicked: () -> Unit
): ListAdapter<AdapterItem, RecyclerView.ViewHolder>(callback) {

    override fun getItemViewType(position: Int) = getItem(position).typeId

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        fun inflate(layoutId: Int) = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return when (currentList.first { it.typeId == viewType }) {
            is AdapterItem.Device -> ViewHolders.Device(inflate(R.layout.fragment_customize_collaborators_view_device))
            is AdapterItem.Headline -> ViewHolders.Headline(inflate(R.layout.fragment_customize_collaborators_view_headline))
            is AdapterItem.InvitationButton -> ViewHolders.InvitationButton(inflate(R.layout.fragment_customize_collaborators_view_invite_button))
            is AdapterItem.Separator -> ViewHolders.Anonymous(inflate(R.layout.fragment_customize_collaborators_view_separator))
            is AdapterItem.User -> ViewHolders.User(inflate(R.layout.fragment_customize_collaborators_view_user))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = getItem(position)) {
            is AdapterItem.Separator -> Unit
            is AdapterItem.Headline -> {
                holder as ViewHolders.Headline
                holder.headline.text = item.text
            }
            is AdapterItem.Device -> {
                holder as ViewHolders.Device
                holder.lastLogin.text = item.device.lastLoginStr
                holder.deviceName.text = item.device.descriptionIncludingMe(holder.itemView.context)
                holder.editButton.isVisible = item.device.canEdit
                holder.editButton.setOnClickListener { onCustomizeDeviceNameButtonClicked(item.device.id, item.device.model, item.device.customizedName) }
            }
            is AdapterItem.InvitationButton -> {
                holder as ViewHolders.InvitationButton
                holder.button.setOnClickListener { onInvitationButtonClicked() }
                holder.button.paintFlags = holder.button.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                holder.button.isAllCaps = false
            }
            is AdapterItem.User -> {
                holder as ViewHolders.User
                holder.userDisplay.text = item.user.description(holder.itemView.context)
                holder.removeButton.isVisible = item.user.canDelete
                holder.removeButton.setOnClickListener { onRemoveUserButtonClicked(item.user.id) }
            }
        }
    }

    private object ViewHolders {
        class Anonymous(itemView: View) : RecyclerView.ViewHolder(itemView)
        class Headline(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val headline = itemView.findViewById<TextView>(R.id.tvHeadline)
        }
        class User(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val userDisplay = itemView.findViewById<TextView>(R.id.tvUserName)
            val removeButton = itemView.findViewById<View>(R.id.buttonDeleteUser)
        }
        class Device(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val deviceName = itemView.findViewById<TextView>(R.id.tvDeviceName)
            val lastLogin = itemView.findViewById<TextView>(R.id.tvDeviceLogin)
            val editButton = itemView.findViewById<View>(R.id.buttonEditDeviceName)
        }
        class InvitationButton(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val button = itemView.findViewById<MaterialButton>(R.id.btn_invite)
        }
    }
}