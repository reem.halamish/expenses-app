package halamish.expenses.ui.main.collaborate

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import extensions.android.await
import halamish.expenses.R
import halamish.expenses.data.link
import kotlinx.android.synthetic.main.fragment_create_invitation_link.*
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class CreateInvitationLinkFragment : Fragment(R.layout.fragment_create_invitation_link) {
    private val viewModel by viewModels<CreateInvitationLinkViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            var lastConsumer: UiConsumer? = null
            viewModel.stateFlow.collect { state ->
                val consumer = when (state) {
                    is CreateInvitationLinkViewModel.State.DataReady -> dataConsumer.also { it.consume(state) }
                    CreateInvitationLinkViewModel.State.Loading -> loadingConsumer
                    CreateInvitationLinkViewModel.State.NotSupported -> notSupportedConsumer
                }
                if (lastConsumer == null) {
                    lastConsumer = consumer
                    consumer.immediateShow()
                    if (consumer == loadingConsumer) {
                        delay(2000)
                    }
                }
                if (lastConsumer != consumer) {
                    coroutineScope {
                        val dOut = async { lastConsumer?.animateOut() }
                        val dIn = async { consumer.animateIn() }
                        dOut.await()
                        dIn.await()
                    }
                }
                consumer.consume(state)
            }
        }
    }

    private interface UiConsumer {
        suspend fun animateIn()
        suspend fun animateOut()
        fun immediateShow()
        fun consume(state: CreateInvitationLinkViewModel.State)
    }

    private open class Consumer<S : CreateInvitationLinkViewModel.State>(
        val views: List<View>
    ) : UiConsumer {
        override suspend fun animateIn() {
            views.animateAllIn()
        }

        override suspend fun animateOut() {
            views.animateAllOut()
        }

        override fun immediateShow() {
            views.forEach { it.isVisible = true }
        }

        override fun consume(state: CreateInvitationLinkViewModel.State) {}
    }

    private val notSupportedConsumer by lazy {
        Consumer<CreateInvitationLinkViewModel.State.NotSupported>(listOf(tvDisabled))
    }

    private val loadingConsumer by lazy {
        Consumer<CreateInvitationLinkViewModel.State.Loading>(
            listOf(progressCreatingLink, tvWip)
        )
    }

    private val dataConsumer by lazy {
        object : Consumer<CreateInvitationLinkViewModel.State.DataReady>(
            listOf(tvLinkPrefix, tvLinkActual, warningContainer,
                btn_share, btn_copy)
        ) {
            override fun consume(state: CreateInvitationLinkViewModel.State) {
                if (state !is CreateInvitationLinkViewModel.State.DataReady) return
                tvLinkActual.text = state.data.link
                btn_share.setOnClickListener {
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, state.data.link)
                        type = "text/plain"
                    }

                    val shareIntent = Intent.createChooser(sendIntent, btn_share.text)
                    startActivity(shareIntent)
                }
                btn_copy.setOnClickListener {
                    val clipBoard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clipData = ClipData.newPlainText("expenses link",state.data.link)
                    clipBoard.setPrimaryClip(clipData)
                    viewLifecycleOwner.lifecycleScope.launch {
                        suspend fun View.quickAlphaAnim(alpha: Float) = animate()
                            .alpha(alpha)
                            .setDuration(100)
                            .await()

                        delay(100) // await ripple effect finish
                        btn_copy.quickAlphaAnim(0f)
                        btn_copy.setText(R.string.collaborate_toast_copied)
                        btn_copy.quickAlphaAnim(1f)
                        delay(5000)
                        btn_copy.quickAlphaAnim(0f)
                        btn_copy.setText(R.string.collaborate_cta_copy_project_invitation)
                        btn_copy.quickAlphaAnim(1f)
                    }
                }
            }
        }
    }


    companion object {
        private suspend fun View.animateOut() {
            if (!isVisible) return
            if (alpha == 0f) {
                isVisible = false
                alpha = 1f
                return
            }
            return suspendCoroutine { continuation ->
                animate()
                    .setDuration(200)
                    .alpha(0f)
                    .withEndAction {
                        isVisible = false
                        alpha = 1f
                        continuation.resume(Unit)
                    }
            }
        }

        private suspend fun View.animateIn() {
            if (!isVisible) {
                isVisible = true
                alpha = 0f
            }
            if (alpha == 1f) return
            return suspendCoroutine { continuation ->
                animate()
                    .setDuration(200)
                    .alpha(1f)
                    .withEndAction {
                        continuation.resume(Unit)
                    }
            }
        }

        private suspend fun Collection<View>.animateAllOut() {
            coroutineScope {
                val deferrable = map { async { it.animateOut() } }
                deferrable.forEach { it.await() }
            }
        }

        private suspend fun Collection<View>.animateAllIn() {
            coroutineScope {
                val deferrable = map { async { it.animateIn() } }
                deferrable.forEach { it.await() }
            }

        }
    }
}