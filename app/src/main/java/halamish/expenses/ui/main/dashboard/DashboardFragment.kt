package halamish.expenses.ui.main.dashboard


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import com.jakewharton.rxbinding3.widget.textChanges
import halamish.expenses.R
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.ui.hideKeyboard
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.dialog_create_budget.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class DashboardFragment : Fragment(R.layout.fragment_dashboard) {


    private val bag = CompositeDisposable()

    val viewModel by viewModels<DashboardViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.expensive.collect { data ->
                if (data.categories.isNotEmpty()) {
                    expensiveCard.setUi(data)
                } else {
                    expensiveCard.setNoData(data.period)
                }
            }
        }

        shareAppCard.onDismissTap = { viewModel.dismissShareClicked() }
        shareAppCard.onShareTap = { startActivity(viewModel.intentForButtonShareClick()) }
        viewModel.showShowShareCard.observe(viewLifecycleOwner) { shouldShow ->
            shareAppCard.isVisible = shouldShow
        }

        budgetCard.callbackOnDismiss = {
            viewModel.dismissBudgetClicked()
        }
        budgetCard.callbackOnDefineBudget = {
            viewModel.defineBudgetClicked()
            openSetupBudget()
        }
        budgetCard.callbackOnCustomizeBudget = {
            viewModel.customizeBudgetClicked()
            openSetupBudget()
        }


        viewModel.budget.observe(viewLifecycleOwner) { details ->
            if (details == null) return@observe

            if (!details.shouldShow) {
                budgetCard.visibility = View.GONE
                return@observe
            }

            budgetCard.visibility = View.VISIBLE
            if (details.budget == null) {
                budgetCard.setupNeedsDefinition()
            } else {
                budgetCard.setupDefined(
                    details.budget,
                    details.spent,
                    details.period
                )
            }
        }
    }

    private fun openSetupBudget() {
        val doneButtonTriggers = BehaviorSubject.createDefault(Unit)
        val context = context ?: return
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_create_budget, null)
        val dialog = AlertDialog.Builder(context).setView(view)
            .setCancelable(true)
            .create()

        val cancelDialogOnStopObserver = object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun cancelDialog() {
                if (!dialog.isShowing) {
                    return
                }
                hideKeyboard()
                doneButtonTriggers.onComplete()
                view.post { dialog.cancel() }
            }
        }
        dialog.setOnCancelListener { viewModel.onCancelDefineBudgetDialog() }

        lifecycle.addObserver(cancelDialogOnStopObserver)
        dialog.show()

        val edt = view.edt_budget.apply { setText("0") }
        val doneButton = view.btn_done

        view.text_budget_currency.text = viewModel.currencySymbol


        // re-check if "done" is enabled when input changes
        bag += edt.textChanges().subscribe { doneButtonTriggers.onNext(Unit) }

        // clear non-digits from the edit text
        bag += edt.textChanges()
            .filter { text -> text.any { !it.isDigit() } }
            .subscribe { text -> edt.setText(text.filter { it.isDigit() }) }

        // clamp too-big numbers from the edit text
        bag += edt.textChanges()
            .filter { text -> text.length > 6 }
            .subscribe { text ->
                edt.setText(text.drop(1))
                edt.setSelection(edt.text.length)
            }

        // enable the "done" button if has budget
        bag += doneButtonTriggers
            .map { (edt.text.toString().toIntOrNull() ?: 0) > 0 }
            .subscribe { canTapDone -> doneButton.isEnabled = canTapDone }

        doneButton.setOnClickListener {
            dialog.hideKeyboard()
            dialog.dismiss()
            viewModel.setBudget(edt.text?.toString()?.toIntOrNull())
        }

        viewModel.detailsForBudgetBuilding.value?.let { details ->
            if (details.expensesAverage != null && details.monthsAmount > 0) {
                view.text_average_description.isVisible = true
                val month = "${details.monthsAmount}"
                val average = CurrencyFormatter.get().format(details.expensesAverage)
                view.text_average_description.text =
                    getString(R.string.dialog_define_budget_text_average, month, average)
                val roundedAverage = details.expensesAverage.let { a -> a + (100 - (a % 100)) }
                edt.setText("$roundedAverage")
            } else {
                view.text_average_description.isVisible = false
            }
        }

        viewModel.budget.value?.budget?.let { budget ->
            edt.setText("$budget")
        }

        // underline until user starts adding text
        edt.paint?.isUnderlineText = true
        bag += edt.textChanges()
            .skipInitialValue()
            .take(1)
            .subscribe { edt.paint?.isUnderlineText = false }
    }

    override fun onDestroy() {
        super.onDestroy()
        bag.clear()
    }
}
