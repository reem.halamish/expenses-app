package halamish.expenses.ui.main.collaborate

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import extensions.android.fromHtml
import extensions.android.toLocalDate
import halamish.expenses.R
import halamish.expenses.infra.DateFormatter
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler.UiAction.DO_NOTHING
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler.UiAction.GO_BACK
import halamish.expenses.v2.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

interface CustomizeCollaboratorsViewModel {
    data class ViewData(
        val users: List<UserData> = emptyList(),
        val devices: List<DeviceData> = emptyList(),
        val canInvite: Boolean = false,
        val shouldGoBack: Boolean = false,
    )
    data class DeviceData(
        val id: String,
        val model: String, // "SAMSUNG Galaxy S20"
        val lastLoginStr: String, // yesterday at 17:43
        val lastLoginMs: Long, // 3123019823
        val customizedName: String?, // Adi's phone
        val isThisDevice: Boolean,
        val canEdit: Boolean,
    ){
    val description: String get() = if (customizedName != null) "$customizedName ($model)" else model
        fun descriptionIncludingMe(context: Context): CharSequence =
            if (isThisDevice) context.getString(R.string.flow_customize_collaborators_this_device_p_description_html).replace("[DESCRIPTION]", description).fromHtml()
            else description
    }

    data class UserData(
        val id: String,
        val email: String,
        val isMe: Boolean,
        val canDelete: Boolean
    ) {
        fun description(context: Context): CharSequence =
            if (isMe) context.getString(R.string.flow_customize_collaborators_your_user_p_email_html).replace("[EMAIL]", email).fromHtml()
            else email
    }

    sealed class Event {
        data class DidSetCustomName(val deviceId: String, val newName: String): Event()
        data class DidClearCustomName(val deviceId: String): Event()
        data class DidRequestRemoveUser(val userId: String): Event()
    }

    val dataFlow: StateFlow<ViewData>
    fun intercept(event: Event)

}

class CustomizeCollaboratorsViewModelImpl(
    private val appDataFlow: StateFlow<V2State> = appRedux.state,
    private val app: Application = appRedux.app,
    private val eventBus: EventBus = appRedux.eventBus,
    private val invitationsHandler: ProjectUsersInvitationsHandler = appRedux.services.projectUsersInvitationsHandler,
    private val dateFormatter: DateFormatter = appRedux.services.dateFormatterForDays,
): CustomizeCollaboratorsViewModel, ViewModel() {
    override val dataFlow = MutableStateFlow(CustomizeCollaboratorsViewModel.ViewData())

    init {
        viewModelScope.launch {
            appDataFlow.map { it.toViewData() }
                .collect { dataFlow.value = it }
        }
    }

    override fun intercept(event: CustomizeCollaboratorsViewModel.Event) {
        when (event) {
            is CustomizeCollaboratorsViewModel.Event.DidSetCustomName -> {
                viewModelScope.launch {
                    eventBus.sendAwaitingNext(UpdateProjectDetailsRunnable("custom name(${event.newName}) for device ${event.deviceId}") {
                        copy(deviceNameByDeviceId = (deviceNameByDeviceId + (event.deviceId to event.newName)))
                    })
                }
            }
            is CustomizeCollaboratorsViewModel.Event.DidClearCustomName -> viewModelScope.launch {
                eventBus.sendAwaitingNext(UpdateProjectDetailsRunnable("clear custom name for device ${event.deviceId}") {
                    copy(deviceNameByDeviceId = (deviceNameByDeviceId - event.deviceId))
                })
            }
            is CustomizeCollaboratorsViewModel.Event.DidRequestRemoveUser -> {
                viewModelScope.launch {
                    val actionToDo = invitationsHandler.removeUser(event.userId)
                    when (actionToDo) {
                        DO_NOTHING -> Unit
                        GO_BACK -> {
                            dataFlow.value = dataFlow.value.copy(shouldGoBack = true)
                        }
                    }
                }
            }
        }
    }

    private fun V2State.toViewData(): CustomizeCollaboratorsViewModel.ViewData {
        val state = this
        val isCreator = state.project.creator == state.userInfo?.uid

        val devices = state.project.devices.keys.map { deviceId ->
            val isThisDevice = state.deviceId == deviceId
            val lastLoginLocalDate = project.devices[deviceId]?.toLocalDate()
            val lastLoginString = lastLoginLocalDate?.let(dateFormatter::format) ?: app.getString(R.string.flow_customize_collaborators_item_device_no_login)
            CustomizeCollaboratorsViewModel.DeviceData(
                id = deviceId,
                model = state.project.deviceModelByDeviceId[deviceId]
                    ?: app.getString(R.string.flow_customize_collaborators_item_device_unknown_model),
                lastLoginStr = app.getString(R.string.flow_customize_collaborators_item_device_last_login_ps, lastLoginString),
                lastLoginMs = project.devices[deviceId]?.time ?: 0L,
                customizedName = state.project.deviceNameByDeviceId[deviceId],
                isThisDevice = isThisDevice,
                canEdit = isCreator || isThisDevice
            )
        }
            .sortedByDescending { it.lastLoginMs }

        val users = state.project.users.keys.map { userId ->
            val isMe = state.userInfo?.uid == userId
            CustomizeCollaboratorsViewModel.UserData(
                id = userId,
                email = project.users[userId] ?: app.getString(R.string.flow_customize_collaborators_item_user_unknown_email),
                isMe = isMe,
                canDelete = (isCreator xor isMe) // creator can remove all others, non-creators can remove themselves
            )
        }
            .sortedBy { it.email }

        return CustomizeCollaboratorsViewModel.ViewData(
            users=users,devices=devices,canInvite=isCreator
        )
    }
}