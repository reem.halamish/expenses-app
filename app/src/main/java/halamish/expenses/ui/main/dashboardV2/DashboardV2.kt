package halamish.expenses.ui.main.dashboardV2

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import halamish.expenses.ui.main.dashboardV2.DashboardV2Screen.DateControlInput
import halamish.expenses.ui.theme.AppTheme

@Preview
@Composable
private fun Preview_DashboardV2Screen() {
    DashboardV2Screen()
}

@Suppress("unused")
@Composable
private fun ExpensesDashboardV2Screen() {
    AppTheme {
        DashboardV2Screen()
    }
}

@Composable
private fun DashboardV2Screen() {
    Column {
        Column(
            modifier =
            Modifier
                .verticalScroll(rememberScrollState())
                .weight(1f, fill = false)
        ) {
            Text("TODO: here put date control (now, +1 -1 month, custom month)")
            Text("TODO: here put main budget")
            Text("TODO: here put last expense")
            Text("TODO: here put list of all categories, each clickable to expand")
        }
    }
}

// todo: colors, textColors
@Composable
private fun DateControl(input: DateControlInput) {
    AppTheme {
        var opened: Boolean = remember { false }
        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .background(MaterialTheme.colors.background)
            .padding(16.dp)) {
            Icon(
                Icons.Rounded.CalendarMonth, "define time range",
                modifier = Modifier
                    .size(36.dp)
                    .clickable(role = Role.Button) { opened = true }
                    .padding(8.dp),
                tint = MaterialTheme.colors.onBackground
            )
            Spacer(modifier = Modifier.width(16.dp))
            Icon(
                Icons.Rounded.ArrowLeft,
                contentDescription = input.leftArrowText,
                modifier = Modifier.size(16.dp),
                tint = MaterialTheme.colors.onBackground
            )
            // todo: how do I set text color from theme?
            Text(text = input.leftArrowText)
            Spacer(modifier = Modifier.width(16.dp))
            Text(text = input.current)
            Spacer(modifier = Modifier.width(16.dp))
            Text(text = input.rightArrowText)
            Icon(
                Icons.Rounded.ArrowRight,
                contentDescription = input.rightArrowText,
                modifier = Modifier.size(16.dp),
                tint = MaterialTheme.colors.onBackground
            )
            Spacer(modifier = Modifier.width((16+24).dp))
        }
    }
}

@Preview
@Composable
private fun Preview_DateControl() {
    DateControl(
        input = DateControlInput(
            "id", "October 2022",
            "Prev",
            "Next",
        )
    )
}


object DashboardV2Screen {
    data class DateControlInput(
        val id: String,
        val current: String,
        val leftArrowText: String,
        val rightArrowText: String,
    )
}
