package halamish.expenses.ui.main.dashboard

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import halamish.expenses.R
import kotlinx.android.synthetic.main.card_share_app.view.*


class ShareAppCard
@JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    init {
        LayoutInflater.from(context).inflate(R.layout.card_share_app, this, true)
    }

    var onShareTap: () -> Unit = {}
    var onDismissTap: () -> Unit = {}

    init {
        btn_share.setOnClickListener { onShareTap() }
        btn_dismiss.setOnClickListener { onDismissTap() }
    }
}