package halamish.expenses.ui.main.settings

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import halamish.expenses.R

class ProgressPreference @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null
) : Preference(context, attrs) {
  private var progress: View? = null
  fun progress():View? = progress

  init {
    widgetLayoutResource = R.layout.settings_item_loading
  }

  override fun onBindViewHolder(holder: PreferenceViewHolder) {
    super.onBindViewHolder(holder)
    progress = holder.findViewById(R.id.settings_progress)
  }
}