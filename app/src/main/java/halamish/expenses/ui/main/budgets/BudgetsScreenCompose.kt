package halamish.expenses.ui.main.budgets

import androidx.annotation.ColorRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import extensions.android.mapState
import halamish.expenses.R
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.data.granulateExpenses
import halamish.expenses.ui.expenseListView.Period
import halamish.expenses.v2.V2State
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.flow.StateFlow
import org.threeten.bp.LocalDate
import kotlin.math.roundToInt

@Composable fun BudgetScreen(){
    BudgetScreenContent()
}

@Composable
@Preview
private fun PreviewBudgetScreen() {
    BudgetScreenContent()
}

@Composable private fun BudgetScreenContent() {
    Column {
        BudgetScreenTitle(text = "TODO: master budget")
        Text(text = "TODO: scrollable list of category budgets")
        Text(text = "TODO: button to add a new budget")
    }
}

@Composable
private fun BudgetScreenTitle(text: String) {
    Card(
        modifier = Modifier.fillMaxWidth(),
        shape = RectangleShape,
        elevation = 4.dp) { // todo: elevation to const
        Spacer(modifier = Modifier.height(4.dp))
        Text(text=text)
        Spacer(modifier = Modifier.height(32.dp))
    }
}


private class BudgetsScreenViewModel(val appState: StateFlow<V2State> = appRedux.state): ViewModel() {
    sealed class BudgetState {
        object NoBudgetDefined: BudgetState()
        data class BudgetData(
            val limit: Int,
            val expensed: Int,
            val left: Int,
            @ColorRes val color: Int,
        ): BudgetState()
    }

    data class ScreenState(
        val period: Period,
        val masterBudget: BudgetState,
        val categoriesBudgets: List<BudgetState.BudgetData>
    )

    val masterBudget = appState.mapState(viewModelScope) { appState ->
        val categoriesBudgets = emptyList<BudgetState.BudgetData>()

        // period is currently hardcoded, maybe in the future we'll enabled going forward and backward.
        val period = Period.Month(LocalDate.now())
        val masterBudget = appState.project.monthlyBudget?.let { buildBudget(period, budget = it, oneTimeExpenses = appState.data.oneTime.values, rules = appState.data.rules.values ) } ?: BudgetState.NoBudgetDefined

        return@mapState ScreenState(period, masterBudget, categoriesBudgets)
    }

    companion object {
        private fun buildBudget(period: Period, budget: Int, oneTimeExpenses: Collection<OneTimeExpenseRecord>, rules: Collection<RecurringExpenseRule>): BudgetState.BudgetData {
            val events = granulateExpenses(rules, oneTimeExpenses, period.dateStart, period.dateEnd)
            val sumEventsDouble = events.sumOf { it.amount }
            val sumEventsInt = sumEventsDouble.roundToInt()
            val expenseRatio = sumEventsDouble / budget.toDouble()
            val color = when (expenseRatio) {
                in 0f..0.6f -> R.color.safe // todo: to material colors / expenseAppColors ?
                in 0.6f..1f -> R.color.warning
                else -> R.color.destructive
            }
            return BudgetState.BudgetData(limit = budget, expensed = sumEventsInt, left = budget - sumEventsInt, color=color)
        }
    }
}
