package halamish.expenses.ui.main.dashboard

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.core.view.updatePaddingRelative
import halamish.expenses.R
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import kotlinx.android.synthetic.main.card_most_expensive_categories.view.*
import kotlin.math.min


class MostExpensiveCategoryCard
@JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    data class UiInput(
        val categories: List<CategoryUiInput>,
        val period: String?
    )

    data class CategoryUiInput(val category: ExpenseCategory, val sum: Int)

    init {
        LayoutInflater.from(context).inflate(R.layout.card_most_expensive_categories, this, true)
        setNoData()
    }

    fun setUi(ui: UiInput){
        setPeriod(ui.period?.takeIf { it.isNotEmpty() })
        setCategories(ui.categories)
    }

    private fun setPeriod(period: String?) {
        title.text = if (period == null)
            context.getString(R.string.title_most_expensive_categories)
        else
            context.getString(R.string.title_most_expensive_categories_period).replace("PERIOD", period)
    }

    private fun setCategories(categories: List<CategoryUiInput>?) {
        groupHasData.visibility = View.VISIBLE
        groupNoData.visibility = View.GONE

        val max = categories?.map { it.sum }?.maxOrNull()?.toDouble() ?: Double.MAX_VALUE

        fun handle(data: CategoryUiInput?, titleView: TextView, sumView: TextView, barView: ImageView) {
            titleView.isVisible = data != null
            sumView.isVisible = data != null
            barView.isVisible = data != null
            if (data == null) return
            titleView.text = data.category.displayName
            sumView.text = CurrencyFormatter.get().format(data.sum)

            if (isInEditMode) return

            barView.setColorFilter(data.category.color)
            barView.post {
                val ratio = 1.0 - (data.sum.toDouble() / max)
                val ratioPretty = min(ratio, 0.93) // so the UI won't show bars too squashed
                val width = barView.width
                val paddingEnd = (ratioPretty * width).toInt()

                ValueAnimator.ofInt(barView.paddingEnd, paddingEnd)
                    .apply { addUpdateListener {
                        val paddingInAnimation = it.animatedValue as? Int ?: barView.paddingEnd
                        barView.updatePaddingRelative(end = paddingInAnimation)
                    } }
                    .setDuration(500)
                    .start()

                barView.updatePaddingRelative(end = paddingEnd)
            }
        }

        handle(categories?.getOrNull(0), titleCat1, sumCat1, barCat1)
        handle(categories?.getOrNull(1), titleCat2, sumCat2, barCat2)
        handle(categories?.getOrNull(2), titleCat3, sumCat3, barCat3)

        if (isInEditMode) return
        invalidate()
    }

    fun setNoData(period: String? = null) {
        groupHasData.visibility = View.GONE
        groupNoData.visibility = View.VISIBLE

        setPeriod(period)
        noDataExplanation.text = context.getString(R.string.most_expensive_categories_no_data, "¯\\_(ツ)_/¯")
    }
}