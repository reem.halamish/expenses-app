package halamish.expenses.ui.main.backup

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import halamish.expenses.R
import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.backup.BackupAndRestoreService.Reason.BEFORE_ACCEPTING_INVITATION
import halamish.expenses.backup.BackupAndRestoreService.Reason.BEFORE_APPLYING_SNAPSHOT
import halamish.expenses.backup.BackupAndRestoreService.Reason.BEFORE_GOOGLE_LOGIN
import halamish.expenses.backup.BackupAndRestoreService.Reason.MANUAL
import halamish.expenses.backup.BackupAndRestoreService.Reason.NIGHTLY
import halamish.expenses.backup.BackupAndRestoreService.Reason.UNKNOWN
import halamish.expenses.backup.BackupAndRestoreService.Snapshot
import halamish.expenses.v2.LoggedInUser
import halamish.expenses.v2.NotLoggedIn
import halamish.expenses.v2.V2State
import halamish.expenses.v2.appRedux
import halamish.expenses.v2.withLanguage
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.temporal.ChronoUnit
import timber.log.Timber

class BackupFragmentViewModel(
  private val backup: BackupAndRestoreService = appRedux.services.backupAndRestore,
  appStateFlow: StateFlow<V2State> = appRedux.state,
) : ViewModel() {

  data class SnapshotUi(
    val snapshot: Snapshot,
    val title: String,
    val subtitle: String,
  ) {
    val id get() = snapshot.id
  }

  val snapshots = combine(backup.snapshots, appStateFlow) { snapshots, appState ->
    val snapshotsToUse = when (appState.user) {
      is LoggedInUser -> {
        val allowedUserId = listOf(null, appState.user.userId)
        snapshots.filter { it.userIdAtSnapshot in allowedUserId }
      }

      NotLoggedIn -> snapshots.filter { it.userIdAtSnapshot == null }
    }

    if (snapshotsToUse.size != snapshots.size) {
      Timber.w("Using only ${snapshotsToUse.size} snapshots, from available ${snapshots.size}")
    }

    snapshotsToUse.sortedByDescending { it.timestampUtcMs }.map { it.wrap() }
  }
    .distinctUntilChanged()

  val hasData = snapshots.map { it.isNotEmpty() }.distinctUntilChanged()
  val subtitle = snapshots.map { it.sumOf { it.snapshot.snapshotFileSizeBytes } }
    .map { totalSize -> getString(R.string.feature_backup_summary)
      .replace("[size]", formatFileSize(totalSize))
    }
    .distinctUntilChanged()

  fun onDeleteAll() = viewModelScope.launch { backup.deleteAll() }
  fun onRestoreToSnapshot(snapshot: Snapshot) = viewModelScope.launch {
    backup.applySnapshot(snapshot)
  }

  fun onCreateBackup() = viewModelScope.launch {
    backup.recordSnapshot(MANUAL)
  }

  private fun getString(@StringRes resId: Int) = appRedux.app.withLanguage().getString(resId)

  private fun Snapshot.wrap() = SnapshotUi(
    snapshot = this,
    title = getString(
      when (this.reason) {
        MANUAL -> R.string.feature_backup_backupType_manual
        UNKNOWN -> R.string.feature_backup_backupType_unknown
        NIGHTLY -> R.string.feature_backup_backupType_nightly

        BEFORE_GOOGLE_LOGIN,
        BEFORE_ACCEPTING_INVITATION,
        BEFORE_APPLYING_SNAPSHOT ->
          R.string.feature_backup_backupType_automatic
      }
    ),
    subtitle = kotlin.run {
      val snapshotTime =
        Instant.ofEpochMilli(timestampUtcMs).atZone(ZoneId.systemDefault())
      val date = snapshotTime.toLocalDate().toString()
      val time = snapshotTime.toLocalTime()
        .truncatedTo(ChronoUnit.MINUTES)
        .toString()

      val creationLine = getString(R.string.feature_backup_created)
        .replace("[date]", date)
        .replace("[time]", time)

      val reasonLine = when (reason) {
        BackupAndRestoreService.Reason.MANUAL, BackupAndRestoreService.Reason.UNKNOWN, BackupAndRestoreService.Reason.NIGHTLY -> null
        BackupAndRestoreService.Reason.BEFORE_GOOGLE_LOGIN ->
          getString(R.string.feature_backup_reason_BEFORE_GOOGLE_LOGIN)
        BackupAndRestoreService.Reason.BEFORE_ACCEPTING_INVITATION ->
          getString(R.string.feature_backup_reason_BEFORE_ACCEPTING_INVITATION)
        BackupAndRestoreService.Reason.BEFORE_APPLYING_SNAPSHOT ->
          getString(R.string.feature_backup_reason_BEFORE_APPLYING_SNAPSHOT)
      }

      val expenses = getString(if (countExpenseOneTimes == 1)
        R.string.feature_backup_expenseCount_one
        else
        R.string.feature_backup_expenseCount_many)

      val itemCountLine = (if (countExpenseRules > 0)
        getString(R.string.feature_backup_totalExpenses_withRecurring)
      else
        getString(R.string.feature_backup_totalExpenses_noRecurring))
        .replace("[expenses]", "$countExpenseOneTimes $expenses")
        .replace("[recurring]", countExpenseRules.toString())

      return@run listOfNotNull(creationLine, reasonLine, itemCountLine)
        .joinToString(separator = "\n")
    }
  )

  private val kb1 = 1024L
  private val mb1 = 1024L * 1024L
  private fun twoDecimalPlaces(double: Double) = "%.2f".format(double)
  private fun formatFileSize(sizeBytes: Long) = when (sizeBytes) {
    in (mb1/10 .. Long.MAX_VALUE) -> twoDecimalPlaces(sizeBytes.toDouble() / mb1.toDouble()) + " MB"
    in (kb1 .. mb1) -> twoDecimalPlaces(sizeBytes.toDouble() / kb1.toDouble()) + " KB"
    else -> "$sizeBytes Bytes"
  }
}
