package halamish.expenses.ui.main.collaborate

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import halamish.expenses.data.ProjectInvitationLink
import halamish.expenses.repo.config.AppConfiguration
import halamish.expenses.repo.dataRepo.DynamicLinksCreator
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CreateInvitationLinkViewModel(
    private val dynamicLinks: DynamicLinksCreator = appRedux.services.dynamicLinks,
    private val config: AppConfiguration = appRedux.state.value.config
): ViewModel() {
    sealed class State {
        object NotSupported: State()
        object Loading: State()
        data class DataReady(val data: ProjectInvitationLink): State()
    }

    private val email = appRedux.state.value.userInfo?.email
    private val _stateFlow = MutableStateFlow<State>(State.NotSupported)
    val stateFlow = _stateFlow.readOnly()

    init {
        runFlow()
    }

    private fun runFlow() {
        val featureActivated = config.projectInvitations.enabled
        if (email == null || !featureActivated) {
            _stateFlow.value = State.NotSupported
            return
        }
        val fetched = dynamicLinks.fetchLinkToCollaborateOnProject(email)
        if (fetched != null) {
            _stateFlow.value = State.DataReady(fetched)
            return
        }
        _stateFlow.value = State.Loading
        viewModelScope.launch {
            val created = dynamicLinks.createLinkToCollaborateOnProject(email)
            _stateFlow.value = State.DataReady(created)
        }
    }

    companion object {
        private fun <T> MutableStateFlow<T>.readOnly(): StateFlow<T> = this
    }
}