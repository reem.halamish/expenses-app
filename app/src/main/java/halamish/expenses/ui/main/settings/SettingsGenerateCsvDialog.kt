package halamish.expenses.ui.main.settings

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.work.OneTimeWorkRequest
import halamish.expenses.databinding.DialogGenerateCsvBinding
import halamish.expenses.repo.csv.GenerateCsvWorker
import halamish.expenses.repo.csv.GenerateCsvWorker.Companion.Sort.BY_CATEGORY
import halamish.expenses.repo.csv.GenerateCsvWorker.Companion.Sort.BY_DATE

class SettingsGenerateCsvDialog(context: Context) : Dialog(context) {
    var callback: (workRequest: OneTimeWorkRequest) -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DialogGenerateCsvBinding.inflate(LayoutInflater.from(context))
        setContentView(binding.root)

        for (v in listOf(binding.radioBtnSortByCategory, binding.textViewSortByCategory)) {
            v.setOnClickListener {
                binding.radioBtnSortByCategory.isChecked = true
                binding.radioBtnSortByDate.isChecked = false
            }
        }

        for (v in listOf(binding.radioBtnSortByDate, binding.textViewSortByDate)) {
            v.setOnClickListener {
                binding.radioBtnSortByDate.isChecked = true
                binding.radioBtnSortByCategory.isChecked = false
            }
        }

        binding.radioBtnSortByDate.isChecked = true

        binding.btnCta.setOnClickListener {
            val request = GenerateCsvWorker.enqueue(
                context,
                sort = if (binding.radioBtnSortByCategory.isChecked) BY_CATEGORY else BY_DATE
            )
            callback(request)
            dismiss()
        }
    }

    companion object {
        fun show(
            context: Context,
            onStartedGeneratingCsv: (workRequest: OneTimeWorkRequest) -> Unit
        ) = SettingsGenerateCsvDialog(context).apply {
            setCancelable(true)
            setCanceledOnTouchOutside(true)
            callback = onStartedGeneratingCsv
            show()
        }
    }
}