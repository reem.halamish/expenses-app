package halamish.expenses.ui.main.dashboard

import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.Intent.EXTRA_TEXT
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import halamish.expenses.R
import halamish.expenses.data.granulateExpenses
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.cancelDialog
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.repo.dashboardScreenTapCardButton
import halamish.expenses.repo.defineBudget
import halamish.expenses.ui.expenseListView.Period
import halamish.expenses.ui.map
import halamish.expenses.ui.mutableLiveDataOf
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.ui.readOnly
import halamish.expenses.v2.UpdateProjectDetailsRunnable
import halamish.expenses.v2.V2State
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import timber.log.Timber

class DashboardViewModel(
  val appStateFlow: StateFlow<V2State> = appRedux.state,
  private val analytics: AnalyticsLogger = appRedux.services.analytics

): ProtoViewModel() {
    private val _expensive = MutableStateFlow(MostExpensiveCategoryCard.UiInput(emptyList(), null))
    val expensive = _expensive.asStateFlow()

    data class BudgetDetails(
        val budget: Int?,
        val spent: Int,
        val shouldShow: Boolean = true,
        val period: String? = null
    )

    private val _budget = mutableLiveDataOf<BudgetDetails>()
    val budget = _budget.readOnly()

    data class DetailsForBuildBudget(val expensesAverage: Int?, val monthsAmount: Int)

    private val _detailsForBudgetBuilding = mutableLiveDataOf<DetailsForBuildBudget>()
    val detailsForBudgetBuilding = _detailsForBudgetBuilding.readOnly()

    init {
        viewModelScope.launch {

            fun build(state: V2State = appRedux.state.value) {
                viewModelScope.launch {
                    val period = Period.Month(LocalDate.now()).prev
                    val list = withContext(Dispatchers.Default) {
                        return@withContext granulateExpenses(
                            startDate = period.dateStart,
                            endDate = period.dateEnd
                        )
                            .groupBy { it.categoryId }
                            .map {
                                state.data.categories[it.key] to it.value.map { it.amount }.sum()
                            }
                            .mapNotNull { if (it.first == null || it.second <= 0) null else it.first!! to it.second }
                            .sortedByDescending { it.second }
                            .map {
                                MostExpensiveCategoryCard.CategoryUiInput(
                                    it.first,
                                    it.second.toInt()
                                )
                            }
                    }
                    _expensive.value = MostExpensiveCategoryCard.UiInput(list, period.display)
                }

                viewModelScope.launch buildBudget@{
                    state.project.defineBudgetLastDismiss
                        ?.takeIf { Duration.between(it, Instant.now()).toDays() <= 7 }
                        ?.run {
                            Timber.d("Noticed dismiss on define-budget, will skip")
                            _budget.value =
                                BudgetDetails(
                                    null,
                                    0,
                                    shouldShow = false
                                )
                            return@buildBudget
                        }

                    val budget = state.project.monthlyBudget
                    val period = Period.Month(LocalDate.now())
                    val spent = withContext(Dispatchers.Default) {
                        granulateExpenses(
                            startDate = period.dateStart,
                            endDate = period.dateEnd
                        )
                            .map { it.amount }
                            .sum()
                            .toInt()
                    }
                    _budget.value =
                        BudgetDetails(
                            budget,
                            spent,
                            period = period.display
                        )
                }

                viewModelScope.launch(Dispatchers.Default) {
                    val maxMonthsAmount = 6
                    var currentMonthAmount = 0
                    var totalExpenses = 0.0
                    var period: Period = Period.Month(LocalDate.now())
                    while (true) {
                        if (currentMonthAmount >= maxMonthsAmount) break
                        val spent = granulateExpenses(
                            startDate = period.dateStart,
                            endDate = period.dateEnd
                        )
                            .map { it.amount }
                            .sum()
                            .toInt()
                        if (spent == 0)
                        // nothing in this month. stop
                            break

                        totalExpenses += spent
                        currentMonthAmount += 1
                        period = period.prev
                    }
                    val details =
                        if (currentMonthAmount == 0 || totalExpenses == 0.0)
                            DetailsForBuildBudget(
                                null,
                                currentMonthAmount
                            )
                        else
                            DetailsForBuildBudget(
                                (totalExpenses / currentMonthAmount).toInt(),
                                currentMonthAmount
                            )

                    _detailsForBudgetBuilding.postValue(details)
                }
            }

            viewModelScope.launch {
                appStateFlow.collect { build() }
            }
        }
    }


    // "share" card handling

    private val _shareShowLastTime = mutableLiveDataOf<Instant?>()
    private var tapDismiss = false // if true, we won't show "dismiss" button in this running time

    val showShowShareCard: LiveData<Boolean> = _shareShowLastTime.map {
        (!tapDismiss) && (it == null || Duration.between(
            it,
            Instant.now()
        ).toDays() >= 7)
    }

    init {
        _shareShowLastTime.value = appStateFlow.value.project.shareButtonLastDismiss
        viewModelScope.launch {
            appStateFlow.map { it.project.shareButtonLastDismiss }.distinctUntilChanged()
                .collect {
                    Timber.d("shareButtonLastDismiss: ${it}")
                    _shareShowLastTime.setValue(it)
                }
        }
    }

    fun dismissShareClicked() {
        analytics.dashboardScreenTapCardButton("SHARE", "DISMISS")
        appRedux.eventBus.send(UpdateProjectDetailsRunnable(Unit, "shareButtonLastDismiss") {
            shareButtonLastDismiss = Instant.now()
        })
    }

    fun defineBudgetClicked() {
        analytics.dashboardScreenTapCardButton("BUDGET", "DEFINE")
    }

    fun setBudget(budget: Int?) {
        analytics.defineBudget(budget, prevBudget = appStateFlow.value.project.monthlyBudget)
        appRedux.eventBus.send(UpdateProjectDetailsRunnable(Unit, "defined budget: $budget") {
            monthlyBudget = budget
        })
    }

    fun onCancelDefineBudgetDialog() {
        analytics.cancelDialog("DEFINE_BUDGET_DIALOG")
    }


    fun customizeBudgetClicked() {
        analytics.dashboardScreenTapCardButton("BUDGET", "CUSTOMIZE")
    }

    fun dismissBudgetClicked() {
        analytics.dashboardScreenTapCardButton("BUDGET", "DISMISS")
        appRedux.eventBus.send(UpdateProjectDetailsRunnable(Unit, "dismiss budget") {
            defineBudgetLastDismiss = Instant.now()
        })
    }

    fun intentForButtonShareClick(): Intent {
        val context = appRedux.app
        analytics.dashboardScreenTapCardButton("SHARE", "SHARE")
        tapDismiss = true
        _shareShowLastTime.postValue(null)

        val googlePlayPrefix = context.getString(R.string.google_play_app_download_prefix)
        val appId = context.getString(R.string.app_id_in_play_store)
        val textProto =
            "Hey check out this app, it helps you keep track of your expenses. Download it from %s%s"

        return Intent().apply {
            action = ACTION_SEND
            putExtra(EXTRA_TEXT, String.format(textProto, googlePlayPrefix, appId))
            type = "text/plain"
        }
    }

    val currencySymbol: String get() = CurrencyFormatter.get().currency.symbol // todo: generify to appRedux.formatters.currency.format(int/double). and remove this?
}