package halamish.expenses.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import halamish.expenses.R
import halamish.expenses.repo.*
import halamish.expenses.repo.PushNotificationSender.NotificationBehavior.*
import halamish.expenses.ui.expenseManipulation.ExpenseManipulationActivity
import halamish.expenses.ui.main.settings.SettingsFragment
import halamish.expenses.ui.newFeature.NewFeatureActivity
import halamish.expenses.ui.popup.PopupRequest
import halamish.expenses.ui.prototypes.ProtoActivity
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : ProtoActivity() {

  override val screenName = "MainActivity"

    private val newFeatureActivityResultHandler = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK &&
                result.data?.getStringExtra(NewFeatureActivity.EXTRA_OUTPUT_ACTION) == NewFeatureActivity.OUTPUT_ACTION_OPEN_DARK_MODE
            ) {
                val navController = nav_host_fragment.findNavController()
                val bundle = bundleOf(SettingsFragment.INPUPT_HIGHLIGHT_ITEM to SettingsFragment.Companion.ItemHighlight.NIGHT_MODE)
                navController.navigate(R.id.settingsFragment, bundle, NavOptions.Builder()
                    .setEnterAnim(android.R.anim.fade_in)
                    .setExitAnim(android.R.anim.fade_out)
                    .build())
            }
        }

    override fun supportsPopup(popupRequest: PopupRequest) = true

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

      if (NewFeatureActivity.shouldOpen()) {
          newFeatureActivityResultHandler.launch(Intent(this, NewFeatureActivity::class.java))
      }

    val navController = nav_host_fragment.findNavController()

    MainLeftDrawer(mainDrawerLayout, leftDrawer, host=this, navController=navController)
    bottom_bar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
    bottom_bar.setNavigationOnClickListener { mainDrawerLayout.open() }
    bottom_bar.setupWithNavController(navController, AppBarConfiguration.Builder(
//      topLevelDestinationIds =
        setOf(
            R.id.settingsFragment,
            R.id.dashboardFragment,
            R.id.expenseMonthlyFragment,
            R.id.viewExpensesHistoryFragment,
        )
    )
      .setOpenableLayout(mainDrawerLayout)
      .build())

    fab.setOnClickListener {
      ExpenseManipulationActivity.startCreateExpense(this)
    }

    fun refreshStartDestination() {
      LocalData().overridenStartDestination?.let { dest ->
        navController.graph.setStartDestination(dest.destinationId)
        navController.graph = navController.graph
      }
    }

    refreshStartDestination()
    navController.addOnDestinationChangedListener { _, destination, _ ->
      Timber.d("did change destination to $destination")
        analytics.fragmentShown(name = (destination.label?.toString() ?: "<UNKNOWN>"))
      refreshStartDestination()
    }

      handlePushIfExists(intent, moveToHistory = {
        navController.navigate(R.id.viewExpensesHistoryFragment)
      })
  }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handlePushIfExists(intent, moveToHistory = {
            val navController = nav_host_fragment.findNavController()
            navController.navigate(R.id.viewExpensesHistoryFragment)
        })
    }

    private fun handlePushIfExists(intent: Intent?, moveToHistory: () -> Unit) {
        val extras = intent?.extras ?: return

        if (extras.getBoolean(PushNotificationSender.EXTRA_IS_INTERNAL_PUSH_NOTIFICATION)) {
            val analyticsData = extras.getSerializable(PushNotificationSender.EXTRA_PUSH_NOTIFICATION_ANALYTICS_DATA) as? AnalyticsPushData
            analyticsData?.let { analytics.pushOpened(it) }
            val behavior = extras.getString(PushNotificationSender.EXTRA_PUSH_NOTIFICATION_BEHAVIOR)
            when (values().firstOrNull { it.name == behavior } ?: UNKNOWN) {
                DO_NOTHING -> Unit
                UNKNOWN -> Unit
                OPEN_HISTORY -> moveToHistory()
            }
        }
    }
}
