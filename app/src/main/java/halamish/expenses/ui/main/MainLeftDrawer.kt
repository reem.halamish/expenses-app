package halamish.expenses.ui.main

import android.app.Activity
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import halamish.expenses.R
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.drawerClose
import halamish.expenses.repo.drawerOpen
import halamish.expenses.v2.appRedux
import timber.log.Timber

class MainLeftDrawer(
    val layout: DrawerLayout,
    val drawer: NavigationView,
val host: Activity,
val navController: NavController,
val analytics: AnalyticsLogger = appRedux.services.analytics) {

    init {
        // todo: font by https://www.fontspace.com/maghrib-font-f43429, can I use a TextView?
        drawer.inflateHeaderView(R.layout.activity_main_left_drawer_header_app_logo)
//        drawer.getHeaderView(0).findViewById<TextView>(R.id.someTextView).text = "Expenses"


        drawer.setupWithNavController(navController)

        layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerStateChanged(newState: Int) {}
            override fun onDrawerOpened(drawerView: View) {
                Timber.d("did open drawer")
                analytics.drawerOpen()
            }
            override fun onDrawerClosed(drawerView: View) {
                Timber.d("did close drawer")
                analytics.drawerClose()
            }
        })
    }
}
