package halamish.expenses.ui.main

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.RippleDrawable
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import halamish.expenses.R
import kotlinx.android.synthetic.main.view_select_date_range_header.view.*

class SelectDateRangeHeaderView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_select_date_range_header, this)
        iv_header_select_date.setImageDrawable(RippleDrawable(
            ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent)),
            iv_header_select_date.drawable, null
        ))
    }
}