package halamish.expenses.ui.main.dashboard

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.transition.ChangeBounds
import androidx.transition.TransitionManager
import halamish.expenses.R
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.repo.currency.of
import kotlinx.android.synthetic.main.card_budget_mode_defined.view.*
import kotlinx.android.synthetic.main.card_budget_mode_needs_definition.view.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


class BudgetCard
@JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var callbackOnDefineBudget: () -> Unit = {}
    var callbackOnDismiss: () -> Unit = {}
    var callbackOnCustomizeBudget: () -> Unit = {}

    private sealed class Mode {
        object Loading : Mode()
        object NeedsDefinition : Mode()
        data class Defined(val budget: Int, val spent: Int, val period: String?) : Mode()
    }

    private var mode: Mode? = null
    private val runningAnimationsPrivate = mutableListOf<ValueAnimator>()

    @Synchronized
    private fun add(animator: ValueAnimator) = runningAnimationsPrivate.add(animator)

    @Synchronized
    private fun remove(animator: ValueAnimator) = runningAnimationsPrivate.remove(animator)
    private fun clearAnimators() {
        val copy = synchronized(this) {
            val copy = runningAnimationsPrivate.toList()
            runningAnimationsPrivate.clear()
            copy
        }
        copy.forEach { it.cancel() }
    }

    init {
        setupLoading()
    }

    private fun boundsTransition() {
        TransitionManager.beginDelayedTransition(this, ChangeBounds())
    }

    fun setupLoading() {
        if (mode == Mode.Loading) return
        mode = Mode.Loading
        boundsTransition()
        removeAllViews()
        LayoutInflater.from(context).inflate(R.layout.card_budget_mode_loading, this, true)
    }

    fun setupNeedsDefinition() {
        if (mode == Mode.NeedsDefinition) return
        mode = Mode.NeedsDefinition
        boundsTransition()
        removeAllViews()
        LayoutInflater.from(context).inflate(R.layout.card_budget_mode_needs_definition, this, true)
        btn_define_budget.setOnClickListener { callbackOnDefineBudget() }
        btn_dismiss.setOnClickListener { callbackOnDismiss() }
    }

    // todo multiple calls to this will result in flickering UI and multiple animations running, how to stop it?
    fun setupDefined(budget: Int, spent: Int, period: String?) {
        val prevMode = this.mode
        this.mode = Mode.Defined(budget, spent, period)

        if (this.mode == prevMode) return

        boundsTransition()

        if (prevMode == null || prevMode !is Mode.Defined) {
            removeAllViews()
            LayoutInflater.from(context).inflate(R.layout.card_budget_mode_defined, this, true)
            progressBudgetNew.progress = 0f
        }

        btn_customize.setOnClickListener { callbackOnCustomizeBudget() }

        title.text =
            if (period == null) context.getString(R.string.budget_card_title)
            else context.getString(R.string.budget_card_title_period, period)


        val budgetColorRes = when (spent.toFloat() / budget.toFloat()) {
            in 0f..0.6f -> R.color.safe
            in 0.6f..1f -> R.color.warning
            else -> R.color.destructive
        }
        val budgetColor = ContextCompat.getColor(context, budgetColorRes)

        if (progressBudgetNew.progressMax != budget.toFloat()) {
            progressBudgetNew.apply {
                progressMax = budget.toFloat()
                progress = 0F
            }
        }

        val targetProgress = min(budget, spent).toFloat()

        if (isInEditMode) {
          val formatter = CurrencyFormatter.of("USD")
            progressBudgetNew.progress = targetProgress
            progressBudgetNew.progressBarColor = budgetColor
            textAmount.text = formatter.format(abs(budget - spent))
            textBudgetSummary.text = context.getString(
                R.string.budget_card_budget_summary,
                formatter.format(spent),
                formatter.format(budget)
            )
            textAmountExplanation.text =
                context.getString(if (budget >= spent) R.string.budget_card_left_to_spare else R.string.budget_card_overspent)
            return
        }

        val currency = CurrencyFormatter.get()

        val animDuration = max(1000L, (3L * spent) / 1000L) // 3 sec for each 1000$ spent
        clearAnimators()

        addAnimation(
            animDuration,
            ValueAnimator.ofFloat(progressBudgetNew.progress, spent.toFloat())
        ) { value ->
            progressBudgetNew.progress = min((value as? Float ?: spent.toFloat()), targetProgress)
        }

        val startColor = ContextCompat.getColor(context, R.color.safe)
        addAnimation(animDuration, ValueAnimator.ofArgb(startColor, budgetColor)) { value ->
            progressBudgetNew.progressBarColor = value as? Int ?: budgetColor
            textAmountExplanation.setTextColor(value as? Int ?: budgetColor)
        }

        val startValueSpent = (prevMode as? Mode.Defined)?.spent ?: 0
        addAnimation(animDuration, ValueAnimator.ofInt(startValueSpent, spent)) { value ->
            val spentNow = value as? Int ?: spent
            val overspent = spentNow > budget

            textAmount.text = currency.format(abs(budget - spentNow))
            textBudgetSummary.text = context.getString(
                R.string.budget_card_budget_summary,
                currency.format(spentNow),
                currency.format(budget)
            )
            textAmountExplanation.text =
                context.getString(if (overspent) R.string.budget_card_overspent else R.string.budget_card_left_to_spare)
        }
    }

    override fun onDetachedFromWindow() {
        clearAnimators()
        super.onDetachedFromWindow()
    }

    private fun addAnimation(
        duration: Long,
        animator: ValueAnimator,
        onTick: (value: Any?) -> Unit
    ) {
        animator
            .setDuration(duration)
            .apply {
                addUpdateListener { animation ->
                    onTick(animation.animatedValue)
                }
            }
            .apply { add(this) }
            .apply animator@{
                addListener(object : Animator.AnimatorListener {
                    private fun remove() { remove(this@animator) }
                    override fun onAnimationStart(animation: Animator) = Unit
                    override fun onAnimationRepeat(animation: Animator) = Unit
                    override fun onAnimationEnd(animation: Animator) = remove()
                    override fun onAnimationCancel(animation: Animator) = remove()
                })
            }
            .start()
    }
}
