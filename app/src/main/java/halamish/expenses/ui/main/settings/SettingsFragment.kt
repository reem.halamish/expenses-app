package halamish.expenses.ui.main.settings


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Spanned
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.preference.CheckBoxPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.work.WorkInfo
import androidx.work.WorkManager
import extensions.android.AndroidExtensions.toHexColorStringNoAlpha
import extensions.android.fromHtml
import halamish.expenses.BuildConfig
import halamish.expenses.R
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.logger.GenerateLogZipFileWorker
import halamish.expenses.repo.csv.GenerateCsvWorker
import halamish.expenses.repo.handlers.DeleteAllUserDataHandler
import halamish.expenses.repo.settingsEvent
import halamish.expenses.repo.settingsItemClicked
import halamish.expenses.ui.language.SelectLanguageActivity
import halamish.expenses.ui.popup.PopupsCreator
import halamish.expenses.ui.showWithCancelOnPause
import halamish.expenses.v2.LocalStorage
import halamish.expenses.v2.LoggedInUser
import halamish.expenses.v2.NightModeRepo
import halamish.expenses.v2.NotLoggedIn
import halamish.expenses.v2.PhoneLanguage.ENGLISH
import halamish.expenses.v2.PhoneLanguage.HEBREW
import halamish.expenses.v2.UpdateProjectDetailsRunnable
import halamish.expenses.v2.User
import halamish.expenses.v2.appRedux
import halamish.expenses.v2.showingAddCategoryButton
import halamish.expenses.v2.updateForShowingAddCategoryButton
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber

class SettingsFragment : PreferenceFragmentCompat() {
  companion object {
    const val INPUPT_HIGHLIGHT_ITEM = "SCROLL_TO_ITEM"

    enum class ItemHighlight : java.io.Serializable { NIGHT_MODE }
  }

  private val analytics get() = appRedux.services.analytics

  override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
    setPreferencesFromResource(R.xml.app_settings, rootKey)

    val languagePref = findPreference<Preference>("language")
    languagePref?.setOnPreferenceClickListener {
      analytics.settingsItemClicked("language")
      requireContext().startActivity(
        Intent(
          requireContext(),
          SelectLanguageActivity::class.java
        ).apply {
          putExtra(SelectLanguageActivity.KEY_BLOCK, false)
        })
      true
    }
    lifecycleScope.launch {
      appRedux.state.map { it.phoneData.language }
        .collect { language ->
          val languageStr = when (language) {
            ENGLISH -> getString(R.string.language_consts_langName_english)
            HEBREW -> getString(R.string.language_consts_langName_hebrew)
            null -> null
          }
          languagePref?.summary =
            if (language == null) getString(R.string.settings_language_summary_not_set)
            else getString(R.string.settings_language_summary_set_ps, languageStr)
        }
    }


    // todo: on logout request, if anonymous user, also delete the auth
    lifecycleScope.launch {
      val loginLogoutPref = findPreference<Preference>("login-logout") ?: return@launch
      val busyLoggingOut = MutableStateFlow(false)

      appRedux.state
        .map { it.user }
        .distinctUntilChanged()
        .combine(busyLoggingOut) { user, logginOut -> listOf(user, logginOut) }
        .collect { list ->
          val (user, isLoggingOut) = list
          user as User
          isLoggingOut as Boolean
          if (isLoggingOut) {
            loginLogoutPref.summary = getString(R.string.settings_logout_summary_wip)
            return@collect
          }
          when (user) {
            is LoggedInUser -> {
              loginLogoutPref.title = getString(R.string.settings_logout_title)

              val email = user.email
              val summary =
                if (email != null)
                  getString(R.string.settings_logout_summary_email_proto, email)
                else getString(R.string.settings_logout_summary_no_email)

              loginLogoutPref.summary = summary
              loginLogoutPref.setOnPreferenceClickListener {
                analytics.settingsItemClicked("logout")
                AlertDialog.Builder(requireContext())
                  .setTitle(R.string.settings_logout_title)
                  .setMessage(getString(R.string.settings_confirm_logout_msg))
                  .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    analytics.settingsItemClicked("logout.confirm")
                    dialog.dismiss()
                    busyLoggingOut.value = true
                    appRedux.scope.launch {
                      appRedux.handlers.logoutHandler.logoutAndClearAllLocalState()
                      busyLoggingOut.value = false
                    }
                  }
                  .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    analytics.settingsItemClicked("logout.cancel")
                    dialog.cancel()
                  }
                  .setCancelable(true)
                  .showWithCancelOnPause(lifecycle)

                return@setOnPreferenceClickListener true
              }
            }

            NotLoggedIn -> {
              loginLogoutPref.title = getString(R.string.settings_login_title)
              loginLogoutPref.summary = getString(R.string.settings_login_summary)
              loginLogoutPref.setOnPreferenceClickListener {
                analytics.settingsItemClicked("login")
                findNavController().navigate(R.id.settingsLoginFragment)
                return@setOnPreferenceClickListener true
              }
            }
          }
        }
    }

    val deleteAllPref = findPreference<ProgressPreference>("delete-all")
    lifecycleScope.launch {
      deleteAllPref ?: return@launch
      val deleteAllHandler = appRedux.handlers.deleteAllUserDataHandler
      deleteAllHandler.stateFlow.collect { state ->
        when (val enabledState = state.enabled) {
          DeleteAllUserDataHandler.Enabled.CanDeleteAllData -> {
            deleteAllPref.isEnabled = true
            deleteAllPref.summary = getString(R.string.settings_deleteAll_summaryEnabled)
          }

          is DeleteAllUserDataHandler.Enabled.DisabledOnlyCollaborating -> {
            deleteAllPref.isEnabled = false
            val owner = enabledState.ownerUserPrettyString
            deleteAllPref.summary = if (owner != null)
              getString(R.string.settings_deleteAll_summaryDisabled, owner)
            else
              getString(R.string.settings_deleteAll_summaryDisabled_noOwner)
          }

          DeleteAllUserDataHandler.Enabled.DisabledWorkingOnIt -> {
            deleteAllPref.isEnabled = false
            deleteAllPref.summary = getString(R.string.settings_deleteAll_summaryBusy)
            deleteAllPref.progress()?.isVisible = true
            delay(2000L) // make a UX of hard work
            // await real deletion done
            deleteAllHandler.stateFlow.first { it != state }

            // show "done" msg for a quick bit
            deleteAllPref.summary = getString(R.string.settings_deleteAll_summaryDone)
            deleteAllPref.progress()?.isVisible = false
            delay(1000)
          }
        }

        val dataToDelete = state.userDataToDelete
        deleteAllPref.setOnPreferenceClickListener {
          analytics.settingsItemClicked("delete_all")

          appRedux.handlers.popups.showDestructivePopup(
            "delete_all",
            requireContext(),
            viewLifecycleOwner.lifecycle,
            PopupsCreator.CancellationPolicy.CANCEL_ON_TAP_OUTSIDE_AND_LIFECYCLE_PAUSE,
            messageText = buildDeleteAllSummaryMsg(dataToDelete),
            destructiveButtonText = getString(R.string.settings_deleteAll_dialog_cta_delete),
            secondaryButtonText = getString(android.R.string.cancel),
            onDestructiveButtonClick = {
              appRedux.scope.launch {
                val result = deleteAllHandler.deleteAllUserData(useCase = "click from settings")
                if (!result) {
                  showErrorDeletingDataPopup()
                }
              }
            },
            onSecondaryButtonClick = {},
          )

          return@setOnPreferenceClickListener true
        }
      }
    }

    val collaboratePref = findPreference<Preference>("collaborate")
    fun buildCollaborate(userInfo: UserInfo?, project: ProjectDetails, isLoggedIn: Boolean) {
      val pref = collaboratePref ?: return
      val otherUsers = (project.users.values - (userInfo?.email ?: "")).toList().sorted()
      val isCollaborating = otherUsers.isNotEmpty()
      val creatorUid = project.creator
      val isCreator = userInfo?.uid == creatorUid

      pref.onPreferenceClickListener = Preference.OnPreferenceClickListener {
        analytics.settingsItemClicked("collaborate")
        if (!isLoggedIn) findNavController().navigate(R.id.action_settingsFragment_to_settingsLoginFragment)
        else if (!isCollaborating) findNavController().navigate(R.id.action_settingsFragment_to_createInvitationLinkFragment)
        else findNavController().navigate(R.id.action_settingsFragment_to_customizeCollaboratorsFragment)
        true
      }

      // todo: R.string now supports plurals. worth checking it out I think
      pref.summary =
        if (!isLoggedIn) getString(R.string.settings_collaborate_summary_needs_login)
        else if (!isCollaborating) getString(R.string.settings_collaborate_summary_tap_to_set)
        else if (!isCreator) getString(R.string.settings_collaborate_summary_collaborating_non_creator)
        else if (otherUsers.size == 1) getString(R.string.settings_collaborate_summary_collaborating_singleUser)
        else /* isCreator, multiple other users */
          getString(R.string.settings_collaborate_summary_collaborating_pd, otherUsers.size)

      pref.isEnabled = true
    }

    lifecycleScope.launch {
      appRedux.state.map { Triple(it.userInfo, it.project, it.user is LoggedInUser) }
        .distinctUntilChanged()
        .collect {
          buildCollaborate(
            userInfo = it.first,
            project = it.second,
            isLoggedIn = it.third
          )
        }
    }

    val aboutPref = findPreference<Preference>("about")
    aboutPref?.setOnPreferenceClickListener {
      analytics.settingsItemClicked("about")
      AlertDialog.Builder(requireContext())
        .setMessage(
          """<H1>${getString(R.string.app_name)}</H1>[-nl]
                    <H3>General data:</H3>[-nl]
                    Version: <b>${BuildConfig.VERSION_NAME}</b>
                    Build: <b>${BuildConfig.VERSION_CODE}</b>
                    Database by <b>Firestore</b>.
                    Icons by <b>Icon8</b>.
                    Play Store pictures by <b>Pixabay</b>.
                    
                    <H3>Your data:</H3>[-nl]
                    - Device: <b>${appRedux.state.value.deviceId}</b>
                    - Project: <b>${appRedux.state.value.project.id}</b>
                    - User: <b>${appRedux.state.value.userInfo?.uid ?: "LOGGED_OUT"}</b>
                    
                    Created by Adi and Re'em Halamish.
                    Thanks for using our app! 💕
                    
                """.trimIndent()
            .replace("[-nl]\n", "") // no new-line
            .replace("\n", "<br/>\n")
            .fromHtml()
        )
        .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
        .setCancelable(true)
        .showWithCancelOnPause(lifecycle)

      return@setOnPreferenceClickListener true
    }

    val feedbackPref = findPreference<ProgressPreference>("feedback")
    feedbackPref?.setOnPreferenceClickListener {
      analytics.settingsItemClicked("feedback")
      lifecycleScope.launch {
        feedbackPref.progress()?.isVisible = true
        feedbackPref.isEnabled = false
        val uri = generateLogsZipFile()
        delay(300)
        feedbackPref.progress()?.isVisible = false
        feedbackPref.isEnabled = true
        sendEmailToDevelopers(
          subject = "${getString(R.string.app_name)} - Feedback",
          attachment = uri
        )
      }
      return@setOnPreferenceClickListener true
    }

    findPreference<CheckBoxPreference>("hide_categories")?.let { checkbox ->
      lifecycleScope.launchWhenStarted {
        appRedux.state.map { it.project }
          .collect { project ->
            checkbox.isChecked = project.hideEmptyCategoriesInMajorExpensesList
            checkbox.onPreferenceChangeListener =
              Preference.OnPreferenceChangeListener { preference, newValue ->
                newValue as Boolean
                analytics.settingsItemClicked("hide_categories", bundleOf("new_value" to newValue))
                appRedux.eventBus.send(
                  UpdateProjectDetailsRunnable(
                    "hide empty categories: $newValue"
                  ) {
                    clone().apply {
                      hideEmptyCategoriesInMajorExpensesList = newValue
                    }
                  })
                true
              }
          }
      }
    }

    findPreference<ListPreference>("night_mode")?.let { preference ->
      val nightModeRepo = appRedux.services.nightModeRepo
      lifecycleScope.launch {
        nightModeRepo.state().collect {
          preference.value = it.name
        }
      }
      preference.setOnPreferenceChangeListener { _, newValue ->
        Timber.d("new night-mode preference change! $newValue")
        newValue as String
        val newNightMode = NightModeRepo.NightMode.valueOf(newValue)
        lifecycleScope.launch { nightModeRepo.setNew(newNightMode) }
        return@setOnPreferenceChangeListener true
      }
    }

    findPreference<CheckBoxPreference>("hide_add_category")?.let { checkbox ->
      lifecycleScope.launchWhenStarted {
        appRedux.state.map { it.localStorage.showingAddCategoryButton.not() }
          .distinctUntilChanged()
          .collect { hideAddCategory ->
            checkbox.isChecked = hideAddCategory
            checkbox.onPreferenceChangeListener =
              Preference.OnPreferenceChangeListener { _, newValue ->
                newValue as Boolean
                analytics.settingsItemClicked(
                  "hide_add_category",
                  bundleOf("new_value" to newValue)
                )
                appRedux.eventBus.send(
                  LocalStorage.updateForShowingAddCategoryButton(showing = !newValue) // notice: negation!
                )
                true
              }
          }
      }
    }

    findPreference<CheckBoxPreference>("notify_on_expenses")?.let { notifyPref ->
      lifecycleScope.launchWhenCreated {
        appRedux.state.collect { state ->
          val isChecked = state.deviceId !in state.project.excludeNotifyDeviceId
          val featureEnabled = state.config.notificationsEnabled
          val enabledForProject = state.project.devices.size > 1

          notifyPref.isChecked = isChecked && featureEnabled && enabledForProject
          notifyPref.isEnabled = featureEnabled && enabledForProject

          notifyPref.setSummary(
            when {
              !featureEnabled -> R.string.settings_notify_on_expenses_summary_feature_disabled
              !enabledForProject -> R.string.settings_notify_on_expenses_summary_disabled_single_device
              !isChecked -> R.string.settings_notify_on_expenses_summary_off
              else -> R.string.settings_notify_on_expenses_summary_on
            }
          )
          notifyPref.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener handle@{ _, newValue: Any ->
              analytics.settingsItemClicked("notify_on_expenses", bundleOf("new_value" to newValue))

              if (newValue !is Boolean) return@handle false
              if (!enabledForProject) return@handle false
              val shouldAdd =
                !isChecked // click when checked means remove, and vice versa
              val description = if (shouldAdd) "add fcm" else "remove fcm"
              val excludeMap = state.project.excludeNotifyDeviceId.toMutableMap()
              if (!shouldAdd) excludeMap[state.deviceId] = true
              if (shouldAdd) excludeMap.remove(state.deviceId)
              appRedux.eventBus.send(
                UpdateProjectDetailsRunnable(description) {
                  copy(excludeNotifyDeviceId = excludeMap)
                })
              return@handle true
            }
        }
      }
    }

    findPreference<Preference>("backup")?.let { backupPref ->
      backupPref.setOnPreferenceClickListener {
        findNavController().navigate(R.id.action_settingsFragment_to_backupFragment)
        true
      }
    }

    findPreference<Preference>("generate_csv")?.setOnPreferenceClickListener {
      analytics.settingsItemClicked("generate_csv")
      SettingsGenerateCsvDialog.show(requireContext()) { csvRequest ->
        WorkManager.getInstance(requireContext())
          .getWorkInfoByIdLiveData(csvRequest.id)
          .observe(this, Observer { info ->
            if (info?.state != WorkInfo.State.SUCCEEDED) return@Observer
            val file = GenerateCsvWorker.file(info.outputData) ?: return@Observer
            val mimeType = "text/csv"
            val activity = requireActivity()
            val uri = FileProvider.getUriForFile(activity, activity.packageName, file)


            val intent = ShareCompat.IntentBuilder(activity)
              .setStream(uri)
              .setType(mimeType)
              .intent.apply {
                action = Intent.ACTION_SEND
                setDataAndType(uri, mimeType)
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
              }
            startActivity(intent)
          })
      }
      true
    }

    // todo: backup highlight for next release
    val inputHighlight = arguments?.getSerializable(INPUPT_HIGHLIGHT_ITEM) as? ItemHighlight
    Timber.d("SettingsFragment opened, highlight request: $inputHighlight")
    when (inputHighlight) {
      null -> Unit
      ItemHighlight.NIGHT_MODE -> {
        scrollToPreference("night_mode")
      }
    }
  }

  private fun showErrorDeletingDataPopup() {
    analytics.settingsEvent("delete_all.error_popup")
    AlertDialog.Builder(requireContext())
      .setTitle(R.string.settings_deleteAll_failed_title)
      .setMessage(
        getString(R.string.settings_deleteAll_failed_message)
          .replace(
            "EMAIL",
            getString(R.string.settings_developer_email)
          )
      )
      .setPositiveButton(R.string.settings_deleteAll_failed_cta_contactUs) { errorDialog, _ ->
        errorDialog.dismiss()
        lifecycleScope.launch {
          val uri = generateLogsZipFile()
          sendEmailToDevelopers(
            subject = getString(R.string.settings_deleteAll_failed_contactUs_emailMsg)
              .replace("[APP-NAME]", getString(R.string.app_name)),
            attachment = uri
          )
        }
      }
      .setNegativeButton(R.string.settings_deleteAll_failed_cta_cancel) { errorDialog, _ ->
        errorDialog.dismiss()
      }
      .setCancelable(false)
      .show()
  }

  private fun buildDeleteAllSummaryMsg(dataToDelete: DeleteAllUserDataHandler.UserDataToDelete): Spanned {
    val boldColor = ContextCompat.getColor(requireContext(), R.color.boldOnSurface)
      .toHexColorStringNoAlpha()
    val boldTextStart = "<b><span style=color:$boldColor>"
    val boldTextEnd = "</span></b>"
    return getString(R.string.settings_deleteAll_dialog_summary)
      .replace("[EXPENSES]", dataToDelete.expensesCount.toString())
      .replace(
        "[COLLABORATORS]",
        if (dataToDelete.hasCollaborators)
          getString(R.string.settings_deleteAll_dialog_collaborators)
        else ""
      )
      .replace("[SNAPSHOTS]", dataToDelete.snapshotsCount.toString())
      .replace("\n", "<br>")
      .replace("<b>", boldTextStart)
      .replace("</b>", boldTextEnd)
      .fromHtml()
  }

  private suspend fun generateLogsZipFile(): Uri? =
    GenerateLogZipFileWorker.generateZip().getOrNull()

  private fun sendEmailToDevelopers(subject: String, attachment: Uri?) {
    val devEmail = getString(R.string.settings_developer_email)

    if (attachment == null) {
      startActivity(
        Intent.createChooser(
          Intent(
            Intent.ACTION_SENDTO,
            Uri.fromParts("mailto", devEmail, null)
          ).apply {
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_EMAIL, arrayOf(devEmail))
          }, getString(R.string.settings_feedback_title)
        )
      )
      return
    }

    // more details on https://stackoverflow.com/a/42856167/3339597
    val emailSelectorIntent = Intent(Intent.ACTION_SENDTO)
    emailSelectorIntent.data = Uri.parse("mailto:")

    val emailIntent = Intent(Intent.ACTION_SEND)
    emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(devEmail))
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
    emailIntent.putExtra(Intent.EXTRA_STREAM, attachment)
    emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    emailIntent.selector = emailSelectorIntent

    startActivity(
      Intent.createChooser(
        emailIntent,
        getString(R.string.settings_feedback_title)
      )
    )
  }
}
