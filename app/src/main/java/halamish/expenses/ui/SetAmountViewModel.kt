package halamish.expenses.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import halamish.expenses.data.cleanedNumberDouble
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.ui.prototypes.ProtoViewModel
import java.text.DecimalFormatSymbols

class SetAmountViewModel(initalAmount: String = ""): ProtoViewModel() {
    class Factory(val initalAmount: String = ""): ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return SetAmountViewModel(initalAmount) as T
        }
    }

    val decimalSeperator: String =
        DecimalFormatSymbols(java.util.Locale.getDefault()).decimalSeparator.toString()

    private val amountStr = MutableLiveData<String>().apply { value = initalAmount }
    fun amount(): LiveData<String> = amountStr
    var curAmountStr: String
        get() = amountStr.value ?: ""
        set(value) { amountStr.value = value }

    fun clearAmount() { curAmountStr = "" }

    fun addDecimalSeparator() {
        if (curAmountStr.contains(decimalSeperator))
            return
        curAmountStr += decimalSeperator
    }

    fun addDigit(digit: String){
        curAmountStr += digit
    }

    val amountDouble: LiveData<Double>
        get() = amountStr.map { it.cleanedNumberDouble() ?: 0.0 }

    val hasAmount: LiveData<Boolean>
        get() = amountDouble.map { it > 0 }

    val intValue: LiveData<String>
        get() = amountDouble.map { it.toInt().toString() }

    val suffixValue: LiveData<String>
        get() = amountStr.map {
            if (!it.contains(decimalSeperator)) return@map "0"
            return@map it.split(decimalSeperator)[1].takeIf { it.isNotEmpty() } ?: "0"
        }

    val currency: String
        get() = CurrencyFormatter.get().currency.symbol
}

