package halamish.expenses.ui.invites

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.lifecycleScope
import extensions.android.fromHtml
import halamish.expenses.R
import halamish.expenses.ui.invites.fragments.ConfirmInvitationFragment
import halamish.expenses.ui.invites.fragments.GoogleLoginToAcceptInvitationFragment
import halamish.expenses.ui.invites.fragments.SimpleTextFragment
import halamish.expenses.ui.main.MainActivity
import halamish.expenses.ui.popup.PopupsCreator
import halamish.expenses.ui.prototypes.ProtoActivity
import halamish.expenses.v2.appRedux
import kotlinx.android.synthetic.main.activity_project_invitation.progressBar
import kotlinx.coroutines.delay

class ProjectInvitationActivity : ProtoActivity() {

    override val screenName = "ProjectInvitationActivity"

    val viewModel by viewModels<ProjectInvitationViewModel> { ProjectInvitationViewModel.Factory(intent?.data) }

    private val fragmentContainer by lazy { findViewById<FragmentContainerView>(R.id.fragmentContainerView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project_invitation)

        lifecycleScope.launchWhenCreated {
            viewModel.stateFlow.collect { state ->
                progressBar.isVisible = state.runningWork
                when (state) {
                    ProjectInvitationViewModel.State.Declined -> finish()
                    ProjectInvitationViewModel.State.NotSupported -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_not_supported))
                    }
                    ProjectInvitationViewModel.State.MalformedUrl -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_alformed_url_ps, viewModel.referrerName))
                    }
                    ProjectInvitationViewModel.State.Initiating -> Unit
                    ProjectInvitationViewModel.State.AwaitingUserConfirmation -> swapFragment(ConfirmInvitationFragment())
                    is ProjectInvitationViewModel.State.AwaitingDeleteLocalItems -> showConfirmDeleteItemsDialog(state.itemsToDelete)
                    ProjectInvitationViewModel.State.AwaitingGoogleLogin -> swapFragment(GoogleLoginToAcceptInvitationFragment())
                    ProjectInvitationViewModel.State.RunningFirebaseLogin -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_firebase_login))
                        delay(400)
                    }
                    ProjectInvitationViewModel.State.RunningBackup -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_backing_up))
                        delay(400)
                    }
                    ProjectInvitationViewModel.State.ReadyToStartInvitationFlow -> swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_downloading))
                    ProjectInvitationViewModel.State.RunningInvitationFlow -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_downloading))
                        delay(400)
                    }
                    ProjectInvitationViewModel.State.Done -> {
                        swapFragment(fragmentWithText(R.string.flow_accept_invitation_screen_success))
                        delay(2000)
                        startActivity(Intent(this@ProjectInvitationActivity, MainActivity::class.java))
                        finish()
                    }
                }
            }
        }
    }

    private fun fragmentWithText(textId: Int) = SimpleTextFragment.newInstance(getString(textId))
    private fun fragmentWithText(textId: Int, vararg others: Any) = SimpleTextFragment.newInstance(getString(textId, *others))

    private fun swapFragment(newFragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(fragmentContainer.id, newFragment)
            .commitAllowingStateLoss()
    }

    private fun showConfirmDeleteItemsDialog(itemsToDelete: Int) {
        appRedux.handlers.popups.showDestructivePopup(
            "delete_expenses_invited_to_collaborate",
            this, lifecycle,
            cancelPolicy = PopupsCreator.CancellationPolicy.CANT_CANCEL,
            messageText = getString(R.string.flow_accept_invitation_popup_delete_msg_pd_html, itemsToDelete).fromHtml(),
            destructiveButtonText = getString(R.string.flow_accept_invitation_popup_delete_btn_confirm_delete),
            secondaryButtonText = getString(R.string.flow_accept_invitation_popup_delete_btn_confirm_decline),
            onDestructiveButtonClick = { viewModel.confirmDeleteLocalItems() },
            onSecondaryButtonClick = { viewModel.declineInvitation() },
        )
    }
}
