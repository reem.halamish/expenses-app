package halamish.expenses.ui.invites.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import halamish.expenses.R
import halamish.expenses.ui.invites.ProjectInvitationViewModel
import kotlinx.android.synthetic.main.fragment_confirm_invitation.view.*

class ConfirmInvitationFragment : Fragment(R.layout.fragment_confirm_invitation) {
    private val viewModel by activityViewModels<ProjectInvitationViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.view_subtitle.text =
            requireContext().getText(R.string.flow_accept_invitation_screen_confirm_subtitle_p_referrer)
                .toString()
            .replace("[REFERRER]", viewModel.referrerName)

        view.btn_accept.setOnClickListener { viewModel.confirmAcceptInvitation() }
        view.btn_decline.setOnClickListener { viewModel.declineInvitation() }
    }
}