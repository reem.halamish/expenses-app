package halamish.expenses.ui.invites

import android.content.Intent
import android.net.Uri
import androidx.annotation.Keep
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.data.ProjectInvitationLink
import halamish.expenses.data.URI_ARG_PROJECT_ID
import halamish.expenses.data.URI_ARG_REFERRER_NAME
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler
import halamish.expenses.v2.*
import halamish.expenses.v2.V2VerificationState.VERIFIED_UPLOADING_DATA
import halamish.expenses.v2.V2VerificationState.VERIFYING
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import timber.log.Timber

class ProjectInvitationViewModel(
    val referrerName: String,
    private val projectIdInvitation: String,
    private val appStateFlow: StateFlow<V2State> = appRedux.state,
    private val eventBus: EventBus = appRedux.eventBus,
    private val invitationsHandler: ProjectUsersInvitationsHandler = appRedux.services.projectUsersInvitationsHandler,
    private val backup: BackupAndRestoreService = appRedux.services.backupAndRestore,
): ViewModel() {

    sealed class State(val runningWork: Boolean) {
        @Keep object Initiating: State(runningWork = true)
        @Keep object Declined: State(runningWork = false)
        @Keep object NotSupported : State(runningWork = false)
        @Keep object MalformedUrl: State(runningWork = false)
        @Keep object AwaitingUserConfirmation: State(runningWork = false)
        @Keep data class AwaitingDeleteLocalItems(val itemsToDelete: Int): State(runningWork = false)
        @Keep object AwaitingGoogleLogin: State(runningWork = false)
        @Keep object RunningFirebaseLogin: State(runningWork = true)
        @Keep object RunningBackup: State(runningWork = true)
        @Keep object ReadyToStartInvitationFlow: State(runningWork = true)
        @Keep object RunningInvitationFlow: State(runningWork = true)
        @Keep object Done: State(runningWork = false)
    }

    private val _stateFlow = MutableStateFlow<State>(State.Initiating)
    val stateFlow: StateFlow<State> get() = _stateFlow

    private data class InnerVariables(
        val didRequestUserConfirmation: Boolean = false,
        val didDecline: Boolean = false,
        val didTriggerProjectChange: Boolean = false,
        val didRequestAnonymousLogin: Boolean = false,
        val didConfirmDeleteLocalItems: Boolean = false,
    )
    private val _variables = MutableStateFlow(InnerVariables())

    private fun calculateState(appState: V2State, variables: InnerVariables): State {
        return when {
            // terminal states
            !appState.config.projectInvitations.enabled -> State.NotSupported
            (projectIdInvitation.isEmpty() || referrerName.isEmpty()) -> State.MalformedUrl
            variables.didDecline -> State.Declined
            appState.project.id == projectIdInvitation -> State.Done

            // progress states
            !variables.didRequestUserConfirmation -> State.AwaitingUserConfirmation
            (appState.data.oneTime.isNotEmpty() || appState.data.rules.isNotEmpty()) && !variables.didConfirmDeleteLocalItems ->
                State.AwaitingDeleteLocalItems(appState.data.oneTime.size + appState.data.rules.size)
            variables.didRequestAnonymousLogin && appState.userInfo == null -> State.RunningFirebaseLogin
            appState.userInfo == null -> State.AwaitingGoogleLogin
            appState.auth.loading -> State.RunningFirebaseLogin
            appState.verificationState == VERIFYING -> State.RunningFirebaseLogin
            appState.verificationState == VERIFIED_UPLOADING_DATA -> State.RunningBackup
            !variables.didTriggerProjectChange -> State.ReadyToStartInvitationFlow
            appState.project.id != projectIdInvitation -> State.RunningInvitationFlow
            else -> State.Done
        }
    }

    init {
        viewModelScope.launch {
            appStateFlow.combine(_variables) { appState, variables ->
                calculateState(appState, variables)
            }
                .distinctUntilChanged()
                .collect { vmState ->
                    Timber.d("settings state: $vmState")
                    _stateFlow.value = vmState
                    if (vmState == State.ReadyToStartInvitationFlow) {
                        Timber.d("triggering invitation! user's project will change to $projectIdInvitation")
                        invitationsHandler.acceptInvitation(projectIdInvitation)
                        _variables.value = _variables.value.copy(didTriggerProjectChange = true)
                    }
                }
        }
    }

    suspend fun requestGoogleLogin(): Intent {
        return eventBus.sendAwaitingNext(RequestGoogleSignInIntent).intent
    }

    fun onGoogleSignInIntent(resultCode: Int, data: Intent?) {
        eventBus.send(GoogleSignInActivityResultEvent(resultCode, data))
    }

    suspend fun requestAnonymousLogin() {
        _variables.value = _variables.value.copy(didRequestAnonymousLogin = true)
        eventBus.sendAwaitingResponse(RequestAnonymousLogin)
    }

    fun confirmAcceptInvitation() {
        viewModelScope.launch {
            backup.recordSnapshot(BackupAndRestoreService.Reason.BEFORE_ACCEPTING_INVITATION)
            _variables.value = _variables.value.copy(didRequestUserConfirmation = true)
        }
    }

    fun confirmDeleteLocalItems() {
        _variables.value = _variables.value.copy(didConfirmDeleteLocalItems = true)
    }

    fun declineInvitation(){
        _variables.value = _variables.value.copy(didDecline = true)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(val data: Uri?): ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass == ProjectInvitationViewModel::class.java) {
                val projectId =
                    data?.getQueryParameter(ProjectInvitationLink.URI_ARG_PROJECT_ID) ?: ""
                val referrerName =
                    data?.getQueryParameter(ProjectInvitationLink.URI_ARG_REFERRER_NAME) ?: ""
                return ProjectInvitationViewModel(
                    projectIdInvitation = projectId,
                    referrerName = referrerName
                ) as T
            }
            throw IllegalArgumentException("can't create VM of type $modelClass")
        }
    }
}
