package halamish.expenses.ui.invites.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import halamish.expenses.R
import kotlinx.android.synthetic.main.fragment_with_text.view.*

class SimpleTextFragment: Fragment(R.layout.fragment_with_text) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val text = arguments?.getString("text") ?: throw IllegalArgumentException("come through newInstance() please")
        view.text.text = text
    }

    companion object {
        fun newInstance(text: String) = SimpleTextFragment().apply { arguments = Bundle().apply { putString("text", text) } }
    }
}