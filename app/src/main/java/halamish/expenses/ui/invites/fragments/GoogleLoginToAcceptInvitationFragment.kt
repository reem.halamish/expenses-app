package halamish.expenses.ui.invites.fragments

import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import halamish.expenses.BuildConfig
import halamish.expenses.R
import halamish.expenses.ui.invites.ProjectInvitationViewModel
import kotlinx.android.synthetic.main.fragment_login_to_accept_invitation.*
import kotlinx.coroutines.launch

class GoogleLoginToAcceptInvitationFragment: Fragment(R.layout.fragment_login_to_accept_invitation) {
    val viewModel by activityViewModels<ProjectInvitationViewModel>()

    private val googleSignInLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult(), ActivityResultCallback { result ->
        viewModel.onGoogleSignInIntent(result.resultCode, result.data)
    })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scope = viewLifecycleOwner.lifecycleScope
        textHeadline.text = requireContext().getString(R.string.flow_accept_invitation_screen_login_rational_we_use_login_to_identify_ps, viewModel.referrerName)
        btn_google_connect.setOnClickListener {
            scope.launch {
                googleSignInLauncher.launch(viewModel.requestGoogleLogin())
            }
        }
        tvAnonymousLogin.isVisible = BuildConfig.DEBUG
        tvAnonymousLogin.setOnClickListener {
            scope.launch {
                viewModel.requestAnonymousLogin()
            }
        }
    }
}
