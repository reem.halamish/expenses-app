@file:Suppress("EXPERIMENTAL_API_USAGE")

package halamish.expenses.ui

import halamish.expenses.data.ExpenseCategory
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class ChooseCategoryViewModel(
    val categories: Flow<List<ExpenseCategory>> = appRedux.state
        .map { it.data.categories.values }
        .distinctUntilChanged()
        .map { list -> list.sortedBy { it.displayName } }
) : ProtoViewModel()