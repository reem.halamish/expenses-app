package halamish.expenses.ui.theme

import androidx.compose.runtime.Composable
import com.google.android.material.composethemeadapter.MdcTheme

@Composable fun AppTheme(content: @Composable () -> Unit) {
  MdcTheme(content=content)
}