package halamish.expenses.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import halamish.expenses.R
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.infra.ShortId
import halamish.expenses.repo.icons.icon8Url
import halamish.expenses.ui.prototypes.ProtoViewHolder
import kotlinx.android.synthetic.main.fragment_choose_category.view.*
import kotlinx.android.synthetic.main.item_select_category.view.*
import kotlinx.coroutines.launch


abstract class ChooseCategoryFragment : Fragment(R.layout.fragment_choose_category) {

    abstract val onCategoryChosen: (ExpenseCategory) -> Unit

    private val chooseCategoryViewModel: ChooseCategoryViewModel by viewModels()
    private val adapter = Adapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.btn_create.setOnClickListener {
            val requestId = ShortId.create()
            ManipulateCategoryFragment.eventsBus().observeUntilDestroyed(lifecycle) {
                if (it.requestId == requestId) {
                    onCategoryChosen(it.category)
                }
            }
            findNavController().navigate(
                R.id.action_chooseCategory_to_newCategoryFragment,
                Bundle().apply { putString("request_id", requestId) })
        }

        view.recycler.adapter = adapter
        view.recycler.layoutManager = FlexboxLayoutManager(view.context).apply {
            justifyContent = JustifyContent.SPACE_BETWEEN
        }

        viewLifecycleOwner.lifecycleScope.launch {
            chooseCategoryViewModel.categories.collect { adapter.submitList(it) }
        }
    }

    object DiffCallback: DiffUtil.ItemCallback<ExpenseCategory>() {
        override fun areItemsTheSame(oldItem: ExpenseCategory, newItem: ExpenseCategory): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ExpenseCategory, newItem: ExpenseCategory): Boolean {
            return oldItem == newItem
        }
    }

    inner class Adapter: ListAdapter<ExpenseCategory, ProtoViewHolder>(DiffCallback) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProtoViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_select_category, parent, false)
            val holder = ProtoViewHolder(view)

            holder.itemView.card_area.setOnClickListener {
                holder.itemView.card_area.setCardBackgroundColor(ContextCompat.getColor(view.context, R.color.colorPrimary))
                view.circle.setColorFilter(ContextCompat.getColor(view.context, R.color.onPrimary))
                view.text.setTextColor(ContextCompat.getColor(view.context, R.color.onPrimary))
                view.icon.setColorFilter(ContextCompat.getColor(view.context, R.color.colorPrimary))
                onCategoryChosen(getItem(holder.adapterPosition))
            }

            return holder
        }

        override fun onBindViewHolder(holder: ProtoViewHolder, position: Int) {
            val category = getItem(position)
            val view = holder.itemView
            view.text.text = category.displayName
            view.circle.setColorFilter(ContextCompat.getColor(view.context, category.resColor()))
            Glide.with(holder.context).load(icon8Url(category.icon)).into(view.icon)
        }
    }
}
