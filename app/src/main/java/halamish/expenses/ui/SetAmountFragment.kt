package halamish.expenses.ui

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import androidx.core.text.scale
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import halamish.expenses.R
import halamish.expenses.data.cleanedNumberWithLocale
import kotlinx.android.synthetic.main.fragment_set_amount.view.*
import kotlinx.android.synthetic.main.fragment_set_amount_inc_buttons.view.*


abstract class SetAmountFragment: Fragment(R.layout.fragment_set_amount) {

    private val viewModel: SetAmountViewModel by viewModels(factoryProducer = { SetAmountViewModel.Factory(initialAmount) })

    open val bottomButtonText: String get() = getString(R.string.continue_btn)
  abstract val bottomButtonListener: () -> Unit

    open val initialAmount: String = ""

    protected var currentAmount: String
        get() = viewModel.curAmountStr.cleanedNumberWithLocale()
        set(value) {
            viewModel.curAmountStr = value.cleanedNumberWithLocale()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listOf(
          view.btn_0,
          view.btn_1,
          view.btn_2,
          view.btn_3,
          view.btn_4,
          view.btn_5,
          view.btn_6,
          view.btn_7,
          view.btn_8,
          view.btn_9
        )
          .withIndex().forEach {
            val digit = it.index.toString()
            val btn = it.value

            btn.setText(digit)
            btn.setOnClickListener { viewModel.addDigit(digit) }
          }


      view.btn_dot.setText(viewModel.decimalSeperator)
      view.btn_dot.setOnClickListener { viewModel.addDecimalSeparator() }

      view.btn_clear.setText("C")
      view.btn_clear.setOnClickListener { viewModel.clearAmount() }


      view.bottom_button_save.text = bottomButtonText
      view.bottom_button_save.setOnClickListener { bottomButtonListener() }

      viewModel.hasAmount.observe(viewLifecycleOwner, { hasAmount ->
        view.bottom_button_save.isEnabled = hasAmount
      })

      viewModel.amount().map { it.cleanedNumberWithLocale() }
        .observe(viewLifecycleOwner, { amount ->
          val currency = viewModel.currency
          val dot = viewModel.decimalSeperator
          if (dot in amount) {
            // has suffix
            val intAmount = amount.split(dot).first()
            val suffixAmount = amount.split(dot).last()
            val span = SpannableStringBuilder()
              .append(intAmount)
              .append(dot)
              .scale(0.3f) { append(suffixAmount) }
              .scale(0.7f) { append(" ") }
              .append(currency)
            view.tv.text = span
          } else {
            val span = SpannableStringBuilder()
              .append(amount)
              .scale(0.7f) { append(" ") }
              .append(currency)
            view.tv.text = span
          }
        })
    }
}