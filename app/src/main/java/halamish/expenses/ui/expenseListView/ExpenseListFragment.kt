package halamish.expenses.ui.expenseListView

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.BOTTOM
import androidx.constraintlayout.widget.ConstraintSet.TOP
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import halamish.expenses.R
import halamish.expenses.infra.EventsBus
import halamish.expenses.ui.ManipulateCategoryFragment
import halamish.expenses.ui.dpToPx
import kotlinx.android.synthetic.main.fragment_expense_list.view.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import timber.log.Timber

open class ExpenseListFragment : Fragment(R.layout.fragment_expense_list) {

    private val eventsBus: EventsBus<Events> by lazy { EventsBus.from(javaClass) }

    private val viewModel by viewModels<ExpenseListViewModel>(factoryProducer = {
        ExpenseListViewModel.Factory(
            eventsBus
        )
    })

    private val adapter: ExpenseListAdapter by lazy {
        ExpenseListAdapter(this, eventsBus, viewModel.dateFormatter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventsBus.observeWhileHasScreen(lifecycle) { event ->
            if (event is ButtonRippleEffectEvent) {
                delay(100)
            }
            when (event) {
                is Events.EditEventRequest -> {
                    findNavController().navigate(R.id.action_expenseListFragment_to_createEditExpenseActivity, Bundle().apply {
                        putString("expense_id", event.eventData.event.dbId)
                    })
                }

                is Events.CreateNewExpenseWithCategory -> {
                    findNavController().navigate(R.id.action_expenseListFragment_to_createEditExpenseActivity, Bundle().apply {
                        putString("category_id", event.data.category.id)
                    })
                }

                is Events.EditCategoryRequest -> {
                    findNavController().navigate(R.id.action_expenseListFragment_to_createOrEditCategory,
                        ManipulateCategoryFragment.argsForEdit(event.data.category.id)
                    )
                }

                is Events.CreateNewCategory -> {
                    findNavController().navigate(R.id.action_expenseListFragment_to_createOrEditCategory)
                }

                is Events.DeleteCategoryRequest -> {
                    if (!event.confirmed) {
                        dialogConfirmDeleteCategory(event.data)
                    }
                }

                is Events.UndoDeleteCategoryRequest -> {
                    adapter.onUndoDeleteCategory(event.data)
                }
                is Events.ChangeCategoryExpandedRequest,
                is Events.DeleteEventRequest,
                Events.FlushDeleteEvents,
                Events.NextPeriod,
                Events.PrevPeriod,
                is Events.SortChangeRequest,
                is Events.StopFutureRecurringExpenseEventRequest,
                is Events.UndoDeleteEventRequest -> {
                    Timber.d("Fragment not handling event $event")
                }
            }
        }
    }

    private fun dialogConfirmDeleteCategory(data: CategoryData) {
        val context = context ?: return

        fun onDialogTappedDelete(dialog: DialogInterface){
            eventsBus.send(Events.DeleteCategoryRequest(data, confirmed = true))
            dialog.dismiss()
        }

        fun onDialogCancel(){
            eventsBus.send(Events.UndoDeleteCategoryRequest(data))
        }

        val dialog = AlertDialog.Builder(context)
                .setTitle("Confirm Delete")
                .setMessage(
                        "You are about to delete category '${data.category.displayName}' and all its related expenses.\n" +
                                "This action cannot be undone.\n" +
                                "\n" +
                                "Continue?")

                .setPositiveButton("Delete") { dialog, _ -> onDialogTappedDelete(dialog) }
                .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
                .setCancelable(true)
                .setOnCancelListener { onDialogCancel() }
                .create()

        lifecycle.addObserver(object : LifecycleObserver{
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun cancelDialog() = dialog.cancel()
        })

        dialog.show()
    }

    override fun onViewCreated(createdView: View, savedInstanceState: Bundle?) {
        val listenerFlush = NavController.OnDestinationChangedListener {_, destination: NavDestination, _ ->
            if (destination.id != R.id.expenseMonthlyFragment) {
                eventsBus.send(Events.FlushDeleteEvents)
            }
        }
        findNavController().addOnDestinationChangedListener(listenerFlush)
        viewLifecycleOwner.lifecycle.addObserver(object: LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun removeObserverOnDestroy() =  findNavController().removeOnDestinationChangedListener(listenerFlush)
        })

        viewLifecycleOwner.lifecycle.addObserver(object: LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun flushDeleteEventsOnStop() = eventsBus.send(Events.FlushDeleteEvents)
        })



        val recycler = createdView.recycler
        val nextDateButton = createdView.btn_next_date
        val prevDateButton = createdView.btn_prev_date

        recycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy < (-2).dpToPx()) {
                    setBottomArrowsVisibility(shouldBeVisible = false)
                }
                if (dy > 2.dpToPx() || !recycler.canScrollVertically(1)) {
                    setBottomArrowsVisibility(shouldBeVisible = true)
                }
            }
        })


        viewLifecycleOwner.lifecycleScope.launch {
            val lstF = viewModel.megaList
            val currencyF = viewModel.currencyCode
            val sortF = viewModel.sort
            val periodF = viewModel.period

            val sortProtoText = getString(R.string.sorting_btn_proto)

            withContext(Dispatchers.Default) {
                lstF.combine(currencyF) { a, b -> a to b }
                    .combine(sortF) { pair, c -> pair to c }
                    .combine(periodF) { ppair, d -> ppair to d }
                    .distinctUntilChanged()
                    .collect { pppair ->
                        val list = pppair.first.first.first
                        val sort = pppair.first.second
                        val period = pppair.second

                        withContext(Dispatchers.Main) {
                            view?.tv_period_display?.text = period.display

                            view?.tv_sort?.text = when (sort) {
                                Sort.DATE -> sortProtoText.replace("[[TYPE]]", getString(R.string.sorting_type_date))
                                Sort.AMOUNT -> sortProtoText.replace("[[TYPE]]", getString(R.string.sorting_type_amount))
                                Sort.ALPHABET -> sortProtoText.replace("[[TYPE]]", getString(R.string.sorting_type_alphabet))
                                Sort.CONVENIENT -> sortProtoText.replace("[[TYPE]]", getString(R.string.sorting_type_convenience))
                            }

                            adapter.submitVmList(list, period.display, viewModel.addCategoryButton.value) {
                                launch { calculateBottomArrowsVisibility() }
                            }
                        }
                    }
            }
        }


        createdView.btn_sort.setOnClickListener {
            val currentSort = viewModel.sort.value
            val nextSort = Sort.values()[(Sort.values().indexOf(currentSort) + 1) % Sort.values().size]
            eventsBus.send(Events.SortChangeRequest(nextSort))
        }

        nextDateButton.setOnClickListener {
            eventsBus.send(Events.NextPeriod)
        }

        prevDateButton.setOnClickListener {
            eventsBus.send(Events.PrevPeriod)
        }

        createdView.recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        createdView.recycler.adapter = adapter
    }


    private fun calculateBottomArrowsVisibility(){
        view?.post {
        val recycler = view?.recycler ?: return@post
        val layoutManager = recycler.layoutManager as? LinearLayoutManager ?: return@post
        val adapter = recycler.adapter ?: return@post

        val isShowingEnd = layoutManager.findLastVisibleItemPosition() == adapter.itemCount -1
        setBottomArrowsVisibility(isShowingEnd)
        }
    }

    private fun setBottomArrowsVisibility(shouldBeVisible: Boolean){
        val nextDateButton = view?.btn_next_date ?: return
        val prevDateButton = view?.btn_prev_date ?: return
        val rootCL = view?.cl_root ?: return

        TransitionManager.beginDelayedTransition(rootCL)
        val set = ConstraintSet().apply { clone(rootCL) }

        if (shouldBeVisible) {
            for (button in listOf(prevDateButton, nextDateButton)) {
                set.clear(button.id, TOP)
                set.clear(button.id, BOTTOM)
                set.connect(button.id, TOP, R.id.space_parent_bottom_and_up, TOP)
                set.connect(button.id, BOTTOM, R.id.space_parent_bottom_and_up, TOP)
            }
        } else {
            for (button in listOf(prevDateButton, nextDateButton)) {
                set.clear(button.id, TOP)
                set.clear(button.id, BOTTOM)
                set.connect(button.id, TOP, R.id.space_parent_bottom_and_down, BOTTOM)
                set.connect(button.id, BOTTOM, R.id.space_parent_bottom_and_down, BOTTOM)
            }
        }
        set.applyTo(rootCL)
    }
}

// todo https://material.io/components/backdrop/#behavior for the sorting and choosing dates and all