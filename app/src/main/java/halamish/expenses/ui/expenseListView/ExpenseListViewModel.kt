package halamish.expenses.ui.expenseListView

import android.annotation.SuppressLint
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import halamish.expenses.BuildConfig
import halamish.expenses.data.*
import halamish.expenses.infra.EventsBus
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.ui.sp
import halamish.expenses.v2.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
internal class ExpenseListViewModel(
    eventsBus: EventsBus<Events>,
    private val appState: StateFlow<V2State> = appRedux.state,
    internal val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(
        appRedux.state.value.project.dateFormatPattern ?: Defaults.localDateFormatPattern,
        Locale.getDefault()
    ),
    private val sp: SharedPreferences = appRedux.app.sp

) : ProtoViewModel() {

    class Factory(private val eventsBus: EventsBus<Events>) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return ExpenseListViewModel(eventsBus) as T
        }
    }

    private companion object {
        const val SORT_KEY = "ExpenseListViewModel::sort"
    }

    val addCategoryButton = MutableStateFlow(true)
    init {
        viewModelScope.launch {
            appState.map { it.localStorage.showingAddCategoryButton }
                .distinctUntilChanged()
                .collect { show ->
                addCategoryButton.value = show
            }
        }
    }


    private val eventsMarkedToDelete =
        mutableMapOf<String, String>() // event.id to (actual-data).pk
    private val expandedCategories = mutableSetOf<String>()

    val period = MutableStateFlow<Period>(Period.Month(LocalDate.now()))
    val sort = MutableStateFlow(
        sp.getString(SORT_KEY, Sort.CONVENIENT.name).let { Sort.valueOf(it ?: Sort.ALPHABET.name) })
    val megaList = MutableStateFlow(emptyList<CategoryData>())

    val currencyCode = appState.map { it.project.currencyCode ?: Defaults.currencyCode }


    private val eventsBusObserve = eventsBus.observe handleEvent@{
        when (it) {
            is Events.ChangeCategoryExpandedRequest -> {
                if (it.newExpand) expandedCategories += it.category.category.id
                if (!it.newExpand) expandedCategories -= it.category.category.id
                buildList()
            }

            is Events.NextPeriod -> {
                period.value = period.value.next
                expandedCategories.clear()
                flushDeleteEvents()
                buildList()
            }

            is Events.PrevPeriod -> {
                period.value = period.value.prev
                expandedCategories.clear()
                flushDeleteEvents()
                buildList()
            }

            is Events.FlushDeleteEvents -> {
                flushDeleteEvents()
            }

            is Events.DeleteEventRequest -> {
                val eventId = it.eventData.event.id
                val categoryId = it.eventData.event.categoryId
                val isRecurring = it.eventData.event.recurring != null
                val dbPk = it.eventData.event.dbId
                eventsMarkedToDelete += eventId to dbPk
                refreshListFromDeleteAndUndoEvent(eventId, categoryId)

                viewModelScope.launch {
                    delay(10_000)
                    actualDeleteEvent(eventId, isRecurring, dbPk)
                    buildList()
                }
            }

            is Events.StopFutureRecurringExpenseEventRequest -> {
                val recurring = it.eventData.event.recurring != null
                val date = it.eventData.event.date
                val dbPk = it.eventData.event.dbId
                if (!recurring) return@handleEvent

                val rule = appState.value.data.rules[dbPk] ?: return@handleEvent
                val newRule =
                    if (it.alsoDeleteCurrent)
                        rule.withClampToBeforeEndDate(date)
                    else rule.withClampToEndDate(date)

                appRedux.eventBus.send(UpdateExpenseRule(newRule))
            }

            is Events.UndoDeleteEventRequest -> {
                val eventId = it.eventData.event.id
                val categoryId = it.eventData.event.categoryId
                eventsMarkedToDelete -= eventId
                refreshListFromDeleteAndUndoEvent(eventId, categoryId)
            }

            is Events.SortChangeRequest -> {
                sort.value = it.newSort
                sp.edit().putString(SORT_KEY, it.newSort.name).apply()
                buildList()
            }

            is Events.DeleteCategoryRequest -> {
                if (it.confirmed) {
                    deleteCategory(it.data)
                }
            }
            Events.CreateNewCategory,
            is Events.CreateNewExpenseWithCategory,
            is Events.EditCategoryRequest,
            is Events.EditEventRequest,
            is Events.UndoDeleteCategoryRequest -> {
                Timber.d("viewModel not handling event $it")
            }
        }
    }

    init {
        viewModelScope.launch {
            buildList(appState.value) // first time
            appState.collect { buildList(it) }
        }
    }

    private fun refreshListFromDeleteAndUndoEvent(eventId: String, categoryId: String) {
        viewModelScope.launch(Dispatchers.Default) {
            megaList.value = megaList.value.map { categoryData ->
                if (categoryData.category.id != categoryId)
                    categoryData
                else
                    categoryData.copy(
                        isExpanded = true,
                        totalAmount = categoryData.events.filter { it.event.id !in eventsMarkedToDelete }
                            .map { it.event.amount }.sum(),
                        events = categoryData.events.map { eventData ->
                            if (eventData.event.id != eventId)
                                eventData
                            else
                                eventData.copy(isAwaitingDelete = eventId in eventsMarkedToDelete)
                        }
                    )
            }
        }
    }

    private fun actualDeleteEvent(eventId: String, recurring: Boolean, dbPk: String) {
        if (eventId !in eventsMarkedToDelete) return // "undo" meanwhile
        appRedux.eventBus.send(
            if (recurring) RequestDeleteSingleExpenseRule(dbPk)
            else RequestDeleteSingleOneTimeExpense(dbPk)
        )
    }

    @SuppressLint("DefaultLocale")
    private fun buildList(state: V2State = appState.value) {
        viewModelScope.launch(Dispatchers.Default) {
            val categories = state.data.categories.map {
                it.key to CategoryData(
                    it.value,
                    0.0,
                    it.key in expandedCategories,
                    emptyList()
                )
            }.toMap().toMutableMap()

            val allEvents = granulateExpenses(
                startDate = period.value.dateStart,
                endDate = period.value.dateEnd
            )
                .toMutableList()

            when (sort.value) {
                Sort.DATE -> allEvents.sortWith(compareBy<ExpenseEvent> { it.date }.thenByDescending { it.amount }
                    .thenBy { it.comments?.toLowerCase() })
                Sort.AMOUNT -> allEvents.sortWith(compareByDescending<ExpenseEvent> { it.amount }.thenBy { it.date }
                    .thenBy { it.comments?.toLowerCase() })
                Sort.ALPHABET -> allEvents.sortWith(compareBy<ExpenseEvent> {
                    it.comments?.toLowerCase() ?: ""
                }.thenBy { it.date }.thenByDescending { it.amount })
                Sort.CONVENIENT -> allEvents.sortWith(compareBy<ExpenseEvent> { it.date }.thenByDescending { it.amount })
            }

            allEvents.forEach { event ->
                val oldCategoryData = categories[event.categoryId] ?: return@forEach
                if (categories.containsKey(event.categoryId)) {
                    val eventData = ExpenseEventData(event, event.id in eventsMarkedToDelete)
                    val amount: Double =
                        event.amount.takeUnless { eventData.isAwaitingDelete } ?: 0.0
                    categories[event.categoryId] = oldCategoryData.let {
                        it.copy(
                            totalAmount = it.totalAmount + amount,
                            events = it.events + eventData
                        )
                    }
                }
            }

            val categoriesList = categories.values.toList()
                .map { it.copy(isExpanded = it.isExpanded && it.events.isNotEmpty()) } // close previousely expanded categories if became empty
                .toMutableList()

            if (state.project.hideEmptyCategoriesInMajorExpensesList) {
                categoriesList.removeAll { it.events.isEmpty() }
            }

            fun CategoryData.name() = category.displayName.lowercase(Locale.getDefault())
            fun CategoryData.date() = events.firstOrNull()?.event?.date ?: LocalDate.MIN


            when (sort.value) {
                Sort.ALPHABET -> categoriesList.sortBy { it.name() }
                Sort.DATE -> categoriesList.sortWith(compareBy<CategoryData> { it.date() }.thenBy { it.name() })
                Sort.AMOUNT -> categoriesList.sortWith(compareByDescending<CategoryData> { it.totalAmount }.thenBy { it.name() })
                Sort.CONVENIENT -> categoriesList.sortWith(compareByDescending<CategoryData> { it.totalAmount }.thenBy { it.name() })
            }

            if (BuildConfig.DEBUG) println("ExpenseListViewModel: built list with ${categoriesList.size} values")


            Timber.d("categories: ${categoriesList.map { it.category.displayName to it.totalAmount }}")

            megaList.value = categoriesList
        }
    }

    private fun deleteCategory(data: CategoryData) {
        appRedux.eventBus.send(RequestDeleteSingleCategory(data.category.id))
    }


    private fun flushDeleteEvents() {
        eventsMarkedToDelete.values.forEach { id ->
            if (id in appState.value.data.rules)
                appRedux.eventBus.send(RequestDeleteSingleExpenseRule(id))
            if (id in appState.value.data.oneTime)
                appRedux.eventBus.send(RequestDeleteSingleOneTimeExpense(id))
        }
        eventsMarkedToDelete.clear()
    }

    override fun onCleared() {
        super.onCleared()

        eventsBusObserve.cancelObservation()
        flushDeleteEvents()
    }
}
