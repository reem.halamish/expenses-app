package halamish.expenses.ui.expenseListView

import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.ExpenseEvent
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.Serializable
import java.util.*

//class ExpenseListScreenClasses {

internal data class ExpenseEventData(val event: ExpenseEvent,
                                val isAwaitingDelete: Boolean)

internal data class CategoryData(val category: ExpenseCategory,
                            val totalAmount: Double,
                            val isExpanded: Boolean,
                            val events: List<ExpenseEventData>)

internal enum class Sort { DATE, AMOUNT, ALPHABET, CONVENIENT }

sealed class Period {
    abstract val dateStart: LocalDate
    abstract val dateEnd: LocalDate
    abstract val display: String
    abstract val next: Period
    abstract val prev: Period

    data class Month(val date: LocalDate): Period() {
        override val dateStart: LocalDate
            get() = date.withDayOfMonth(1)
        override val dateEnd: LocalDate
            get() = dateStart.plusMonths(1).minusDays(1)
        override val display: String
            get() = DateTimeFormatter.ofPattern("MMMM yyyy", Locale.getDefault()).format(date)
        override val next: Period
            get() = Month(dateStart.plusMonths(1))
        override val prev: Period
            get() = Month(dateStart.minusMonths(1))
    }
}

interface ButtonRippleEffectEvent

internal sealed class Events: Serializable {
    data class ChangeCategoryExpandedRequest(val category: CategoryData, val newExpand: Boolean): Events()
    data class EditEventRequest(val eventData: ExpenseEventData): Events()
    data class StopFutureRecurringExpenseEventRequest(
        val eventData: ExpenseEventData,
        val alsoDeleteCurrent: Boolean
    ) : Events()
    data class EditCategoryRequest(val data: CategoryData): Events()
    data class CreateNewExpenseWithCategory(val data: CategoryData): Events(), ButtonRippleEffectEvent
    data class DeleteEventRequest(val eventData: ExpenseEventData): Events()
    data class UndoDeleteEventRequest(val eventData: ExpenseEventData): Events()
    data class DeleteCategoryRequest(val data: CategoryData, val confirmed: Boolean): Events()
    data class UndoDeleteCategoryRequest(val data: CategoryData): Events()
    data class SortChangeRequest(val newSort: Sort): Events()
    object NextPeriod: Events()
    object PrevPeriod: Events()
    object FlushDeleteEvents: Events()
    object CreateNewCategory: Events(), ButtonRippleEffectEvent
}