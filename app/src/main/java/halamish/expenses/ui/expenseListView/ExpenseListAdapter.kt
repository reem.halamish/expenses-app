package halamish.expenses.ui.expenseListView

import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.core.widget.ImageViewCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import extensions.android.fromHtml
import halamish.expenses.BuildConfig
import halamish.expenses.R
import halamish.expenses.infra.EventsBus
import halamish.expenses.repo.*
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.repo.icons.icon8Url
import halamish.expenses.ui.ConfirmDeleteDialog
import halamish.expenses.ui.dpToPx
import halamish.expenses.ui.strikeThrough
import halamish.expenses.ui.withAutomaticCancel
import halamish.expenses.v2.appRedux
import kotlinx.android.synthetic.main.dialog_actions_for_category_or_expense.*
import kotlinx.android.synthetic.main.dialog_actions_for_category_or_expense.btn_delete
import kotlinx.android.synthetic.main.dialog_confirm_delete_future_recurring_expense.*
import kotlinx.android.synthetic.main.events_item_button_add_category.view.*
import kotlinx.android.synthetic.main.events_item_category.view.*
import kotlinx.android.synthetic.main.events_item_single_expense_awaiting_delete.view.*
import kotlinx.android.synthetic.main.events_item_single_expense_normal.view.*
import kotlinx.android.synthetic.main.events_item_total.view.*
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber

internal class ExpenseListAdapter(
  private val lifecycleOwner: LifecycleOwner,
  private val eventsBus: EventsBus<Events>,
  private val dateFormatter: DateTimeFormatter,
  private val analytics: AnalyticsLogger = appRedux.services.analytics
)
    : ListAdapter<ExpenseListAdapter.Item, ExpenseListAdapter.ViewHolder>(Callback) {

    interface Equatable  {
        override fun equals(other: Any?) : Boolean
    }

    sealed class Item(val id: String, val viewType: Int): Equatable {
        internal data class CategorySum(val data: CategoryData) : Item(
            data.category.id,
            if (data.isExpanded) TYPE_CATEGORY_OPEN_HEADER else TYPE_CATEGORY_CLOSED
        )

        internal data class Expense(val data: ExpenseEventData) : Item(
            data.event.id,
            if (data.isAwaitingDelete) TYPE_EXPENSE_EVENT_UNDO_DELETE else TYPE_EXPENSE_EVENT_NORMAL
        )

        internal data class Total(val total: Double, val period: String, val position: Int) :
            Item("total:$position", TYPE_TOTAL)
        internal data class Space(val spaceDp: Int, val position: Int): Item("space$position", TYPE_SPACE)

        internal data class AddCategoryButton(val buttonId: String): Item(buttonId, TYPE_ADD_CATEGORY_BUTTON)
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    object Callback: DiffUtil.ItemCallback<Item>(){
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean = oldItem == newItem
    }


    inner class SwipeCallback(
            movementFlag: Int,
            val mainView: (ViewHolder) -> View?,
            val isSwipeEnabled: (ViewHolder) -> Boolean,
            val onSwiping: (ViewHolder) -> Unit,
            val onSwiped: (ViewHolder) -> Unit
    ): ItemTouchHelper.SimpleCallback(0, movementFlag) {

        override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
            val holder = viewHolder as? ViewHolder ?: return 0
            if (!isSwipeEnabled(holder)) return 0
            return super.getSwipeDirs(recyclerView, viewHolder)
        }

        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
            if (actionState != ItemTouchHelper.ACTION_STATE_SWIPE)
                return super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            onSwiping(viewHolder as ViewHolder)
            mainView(viewHolder)?.translationX = dX
        }

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false // not supporting drag
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            onSwiped(viewHolder as ViewHolder)
        }

        fun attach(recycler: RecyclerView) = ItemTouchHelper(this).attachToRecyclerView(recycler)

    }

    private companion object {
        const val TYPE_CATEGORY_CLOSED = 0
        const val TYPE_CATEGORY_OPEN_HEADER = 1
        const val TYPE_EXPENSE_EVENT_NORMAL = 3
        const val TYPE_EXPENSE_EVENT_UNDO_DELETE = 4
        const val TYPE_TOTAL = 5
        const val TYPE_SPACE = 6
        const val TYPE_ADD_CATEGORY_BUTTON = 7
    }



    private val swipeToEditExpenseEvent = SwipeCallback(ItemTouchHelper.END,
            mainView = { it.itemView.expense_event_card },
            isSwipeEnabled = { holder ->
                return@SwipeCallback getItem(holder.adapterPosition).let { it is Item.Expense && !it.data.isAwaitingDelete }
            },
            onSwiping = { holder ->
                holder.view.expense_edit_container.isVisible = true
                holder.view.expense_delete_container.isVisible = false
            },
            onSwiped = { holder ->
                val expense = getItem(holder.adapterPosition) as? Item.Expense ?: return@SwipeCallback
                analytics.swipeToEditExpense(expense.data.event.dbId)
                eventsBus.send(Events.EditEventRequest(expense.data))
                holder.view.post {
                    notifyItemChanged(holder.adapterPosition)
                }
            })

    private val swipeToDeleteExpenseEvent = SwipeCallback(ItemTouchHelper.START,
            mainView = { it.itemView.expense_event_card },
            isSwipeEnabled = { holder ->
                return@SwipeCallback getItem(holder.adapterPosition).let { it is Item.Expense && !it.data.isAwaitingDelete }
            },
            onSwiping = { holder ->
                holder.itemView.expense_delete_container.isVisible = true
                holder.itemView.expense_edit_container.isVisible = false
            },
            onSwiped = { holder ->
                val expense = getItem(holder.adapterPosition) as? Item.Expense ?: return@SwipeCallback
                analytics.swipeToDeleteExpense(expense.data.event.dbId)
                eventsBus.send(Events.DeleteEventRequest(expense.data))
            })




    private val swipeToEditCategory = SwipeCallback(ItemTouchHelper.END,
            mainView = { it.itemView.content_card },
            isSwipeEnabled = { holder ->
                return@SwipeCallback getItem(holder.adapterPosition).let { it is Item.CategorySum }
            },
            onSwiping = { holder ->
                holder.view.category_edit_container.isVisible = true
                holder.view.category_delete_container.isVisible = false
            },
            onSwiped = { holder ->
                val category = getItem(holder.adapterPosition) as? Item.CategorySum ?: return@SwipeCallback
                analytics.swipeToEditCategory(category.id)
                eventsBus.send(Events.EditCategoryRequest(category.data))
                holder.view.post {
                    notifyItemChanged(holder.adapterPosition)
                }
            })

    private val swipeToDeleteCategory = SwipeCallback(ItemTouchHelper.START,
            mainView = { it.itemView.content_card },
            isSwipeEnabled = { holder ->
                return@SwipeCallback getItem(holder.adapterPosition).let { it is Item.CategorySum }
            },
            onSwiping = { holder ->
                holder.itemView.category_delete_container.isVisible = true
                holder.itemView.category_edit_container.isVisible = false
            },
            onSwiped = { holder ->
                val category = getItem(holder.adapterPosition) as? Item.CategorySum ?: return@SwipeCallback
                analytics.swipeToDeleteCategory(category.id)
                eventsBus.send(Events.DeleteCategoryRequest(category.data, confirmed = false))
            })



    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        swipeToEditExpenseEvent.attach(recyclerView)
        swipeToDeleteExpenseEvent.attach(recyclerView)
        swipeToEditCategory.attach(recyclerView)
        swipeToDeleteCategory.attach(recyclerView)
    }


    internal fun submitVmList(
        list: List<CategoryData>,
        period: String,
        addCategoryButton: Boolean,
        callback: () -> Unit = {}
    ) {
        val actualList: List<Item> = translateVmList(list, period, addCategoryButton)
        submitList(actualList, callback)
    }

    internal fun onUndoDeleteCategory(categoryData: CategoryData) {
        currentList
                .withIndex()
                .firstOrNull { (it.value as? Item.CategorySum)?.data?.category?.id == categoryData.category.id }
                ?.index
                ?.let { index -> notifyItemChanged(index) }
    }

    private fun translateVmList(input: List<CategoryData>, period: String, addCategoryButton: Boolean): List<Item> {
        if (BuildConfig.DEBUG) println("Adapter: got list with ${input.size} values")

        val output = ArrayList<Item>(input.size)

        for (iteration in input.withIndex()) {
            val index = iteration.index
            val categoryData = iteration.value
            output += Item.CategorySum(categoryData)
            if (categoryData.isExpanded) {
                output += categoryData.events.map { Item.Expense(it) }
            }
            output += Item.Space(spaceDp = 1, position = index)
        }

        if (addCategoryButton) {
            output += Item.AddCategoryButton("button_after_list")
        }
        output += Item.Total(input.map { it.totalAmount }.sum(), period=period, position = 0)
        output += Item.Space(spaceDp = 20, position = Int.MAX_VALUE)

        if (BuildConfig.DEBUG) Timber.d("Adapter: built list with ${output.size} values")
        return output
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        fun holder(layoutId: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(layoutId, parent, false))

        return when(viewType) {
            TYPE_CATEGORY_CLOSED -> holder(R.layout.events_item_category)
            TYPE_CATEGORY_OPEN_HEADER -> holder(R.layout.events_item_category)
            TYPE_EXPENSE_EVENT_NORMAL -> holder(R.layout.events_item_single_expense_normal)
            TYPE_EXPENSE_EVENT_UNDO_DELETE -> holder(R.layout.events_item_single_expense_awaiting_delete)
            TYPE_TOTAL -> holder(R.layout.events_item_total)
            TYPE_SPACE -> holder(R.layout.events_item_space)
            TYPE_ADD_CATEGORY_BUTTON -> holder(R.layout.events_item_button_add_category)
            else -> throw IllegalArgumentException("type $viewType not supported!")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return when(val item = getItem(position)) {
            is Item.Space -> bindSpace(holder, item)
            is Item.CategorySum -> bindCategorySum(item, holder)
            is Item.Expense -> bindExpenseEvent(item, holder)
            is Item.Total -> bindTotal(item, holder)
            is Item.AddCategoryButton -> bindAddCategoryButton(holder)
        }
    }

    private fun bindAddCategoryButton(holder: ViewHolder) {
        holder.view.buttonAddCategoryClickArea.setOnClickListener {
            analytics.buttonClick("add_new_category_from_mega_list")
            eventsBus.send(Events.CreateNewCategory)
        }
    }

    private fun bindTotal(total: Item.Total, holder: ViewHolder){
        val text = "${holder.view.context.getString(R.string.adapter_item_total)} (${total.period}): <b>${currency(total.total)}</b>".fromHtml()
        holder.view.tv_megalist_total.text = text
    }

    private fun bindSpace(holder: ViewHolder, space: Item.Space){
        holder.view.layoutParams.height = space.spaceDp.dpToPx()
        holder.view.requestLayout()
    }

    private fun bindCategorySum(categorySum: Item.CategorySum, holder: ViewHolder) {
        ImageViewCompat.setImageTintList(holder.view.categoryCircle, ColorStateList.valueOf(categorySum.data.category.color))
        holder.view.categoryAmount.text = currency(categorySum.data.totalAmount)
        loadIconWithRetry(icon8Url(categorySum.data.category.icon), holder.view.category_icon)
        holder.view.categoryName.text = categorySum.data.category.displayName
        holder.view.btn_expand.visibility = if (categorySum.data.events.isNotEmpty()) View.VISIBLE else View.INVISIBLE
        holder.view.btn_expand.setImageResource(if (categorySum.data.isExpanded) R.drawable.ic_expand_less_black_24dp else R.drawable.ic_expand_more_black_24dp)
        holder.view.btn_expand.setOnClickListener { eventsBus.send(Events.ChangeCategoryExpandedRequest(categorySum.data, !categorySum.data.isExpanded)) }
        holder.view.content_card.translationX = 0f
        holder.view.btn_add_from_category.setOnClickListener { eventsBus.send(Events.CreateNewExpenseWithCategory(categorySum.data)) }
        holder.view.categoryContent.setOnClickListener { eventsBus.send(Events.ChangeCategoryExpandedRequest(categorySum.data, !categorySum.data.isExpanded)) }
        holder.view.categoryContent.setOnLongClickListener {
            analytics.longClickToEditCategory(categorySum.data.category.id)
            eventsBus.send(Events.EditCategoryRequest(categorySum.data))
            true
        }
    }

    private fun bindExpenseEvent(item: Item.Expense, holder: ViewHolder) {
        val context = holder.view.context

        if (item.data.isAwaitingDelete) {
            holder.view.deleted_expense_details.text = item.data.event.comments ?: ""
            holder.view.deleted_expense_details.strikeThrough = true
            holder.view.btn_undo.setOnClickListener {
                eventsBus.send(Events.UndoDeleteEventRequest(item.data))
            }
            return
        }

        // ok normal event. let's configure!
        holder.view.expense_event_card.translationX = 0f
        holder.view.expenseDetails.text = item.data.event.comments ?: ""
        holder.view.expenseDate.text = dateFormatter.format(item.data.event.date)
        holder.view.expenseAmount.text = currency(item.data.event.amount)

        val recurring = item.data.event.recurring
        holder.view.imgRepeat.isVisible = recurring != null
        holder.view.expenseRecurringDetails.isVisible = recurring != null
        if (recurring != null) {
            val text = context.getString(R.string.item_expense_recurring)
                .replace("[CURRENT_INDEX]", "${recurring.currentIndex + 1}")
                .replace(
                    "[TOTAL]",
                    if (recurring.forever) context.getString(R.string.item_expense_recurring_infinity)
                    else "${recurring.totalOccurrences}"
                )

            holder.view.expenseRecurringDetails.text = text
        }

        holder.view.tap_area.setOnClickListener {
            showActionsDialog(context, lifecycleOwner.lifecycle, R.layout.dialog_actions_for_category_or_expense).apply {
                val name = item.data.event.comments?.takeIf { it.isNotEmpty() }
                    ?: context.getString(R.string.single_expense)
                val text = "$name - ${currency(item.data.event.amount)}"
                title.text = text

                btn_edit.setText(R.string.cta_edit_expense)
                btn_edit.setOnClickListener {
                    dismiss()
                    eventsBus.send(Events.EditEventRequest(item.data))
                }

                btn_stop_payments.isVisible = item.data.event.recurring != null
                btn_stop_payments.text =
                    context.getString(R.string.cta_stop_future_recurring_expense)
                btn_stop_payments.setOnClickListener {
                    dismiss()
                    showActionsDialog(
                        context,
                        lifecycleOwner.lifecycle,
                        R.layout.dialog_confirm_delete_future_recurring_expense
                    ).apply {
                        btn_cancel.setOnClickListener { cancel() }
                        btn_delete.setOnClickListener {
                            val alsoDeleteCurrent = checkbox_delete_current.isChecked
                            dismiss()
                            eventsBus.send(
                                Events.StopFutureRecurringExpenseEventRequest(
                                    item.data,
                                    alsoDeleteCurrent
                                )
                            )
                        }
                    }
                }

                btn_delete.setText(
                    if (item.data.event.recurring != null)
                        R.string.cta_delete_expense_recurring
                    else R.string.cta_delete_expense
                )
                btn_delete.setOnClickListener {
                    dismiss()
                    ConfirmDeleteDialog(context) {
                        eventsBus.send(Events.DeleteEventRequest(item.data))
                    }
                }
            }
        }
        // one time expense
    }


    private fun loadIconWithRetry(url: String, imageView: ImageView){
        Glide.with(imageView)
            .load(url)
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    imageView.post { loadIconWithRetry(url, imageView) }; return true }
                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    return false }
            })
            .into(imageView)
    }


    private fun showActionsDialog(context: Context, lifecycle: Lifecycle, @LayoutRes layout: Int): Dialog {
        val dialogView = LayoutInflater
            .from(context)
            .inflate(layout, null)

        return AlertDialog.Builder(context)
            .setView(dialogView)
            .setCancelable(true)
            .create()
            .apply { show() }
            .withAutomaticCancel(lifecycle)
    }

    private fun currency(amount: Double) = CurrencyFormatter.get().format(amount.toInt())
}
