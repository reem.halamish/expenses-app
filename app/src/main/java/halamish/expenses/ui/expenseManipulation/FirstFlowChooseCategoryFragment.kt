package halamish.expenses.ui.expenseManipulation


import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import halamish.expenses.R
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.ui.ChooseCategoryFragment


class FirstFlowChooseCategoryFragment : ChooseCategoryFragment() {

    val expenseViewModel: ExpenseManipulationViewModel by activityViewModels()

    override val onCategoryChosen: (ExpenseCategory) -> Unit
        get() = {category ->
            expenseViewModel.category.value = category
            findNavController().navigate(R.id.action_firstFlowChooseCategory_to_newExpenseSummary)
        }
}
