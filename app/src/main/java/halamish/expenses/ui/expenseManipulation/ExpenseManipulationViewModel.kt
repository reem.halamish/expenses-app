package halamish.expenses.ui.expenseManipulation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import halamish.expenses.data.*
import halamish.expenses.infra.ShortId
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.saveExpense
import halamish.expenses.repo.startCreateNewExpense
import halamish.expenses.repo.startEditExistingExpense
import halamish.expenses.ui.mutableLiveDataOf
import halamish.expenses.ui.prototypes.ProtoViewModel
import halamish.expenses.v2.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class ExpenseManipulationViewModel(
    private val expenseId: String = ShortId.create(),
    val canDelete: Boolean = false,
    private val oneTimeExpenses: Map<String, OneTimeExpenseRecord> = appRedux.state.value.data.oneTime,
    private val rules: Map<String, RecurringExpenseRule> = appRedux.state.value.data.rules,
    private val categories: Map<String, ExpenseCategory> = appRedux.state.value.data.categories,
    val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(
        appRedux.state.value.project.dateFormatPattern ?: Defaults.localDateFormatPattern,
        Locale.getDefault()
    ),
    val analytics: AnalyticsLogger = appRedux.services.analytics,
    val startingCategoryId: String? = null
)
    : ProtoViewModel() {


    data class Recurring(
        val isRecurring: Boolean = false,
        val isForever: Boolean = false,
        val occourencesAmount: Long = 12
    )

    var amount = mutableLiveDataOf("0")
    var category = mutableLiveDataOf<ExpenseCategory>()
    var date = mutableLiveDataOf(LocalDate.now())
    var comments = MutableLiveData<String?>()
    var recurring = mutableLiveDataOf(Recurring())
    val isNew: Boolean get() = expenseId !in oneTimeExpenses && expenseId !in rules


    @Suppress("UNCHECKED_CAST") // todo create "FactoryFun { ... }" instead of all of those factories
    class Factory(
        private val expenseId: String,
        private val categoryId: String?,
        private val canDelete: Boolean
    ) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return ExpenseManipulationViewModel(
                expenseId,
                canDelete,
                startingCategoryId = categoryId
            ) as T
        }
    }


    init {
        if (!isNew) {
            // inflate from existing
            oneTimeExpenses[expenseId]?.let {
                amount.value = it.amount.toString()
                category.value = categories[it.categoryId]
                date.value = it.date()
                comments.value = it.comments
                recurring.apply { value = value?.copy(isRecurring = false) }
            }

            rules[expenseId]?.let { rule ->
                amount.value = rule.amount.toString()
                category.value = categories[rule.categoryId]
                date.value = rule.dateStart()
                comments.value = rule.comments
                recurring.apply {
                    val repeatitions = calcRepetitions(
                        rule.dateStart(),
                        rule.repeatStrategy,
                        rule.dateEnd()
                    )
                    value = value?.copy(
                        isRecurring = true,
                        isForever = rule.isForever(),
                        occourencesAmount = 1 + repeatitions
                    )
                }
            }
        }
    }

    init {
        if (isNew) {
            analytics.startCreateNewExpense(expenseId)
        } else {
            analytics.startEditExistingExpense(
                expenseId,
                recurring = recurring.value?.isRecurring == true
            )
        }
    }

    init {
        if (category.value == null && startingCategoryId != null) {
            category.value = categories[startingCategoryId]
        }
    }

    fun delete() {
        appRedux.eventBus.send(RequestDeleteSingleOneTimeExpense(expenseId))
        appRedux.eventBus.send(RequestDeleteSingleExpenseRule(expenseId))
    }

    fun save() {
        val isNew = this.isNew
        val amount = amount.value?.cleanedNumberDouble() ?: return
        val comments = comments.value?.trim()
        val date = date.value ?: return
        val categoryId = category.value?.id ?: return

        val recurringOptions = recurring.value!!
        val recurring = recurringOptions.isRecurring

        analytics.saveExpense(expenseId, isNew, recurring)

        if (recurring) {
            // recurring expense
            val endDate = if (recurringOptions.isForever) distantFuture else calcEndDate(
                date,
                ExpenseRuleRepeat.MONTHLY,
                recurringOptions.occourencesAmount
            )
            appRedux.eventBus.send(
                UpdateExpenseRule(
                    RecurringExpenseRule(
                        amount = amount,
                        categoryId = categoryId,
                        repeatStrategy = ExpenseRuleRepeat.MONTHLY,
                        startLong = date.toEpochDay(),
                        endLong = endDate.toEpochDay(),
                        comments = comments,
                        id = expenseId
                    )
                )
            )
            appRedux.eventBus.send(RequestDeleteSingleOneTimeExpense(expenseId))
        } else {
            // one time expense
            appRedux.eventBus.send(
                UpdateOneTimeExpense(
                    OneTimeExpenseRecord(
                        amount = amount,
                        categoryId = categoryId,
                        dateLong = date.toEpochDay(),
                        comments = comments,
                        id = expenseId
                    )
                )
            )
            appRedux.eventBus.send(RequestDeleteSingleExpenseRule(expenseId))
        }
    }
}