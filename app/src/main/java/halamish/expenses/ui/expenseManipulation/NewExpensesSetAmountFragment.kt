package halamish.expenses.ui.expenseManipulation


import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import halamish.expenses.ui.SetAmountFragment


class NewExpensesSetAmountFragment : SetAmountFragment() {
    val expenseViewModel: ExpenseManipulationViewModel by activityViewModels()

    override val bottomButtonListener: () -> Unit
        get() = {
            expenseViewModel.amount.value = currentAmount
            findNavController().popBackStack()
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        expenseViewModel.amount.value?.let { currentAmount = it }
    }
}
