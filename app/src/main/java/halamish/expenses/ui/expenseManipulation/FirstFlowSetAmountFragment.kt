package halamish.expenses.ui.expenseManipulation


import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import halamish.expenses.R
import halamish.expenses.ui.SetAmountFragment


class FirstFlowSetAmountFragment : SetAmountFragment() {
    private val expenseViewModel: ExpenseManipulationViewModel by activityViewModels()

    override val bottomButtonListener: () -> Unit
        get() = {
            expenseViewModel.amount.value = currentAmount
            val action = if (expenseViewModel.startingCategoryId != null)
                R.id.action_firstFlowSelectAmount_to_newExpenseSummary
            else
                R.id.action_firstFlowSelectAmount_to_firstFlowChooseCategory

            findNavController().navigate(action)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        expenseViewModel.amount.value?.let { currentAmount = it }
    }
}
