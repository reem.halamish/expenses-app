package halamish.expenses.ui.expenseManipulation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.fragment.findNavController
import halamish.expenses.R
import halamish.expenses.infra.ShortId
import halamish.expenses.ui.prototypes.ProtoActivity
import kotlinx.android.synthetic.main.activity_main.*

class ExpenseManipulationActivity : ProtoActivity() {

    override val screenName: String = "ExpenseManipulationActivity"

    private val expenseViewModel: ExpenseManipulationViewModel by viewModels(factoryProducer = {
        val expenseId = intent.getStringExtra("expense_id") ?: ShortId.create()
        val categoryId = intent.getStringExtra("category_id")
        val canDelete = intent.getBooleanExtra("can_delete", false)
        return@viewModels ExpenseManipulationViewModel.Factory(expenseId, categoryId, canDelete)
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        expenseViewModel // initialize
        setContentView(R.layout.activity_create_edit_expense)
        initNavigation()
    }

    private fun initNavigation(){
        if (!expenseViewModel.isNew) {
            // skip to the "update" part
            nav_host_fragment.findNavController().navigate(R.id.action_firstFlowSelectAmount_to_newExpenseSummary)
        }
    }


    companion object {
        @JvmStatic
        fun startCreateExpense(context: Context) {
            context.startActivity(Intent(context, ExpenseManipulationActivity::class.java))
        }

        @JvmStatic
        fun startEditExpense(context: Context, expenseId: String, canDelete: Boolean = false) {
            context.startActivity(Intent(context, ExpenseManipulationActivity::class.java).apply {
                putExtra("expense_id", expenseId)
                putExtra("can_delete", canDelete)
            })
        }
    }
}
