package halamish.expenses.ui.expenseManipulation


import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.ui.ChooseCategoryFragment


class NewExpenseChooseCategoryFragment : ChooseCategoryFragment() {

    val expenseViewModel: ExpenseManipulationViewModel by activityViewModels()

    override val onCategoryChosen: (ExpenseCategory) -> Unit
        get() = {category ->
            expenseViewModel.category.value = category
            findNavController().popBackStack()
        }
}
