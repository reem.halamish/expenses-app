package halamish.expenses.ui.expenseManipulation


import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding3.widget.textChanges
import halamish.expenses.R
import halamish.expenses.data.cleanedNumberDouble
import halamish.expenses.repo.cancelDialog
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.repo.currency.get
import halamish.expenses.repo.icons.icon8Url
import halamish.expenses.ui.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.dialog_recurring_expense.view.*
import kotlinx.android.synthetic.main.fragment_new_expense_summary.view.*
import kotlinx.android.synthetic.main.inc_expense_summary_line.view.*
import kotlinx.android.synthetic.main.inc_expense_summary_line.view.icon_end
import kotlinx.android.synthetic.main.inc_expense_summary_line.view.title_icon
import kotlinx.android.synthetic.main.inc_expense_summary_line_edt.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.threeten.bp.LocalDate

@ExperimentalCoroutinesApi
class NewExpenseSummary : Fragment() {

    private val bag = CompositeDisposable()
    private val expenseViewModel: ExpenseManipulationViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_new_expense_summary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initLabelsAndIcons(view)
        initButtonListeners(view)
        textViewsListenToValues(view)
        initCommentsText(view)
        listenToBackPress()
    }

    private fun listenToBackPress() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().setResult(Activity.RESULT_CANCELED)
                    requireActivity().finish()
                }
            })
    }

    private fun initLabelsAndIcons(view: View) {
        view.amount.title_icon.setImageResource(R.drawable.ic_money_black_24dp)
        view.category.title_icon.setImageResource(R.drawable.ic_label_outline_black_24dp)
        view.date.title_icon.setImageResource(R.drawable.ic_date_range_black_24dp)
        view.repeat.title_icon.setImageResource(R.drawable.ic_repeat_24dp)
        view.comments.title_icon.setImageResource(R.drawable.ic_comment_black_24dp)
    }

    private fun initButtonListeners(view: View) {
        view.amount.setOnClickListener { openAmountSelector() }

        view.category.setOnClickListener { openCategoryChooser() }

        view.date.setOnClickListener { openDate() }

        view.repeat.setOnClickListener { openRepeat() }

        view.bottom_button_save.setOnClickListener {
            expenseViewModel.save()
            requireActivity().setResult(Activity.RESULT_OK)
            requireActivity().finish()
        }

        view.bottom_button_delete.isVisible = expenseViewModel.canDelete && !expenseViewModel.isNew
        view.bottom_button_delete.setOnClickListener {
            ConfirmDeleteDialog(requireContext()) {
                expenseViewModel.delete()
                requireActivity().setResult(Activity.RESULT_OK)
                requireActivity().finish()
            }
        }
    }

    private fun initCommentsText(view: View){
        expenseViewModel.comments.value.let { text ->
            view.comments.edt.setText(text)
        }

        bag += view.comments.edt.textChanges().subscribe {
            expenseViewModel.comments.value = it.toString().takeIf { it.isNotEmpty() }
        }

        view.comments.edt.setHint(R.string.flow_saveExpense_comments_hint)

        lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            fun clearKeyboard() = view.comments.edt.clearFocusAndHideKeyboard()
        })
    }


    private fun textViewsListenToValues(view: View) {

        val oneTimeStr = getString(R.string.recurrening_expense_option_one_time)
        val monthlyStrProto = getString(R.string.recurrening_expense_proto_times)
        val foreverStr = getString(R.string.recurrening_expense_option_repeat_monthly_forever)

        view.amount.tv.textBy(expenseViewModel.amount, this) {
            CurrencyFormatter.get().format(it?.cleanedNumberDouble() ?: 0.0)
        }
        view.category.tv.textBy(expenseViewModel.category, this) { it?.displayName }
        view.date.tv.textBy(
            expenseViewModel.date,
            this
        ) { it?.format(expenseViewModel.dateFormatter) }
        view.repeat.tv.textBy(expenseViewModel.recurring, this) {
            when {
                it == null -> oneTimeStr
                it.isForever -> foreverStr
                it.isRecurring -> monthlyStrProto.replace("[[AMOUNT]]", "${it.occourencesAmount}")
                else -> oneTimeStr
            }
        }

        expenseViewModel.category.observe(viewLifecycleOwner) {
            it ?: return@observe
            Glide.with(view)
                .load(icon8Url(it.icon))
                .withCallback { view.category.icon_end.isVisible = true }
                .into(view.category.icon_end)
        }

        if (expenseViewModel.isNew) {
            view.view_title.text = getString(R.string.title_new_expense)
            view.bottom_button_save.text = getString(R.string.add_expense_btn)
        } else {
            view.view_title.text = getString(R.string.title_update_expense)
            view.bottom_button_save.text = getString(R.string.update_expense_btn)
        }
    }

    private fun openAmountSelector(){
        findNavController().navigate(R.id.action_newExpenseSummary_to_newExpensesSelectAmount)
    }

    private fun openCategoryChooser(){
        findNavController().navigate(R.id.action_newExpenseSummary_to_newExpenseChooseCategory)
    }

    private fun openRepeat(){
        val recurring = expenseViewModel.recurring.value!!
        val doneButtonTriggers = BehaviorSubject.createDefault(Unit)
        val context = context ?: return
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_recurring_expense,null)
        val dialog = AlertDialog.Builder(context).setView(view).create()
        val cancelDialogOnStopObserver = object: LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun cancelDialog() {
                if (dialog.isShowing) dialog.cancel()
                doneButtonTriggers.onComplete()
            }
        }

        lifecycle.addObserver(cancelDialogOnStopObserver)
        dialog.show()
        dialog.setOnCancelListener { expenseViewModel.analytics.cancelDialog("RECURRING_EXPENSES_DIALOG") }

        val radioOneTime = view.radio_one_time
        val radioRepeatingMonthly = view.radio_repeating_monthly
        val radioRepeatingForever = view.radio_repeating_forever
        val edtRepeatTimes = view.edt_times
        val doneButton = view.btn_done


        // radio buttons
        fun otherRadios(radio: View) =
            listOf(radioOneTime, radioRepeatingMonthly, radioRepeatingForever)
                .filter { it != radio }

        fun radioListener() = CompoundButton.OnCheckedChangeListener { radio, isChecked ->
            if (!isChecked) /* automatically */ return@OnCheckedChangeListener

            hideKeyboard()
            otherRadios(radio).forEach { it.isChecked = false }
            doneButtonTriggers.onNext(Unit)
        }

        val radioToCheck = when {
            recurring.isRecurring && recurring.isForever -> radioRepeatingForever
            recurring.isRecurring && !recurring.isForever -> radioRepeatingMonthly
            else -> radioOneTime
        }

        radioToCheck.isChecked = true
        otherRadios(radioToCheck).forEach { it.isChecked = false }
        radioOneTime.setOnCheckedChangeListener(radioListener())
        radioRepeatingMonthly.setOnCheckedChangeListener(radioListener())
        radioRepeatingForever.setOnCheckedChangeListener(radioListener())


        edtRepeatTimes.setText("${expenseViewModel.recurring.value?.occourencesAmount}")

        fun getCurrentRecurringAmount(): Long {
            return edtRepeatTimes.text.toString().toLongOrNull() ?: 0
        }

        bag += edtRepeatTimes.textChanges().subscribe { doneButtonTriggers.onNext(Unit) }

        // clear non-digits from the edit text
        bag += edtRepeatTimes.textChanges()
            .filter { text -> text.any { !it.isDigit() } }
            .subscribe { text -> edtRepeatTimes.setText(text.filter { it.isDigit() }) }

        // clamp too-big numbers from the edit text
        bag += edtRepeatTimes.textChanges()
            .filter { text -> text.length > 4 }
            .subscribe { text ->
                edtRepeatTimes.setText(text.drop(1))
                edtRepeatTimes.setSelection(edtRepeatTimes.text.length)
            }


        // enable the "done" button
        bag += doneButtonTriggers
            .map {
                radioOneTime.isChecked
                        || (radioRepeatingMonthly.isChecked && getCurrentRecurringAmount() > 0)
                        || radioRepeatingForever.isChecked
            }
            .subscribe { canTapDone -> doneButton.isEnabled = canTapDone }


        doneButton.setOnClickListener {
            val newRecurring = recurring.copy(
                isRecurring = radioRepeatingMonthly.isChecked || radioRepeatingForever.isChecked,
                isForever = radioRepeatingForever.isChecked,
                occourencesAmount = getCurrentRecurringAmount()
            )

            expenseViewModel.recurring.value = newRecurring

            dialog.hideKeyboard()
            dialog.dismiss()
        }

        edtRepeatTimes.setSelection(edtRepeatTimes.text.length)
    }

    private fun openDate(){
        val currentTime = expenseViewModel.date.value ?: return

        val listener = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            expenseViewModel.date.value = LocalDate.of(year, month + 1, dayOfMonth)
        }

        DatePickerDialog(requireContext(), listener,
            currentTime.year, currentTime.monthValue - 1, currentTime.dayOfMonth)
            .withAutomaticCancel(lifecycle)
            .show()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        bag.clear()
    }
}
