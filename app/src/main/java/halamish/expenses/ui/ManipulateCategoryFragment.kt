package halamish.expenses.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.widget.textChanges
import dev.sasikanth.colorsheet.ColorSheet
import halamish.expenses.R
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.infra.EventsBus
import halamish.expenses.repo.icons.icon8Url
import halamish.expenses.v2.appRedux
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_category_manipulation.*
import kotlinx.android.synthetic.main.fragment_category_manipulation.view.*
import java.io.Serializable


class ManipulateCategoryFragment : Fragment() {
    val bag = CompositeDisposable()

    val viewModel by lazy { ManipulateCategoryViewModel.get(this) }

    val analytics by lazy { appRedux.services.analytics }

    val requestId: String?
        get() = arguments?.getString("request_id", null)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category_manipulation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initFirstTextValues(view)
        handleColor(view)
        handleIcon(view)
        handleBtnCreate(view)
        handleBtnDelete(view)
    }

    private fun handleBtnCreate(view: View) {
        bag += view.edit_category_name.textChanges()
            .map { it.isNotEmpty() }
            .subscribe { hasText -> view.btn_create.isEnabled = hasText }

        bag += view.btn_create.clicks()
            .subscribe {
                val isNew = viewModel.existingCategory == null
                val category = viewModel.saveCategory(
                    displayName = edit_category_name.text.toString().trim()
                )
                // todo store them in vm when changed, not pass them on button click
                hideKeyboard()
                findNavController().popBackStack()
                eventsBus().send(CategoryChanged(isNew, category, requestId))
                analytics.event("edit_category_screen_click", bundleOf("action" to "save"))
            }
    }

    // todo: if needs verification, show popup before deleting (currently just hide the button)
    private fun handleBtnDelete(view: View) {
        val button = view.btn_delete_category
        button.isVisible = viewModel.canDeleteCategory && !viewModel.needsVerificationBeforeDeleteCategory()
        if (!button.isVisible) return
        bag += button.clicks()
            .subscribe {
                viewModel.deleteCategory()
                hideKeyboard()
                findNavController().popBackStack()
                analytics.event("edit_category_screen_click", bundleOf("action" to "delete"))
            }
    }

    private fun handleColor(view: View) {
        val circleDrawable = view.circle.background
        val wrapperDrawable = DrawableCompat.wrap(circleDrawable)
        view.circle.background = wrapperDrawable

        viewModel.chosenColorLiveData { context }.observe(viewLifecycleOwner, Observer { color ->
            color ?: return@Observer
            wrapperDrawable.setTint(color)
        })

        fun showColorSheet(){
            ColorSheet()
                .colorPicker(colors = viewModel.availableColorValues(requireContext()),
                    selectedColor = viewModel.chosenColorValue(requireContext()),
                    listener = { color -> viewModel.setChosenColor(color) })
                .show(parentFragmentManager)
        }

        bag += view.circle.clicks()
            .subscribe {
                analytics.event("edit_category_screen_click", bundleOf("action" to "color", "material_button" to false))
                hideKeyboard()
                showColorSheet()
            }
        bag += view.circleButton.clicks()
            .subscribe {
                analytics.event("edit_category_screen_click", bundleOf("action" to "color", "material_button" to true))
                hideKeyboard()
                showColorSheet()
            }
    }

    private fun handleIcon(view: View){
        viewModel.iconName.observe(viewLifecycleOwner, Observer { iconName -> iconName ?: return@Observer
            Glide.with(this)
                .load(icon8Url(iconName))
                .error(R.drawable.ic_warning_red400_24dp)
                .into(view.btn_icon)
        })

        fun goToIcons(){
            val categoryName = edit_category_name.text.toString().takeIf { it.isNotEmpty() }
            findNavController().navigate(R.id.action_categoryFragment_to_selectIconFragment, Bundle().apply {
                categoryName?.let { putString("search_term", it) }
            })
        }

        view.btn_icon.setOnClickListener {
            analytics.event("edit_category_screen_click", bundleOf("action" to "icon", "material_button" to false))
            hideKeyboard()
            goToIcons()
        }
        view.iconEditButton.setOnClickListener {
            analytics.event("edit_category_screen_click", bundleOf("action" to "icon", "material_button" to true))
            hideKeyboard()
            goToIcons()
        }
    }

    private fun initFirstTextValues(view: View){
        viewModel.existingCategory?.let { category ->
            view.edit_category_name.setText(category.displayName)
            view.view_title.text = getString(R.string.flow_editCategory_title)
            view.btn_create.text = getString(R.string.flow_editCategory_mainButton)
        }
    }

    override fun onDestroyView() {
        bag.clear()
        super.onDestroyView()
    }

    data class CategoryChanged(val isNew: Boolean, val category: ExpenseCategory, val requestId: String?): Serializable

    companion object {
        @JvmStatic fun argsForEdit(categoryId: String): Bundle = Bundle().apply { putString("category_id", categoryId) }
        @JvmStatic fun categoryIdToEdit(bundle: Bundle?): String? = bundle?.getString("category_id")
        @JvmStatic fun eventsBus() = EventsBus.from<CategoryChanged>(ManipulateCategoryFragment::class.java)
    }
}
