package halamish.expenses.ui.prototypes

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class ProtoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    inline val context: Context get() = itemView.context
}