package halamish.expenses.ui.prototypes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.play.core.review.ReviewInfo
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.ui.language.SelectLanguageActivity
import halamish.expenses.ui.popup.PopupRequest
import halamish.expenses.v2.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.*
import timber.log.Timber

abstract class ProtoActivity: AppCompatActivity() {
    val analytics by lazy { appRedux.services.analytics }
    private val analyticsViewModel by viewModels<AnalyticsScreenViewModel> {
        AnalyticsScreenViewModel.Factory(screenName)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase?.withLanguage())
    }

    data class LanguageConfig(
        val recreateOnLanguageChange: Boolean = true,
        val startLanguageSelectionIfMissing: Boolean = true
    )

    open val languageConfig = LanguageConfig()

    protected abstract val screenName: String

    protected open fun supportsPopup(popupRequest: PopupRequest) = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        analyticsViewModel // initialize

        onBackPressedDispatcher.addCallback(this) {
            analytics.event("tap_back",
                bundleOf("activity" to this@ProtoActivity.javaClass.simpleName))

            remove()
            onBackPressedDispatcher.onBackPressed()
        }


        val phoneData = appRedux.state.value.phoneData
        val startLanguage = phoneData.language

        window.decorView.layoutDirection =
            if (phoneData.isRtl(this))
                View.LAYOUT_DIRECTION_RTL
            else View.LAYOUT_DIRECTION_LTR

        if (languageConfig.startLanguageSelectionIfMissing && phoneData.needsLanguageSetup(this)) {
            startActivity(Intent(this, SelectLanguageActivity::class.java).apply {
                putExtra(SelectLanguageActivity.KEY_BLOCK, true)
            })
        }

        if (languageConfig.recreateOnLanguageChange) {
            lifecycleScope.launchWhenCreated {
                // might never happen
                val change = appRedux.state.map { it.phoneData.language }
                    .first { it != startLanguage }
                Timber.d("activity $this detected language change (was $startLanguage, now $change). will recreate()")
                recreate()
            }
        }

        handlePopups()
    }

    private fun handlePopups(){
        val activity = this

        val googlePlayReviewInfo = CompletableDeferred<ReviewInfo>()
        if (supportsPopup(PopupRequest.ReviewInGooglePlay)) {
            appRedux.services.googlePlayAppReviewManager.requestReviewFlow().addOnSuccessListener { reviewInfo ->
                googlePlayReviewInfo.complete(reviewInfo)
            }
        }


        lifecycleScope.launchWhenResumed {
            appRedux.state.map { it.popupRequests.firstOrNull { supportsPopup(it) } }
                .filterNotNull()
                .distinctUntilChanged()
                .collect { request ->
                    when (request) {
                        is PopupRequest.Descriptor -> {
                            request.creator.showPopup(
                                context = activity,
                                lifecycleOwner = activity,
                                data = request.data, onDone = { appRedux.eventBus.send(PopupRequestWasHandled(request)) }
                            )
                        }
                        PopupRequest.ReviewInGooglePlay -> {
                            val reviewInfo = googlePlayReviewInfo.await()
                            val reviewManager = appRedux.services.googlePlayAppReviewManager
                            reviewManager.launchReviewFlow(activity, reviewInfo).addOnCompleteListener { task ->
                                Timber.d("done reviewing in GooglePlay (success?${task.isSuccessful}, canceled?${task.isCanceled}) - $task")
                                appRedux.eventBus.send(PopupRequestWasHandled(request))
                            }
                        }
                    }
                }
        }
    }
}


private class AnalyticsScreenViewModel(
    val screenName: String,
    val analytics: AnalyticsLogger =appRedux.services.analytics
): ViewModel(){
    init {
        analytics.event("activity_lifecycle_event",
            bundleOf("activity" to screenName, "event" to "ON_CREATE"))
    }

    override fun onCleared() {
        analytics.event("activity_lifecycle_event",
            bundleOf("activity" to screenName, "event" to "ON_DESTROY"))
    }

    class Factory(val screenName: String): ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            return AnalyticsScreenViewModel(screenName) as T
        }
    }
}
