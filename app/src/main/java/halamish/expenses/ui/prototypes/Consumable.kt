package halamish.expenses.ui.prototypes

import java.util.concurrent.atomic.AtomicReference

class Consumable<T>(value: T) {
    val reference = AtomicReference(value)

    inline fun consume(crossinline block: (T) -> Unit) {
        reference.getAndSet(null)?.let { block(it) }
    }
}