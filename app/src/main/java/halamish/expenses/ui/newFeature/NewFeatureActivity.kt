package halamish.expenses.ui.newFeature

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.lifecycleScope
import halamish.expenses.BuildConfig
import halamish.expenses.databinding.ActivityNewFeatureBinding
import halamish.expenses.ui.prototypes.ProtoActivity
import halamish.expenses.v2.LocalStorage
import halamish.expenses.v2.appRedux
import halamish.expenses.v2.updateForLastNewFeatureShownVersionCode
import kotlinx.coroutines.launch

class NewFeatureActivity: ProtoActivity() {
    override val screenName: String = "NewFeatureActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(0, 0)
        val binding = ActivityNewFeatureBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        lifecycleScope.launch {
            appRedux.eventBus.send(LocalStorage.updateForLastNewFeatureShownVersionCode(BuildConfig.VERSION_CODE))
        }
        val container = binding.newFeatureContainer
        container.alpha = 0f
        container.animate()
            .alpha(1f)
            .setDuration(200L)
            .start()

        binding.newFeatureMainButton.setOnClickListener {
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtra(EXTRA_OUTPUT_ACTION, OUTPUT_ACTION_OPEN_DARK_MODE)
            })
            exit(binding)
        }

        binding.newFeatureSecondaryButton.setOnClickListener { exit(binding) }
        binding.root.setOnClickListener { exit(binding) } // exit on tap outside
    }

    private fun exit(binding: ActivityNewFeatureBinding) {
        binding.newFeatureContainer.animate()
            .alpha(0f)
            .setDuration(200L)
            .withEndAction { finish() }
            .start()
    }

    companion object {
        const val EXTRA_OUTPUT_ACTION = "EXTRA_OUTPUT_ACTION"
        const val OUTPUT_ACTION_OPEN_DARK_MODE = "OUTPUT_ACTION_OPEN_DARK_MODE"

        fun shouldOpen() = false
    }
}
