package halamish.expenses.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.preference.PreferenceManager
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.target.ViewTarget
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.channels.SendChannel
import timber.log.Timber


fun FloatingActionButton.setText(text: String){
    setTextBitmap(text, 75f, Color.BLACK)
    scaleType = ImageView.ScaleType.CENTER
    adjustViewBounds = false
}


fun ImageView.setTextBitmap(text: String, textSize: Float, textColor: Int) {
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    paint.textSize = textSize
    paint.color = textColor
    paint.textAlign = Paint.Align.LEFT
    val lines = text.split("\n")
    var maxWidth = 0
    for (line in lines) {
        val width = paint.measureText(line).toInt()
        if (width > maxWidth) {
            maxWidth = width
        }
    }
    val height = paint.descent() - paint.ascent()
    val bitmap = Bitmap.createBitmap(maxWidth, height.toInt() * lines.size, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    var y = - paint.ascent()
    for (line in lines) {
        canvas.drawText(line, 0f, y, paint)
        y += height
    }
    setImageBitmap(bitmap)
}


fun <T, R> LiveData<T>.map(mapperFun: (T)->R): LiveData<R> {
    return Transformations.map(this, mapperFun)
}


fun<T> mutableLiveDataOf(initialValue: T? = null) = MutableLiveData<T?>().apply { value = initialValue }
fun <T> MutableLiveData<T>.readOnly() = this as LiveData<T?>
fun <T> LiveData<T?>.assureNoNulls() = this as LiveData<T>

fun TextView.textBy(data: LiveData<String>, lifecycle: LifecycleOwner){
    data.observe(lifecycle, Observer { text = it ?: "" })
}

fun <T> TextView.textBy(data: LiveData<T>, lifecycle: LifecycleOwner, map: (T?) -> String? = { "$it" }){
    data.observe(lifecycle, Observer { text = map(it) ?: "" })
}

fun TextView.textColorBy(data: LiveData<Int>, lifecycle: LifecycleOwner){
    data.observe(lifecycle, Observer { setTextColor(it) })
}



@SuppressLint("ServiceCast")
fun Activity.hideKeyboard() {
    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = currentFocus
    if (view != null) {
        inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}


fun Fragment.hideKeyboard() {
    activity?.hideKeyboard()
}

@SuppressLint("ServiceCast")
fun Dialog.hideKeyboard() {
    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = currentFocus
    if (view != null) {
        inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

fun EditText.requestFocusAndOpenKeyboard(){
    requestFocus()
    post {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
}


fun EditText.clearFocusAndHideKeyboard(){
    clearFocus()
    post {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}

fun Fragment.currentInnerVisibleFragment() : Fragment? = childFragmentManager.fragments.firstOrNull()

private val displayMetrics get() = Resources.getSystem().displayMetrics

fun Int.dpToPx(): Int = (this * displayMetrics.density).toInt()
fun Int.pxToDp(): Int = (this / displayMetrics.density).toInt()


val Int.f: Float
    get() = toFloat()


fun View.marginBottom(newValueDp: Int){
    (layoutParams as? ViewGroup.MarginLayoutParams)?.let { p ->
        p.updateMargins(bottom = newValueDp.dpToPx())
        requestLayout()
    }
}
fun View.marginTop(newValueDp: Int){
    (layoutParams as? ViewGroup.MarginLayoutParams)?.let { p ->
        p.updateMargins(top = newValueDp.dpToPx())
        requestLayout()
    }
}


val Context.sp: SharedPreferences get() = PreferenceManager.getDefaultSharedPreferences(this)
fun Context.spWithName(name: String): SharedPreferences =
    getSharedPreferences(name, Context.MODE_PRIVATE)

inline fun <reified T> SharedPreferences.getOrSetDefault(key: String, value: T): T {
    when (value) {
        is String -> {
            val stored = getString(key, null)
            if (stored != null)
                return stored as T
            edit().putString(key, value).apply()
            return value
        }

        is Boolean -> {
            if (contains(key))
                return getBoolean(key, value) as T
            edit().putBoolean(key, value).apply()
            return value
        }

        is Int -> {
            if (contains(key))
                return getInt(key, value) as T
            edit().putInt(key, value).apply()
            return value
        }

        else -> {
            val gson = Gson()
            val storedValue = getString(key, null)
            if (storedValue != null)
                return gson.fromJson(storedValue, T::class.java)
            edit().putString(key, gson.toJson(value)).apply()
            return value
        }
    }
}

fun <T> SendChannel<T>.offerIfPossible(element: T) =
    if (!isClosedForSend) trySend(element).isSuccess else false

fun RecyclerView.addDivider(@DrawableRes drawableRes: Int) {
    val drawable = ContextCompat.getDrawable(context, drawableRes) ?: return
    val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL).apply { setDrawable(drawable) }
    addItemDecoration(divider)
}

inline fun <reified T> RequestBuilder<T>.withCallback(crossinline callback: () -> Unit) = addListener(object: RequestListener<T>{
    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<T>?, isFirstResource: Boolean) = false

    override fun onResourceReady(resource: T, model: Any?, target: Target<T>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
        callback(); return false
    }
})


inline fun <reified T> RequestBuilder<T>.withCallbacks(crossinline failureCallback: () -> Unit, crossinline successCallback: () -> Unit) = addListener(object: RequestListener<T>{
    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<T>?, isFirstResource: Boolean): Boolean {
        failureCallback(); return false
    }

    override fun onResourceReady(resource: T, model: Any?, target: Target<T>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
        successCallback(); return false
    }
})

fun <T> Observable<T>.toLiveData() = LiveDataReactiveStreams.fromPublisher(toFlowable(BackpressureStrategy.LATEST))

fun <T : Dialog> T.withAutomaticCancel(lifecycle: Lifecycle): T {
    lifecycle.addObserver(object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun cancelOnPause() { if (isShowing) cancel() }
    })
    return this
}

fun AlertDialog.Builder.showWithCancelOnPause(lifecycle: Lifecycle) =
    create()
        .apply {
            lifecycle.addObserver(object: LifecycleObserver {
                @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
                fun cancelDialog() = if (isShowing) cancel() else Unit
            })
        }
        .show()


var TextView.strikeThrough: Boolean
    get() = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG != 0
    set(value) {
        paintFlags =
            if (value) paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            else paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }


inline fun <T, R> T?.withErrorLog(msg: String, block: () -> R): R {
    Timber.e("error: $msg")
    return block()
}


private var neverDisposed = listOf<Disposable>()
fun Disposable.neverDispose() {
    neverDisposed = neverDisposed.filter { !it.isDisposed } + this
    Timber.d("adding $this to never disposed (list size: ${neverDisposed.size})")
}
