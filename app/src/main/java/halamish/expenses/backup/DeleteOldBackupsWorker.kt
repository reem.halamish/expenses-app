package halamish.expenses.backup

import android.content.Context
import androidx.work.Data
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import halamish.expenses.logger.Logger
import halamish.expenses.v2.appRedux
import halamish.expenses.work.DailyWorker
import halamish.expenses.work.WorkManagerHelper
import kotlinx.coroutines.runBlocking
import org.threeten.bp.Duration
import org.threeten.bp.Instant

class DeleteOldBackupsWorker(context: Context, workerParams: WorkerParameters) :
  DailyWorker(context, workerParams) {


  companion object {
    suspend fun setupNightly(workManager: WorkManager) = WorkManagerHelper.setupNightly<DeleteOldBackupsWorker>(
      workManager,
      tag = "delete_old_back_snapshots",
      input = Data.Builder()
        .putLong(KEY_OLD_ENOUGH_DAYS, 30)
        .build(),
    )

    private const val KEY_OLD_ENOUGH_DAYS = "OLD_ENOUGH_DAYS"
  }

  private val logger = Logger("DeleteOldBackupsRequestWorker")

  override fun doWorkAfterVerifications(): Result {
    logger.d("starting. input: $inputData")

    val oldDays = inputData.getLong(KEY_OLD_ENOUGH_DAYS, -1)
      .takeIf { it >= 0 }
      ?: return Result.failure()

    val cutoff = Instant.now().minus(Duration.ofDays(oldDays))
    val snapshotResult = runBlocking {
       appRedux.services.backupAndRestore.deleteAllAutomaticsBefore(cutoff)
    }

    return if (snapshotResult.isSuccess) {
      Result.success()
    } else {
      Result.failure()
    }
  }
}
