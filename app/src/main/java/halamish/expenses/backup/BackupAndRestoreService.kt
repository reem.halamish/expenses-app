package halamish.expenses.backup

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.Keep
import extensions.android.fromJson
import extensions.android.toJson
import halamish.expenses.BuildConfig
import halamish.expenses.backup.BackupAndRestoreService.Reason
import halamish.expenses.backup.BackupAndRestoreService.Snapshot
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.infra.ShortId
import halamish.expenses.logger.Logger
import halamish.expenses.v2.EventBus
import halamish.expenses.v2.ExpensesData
import halamish.expenses.v2.OverrideExpenseCategories
import halamish.expenses.v2.OverrideExpenseRules
import halamish.expenses.v2.OverrideOneTimeExpenses
import halamish.expenses.v2.sendAwaitingResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.withContext
import org.threeten.bp.Instant
import java.io.File
import java.io.Serializable

interface BackupAndRestoreService {
  @Keep
  data class Snapshot(
    val id: String = "",
    val reason: Reason = Reason.UNKNOWN,
    val timestampUtcMs: Long = 0,
    val snapshotFilePath: String = "",
    val snapshotFileSizeBytes: Long,
    val userIdAtSnapshot: String? = null,
    val projectIdAtSnapshot: String = "",
    val appVersionCodeAtSnapshot: Int = 0,
    val appVersionNameAtSnapshot: String = "",
    val countCategories: Int = 0,
    val countExpenseOneTimes: Int = 0,
    val countExpenseRules: Int = 0,
  ) : Serializable

  @Keep
  enum class Reason {
    UNKNOWN, NIGHTLY, MANUAL, BEFORE_GOOGLE_LOGIN, BEFORE_ACCEPTING_INVITATION,
    BEFORE_APPLYING_SNAPSHOT,
  }


  val snapshots: StateFlow<Set<Snapshot>>
  suspend fun recordSnapshot(reason: Reason): Result<Snapshot>
  suspend fun applySnapshot(snapshot: Snapshot): Result<Unit>
  suspend fun deleteSnapshot(snapshot: Snapshot): Result<Unit>
  suspend fun deleteAllAutomaticsBefore(cutoff: Instant): Result<Int>
  suspend fun deleteAll(): Result<Int>
}

class BackupAndRestoreServiceImpl(
  private val context: Context,
  private val sp: SharedPreferences,
  private val getCurrentUserId: () -> String?,
  private val getCurrentData: () -> ExpensesData,
  private val eventBus: EventBus,
) : BackupAndRestoreService {

  override val snapshots = MutableStateFlow(sp.load())
  private val logger = Logger("BackupAndRestoreService")
  
  override suspend fun recordSnapshot(reason: Reason): Result<Snapshot> {
    logger.d("recordSnapshot - reason($reason)")
    try {
      val id = ShortId.create()
      val timestamp = Instant.now()
      val data = getCurrentData()
      val snapshotFilePathRelative = "snapshot_${id}_${timestamp}.json"
      val snapshotFile = getSnapshotFile(context, snapshotFilePathRelative)
      saveDataToFile(data, snapshotFile)
      val snapshot = Snapshot(
        id = id,
        reason = reason,
        timestampUtcMs = timestamp.toEpochMilli(),
        snapshotFilePath = snapshotFilePathRelative,
        snapshotFileSizeBytes = snapshotFile.length(),
        userIdAtSnapshot = getCurrentUserId(),
        projectIdAtSnapshot = data.project.id,
        appVersionCodeAtSnapshot = BuildConfig.VERSION_CODE,
        appVersionNameAtSnapshot = BuildConfig.VERSION_NAME,
        countCategories = data.categories.size,
        countExpenseOneTimes = data.oneTime.size,
        countExpenseRules = data.rules.size,
      )
      snapshots.update { it + snapshot }
      sp.store(snapshots.value)
      logger.d("created snapshot $snapshot")
      return Result.success(snapshot)
    } catch (e: Exception) {
      logger.e( "error creating snapshot", e)
      return Result.failure(e)
    }
  }

  private suspend fun saveDataToFile(data: ExpensesData, file: File) = withContext(Dispatchers.IO) {
    if (file.exists()) throw RuntimeException("File already exists. path ${file.path}")
    val serializable = ExpensesDataSerializable(
      data.project,
      data.categories,
      data.oneTime,
      data.rules,
    )
    val json = serializable.toJson()
    file.writeText(json, charset = Charsets.UTF_16)
  }

  private suspend fun loadDataFromFile(file: File): ExpensesData = withContext(Dispatchers.IO) {
    if (!file.exists()) throw RuntimeException("File doesn't exists. path ${file.path}")
    val json = file.readText(charset = Charsets.UTF_16)
    val serializable = json.fromJson<ExpensesDataSerializable>()
      ?: throw RuntimeException("couldn't convert to json")

    ExpensesData(
      serializable.project,
      serializable.categories,
      serializable.oneTime,
      serializable.rules,
    )
  }

  private data class ExpensesDataSerializable(
    val project: ProjectDetails = ProjectDetails.EMPTY_INSTANCE,
    val categories: Map<String, ExpenseCategory> = emptyMap(),
    val oneTime: Map<String, OneTimeExpenseRecord> = emptyMap(),
    val rules: Map<String, RecurringExpenseRule> = emptyMap(),
  ) : Serializable

  override suspend fun applySnapshot(snapshot: Snapshot): Result<Unit> {
    logger.d("applySnapshot($snapshot)")

    recordSnapshot(Reason.BEFORE_APPLYING_SNAPSHOT)

    try {
      val currentData = getCurrentData()
      val currentUserId = getCurrentUserId()
      val file = getSnapshotFile(context, snapshot.snapshotFilePath)
      val storedData = loadDataFromFile(file)

      logger.d("currentUserId:$currentUserId, currentProjectId:${currentData.project.id}")
      eventBus.sendAwaitingResponse(
        OverrideExpenseCategories(
          storedData.categories.values
        )
      )
      logger.i("did override to ${storedData.categories.values.size} categories")
      eventBus.sendAwaitingResponse(
        OverrideOneTimeExpenses(
          storedData.oneTime.values
        )
      )
      logger.i("did override to ${storedData.oneTime.values.size} one-time expenses")
      eventBus.sendAwaitingResponse(
        OverrideExpenseRules(
          storedData.rules.values
        )
      )
      logger.i("did override to ${storedData.rules.values.size} recurring expenses")
      logger.d("Done")
      return Result.success(Unit)
    } catch (e: Exception) {
      logger.e("error applying snapshot $snapshot", e)
      return Result.failure(e)
    }
  }

  override suspend fun deleteSnapshot(snapshot: Snapshot): Result<Unit> {
    logger.d("deleteSnapshot($snapshot)")
    try {
      withContext(Dispatchers.IO) {
        val file = getSnapshotFile(context, snapshot.snapshotFilePath)
        if (file.exists()) file.delete()
      }
    } catch (e: Exception) {
      logger.e("error deleting snapshot $snapshot", e)
      return Result.failure(e)
    }
    snapshots.update { it - snapshot }
    sp.store(snapshots.value)
    return Result.success(Unit)
  }

  override suspend fun deleteAll(): Result<Int> {
    return deleteAllGivenPredicate { true }
  }

  override suspend fun deleteAllAutomaticsBefore(cutoff: Instant): Result<Int> {
    return deleteAllGivenPredicate { it.reason != Reason.MANUAL && Instant.ofEpochMilli(it.timestampUtcMs).isBefore(cutoff) }
  }

  private suspend fun deleteAllGivenPredicate(predicate: (Snapshot) -> Boolean): Result<Int> {
    var deleted = 0
    while (snapshots.value.any(predicate)) {
      val snapshot = snapshots.value.firstOrNull(predicate) ?: continue
      val result = deleteSnapshot(snapshot)
      // notify & stop on first failure
      result.exceptionOrNull()?.let { return Result.failure(it) }
      deleted++
    }
    logger.w("Deleted $deleted snapshots")
    return Result.success(deleted)
  }

  companion object {
    private fun SharedPreferences.load(): Set<Snapshot> {
      return all.keys
        .mapNotNull { key -> getString(key, null) }
        .mapNotNull { json -> json.fromJson<Snapshot>() }
        .filter { snapshot -> verifyAllFields(snapshot) }
        .toSet()
    }

    private fun verifyAllFields(snapshot: Snapshot): Boolean {
      val check = kotlin.runCatching {
        requireNotNull(snapshot.id)
        requireNotNull(snapshot.reason)
        requireNotNull(snapshot.timestampUtcMs)
        requireNotNull(snapshot.snapshotFilePath)
        requireNotNull(snapshot.snapshotFileSizeBytes)
        requireNotNull(snapshot.projectIdAtSnapshot)
        requireNotNull(snapshot.appVersionCodeAtSnapshot)
        requireNotNull(snapshot.appVersionNameAtSnapshot)
        requireNotNull(snapshot.countCategories)
        requireNotNull(snapshot.countExpenseOneTimes)
        requireNotNull(snapshot.countExpenseRules)
      }
      return check.isSuccess
    }

    private fun SharedPreferences.store(snapshots: Set<Snapshot>) {
      edit()
        .clear()
        .apply {
          for (snapshot in snapshots) {
            putString(snapshot.id, snapshot.toJson())
          }
        }
        .apply()
    }

    private fun getSnapshotFile(context: Context, relativePath: String): File {
      val snapshotsFolder = File(context.filesDir, "expenses_backup_snapshots")
      if (!snapshotsFolder.exists()) {
        snapshotsFolder.mkdirs()
      }
      return File(snapshotsFolder, relativePath)
    }
  }
}
