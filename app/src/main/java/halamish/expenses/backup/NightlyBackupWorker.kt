package halamish.expenses.backup

import android.content.Context
import androidx.work.Data
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import halamish.expenses.logger.Logger
import halamish.expenses.v2.appRedux
import halamish.expenses.work.DailyWorker
import halamish.expenses.work.WorkManagerHelper
import kotlinx.coroutines.runBlocking

class NightlyBackupWorker(context: Context, workerParams: WorkerParameters) :
  DailyWorker(context, workerParams) {

  companion object {
    suspend fun setupNightly(workManager: WorkManager) = WorkManagerHelper.setupNightly<NightlyBackupWorker>(
      workManager,
      tag = "nightly_backup_snapshot_create",
      input = Data.Builder()
        .putString(KEY_REASON, BackupAndRestoreService.Reason.NIGHTLY.name)
        .build(),
    )
  }

  private val logger = Logger("BackupRequestWorker")

  override fun doWorkAfterVerifications(): Result {
    logger.d("starting. input: $inputData")

    val reasonString = inputData.getString(KEY_REASON) ?: run {
      logger.e("missing reason")
      return Result.failure()
    }

    val reasonEnum = BackupAndRestoreService.Reason.valueOf(reasonString)
    val snapshotResult = runBlocking {
       appRedux.services.backupAndRestore.recordSnapshot(reason = reasonEnum)
    }

    return if (snapshotResult.isSuccess) {
      val snapshot = snapshotResult.getOrThrow()
      logger.d("done! created snapshot $snapshot")
      Result.success(
        Data.Builder()
          .putString(KEY_SNAPSHOT_ID, snapshot.id)
          .build()
      )
    } else {
      logger.e("failed creating snapshot")
      Result.failure()
    }
  }
}

private const val KEY_REASON = "halamish.expenses.backup.BackupRequestWorker.REASON"
private const val KEY_SNAPSHOT_ID = "halamish.expenses.backup.BackupRequestWorker.SNAPSHOT_ID"
