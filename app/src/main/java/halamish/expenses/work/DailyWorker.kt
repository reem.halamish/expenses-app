package halamish.expenses.work

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import halamish.expenses.logger.Logger
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.temporal.ChronoUnit

abstract class DailyWorker(context: Context, workerParams: WorkerParameters) :
  Worker(context, workerParams) {

  private val loggerParent by lazy { Logger("DailyWorker(${tag()}") }

  final override fun doWork(): Result {
    loggerParent.d("doWork() starting. inputData: $inputData")

    val now = Instant.now().atZone(ZoneId.systemDefault())
    val midnight = now.toLocalDate()
      .atStartOfDay()
      .atZone(ZoneId.systemDefault())
    val offsetFromMidnightMinutes = inputData.getLong(KEY_OFFSET_FROM_MIDNIGHT_MINUTES, 0)
    val deadline = midnight.plusMinutes(offsetFromMidnightMinutes)

    if (!now.isAfter(deadline)) {
      val durationToDeadlineMinutes = now.until(deadline, ChronoUnit.MINUTES)
      loggerParent.d("still $durationToDeadlineMinutes minutes until cutoff, rescheduling + early return")
      WorkManagerHelper.rescheduleLater(this, durationToDeadlineMinutes + 1, inputData)
      return defaultResultUponRetry()
    }

    val safeResult = kotlin.runCatching { doWorkAfterVerifications() }

    val tomorrowDeadline = deadline.plusDays(1)
    val durationToRescheduleMinutes = now.until(tomorrowDeadline, ChronoUnit.MINUTES)
    loggerParent.d("did run (resultSuccess: ${safeResult.isSuccess}), rescheduling to run again tomorrow (in $durationToRescheduleMinutes minutes)")
    WorkManagerHelper.rescheduleLater(this, durationToRescheduleMinutes + 1, inputData)
    return safeResult.getOrNull() ?: Result.failure()
  }

  abstract fun doWorkAfterVerifications(): Result

  open fun defaultResultUponRetry() = Result.success()

  internal fun tag() = inputData.getString(KEY_TAG) ?: javaClass.name

  companion object {
    internal const val KEY_OFFSET_FROM_MIDNIGHT_MINUTES = "KEY_OFFSET_FROM_MIDNIGHT_MINUTES"
    internal const val KEY_TAG = "KEY_TAG"
  }
}
