package halamish.expenses.work

import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo.State
import androidx.work.WorkManager
import halamish.expenses.logger.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

object WorkManagerHelper {

  suspend inline fun <reified T: DailyWorker> setupNightly(
    workManager: WorkManager,
    tag: String,
    input: Data = Data.EMPTY,
    replaceExisting: Boolean = false, // set TRUE on debug
    offsetFromMidnightMinutes: Long = 60 * 3, // 3:00AM by default
  ) {
    setupNightly(T::class.java, workManager, tag, input, replaceExisting, offsetFromMidnightMinutes)
  }

  suspend fun <T: DailyWorker> setupNightly(
    type: Class<T>,
    workManager: WorkManager,
    tag: String,
    input: Data = Data.EMPTY,
    replaceExisting: Boolean = false, // set TRUE on debug
    offsetFromMidnightMinutes: Long = 60 * 3, // 3:00AM by default
  ) {

    // logic: start immediately and let the worker reschedule if needed.
    // I tried using workManager.enqueueUniquePeriodicWork() but it didn't work for some reason.
    // see in git history.

    withContext(Dispatchers.IO) schedule@{
      val existing = workManager.getWorkInfosByTag(tag).get(Long.MAX_VALUE, TimeUnit.MINUTES)
        .filter { it.state in listOf(State.ENQUEUED, State.RUNNING) }

      logger.d("setupNightly(existingWorkers:${existing.size} tag:$tag)")
      if (existing.isNotEmpty() && !replaceExisting) {
        return@schedule
      } else if (existing.isNotEmpty()) {
        workManager.cancelAllWorkByTag(tag).result.get(Long.MAX_VALUE, TimeUnit.MINUTES)
        logger.d("did enqueue all work by tag $tag")
      }

      val request = OneTimeWorkRequest.Builder(type)
        .setInputData(
          Data.Builder()
            .putAll(input)
            .putString(DailyWorker.KEY_TAG, tag)
            .putLong(DailyWorker.KEY_OFFSET_FROM_MIDNIGHT_MINUTES, offsetFromMidnightMinutes)
            .build()
        )
        .addTag(tag)
        .build()

      workManager.enqueue(request).result.get(Long.MAX_VALUE, TimeUnit.MINUTES)
      logger.d("did enqueue first worker of type ${type.name} tag $tag")
    }
  }

  internal fun rescheduleLater(worker: DailyWorker, minutesFromNow: Long, input: Data) {
    val workManager = WorkManager.getInstance(worker.applicationContext)
    val request = OneTimeWorkRequest.Builder(worker.javaClass)
      .setInputData(Data(input))
      .setInitialDelay(minutesFromNow, TimeUnit.MINUTES)
      .addTag(worker.tag())
      .build()

    workManager.enqueue(request)
    logger.d("did enqueue worker of type ${worker.javaClass} delay $minutesFromNow minutes")
  }

  private val logger = Logger("WorkManagerHelper")
}