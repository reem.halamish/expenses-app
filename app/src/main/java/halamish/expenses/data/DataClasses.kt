package halamish.expenses.data

import android.graphics.Color
import android.net.Uri
import androidx.annotation.ColorRes
import androidx.annotation.Keep
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.ServerTimestamp
import halamish.expenses.infra.ShortId
import halamish.expenses.repo.CategoriesColorsProvider
import halamish.expenses.repo.PkData
import halamish.expenses.v2.SESSION_START
import halamish.expenses.v2.appRedux
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import java.io.Serializable
import java.util.*
import kotlin.reflect.KProperty0


sealed interface ExpensesDbItem {
  val categoryId: String
  val comments: String?
  val amount: Double
}

@Keep
data class RecurringExpenseRule(
  val id: String = ShortId.create(),
  override val amount: Double = 0.0,
  override val categoryId: String = "",
  val startLong: Long = distantPast.toEpochDay(),
  val endLong: Long = distantFuture.toEpochDay(),
  val repeatStrategy: ExpenseRuleRepeat = ExpenseRuleRepeat.MONTHLY,
  override val comments: String? = null,
  override var deleted: Boolean = false,
  override var lastEditBy: String = "",
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,
) : PkData(), ExpensesDbItem {
  override fun pk() = id

  fun dateStart(): LocalDate = LocalDate.ofEpochDay(startLong)
  fun dateEnd(): LocalDate = LocalDate.ofEpochDay(endLong)

  @Exclude
  fun isForever() = dateEnd() == distantFuture

  override fun allNullableFields(): List<KProperty0<Any?>> = listOf(this::comments)
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = emptyList()
}

@Keep
data class OneTimeExpenseRecord(
  val id: String = ShortId.create(),
  override val amount: Double = 0.0,
  val dateLong: Long = 0,
  override val categoryId: String = "",
  override val comments: String? = null,
  override var deleted: Boolean = false,
  override var lastEditBy: String = "",
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,
) : PkData(), ExpensesDbItem {
  override fun pk() = id

  fun date(): LocalDate = LocalDate.ofEpochDay(dateLong)

  override fun allNullableFields(): List<KProperty0<Any?>> = listOf(this::comments)
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = emptyList()
}


data class ExpenseEvent(
  val id: String = ShortId.create(),
  val amount: Double,
  val date: LocalDate,
  val categoryId: String,
  val recurring: ExpenseEventRecurringInfo? = null,
  val dbId: String = "",
  val comments: String? = null
)

data class ExpenseEventRecurringInfo(
  val totalOccurrences: Long,
  val currentIndex: Long, // running from 0
  val forever: Boolean
)

@Keep
data class ExpenseCategory(
  val id: String = ShortId.create(),
  val displayName: String = "",
  val icon: String = "question-mark",
  override var deleted: Boolean = false,
  override var lastEditBy: String = "",
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,

  // DONT USE THIS DIRECTLY!!
  // we stored the color to server, now we can't get rid of it.
  // even though the color doesn't change between day/night modes.
  // please use `ContextCompat.getColor(context, category.resColor())` instead
  @Deprecated(message = "dont use the hardcoded day-mode value", replaceWith = ReplaceWith("ContextCompat.getColor(context, resColor())"))
  val color: Int = Color.GRAY,

  ) : PkData() {
  override fun pk() = id

  override fun allNullableFields(): List<KProperty0<Any?>> = emptyList()
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = emptyList()

  @Suppress("DEPRECATION") // this is the replacement
  @ColorRes fun resColor(categoriesColorsProvider: CategoriesColorsProvider = appRedux.services.categoriesColorsProvider): Int {
    return categoriesColorsProvider.resolveStoredColor(this.color)
  }
}

@Keep
enum class ExpenseRuleRepeat {
  DAILY, WEEKLY, MONTHLY
}

@Keep
data class ProjectDetails(
  val id: String = ShortId.create(),
  val creator: String = "", // user-id of the owner
  val devices: Map<String, Date> = emptyMap(), // device-id --> last update
  val users: Map<String, String> = emptyMap(), // user-id --> email
  val settings: MutableMap<String, String> = mutableMapOf(), // setting-id -> repr(bool\int\str) // TODO IMMUTABLE!
  val fcmTokensByDeviceIds: Map<String, String> = emptyMap(),
  val excludeNotifyDeviceId: Map<String, Boolean> = emptyMap(), // device-id --> TRUE
  val deviceNameByDeviceId: Map<String, String> = emptyMap(),
  val deviceModelByDeviceId: Map<String, String> = emptyMap(),
  val userIdByDeviceId: Map<String, String> = emptyMap(),
  val invitationLinks: Map<String, ProjectInvitationLink> = emptyMap(),
  val history: Map<String, String> = emptyMap(),
  override var deleted: Boolean = false,
  override var lastEditBy: String = "",
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,
) : PkData() {

  override fun allNullableFields(): List<KProperty0<Any?>> = emptyList()
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = listOf(
    this::devices,
    this::users,
    this::settings,
    this::fcmTokensByDeviceIds,
    this::excludeNotifyDeviceId,
    this::deviceNameByDeviceId,
    this::deviceModelByDeviceId,
    this::userIdByDeviceId,
    this::invitationLinks,
    this::history,
  )

  companion object {
    val EMPTY_INSTANCE = ProjectDetails(id = "NOT_INITIALIZED_YET")
    fun createClean(deviceId: String, creationReason: String) = ProjectDetails(
      id = ShortId.create(),
      devices = mapOf(deviceId to SESSION_START),
      settings = mutableMapOf("creation_reason" to creationReason),
    )
  }

  // deep copy for the mutable hash map
  // TODO: remove this mutability!!!!!!!
  fun clone(): ProjectDetails {
    return copy(settings = HashMap(this.settings))
  }

  override fun pk() = id

  @delegate:Transient
  @get:Exclude
  var currencyCode: String? by mapDelegator("currency_code")

  @delegate:Transient
  @get:Exclude
  var dateFormatPattern: String? by mapDelegator("date_format")

  @delegate:Transient
  @get:Exclude
  var shareButtonLastDismiss: Instant? by mapDelegator("share_btn_last_dismiss")

  @delegate:Transient
  @get:Exclude
  var defineBudgetLastDismiss: Instant? by mapDelegator("define_budget_last_dismiss")

  @delegate:Transient
  @get:Exclude
  var monthlyBudget: Int? by mapDelegator("budget")

  @delegate:Transient
  @get:Exclude
  var hideEmptyCategoriesInMajorExpensesList: Boolean by mapDelegatorWithDefault(
    "hide_empty_categories",
    default = false
  )

  private inline fun <reified T> format(field: String, value: T?): String {
    val valueString =
      if (value is Map<*, *>) value.keys.toString()
      else if (value is Collection<*>) value.size.toString()
    else value?.toString()
    if (valueString in listOf(null, "", "0", "[]", "{}", "false")) return ""
    return "$field:$value, "
  }

  override fun toString(): String {
    return "Project(" +
    format("id", id) +
    format("creator", creator) +
    format("devices", devices) +
    format("users", users) +
    format("settings", settings) +
    format("fcmTokensByDeviceIds", fcmTokensByDeviceIds) +
    format("excludeNotifyDeviceId", excludeNotifyDeviceId) +
    format("deviceNameByDeviceId", deviceNameByDeviceId) +
    format("deviceModelByDeviceId", deviceModelByDeviceId) +
    format("userIdByDeviceId", userIdByDeviceId) +
    format("invitationLinks", invitationLinks) +
    format("history", history) +
    format("deleted", deleted) +
            ")"
  }

  fun withHistoryEntry(msg: String): ProjectDetails {
    val timestamp = Instant.now().epochSecond.toString()
    return copy(history = history + (timestamp to msg))
  }
}


@Keep
data class ProjectInvitationLink(
  val id: String = "",
  val longLink: String = "",
  val shortLink: String? = null,
  val referrerName: String = ""
): Serializable { companion object }
val ProjectInvitationLink.link: String get() = shortLink ?: longLink
val ProjectInvitationLink.longLinkUri: Uri get() = Uri.parse(longLink)
val ProjectInvitationLink.Companion.URI_ARG_REFERRER_NAME get() = "referrerName"
val ProjectInvitationLink.Companion.URI_ARG_PROJECT_ID get() = "projectId"

@Keep
data class UserInfo(
  val uid: String = "",
  val email: String? = null,
  val projectName: String = "", // this is projectId. keeping old var name
  val devices: Map<String, Boolean> = emptyMap(), // devices for this user. more data on the devices is in the project
  override var deleted: Boolean = false,
  override var lastEditBy: String = "",
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,
) : PkData() {
  override fun pk() = uid
  override fun allNullableFields(): List<KProperty0<Any?>> = listOf(this::email)
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = listOf(this::devices)
}


@Keep
enum class BackRequestType{
  DELETE_USER_DATA
}

@Keep
sealed class BackendRequest(val type:BackRequestType): PkData()

@Keep
data class RequestUserDataDelete(
  val deviceIdRequester: String = "", // the device id who requested this request
  val requestId: String = "",
  val userId: String = "", // the user to delete
  val projectId: String? = null, // optional: user's expenses project
  override var deleted: Boolean = false, // ignored
  override var lastEditBy: String = "", // ignored
  val status: Status = Status.REQUESTED,
  @ServerTimestamp override var created: Timestamp? = null,
  @ServerTimestamp override var edit: Timestamp? = null,
  ): BackendRequest(BackRequestType.DELETE_USER_DATA) {
  enum class Status {
    REQUESTED, // requested by client
    STARTED, // server started deletion
    COMPLETED, // server completed deletion
    IRRELEVANT, // this user is not deleted anymore (was re-created)
  }

  override fun pk() = requestId
  override fun allNullableFields(): List<KProperty0<Any?>> = listOf(this::projectId)
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = emptyList()

}

object Defaults {
  const val currencyCode = "USD"
  const val localDateFormatPattern = "dd-MM-yy"
}
