package halamish.expenses.data

import android.net.Uri
import extensions.android.addressInMemory
import org.threeten.bp.Instant
import timber.log.Timber
import kotlin.reflect.KProperty


private val serializeByType = mapOf<Class<*>, (Any) -> String>(
    Boolean::class.java to { v -> if (v == true) "true" else "false" },
    Int::class.java to { v -> "$v" },
    Double::class.java to { v -> "$v" },
    Long::class.java to { v -> "$v" },
    String::class.java to { v -> "$v" },
    java.lang.Boolean::class.java to { v -> if (v == true) "true" else "false" },
    java.lang.Integer::class.java to { v -> "$v" },
    java.lang.Double::class.java to { v -> "$v" },
    java.lang.Long::class.java to { v -> "$v" },
    java.lang.String::class.java to { v -> "$v" },
    Instant::class.java to { v -> "${(v as Instant).toEpochMilli()}" },
    Uri::class.java to { v -> v.toString() }
)

private val deserializeByType = mapOf<Class<*>, (String) -> Any>(
    String::class.java to { s -> s },
    java.lang.String::class.java to { s -> s },

    Boolean::class.java to { s -> s == "true" },
    java.lang.Boolean::class.java to { s -> s == "true" },

    Int::class.java to { s -> s.toInt() },
    java.lang.Integer::class.java to { s -> s.toInt() },

    Double::class.java to { s -> s.toDouble() },
    java.lang.Double::class.java to { s -> s.toDouble() },

    Long::class.java to { s -> s.toLong() },
    java.lang.Long::class.java to { s -> s.toLong() },

    Instant::class.java to { s -> Instant.ofEpochMilli(s.toLong()) },

    Uri::class.java to { s -> Uri.parse(s) }

)


// helper class for ProjectDetails
@Suppress("UNCHECKED_CAST")
internal class MapDelegator<T>(
    val cls: Class<T>,
    val map: () -> MutableMap<String, String>,
    val key: String
) {
    operator fun getValue(thisRef: Any, property: KProperty<*>): T? {
        val map = map()
        val stored = map[key] ?: return null
        val deserialize = deserializeByType[cls] ?: error("can't find deserializer for $cls")
        return stored.let(deserialize) as? T?
    }

    operator fun setValue(thisRef: Any, property: KProperty<*>, value: T?) {
        val map = map()
        if (value == null) {
          Timber.d("serialized: will remove (key=$key, prev=${map[key]}, map:${map.addressInMemory})")
          map.remove(key)
        } else {
            val serialize = serializeByType[cls] ?: error("can't find serializer for $cls")
            val string = serialize(value)
          Timber.d("serialized (key=$key, raw=$value, serialized=$string, map:${map.addressInMemory})")
            map[key] = string
        }
    }
}


internal class MapDelegatorWithDefault<T>(
    private val delegator: MapDelegator<T>,
    private val default: T
) {

    operator fun getValue(thisRef: Any, property: KProperty<*>): T {
        return delegator.getValue(thisRef, property) ?: default
    }

    operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) =
        if (value == default) // remove
            delegator.setValue(thisRef, property, null)
        else // store
            delegator.setValue(thisRef, property, value)
}

internal inline fun <reified T> ProjectDetails.mapDelegator(key: String): MapDelegator<T> =
    MapDelegator(T::class.java, { settings }, key)

internal inline fun <reified T> ProjectDetails.mapDelegatorWithDefault(key: String, default: T)
: MapDelegatorWithDefault<T> =
    MapDelegatorWithDefault(MapDelegator(T::class.java, { settings }, key), default)