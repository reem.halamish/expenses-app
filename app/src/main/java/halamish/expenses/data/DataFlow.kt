package halamish.expenses.data

import com.google.gson.Gson
import halamish.expenses.data.ExpenseRuleRepeat.*
import halamish.expenses.v2.appRedux
import org.threeten.bp.LocalDate


val distantPast = LocalDate.of(1970, 1, 1)
val distantFuture = LocalDate.of(2500, 1, 1)

fun granulateExpenses(
    rules: Collection<RecurringExpenseRule> = appRedux.state.value.data.rules.values,
    oneTimeRecords: Collection<OneTimeExpenseRecord> = appRedux.state.value.data.oneTime.values,
    startDate: LocalDate = distantPast,
    endDate: LocalDate = distantFuture
): List<ExpenseEvent> {
    val expenses = mutableListOf<ExpenseEvent>()
    val startLong = startDate.toEpochDay()
    val endLong = endDate.toEpochDay()

    expenses.addAll(oneTimeRecords
        .filter { it.dateLong in startLong..endLong }
        .map {
            ExpenseEvent(
                it.id, it.amount, it.date(), it.categoryId,
                dbId = it.id, comments = it.comments
            )
        })

    fun advanceDate(rule: RecurringExpenseRule, currentDate: LocalDate) = when (rule.repeatStrategy) {
        DAILY -> currentDate.plusDays(1)
        WEEKLY -> currentDate.plusWeeks(1)
        MONTHLY -> {
            currentDate.plusMonths(1)
                .let { // if original day of month (dom) was 31, after February it gets set to 29, so we need to revert it to 30
                it.withDayOfMonth(minOf(it.month.length(it.isLeapYear), maxOf(it.dayOfMonth, rule.dateStart().dayOfMonth)))
            }
        }
    }

    for (rule in rules) {
        // if this rule is too old or too new for this period, ignore it
        if (rule.dateStart() > endDate ||
            rule.dateEnd() < startDate
        ) {
            continue // date irrelevant
        }

        val occourences = rule.repeatAmount
        var date = rule.dateStart()
        var index = 0L


        while (date < startDate) {
            date = advanceDate(rule, date)
            index++
        }
        while (date in startDate..endDate && date <= rule.dateEnd()) {
            val expenseId = rule.id + "_" + date.toString()
            val recurringData = ExpenseEventRecurringInfo(
                totalOccurrences = occourences,
                currentIndex = index,
                forever = rule.isForever()
            )
            expenses.add(
                ExpenseEvent(
                    expenseId, rule.amount, date, rule.categoryId,
                    recurring = recurringData,
                    dbId = rule.id, comments = rule.comments
                )
            )
            date = advanceDate(rule, date)
            index++
        }
    }

    return expenses
}

val RecurringExpenseRule.repeatAmount: Long
    get() {
        return 1 + calcRepetitions(dateStart(), repeatStrategy, dateEnd())
    }

fun calcEndDate(startDate: LocalDate, repeat: ExpenseRuleRepeat, occourences: Long): LocalDate {
    val repeatitions = occourences - 1

    return when (repeat) {
        DAILY -> startDate.plusDays(repeatitions)
        WEEKLY -> startDate.plusWeeks(repeatitions)
        MONTHLY -> startDate.plusMonths(repeatitions)
    }
}

fun RecurringExpenseRule.withClampToEndDate(newEndDate: LocalDate): RecurringExpenseRule {
    return copy(endLong = newEndDate.toEpochDay())
}

fun RecurringExpenseRule.withClampToBeforeEndDate(newEndDate: LocalDate): RecurringExpenseRule {
    var endDateToUse = dateStart()
    var lastEndDate = endDateToUse
    while (endDateToUse < newEndDate) {
        lastEndDate = endDateToUse
        endDateToUse = when (repeatStrategy) {
            DAILY -> endDateToUse.plusDays(1)
            WEEKLY -> endDateToUse.plusWeeks(1)
            MONTHLY -> endDateToUse.plusMonths(1)
                .let { // if original day of month (dom) was 31, after February it gets set to 29, so we need to revert it to 30
                    it.withDayOfMonth(
                        minOf(
                            it.month.length(it.isLeapYear),
                            maxOf(it.dayOfMonth, dateStart().dayOfMonth)
                        )
                    )
                }
        }
    }
    return copy(endLong = lastEndDate.toEpochDay())
}


fun calcRepetitions(startDate: LocalDate, repeat: ExpenseRuleRepeat, endDate: LocalDate): Long {
    return when {
        endDate.isBefore(startDate) -> 0
        endDate.isEqual(startDate) -> 0
        endDate == distantFuture -> Long.MAX_VALUE
        else -> when (repeat){
            DAILY -> (endDate.dayOfYear - startDate.dayOfYear + (365 * (endDate.year - startDate.year))).toLong()
            WEEKLY -> calcRepetitions(startDate, DAILY, endDate) / 7
            MONTHLY -> (endDate.month.value - startDate.month.value + (12 * (endDate.year - startDate.year))).toLong()
        }
    }
}



// merging all nullable fields. on collisions always selects right \ left based on the boolean
fun <T: Any> mergeLeftRight(left: T, right: T, preferRightOnCollisions: Boolean = true): T {
    val tClass = left::class.java // could also be right, not matter
    val gson = Gson()
    val rightMap = gson.fromJson(gson.toJson(right), Map::class.java)
    val leftMap = gson.fromJson(gson.toJson(left), Map::class.java)
    val p1 = if (preferRightOnCollisions) leftMap else rightMap
    val p2 = if (preferRightOnCollisions) rightMap else leftMap
    val mergedMap = p1 + p2
    return gson.fromJson(gson.toJson(mergedMap), tClass)
}

