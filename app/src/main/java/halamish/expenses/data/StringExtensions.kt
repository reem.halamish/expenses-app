package halamish.expenses.data

import java.text.DecimalFormatSymbols
import java.util.*

private val decimalSep get() = DecimalFormatSymbols(Locale.getDefault()).decimalSeparator.toString() // USD: .
private val thousandSep get() = DecimalFormatSymbols(Locale.getDefault()).groupingSeparator.toString() // USD: ,


fun String.cleanedNumberWithLocale(): String {

    if (this.isEmpty())
        return "0"

    var cleaned = this.replace(thousandSep, "").replace(decimalSep, ".")

    if (cleaned.startsWith(decimalSep))
        cleaned = "0$cleaned"

    if (cleaned.endsWith(decimalSep))
        cleaned = cleaned.replace(decimalSep, "")

    val asDouble = cleaned.toDoubleOrNull() ?: return "" // if can't convert, number is no good

    if (asDouble == asDouble.toInt().toDouble()) // if ends with ".0000", clear after dot
        return asDouble.toInt().toString()

    return "%.2f".format(asDouble).replace(".", decimalSep)
}

fun String.cleanedNumberDouble(): Double? {
    return cleanedNumberWithLocale().replace(decimalSep, ".").toDoubleOrNull()
}

fun String.cleanedEmail(): String {
    val split = split("@")
    require(split.size == 2) { "bad string $this" }

    val domain = split.last()
    var username = split.first()

    username = username
        .lowercase(Locale.getDefault())
        .replace(".", "")

    if (username.contains("+"))
        username = username.split("+").first()

    return "$username@$domain"
}

// clean so we can use this string as a substring in an "id" field
fun String.cleanedId(): String {
    return replace(" ", "_")
        .replace(Regex("[^A-Za-z0-9_.,@]"), "")
}