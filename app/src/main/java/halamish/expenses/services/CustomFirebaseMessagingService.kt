package halamish.expenses.services

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import halamish.expenses.v2.GotFcmMessage
import halamish.expenses.v2.NewFcmTokenEvent
import halamish.expenses.v2.appRedux
import timber.log.Timber

class CustomFirebaseMessagingService : FirebaseMessagingService() {

  override fun onNewToken(token: String) {
    Timber.i("got new fcm token:${token}")
    super.onNewToken(token)
    appRedux.eventBus.send(NewFcmTokenEvent(token))
  }

  override fun onMessageReceived(msg: RemoteMessage) {
    Timber.i("got msg. data:${msg.data}, msg:$msg")
    appRedux.eventBus.send(GotFcmMessage(msg.data.toMap())) // copy
  }
}
