package halamish.expenses.app

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface AppLifecycleState {
  val lifecycleState: StateFlow<Lifecycle.State>
}

class AppLifecycleStateImpl: AppLifecycleState {

  override val lifecycleState = MutableStateFlow(Lifecycle.State.INITIALIZED)

  init {
    ProcessLifecycleOwner.get().lifecycle.addObserver(object : DefaultLifecycleObserver {
      override fun onCreate(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.CREATED
      }
      override fun onStart(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.STARTED
      }
      override fun onResume(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.RESUMED
      }

      override fun onPause(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.STARTED
      }

      override fun onStop(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.CREATED
      }

      override fun onDestroy(owner: LifecycleOwner) {
        lifecycleState.value = Lifecycle.State.DESTROYED
      }
    })
  }
}
