package halamish.expenses.logger

import android.content.Context
import androidx.work.Data
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import halamish.expenses.work.DailyWorker
import halamish.expenses.work.WorkManagerHelper
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.io.File

class DeleteOldLoggerFilesWorker(context: Context, workerParams: WorkerParameters) :
  DailyWorker(context, workerParams) {

  companion object {
    suspend fun setupNightly(workManager: WorkManager) = WorkManagerHelper.setupNightly<DeleteOldLoggerFilesWorker>(
      workManager,
      tag = "delete_old_log_files",
      input = Data.Builder()
        .putLong(KEY_OLD_ENOUGH_DAYS, 30)
        .build(),
    )

    private const val KEY_OLD_ENOUGH_DAYS = "OLD_ENOUGH_DAYS"
  }


  private val logger = Logger("DeleteOldLoggerFilesWorker")

  override fun doWorkAfterVerifications(): Result {
    logger.d("starting. input: $inputData")

    val oldDays = inputData.getLong(KEY_OLD_ENOUGH_DAYS, -1)
      .takeIf { it > 0 }
      ?: return Result.failure()


    val logsFolder = File(applicationContext.filesDir, FileLoggerTree.DEFAULT_LOGS_FOLDER)
    if (!logsFolder.exists() || !logsFolder.isDirectory) {
      logger.d("folder doesn't exist, nothing to do - $logsFolder")
      return Result.success()
    }


    val cutoff = Instant.now().minus(Duration.ofDays(oldDays))
    val filesToDelete = (logsFolder.listFiles() ?: emptyArray())
      .filterNotNull()
      .filter { Instant.ofEpochMilli(it.lastModified()).isBefore(cutoff) }

    if (filesToDelete.isEmpty()) {
      logger.d("no old files to delete")
      return Result.success()
    }

    logger.w("found ${filesToDelete.size} old log files, will delete them")
    filesToDelete.forEach { file ->
      try {
        val successDelete = file.delete()
        logger.w("did delete (success:$successDelete) log file at ${file.path}")
      } catch (e: Exception) {
        logger.e("error deleting file ${file.path}", e)
      }
    }

    return Result.success()
  }
}
