package halamish.expenses.logger

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkInfo.State.SUCCEEDED
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import halamish.expenses.infra.ShortId
import halamish.expenses.v2.appRedux
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class GenerateLogZipFileWorker(context: Context, workerParams: WorkerParameters) :
  Worker(context, workerParams) {

  private val logger = Logger("GenerateLogZipFileWorker")

  override fun doWork(): Result {
    val maxFileSizeMb = inputData.getInt(KEY_MAX_FILE_SIZE_MB, Int.MAX_VALUE)
    val logsFolder = File(applicationContext.filesDir, FileLoggerTree.DEFAULT_LOGS_FOLDER)
    if (!logsFolder.exists() || !logsFolder.isDirectory) {
      return Result.success() // always return success.
    }

    val files = (logsFolder.listFiles() ?: emptyArray())
      .filterNotNull()
      .sortedByDescending { it.lastModified() }

    logger.d("total ${files.size} log files")

    val maxSizeBytes = maxFileSizeMb * 1024L * 1024L
    var currentSize = 0L
    val filesToUse = mutableListOf<File>()
    for (file in files) {
      currentSize += file.length()
      if (currentSize <= maxSizeBytes) {
        filesToUse += file
      }
    }

    logger.d("will zip ${filesToUse.size} files")
    if (filesToUse.isEmpty()) return Result.success()

    val zipFile = createZipFile(applicationContext, filesToUse)
    return Result.success(
      Data.Builder()
        .putString(KEY_OUTPUT_PATH_ABSOLUTE, zipFile.path)
        .build()
    )
  }

  private fun createZipFile(context: Context, filesToUse: List<File>): File {
    val logsZipFolder = File(context.cacheDir, "app_logs")
    if (!logsZipFolder.exists()) {
      logsZipFolder.mkdirs()
    }

    val projectId = appRedux.state.value.project.id
    val outputZipFile =
      File(logsZipFolder, "app_logs.${projectId}.${ShortId.create(length = 4)}.zip")
    if (outputZipFile.exists()) {
      outputZipFile.delete()
    }

    outputZipFile.createNewFile()

    ZipOutputStream(BufferedOutputStream(FileOutputStream(outputZipFile))).use { zos ->
      filesToUse.forEach { file ->
        zos.putNextEntry(ZipEntry(file.name))
        if (file.isFile) {
          file.inputStream().use { fis -> fis.copyTo(zos) }
        }
      }
    }

    logger.d("did create zip file with ${filesToUse.size} files, total size: ${(outputZipFile.length() / (1024.0 * 1024.0))} MB")
    return outputZipFile
  }


  companion object {

    private fun <T> fail(msg: String) = kotlin.Result.failure<T>(RuntimeException(msg))

    suspend fun generateZip(maxFileSizeMb: Int = 10): kotlin.Result<Uri> {
      val context = appRedux.app
      val request = OneTimeWorkRequestBuilder<GenerateLogZipFileWorker>()
        .setInputData(
          Data.Builder()
            .putInt(KEY_MAX_FILE_SIZE_MB, maxFileSizeMb)
            .build()
        )
        .build()

      val workManager = WorkManager.getInstance(context)
      workManager.enqueue(request)

      // await success
      val output = CompletableDeferred<Data>()
      val observer = Observer<WorkInfo> {
        if (it.state == SUCCEEDED) {
          output.complete(it.outputData)
        }
      }
      val liveData = workManager.getWorkInfoByIdLiveData(request.id)
      withContext(Dispatchers.Main) {
        liveData.observeForever(observer)
      }
      val data = output.await()
      withContext(Dispatchers.Main) {
        liveData.removeObserver(observer)
      }
      val filePath = data.getString(KEY_OUTPUT_PATH_ABSOLUTE)
        ?: return fail("no output file path")
      val uri: Uri = FileProvider.getUriForFile(context, context.packageName, File(filePath))
        ?: return fail("couldn't create URI from file $filePath")
      return kotlin.Result.success(uri)
    }

    private const val KEY_MAX_FILE_SIZE_MB = "KEY_MAX_FILE_SIZE_MB"
    private const val KEY_OUTPUT_PATH_ABSOLUTE = "KEY_OUTPUT_PATH_ABSOLUTE"
  }
}