package halamish.expenses.logger

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class FileLoggerTree(private val folder: File, scope: CoroutineScope): Timber.Tree() {
  constructor(context: Context, scope: CoroutineScope, folderPath: String = DEFAULT_LOGS_FOLDER): this(File(context.filesDir, folderPath), scope)

  private val requests = MutableSharedFlow<String>(extraBufferCapacity = 1024)

  init {
    scope.launch {
      folder.mkdirs()
      requests.collect { msg ->
        with(calculateLogFile(folder, Date())) {
          if (!exists()) createNewFile()
          appendText(msg + "\n")
        }
      }
    }
  }

  override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
    val timestamp = calculateEntryTime(Date())
    val line = if (tag != null) "$timestamp [$tag] $message" else "$timestamp $message"
    requests.tryEmit(line)
    if (t != null) {
      requests.tryEmit("    $t")
    }
  }

  companion object {
    const val DEFAULT_LOGS_FOLDER = "logs"
  }
}



private val dateFormatter by lazy { SimpleDateFormat("yyyy.MM.dd", Locale.getDefault()) }
private fun calculateLogFile(parent: File, date: Date) : File {
  val filename =  "log_" + dateFormatter.format(date) + ".txt"
  return File(parent, filename)
}

private val timeFormatter by lazy { SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault()) }
private fun calculateEntryTime(date: Date): String { return  timeFormatter.format(date) }