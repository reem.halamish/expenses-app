package halamish.expenses.logger
import timber.log.Timber

class Logger(val tag: String) {
  fun d(msg: String) = Timber.tag(tag).d(msg)
  fun i(msg: String) = Timber.tag(tag).i(msg)
  fun w(msg: String) = Timber.tag(tag).w(msg)
  fun e(msg: String) = Timber.tag(tag).e(msg)
  fun e(msg: String, throwable: Throwable?) = Timber.tag(tag).e(throwable, msg)
}
