package halamish.expenses.repo.handlers

import halamish.expenses.backup.BackupAndRestoreService
import halamish.expenses.data.RequestUserDataDelete
import halamish.expenses.infra.ShortId
import halamish.expenses.v2.*
import halamish.expenses.v2.V2FirestoreRepo.Companion.doBackendRequest
import kotlinx.coroutines.flow.*
import timber.log.Timber


/*
when QA'ing, we have a few flows to check:
1. user not logged in, user click settings "delete all data" --> all local data deleted
2. user is logged in, user click settings "delete all data" --> all local data deleted + BE request to delete user's info from `user/` collection in firestore + if anonymous, delete auth data as well
3. user is logged in, kill app, simulate request to delete user + delete user info from firestore 'users/' collection --> on next app launch user should be automatically disconnected + all local data deleted
4. user not logged in, after was "delete all data" fullfilled, logging in again to their account --> should successfully login and automatically upload their expenses data they created meanwhile
 */
interface DeleteAllUserDataHandler {
    val stateFlow: Flow<State>

    suspend fun deleteAllUserData(useCase: String): Boolean

    data class State(
        val userDataToDelete: UserDataToDelete,
        val enabled: Enabled
    )

    data class UserDataToDelete(
        val expensesCount: Int,
        val hasCollaborators: Boolean,
        val snapshotsCount: Int,
    )

    sealed class Enabled {
        object CanDeleteAllData: Enabled()
        data class DisabledOnlyCollaborating(val ownerUserPrettyString: String?): Enabled()
        object DisabledWorkingOnIt: Enabled()
    }
}

class DeleteAllUserDataHandlerImpl(
    private val appState: StateFlow<V2State>,
    private val eventBus: EventBus,
    private val firestoreRepo: V2FirestoreRepo,
    private val backup: BackupAndRestoreService,
    private val localDataDeleteHandler: LocalDataDeleteHandler,
): DeleteAllUserDataHandler {

    private val busy = MutableStateFlow(false)

    private suspend fun awaitUserLoggedOut() = appState.map { when (it.user) {
        is LoggedInUser -> true
        NotLoggedIn -> false
    } }
        .first { !it }

    override val stateFlow: Flow<DeleteAllUserDataHandler.State>
        get() = combine(busy, appState, backup.snapshots) { isBusy, appState, snapshots ->
            DeleteAllUserDataHandler.State(
                userDataToDelete = getAllUserDataToDelete(appState, snapshots),
                enabled = calculateEnabled(isBusy, appState)
            )
        }.distinctUntilChanged()

    private fun getAllUserDataToDelete(
        appState: V2State,
        snapshots: Set<BackupAndRestoreService.Snapshot>
    ): DeleteAllUserDataHandler.UserDataToDelete {
        val expensesData = appState.data
        val userId = appState.userInfo?.uid
        expensesData.project.users
        return DeleteAllUserDataHandler.UserDataToDelete(
            expensesCount = expensesData.rules.size + expensesData.oneTime.size,
            hasCollaborators = expensesData.project.users.keys.any { it != userId },
            snapshotsCount = snapshots.size,
        )
    }

    private fun calculateEnabled(isBusy: Boolean, appState: V2State): DeleteAllUserDataHandler.Enabled {
        val userId = appState.userInfo?.uid
        return when {
            isBusy -> DeleteAllUserDataHandler.Enabled.DisabledWorkingOnIt
            userId != null && userId != appState.project.creator ->
                DeleteAllUserDataHandler.Enabled.DisabledOnlyCollaborating(
                ownerUserPrettyString = appState.project.users[appState.project.creator]
            )
            else -> DeleteAllUserDataHandler.Enabled.CanDeleteAllData
        }
    }

    override suspend fun deleteAllUserData(useCase: String): Boolean {
        busy.value = true

        Timber.d("deleteAllUserData(useCase:$useCase)")

        fun <T> Result<T>.handleIsError(failureMsg: String): Boolean {
            exceptionOrNull()?.let {
                Timber.e(it, "failed: $failureMsg")
                busy.value = false
                return true
            }
            return false
        }

        val deviceId = appState.value.deviceId
        val userInfo = appState.value.userInfo
        if (userInfo != null) {

            val request = RequestUserDataDelete(
                deviceIdRequester = deviceId,
                userId = userInfo.uid,
                projectId = userInfo.projectName,
                requestId = "${userInfo.projectName}_${userInfo.uid.take(5)}_${ShortId.create(5)}",
                )

            Timber.i("userInfo found. requesting backend to delete all cloud data: $request")
            val backendResult = kotlin.runCatching { firestoreRepo.doBackendRequest(request) }
            if (backendResult.handleIsError("error uploading request to BE")) {
                return false
            }

//            if (user is LoggedInUser && user.isAnonymous) {
//                // will never get back to this account, so safe to delete it
//                Timber.d("anonymous user, deleting auth data")
//                val deleteAuthUserEvent = eventBus.sendAwaitingNext(RequestDeleteAuthUser)
//                if (!deleteAuthUserEvent.success) {
//                    Timber.w("error deleting user auth, continue anyway")
//                }
//            }

            eventBus.send(RequestSignOutEvent)
        }

        Timber.d("awaiting until user is logged out")
        awaitUserLoggedOut()

        Timber.d("deleting all local data")
        localDataDeleteHandler.deleteAllLocalDataAndCreateNewProjectWithDefaultCategories(reason = "user requested delete all their data (useCase:$useCase)")
        backup.deleteAll()
        busy.value = false
        return true
    }
}
