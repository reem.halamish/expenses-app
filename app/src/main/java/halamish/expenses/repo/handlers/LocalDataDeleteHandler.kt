package halamish.expenses.repo.handlers

import halamish.expenses.v2.*
import timber.log.Timber

interface LocalDataDeleteHandler {
    /** @return the new project details */
    suspend fun deleteAllLocalDataAndCreateNewProjectWithDefaultCategories(reason: String)
}

// todo: maybe not needed anymore? check
class LocalDataDeleteHandlerImpl(private val eventBus: EventBus): LocalDataDeleteHandler {

    override suspend fun deleteAllLocalDataAndCreateNewProjectWithDefaultCategories(reason: String) {
        Timber.w("deleteAllLocalDataAndCreateNewProjectWithDefaultCategories(reason=$reason)")

        eventBus.sendAwaitingResponse(RequestDeleteLocalExpensesData(reason))
        eventBus.sendAwaitingResponse(RequestDeleteLocalStorageData(reason))
        // at this point, a new project was created. create default categories
        eventBus.sendAwaitingResponse(RequestCreateDefaultCategories)
    }
}
