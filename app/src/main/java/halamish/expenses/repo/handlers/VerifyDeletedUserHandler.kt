package halamish.expenses.repo.handlers

import halamish.expenses.data.RequestUserDataDelete
import halamish.expenses.data.RequestUserDataDelete.Status.*
import halamish.expenses.v2.V2FirestoreRepo
import timber.log.Timber

interface VerifyDeletedUserHandler {
    /** @return true if was requested, false if not, null if we're not sure */
    suspend fun wasRequestedDelete(userId: String): Boolean?

    /** clear needed BE data when user logs-in manually from a new session */
    suspend fun markUserAsNew(userId: String)
}

class VerifyDeletedUserHandlerImpl(
    val firestoreRepo: V2FirestoreRepo
): VerifyDeletedUserHandler {

    override suspend fun wasRequestedDelete(userId: String): Boolean? {
        // one day, add a BE rest endpoint to check this (toggle this impl and the BE endpoint using `config.hasBackend`)
        //  meanwhile check manually in the client (download firestore BE requests and check them)

        val collectionResult = getBackendRequestsToDeleteUserId(userId)
        if (collectionResult.isFailure) {
            Timber.w(collectionResult.exceptionOrNull() ?: RuntimeException("download failed"), "download failed, can't trust this email, we don't know")
            return null
        }
        val collection = collectionResult.getOrNull() ?: return null
        val requestsForDelete = filterRelevantDeleteRequests(collection, userId)
        if (requestsForDelete.isNotEmpty()) {
            Timber.e("found request(s) to delete this user-email! $requestsForDelete")
            return true // was requested delete
        } else {
            Timber.d("no request-delete was found, go on")
            return false
        }
    }

    override suspend fun markUserAsNew(userId: String) {
        // one day, add a BE rest endpoint to notify this (toggle this impl and the BE endpoint using `config.hasBackend`)
        //  meanwhile cleanup manually in the client (update all email-specific firestore BE requests to IRRELEVANT)

        val collectionResult = getBackendRequestsToDeleteUserId(userId)
        if (collectionResult.isFailure) {
            Timber.w(collectionResult.exceptionOrNull() ?: RuntimeException("download failed"), "download failed, can't markUserAsNew()")
            return
        }
        val collection = collectionResult.getOrNull() ?: return
        val requestsForDelete = filterRelevantDeleteRequests(collection, userId)
        for (request in requestsForDelete) {
            val newRequest = request.copy(status = IRRELEVANT)
            Timber.d("modifying request to irrelevant (user is no longer deleted): $newRequest")
            firestoreRepo.upload(V2FirestoreRepo.collectionBackendRequests, newRequest, oldDocument = request)
        }
    }

    private suspend fun getBackendRequestsToDeleteUserId(userId: String) =
        kotlin.runCatching {
            firestoreRepo.downloadCollectionOnceWithFilters(
                V2FirestoreRepo.collectionBackendRequests,
                RequestUserDataDelete::class.java,
                equalsFilters = mapOf(
                    RequestUserDataDelete::userId.name to userId
                )
            )
        }


    private fun filterRelevantDeleteRequests(
        collection: V2FirestoreRepo.CollectionEmit<RequestUserDataDelete>,
        userId: String
    ) = collection.values.filter { it.userId == userId && it.status in listOf(REQUESTED, STARTED, COMPLETED) }

}