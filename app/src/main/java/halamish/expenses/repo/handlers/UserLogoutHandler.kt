package halamish.expenses.repo.handlers

import halamish.expenses.v2.EventBus
import halamish.expenses.v2.RequestSignOutEvent
import halamish.expenses.v2.V2UserLoginVerifier
import halamish.expenses.v2.sendAwaitingResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import timber.log.Timber


interface UserLogoutHandler {
    suspend fun logoutAndClearAllLocalState()
}

class UserLogoutHandlerImpl(
    private val isUserConnected: Flow<Boolean>,
    private val awaitUserVerified: suspend () -> Unit,
    private val eventBus: EventBus,
    private val localDataDeleteHandler: LocalDataDeleteHandler,
): UserLogoutHandler {

    override suspend fun logoutAndClearAllLocalState() {
        Timber.d("logoutAndClearAllLocalState()")

        if (isUserConnected.first()) {
            Timber.d("user is logged in, awaiting verification and logging out")
            awaitUserVerified()
            Timber.d("user is verified, logging out")
            eventBus.sendAwaitingResponse(
                V2UserLoginVerifier.DeleteDeviceIdFromFirestoreBeforeLogout()
            )
            eventBus.send(RequestSignOutEvent)
        }
        isUserConnected.first { !it }

        Timber.d("user is logged out, continue to delete all local data")
        localDataDeleteHandler.deleteAllLocalDataAndCreateNewProjectWithDefaultCategories(reason = "clean state after user logout")
    }
}
