package halamish.expenses.repo.dataRepo

import android.app.Application
import android.net.Uri
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.firebase.dynamiclinks.ktx.*
import com.google.firebase.ktx.Firebase
import halamish.expenses.R
import halamish.expenses.data.*
import halamish.expenses.infra.ShortId
import halamish.expenses.v2.UpdateProjectDetailsRunnable
import halamish.expenses.v2.appRedux
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

interface DynamicLinksCreator {
    fun fetchLinkToCollaborateOnProject(referrerName: String): ProjectInvitationLink?
    suspend fun createLinkToCollaborateOnProject(referrerName: String): ProjectInvitationLink
}

class DynamicLinksCreatorImpl(
    val app: Application,
    val project: () -> ProjectDetails = { appRedux.state.value.project },
    val updateProject: (ProjectDetails.() -> ProjectDetails) -> Unit = {
        appRedux.eventBus.send(
            UpdateProjectDetailsRunnable("dynamic link data changed", it)
        )
    },
    val userInfo: () -> UserInfo? = { appRedux.state.value.userInfo }
) :
    DynamicLinksCreator {

    override fun fetchLinkToCollaborateOnProject(referrerName: String): ProjectInvitationLink? {
        val project = project()
        val existingLinkData =
            project.invitationLinks.values.firstOrNull { it.referrerName == referrerName }
        return existingLinkData?.takeIf { it.shortLink != null }
    }

    override suspend fun createLinkToCollaborateOnProject(referrerName: String): ProjectInvitationLink {
        val existingLinkData = fetchLinkToCollaborateOnProject(referrerName)
        val linkData = when {
            existingLinkData == null -> {
                val longLink = createLongLink(project().id, referrerName)
                ProjectInvitationLink(
                    id = "${referrerName.cleanedId()}_${ShortId.create()}",
                    longLink = longLink.toString(),
                    shortLink = suspendShortenLink(longLink)?.toString(),
                    referrerName = referrerName
                )
            }
            existingLinkData.shortLink == null -> existingLinkData.copy(
                shortLink = suspendShortenLink(
                    existingLinkData.longLinkUri
                )?.toString()
            )
            else -> existingLinkData
        }
        updateProject {
            copy(invitationLinks = invitationLinks + (linkData.id to linkData))
        }

        return linkData
    }

    private fun createLongLink(projectId: String, referrerName: String): Uri {
        val createdLongLink = Firebase.dynamicLinks.dynamicLink {
            link = Uri.parse("https://halamish.page.link/expenses/collaborate" +
                    "?${ProjectInvitationLink.URI_ARG_PROJECT_ID}=$projectId" +
                    "&${ProjectInvitationLink.URI_ARG_REFERRER_NAME}=$referrerName"
            )
            domainUriPrefix = "https://halamish.page.link"
            androidParameters {}
            googleAnalyticsParameters {
                source = "dynamic_link"
                medium = "social"
                campaign = "collaborate"
            }
            socialMetaTagParameters {
                title = app.getString(R.string.dynamic_link_collaborate_title)
                description =
                    app.getString(R.string.dynamic_link_collaborate_description_PS, referrerName)
                // todo image?
            }
        }
        return createdLongLink.uri
    }

    private fun createShortLinkFromLong(longLink: Uri, onShortLinkCreated: (Uri?) -> Unit) {
        Firebase.dynamicLinks.shortLinkAsync(ShortDynamicLink.Suffix.SHORT) {
            this.longLink = longLink
        }.addOnSuccessListener { result ->
            result.warnings.forEach { Timber.w("${it.code}:${it.message}") }
            Timber.i("short link created: ${result.shortLink}, ${result.previewLink}")

            onShortLinkCreated(result.shortLink)

        }.addOnFailureListener { exception ->
            Timber.w(exception, "short link was not created")
            onShortLinkCreated(null)
        }
    }

    private suspend fun suspendShortenLink(longLink: Uri): Uri? {
        return suspendCoroutine { continuation ->
            createShortLinkFromLong(longLink) { shortLink ->
                continuation.resume(shortLink)
            }
        }
    }
}