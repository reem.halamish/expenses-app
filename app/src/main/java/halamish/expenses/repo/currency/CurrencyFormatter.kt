package halamish.expenses.repo.currency

import halamish.expenses.data.Defaults
import halamish.expenses.data.ProjectDetails
import halamish.expenses.ui.withErrorLog
import halamish.expenses.v2.appRedux
import java.text.NumberFormat
import java.util.*

interface CurrencyFormatter {
    val currency: Currency

    fun format(amount: Double): String
    fun format(amount: Long): String
    fun format(amount: Int): String = format(amount.toLong())

    companion object
}

class CurrencyFormatterImpl(currencyCode: String) : CurrencyFormatter {
    override val currency = Currency.getInstance(currencyCode.uppercase(Locale.getDefault()))
        ?: withErrorLog("can't find currency $currencyCode") { Currency.getInstance(Locale.getDefault()) }

    override fun format(amount: Double): String =
        NumberFormat.getCurrencyInstance()
            .apply {
                currency = this@CurrencyFormatterImpl.currency
                minimumFractionDigits = 0
                maximumFractionDigits = 2
                isParseIntegerOnly = false
            }
            .format(amount)

    override fun format(amount: Long): String =
        NumberFormat.getCurrencyInstance().apply {
            currency = this@CurrencyFormatterImpl.currency
            maximumFractionDigits = 0
            isParseIntegerOnly = true
        }.format(amount)
}

private val formatterByCode = mutableMapOf<String, CurrencyFormatter>()

@Synchronized
fun CurrencyFormatter.Companion.of(currencyCode: String): CurrencyFormatter {
    formatterByCode[currencyCode]?.let { return it }
    val formatter = CurrencyFormatterImpl(currencyCode)
    formatterByCode += currencyCode to formatter
    return formatter
}

fun CurrencyFormatter.Companion.of(project: ProjectDetails) =
    of(project.currencyCode ?: Defaults.currencyCode)

fun CurrencyFormatter.Companion.get() =
    of(appRedux.state.value.project)

