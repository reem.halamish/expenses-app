package halamish.expenses.repo.currency

import android.content.res.Resources
import android.os.Build
import halamish.expenses.repo.icons.CurrencyData
import halamish.expenses.repo.icons.CurrencySymbolServer
import io.reactivex.Observable
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class CurrencyProvider @Inject constructor(private val currencyServer: CurrencySymbolServer) {
    fun currencySymbolFromLocale(): String? {
        val configuration = Resources.getSystem().configuration
        @Suppress("DEPRECATION")
        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.locales[0]
        } else configuration.locale
        return try {
            Currency.getInstance(locale).symbol
        } catch (e: Exception) {
            Timber.w(e, "error trying to fetch currency from locale ($locale)")
            return null
        }
    }

    fun currencyDateFromIp(): Observable<CurrencyData> =
        currencyServer.getUserCurrencyOld().toObservable()
//        currencyServer.getCurrencyFromIp("178.237.33.50").toObservable()


    companion object {
        fun allPossibleCurrencies(): List<CurrencyData> =
            Locale.getAvailableLocales()
                .filter { it.country.isNotEmpty() && it.language.isNotEmpty() && it.displayCountry != "World" }
                .groupBy { it.country }
                .mapNotNull {
                    val currency =
                        try { Currency.getInstance(it.value.first()) }
                        catch (e: RuntimeException) { return@mapNotNull null }

                    return@mapNotNull CurrencyData(
                        symbol = currency.symbol,
                        currencyCode = currency.currencyCode,
                        countryCode = it.key,
                        countryName = it.value.first().displayCountry
                    )
                }
    }
}