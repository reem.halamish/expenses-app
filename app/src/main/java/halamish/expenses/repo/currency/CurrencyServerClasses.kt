package halamish.expenses.repo.icons

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

@Keep
data class IpResult(val ip: String = "")

interface IpServer {
    @GET("/")
    fun getIp(@Query("format") format: String): Single<IpResult>
}

//example: https://api.ipify.org/?format=json

@Keep
data class CurrencyData(
    @SerializedName("geoplugin_currencySymbol_UTF8")
    val symbol: String = "", // ₪, $

    @SerializedName("geoplugin_currencyCode")
    val currencyCode: String = "", // ILS, USD

    @SerializedName("geoplugin_countryCode")
    val countryCode: String = "", // IL, US

    @SerializedName("geoplugin_countryName")
    val countryName: String? = null // Israel, United States
)


interface CurrencySymbolServer {
  @GET("/json.gp")
  suspend fun getUserCurrency(): CurrencyData

  @GET("/json.gp")
  fun getUserCurrencyOld(): Single<CurrencyData>

  @GET("/json.gp")
  fun getCurrencyFromIp(@Query("ip") ip: String): Single<CurrencyData>
}

//example: http://www.geoplugin.net/json.gp
//example: http://www.geoplugin.net/json.gp?ip=8.8.8.8