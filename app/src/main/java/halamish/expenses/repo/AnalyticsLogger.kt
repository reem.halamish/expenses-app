package halamish.expenses.repo

import android.app.Application
import android.os.Bundle
import androidx.core.os.bundleOf
import com.google.firebase.analytics.FirebaseAnalytics
import halamish.expenses.BuildConfig
import timber.log.Timber
import java.io.Serializable

interface AnalyticsLogger {
    fun setUserId(userId: String?)
    fun event(eventName: String, extras: Bundle? = null)
}

class FirebaseAnalyticsLogger(
    app: Application,
    val analytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(app)
) : AnalyticsLogger {

    init {
        @Suppress("KotlinConstantConditions") // depends on flavor/configuration
        analytics.setAnalyticsCollectionEnabled(BuildConfig.BUILD_TYPE == "release")
    }

    override fun setUserId(userId: String?) {
        analytics.setUserId(userId)
    }

    override fun event(eventName: String, extras: Bundle?) {
        Timber.d("Will log event: name:$eventName, extras:$extras")
        analytics.logEvent(eventName, extras)
    }
}

fun AnalyticsLogger.signUp(userId: String, method: String) {
    setUserId(userId)
    event("sign_up", bundleOf("method" to method))
}

fun AnalyticsLogger.logIn(userId: String, method: String) {
    setUserId(userId)
    event("login", bundleOf("method" to method))
}

fun AnalyticsLogger.buttonClick(buttonId: String) =
    event("button_click", bundleOf("button_id" to buttonId))

fun AnalyticsLogger.swipeToDeleteExpense(expenseId: String) =
    event("swipe_delete_event", bundleOf("expense_id" to expenseId))

fun AnalyticsLogger.swipeToEditExpense(expenseId: String) =
    event("swipe_edit_event", bundleOf("expense_id" to expenseId))

fun AnalyticsLogger.swipeToDeleteCategory(categoryId: String) =
    event("swipe_delete_event", bundleOf("category_id" to categoryId))

fun AnalyticsLogger.swipeToEditCategory(categoryId: String)
        = event("swipe_edit_event", bundleOf("category_id" to categoryId))

fun AnalyticsLogger.longClickToEditCategory(categoryId: String)
        = event("longClick_edit_event", bundleOf("category_id" to categoryId))

fun AnalyticsLogger.startCreateNewExpense(expenseId: String)
        = event("create_expense", bundleOf("expense_id" to expenseId))

fun AnalyticsLogger.startEditExistingExpense(expenseId: String, recurring: Boolean)
        = event("edit_expense", bundleOf("expense_id" to expenseId, "recurring" to recurring))

fun AnalyticsLogger.saveExpense(expenseId: String, isNew: Boolean, recurring: Boolean)
        = event("save_expense", bundleOf("expense_id" to expenseId, "is_new" to isNew, "recurring" to recurring))

fun AnalyticsLogger.dashboardScreenTapCardButton(cardType: String, buttonType: String) =
    event("dashboard_card_tap", bundleOf("card_type" to cardType, "button_type" to buttonType))

fun AnalyticsLogger.defineBudget(budget: Int?, prevBudget: Int?) =
    event("define_budget", bundleOf("budget" to budget, "prev_budget" to prevBudget))

fun AnalyticsLogger.cancelDialog(dialogName: String) =
    event("cancel_dialog", bundleOf("dialog_name" to dialogName))

fun AnalyticsLogger.drawerOpen() = event("drawer_open")
fun AnalyticsLogger.drawerClose() = event("drawer_close")

fun AnalyticsLogger.settingsItemClicked(name: String, extras: Bundle? = null) = event("settings_item_click", bundleOf("name" to name).apply { if (extras!=null) putAll(extras) })
fun AnalyticsLogger.settingsEvent(name: String) = event("settings_event", bundleOf("name" to name))

fun AnalyticsLogger.fragmentShown(name: String) = event("fragment_shown", bundleOf("name" to name))

data class AnalyticsPushData(val type: String, val id: String): Serializable
private fun AnalyticsPushData.toBundle() = bundleOf("type" to type, "id" to id)
fun AnalyticsLogger.pushCreated(data: AnalyticsPushData) = event("push_opened", data.toBundle())
fun AnalyticsLogger.pushOpened(data: AnalyticsPushData) = event("push_opened", data.toBundle())

fun AnalyticsLogger.infromativePopup(screenName: String, action: String) = event("confirm_destructive", bundleOf("screenName" to screenName, "action" to action))
fun AnalyticsLogger.confirmDestructiveActionPopup(screenName: String, action: String) = event("confirm_destructive", bundleOf("screenName" to screenName, "action" to action))