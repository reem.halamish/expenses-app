package halamish.expenses.repo

import androidx.annotation.Keep
import com.google.firebase.Timestamp
import java.io.Serializable
import kotlin.reflect.KProperty0

@Keep
abstract class PkData : Serializable {
  abstract fun pk(): String

  abstract var edit: Timestamp?
  abstract var created: Timestamp?
  abstract var deleted: Boolean
  abstract var lastEditBy: String // device-id

  abstract fun allNullableFields(): List<KProperty0<Any?>>
  abstract fun allMapTypeFields(): List<KProperty0<Map<String,Any>>>
}

@Keep
data class EmptyPkData(
  val id: String = "",
  override var edit: Timestamp? = null,
  override var created: Timestamp? = null,
  override var deleted: Boolean = false,
  override var lastEditBy: String = ""
) : PkData() {
  override fun pk() = id
  override fun allNullableFields(): List<KProperty0<Any?>> = emptyList()
  override fun allMapTypeFields(): List<KProperty0<Map<String, Any>>> = emptyList()
}

fun <T : PkData> T.withClearedEditTimestamp(): T {
  edit = null
  return this
}

fun <T : PkData> T.withCreationTimestamp(other: PkData?): T {
  created = other?.created
  return this
}

fun <T : PkData> T.withEditedBy(deviceId: String): T {
  lastEditBy = deviceId
  return this
}
