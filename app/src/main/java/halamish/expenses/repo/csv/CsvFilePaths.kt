package halamish.expenses.repo.csv

import android.content.Context
import java.io.File

object CsvFilePaths {
    fun file(context: Context, fileName: String): File? {
        val cacheDir = context.cacheDir ?: return null
        val csvFolder = File(cacheDir, "csv_reports")
        csvFolder.mkdirs()
        return File(csvFolder, fileName)
    }
}