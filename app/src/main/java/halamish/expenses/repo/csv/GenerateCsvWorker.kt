package halamish.expenses.repo.csv

import android.content.Context
import androidx.work.*
import halamish.expenses.R
import halamish.expenses.data.ExpenseEvent
import halamish.expenses.data.distantPast
import halamish.expenses.data.granulateExpenses
import halamish.expenses.v2.appRedux
import org.threeten.bp.LocalDate
import timber.log.Timber
import java.io.File
import java.util.*

class GenerateCsvWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    override fun doWork(): Result {

        val categories = appRedux.state.value.data.categories
        val startDate = LocalDate.ofEpochDay(inputData.getLong("start", distantPast.toEpochDay()))
        val endDate = LocalDate.ofEpochDay(inputData.getLong("end", LocalDate.now().toEpochDay()))
        val sort = Sort.valueOf(inputData.getString("sort") ?: Sort.BY_CATEGORY.name)
        val expenses = granulateExpenses(startDate = startDate, endDate = endDate).toMutableList()


        val selectors = mutableListOf<(ExpenseEvent) -> Comparable<*>?>()
        selectors += { it.date }
        selectors += { categories[it.categoryId]?.displayName ?: "" }
        if (sort == Sort.BY_CATEGORY) {
            selectors.reverse()
        }

        expenses.sortWith(compareBy(*selectors.toTypedArray()))

        val csvFile = CsvFilePaths.file(applicationContext, "report_${Date().time}.csv")
            ?: return Result.failure()
        if (csvFile.exists()) return Result.failure()

        // tell the world this is a UTF16 file
        // see https://stackoverflow.com/a/67117231/3339597
        csvFile.writeText("\ufeff", charset = Charsets.UTF_16)

        csvFile.appendText(
            listOf(
                "date",
                "amount",
                "category",
                "recurring",
                "comments"
            ).joinToString(separator = ",", postfix = "\n"),
            charset = Charsets.UTF_16
        )
        expenses.forEach { expense ->

            val recurringText =
                if (expense.recurring == null) ""
                else
                    applicationContext.getString(R.string.item_expense_recurring)
                        .replace("[CURRENT_INDEX]", "${expense.recurring.currentIndex + 1}")
                        .replace(
                            "[TOTAL]",
                            if (expense.recurring.forever) applicationContext.getString(R.string.item_expense_recurring_infinity)
                            else "${expense.recurring.totalOccurrences}"
                        )

            csvFile.appendText(
                StringBuilder()
                    .append(expense.date).append(",")
                    .append(expense.amount).append(",")
                    .append(categories[expense.categoryId]?.displayName ?: "").append(",")
                    .append(recurringText).append(",")
                    .append(expense.comments?.replace(",", ".")?.replace("\n", " ") ?: "")
                    .append("\n")
                    .toString(),
                charset = Charsets.UTF_16
            )
        }

        Timber.i("did create csv report with ${expenses.size} values at ${csvFile.name}")
        return Result.success(
            Data.Builder()
                .putString("file", csvFile.absolutePath)
                .build()
        )
    }


    companion object {
        enum class Sort { BY_DATE, BY_CATEGORY }

        fun enqueue(
            context: Context,
            sort: Sort,
            dateStart: LocalDate = distantPast,
            dateEnd: LocalDate = LocalDate.now()
        ) = OneTimeWorkRequestBuilder<GenerateCsvWorker>()
            .setInputData(
                Data.Builder()
                    .putString("sort", sort.name)
                    .putLong("start", dateStart.toEpochDay())
                    .putLong("end", dateEnd.toEpochDay())
                    .build()
            )
            .build()
            .also { WorkManager.getInstance(context).enqueue(it) }

        fun file(data: Data) = data.getString("file")?.let { File(it) }
    }
}