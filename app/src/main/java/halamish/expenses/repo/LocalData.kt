package halamish.expenses.repo

import android.content.SharedPreferences
import androidx.annotation.IdRes
import halamish.expenses.R
import halamish.expenses.ui.sp
import halamish.expenses.v2.appRedux

class LocalData(val sp: SharedPreferences = appRedux.app.sp)

enum class LocalDataStartDestination(@IdRes val destinationId: Int) {
  MONTHLY(R.id.expenseMonthlyFragment),
  HISTORY(R.id.viewExpensesHistoryFragment),
  DASHBOARD(R.id.dashboardFragment)
}

var LocalData.overridenStartDestination: LocalDataStartDestination?
  get() = sp.getString("overridenStartDestination", null)
    ?.let { LocalDataStartDestination.valueOf(it) }
  set(value) = sp.edit().putString("overridenStartDestination", value?.name).apply()
