package halamish.expenses.repo

import androidx.annotation.Keep
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import halamish.expenses.infra.ShortId
import halamish.expenses.v2.LoggedInUser
import org.threeten.bp.Instant
import java.io.Serializable


@Keep
sealed class FcmRequestData(
    val type: FcmRequestDataType,
    open val projectId: String,
    open val initiatorDeviceId: String,
    val id: String = "${Instant.now().epochSecond}_${projectId}_${ShortId.create()}",
    @ServerTimestamp val creationTime: Timestamp? = null
): Serializable

enum class FcmRequestDataType(val clazz: Class<out FcmRequestData>) {
    NEW_USER_IN_PROJECT(NewUserInProjectFcmRequest::class.java),
    USER_REMOVED_FROM_PROJECT(UserRemovedFromProjectFcmRequest::class.java)
}

@Keep
data class NewUserInProjectFcmRequest(
    override val projectId: String,
    val deviceId: String,
    val userId: String,
    val email: String?,
    val displayName: String?,
) : FcmRequestData(type = FcmRequestDataType.NEW_USER_IN_PROJECT, projectId=projectId, initiatorDeviceId = deviceId) {
    constructor(projectId: String, deviceId: String, userData: LoggedInUser): this(
        projectId, deviceId,
        userData.userId,
        userData.email,
        userData.displayName
    )
}

@Keep
data class UserRemovedFromProjectFcmRequest(
    override val projectId: String,
    val deviceId: String,
    val userId: String,
    ) : FcmRequestData(type = FcmRequestDataType.USER_REMOVED_FROM_PROJECT, projectId = projectId, initiatorDeviceId = deviceId)