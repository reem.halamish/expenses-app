package halamish.expenses.repo.icons

import androidx.annotation.Keep

@Keep
data class IconSearchResult(
    val icons: List<IconData> = emptyList(),
    val platforms: List<String> = emptyList(), // should only contain "androidL"
    val parameters: IconsParameterObject = IconsParameterObject()
)

fun IconSearchResult.validate() = copy(icons = icons.filter { it.platform == "androidL" })





@Keep
data class IconsParameterObject(val amount: Int = Int.MIN_VALUE,
                                val term: String = "",
                                val platform: String = "",
                                val offset: Int = Int.MIN_VALUE,
                                val language: String = "")




@Keep
data class IconData(val platform: String = "",
                    val commonName: String = "")


val IconData.pngUrl: String
    get() = icon8Url(commonName)

fun icon8Url(iconName: String) = "https://img.icons8.com/material/3x/$iconName.png"
//fun icon8Url(iconName: String) = "https://img.icons8.com/material/$iconName.png"

/*
FULL VALUES FOR ICON_DATA
data class IconData(val id: String = "",
                    val name: String = "",
                    val platform: String = "",
                    val commonId: String = "",
                    val commonName: String = "",
                    val timestamp: String = "")*/

