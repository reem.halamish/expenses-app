package halamish.expenses.repo.icons

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface IconsServer {
    @GET("/api/iconsets/v4/search")
    fun search(@Query("term") term: String,
               @Query("amount") amount: Int,
               @Query("platform") platform: String): Single<IconSearchResult>
}

//example: https://api.icons8.com/api/iconsets/v4/search?term=food&amount=20&platform=androidL