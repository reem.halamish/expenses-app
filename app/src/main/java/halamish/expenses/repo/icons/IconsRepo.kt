package halamish.expenses.repo.icons

import io.reactivex.Single
import javax.inject.Inject

class IconsRepo @Inject constructor(private val server: IconsServer) {
  fun icons(searchTerm: String): Single<List<IconData>> =
    server.search(searchTerm, 50, "androidL")
      .map { it.validate().icons }
}