package halamish.expenses.repo

import android.content.Context
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import extensions.android.AndroidExtensions.toHexColorString
import halamish.expenses.BuildConfig
import halamish.expenses.R
import timber.log.Timber

interface CategoriesColorsProvider {
  val colorsRes: List<Int> // List<ResColor> (of the type R.color.category_***)
  fun colorValues(context: Context): List<Int> // List<IntColor> (of the type Color.RED)

  /**
   * @return an int of the type R.color.***
   * caller will need to call ContextCompat.getColor(context, <returned-value>) on the result
   */
  @ColorRes fun resolveStoredColor(@ColorInt categoryHardcodedColor: Int): Int

  /**
   * assuming input to this fun was received from `CategoriesColorsProvider::colorsRes`
   * @return an int of the type Color.RED
   */
  @ColorInt fun calculateHardcodedColor(@ColorRes resColor: Int): Int
}

class AvailableCategoriesColorsProvider(
  context: Context,
  override val colorsRes: List<Int> = listOf(
    R.color.category_teal,
    R.color.category_green,
    R.color.category_light_green,
    R.color.category_yellow,
    R.color.category_amber,
    R.color.category_orange,
    R.color.category_red,
    R.color.category_pink,
    R.color.category_purple,
    R.color.category_deep_purple,
    R.color.category_indigo,
    R.color.category_blue,
    R.color.category_cyan,
    R.color.category_brown,
  )
) : CategoriesColorsProvider {

  private val hardcodedColors: Map<Int, Int> // resource to light-mode value. example: {R.color.yellow to Color.YELLOW}
  private val resolvedColors: Map<Int,Int> // value to resource, including both day & night values. example: {Color.YELLOW to R.color.yellow}
  private val mappingReadableString: String
  private val resourceColorsToResourceNames: Map<Int, String> // resource to string. example: {R.color.yellow to "R.color.yellow"}
  @ColorInt private val hardcodedUnknown: Int

  init {
    val forceLightModeConfiguration = Configuration().apply { uiMode = UI_MODE_NIGHT_NO }
    val forceDarkModeConfiguration = Configuration().apply { uiMode = UI_MODE_NIGHT_YES }
    val lightModeContext = context.createConfigurationContext(forceLightModeConfiguration)
    val darkModeContext = context.createConfigurationContext(forceDarkModeConfiguration)
    fun resName(resColor: Int) = lightModeContext.resources.getResourceName(resColor).replace("${BuildConfig.APPLICATION_ID}:color/", "R.color.")

    // {R.color.yellow --> COLOR.YELLOW}
    val resToHardcodedMappingLightMode = colorsRes.associateWith { ContextCompat.getColor(lightModeContext, it) }
    val resToHardcodedMappingDarkMode = colorsRes.associateWith { ContextCompat.getColor(darkModeContext, it) }

    // {Color.YELLOW --> R.color.yellow}
    val hardcodedToResMapping = (resToHardcodedMappingLightMode.entries + resToHardcodedMappingDarkMode.entries).associate { it.value to it.key }

    this.hardcodedColors = resToHardcodedMappingLightMode
    this.resolvedColors = hardcodedToResMapping
    this.hardcodedUnknown = ContextCompat.getColor(lightModeContext, R.color.category_unknown)
    this.mappingReadableString = hardcodedToResMapping.map { it.key.toHexColorString() + " --> " + resName(it.value) }.joinToString(separator = "\n", prefix = "{\n", postfix = "\n}")
    this.resourceColorsToResourceNames = colorsRes.associateWith { resName(it) }
  }

  override fun resolveStoredColor(categoryHardcodedColor: Int): Int {
    return resolvedColors[categoryHardcodedColor] ?: run {
      Timber.e("could not resolve R.color res for hardcoded color ${categoryHardcodedColor.toHexColorString()}")
      Timber.w("mapping: $mappingReadableString")
      R.color.category_unknown
    }
  }

  override fun calculateHardcodedColor(resColor: Int): Int {
    return hardcodedColors[resColor] ?: run {
      Timber.e("can't calculate hardcoded color for res ${resourceColorsToResourceNames[resColor] ?: resColor}")
      Timber.w("mapping: $mappingReadableString")
      hardcodedUnknown
    }
  }

  override fun colorValues(context: Context): List<Int> {
    return colorsRes.map { ContextCompat.getColor(context, it) }
  }
}