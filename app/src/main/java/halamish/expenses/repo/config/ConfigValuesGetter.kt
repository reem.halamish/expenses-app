package halamish.expenses.repo.config

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import java.util.concurrent.TimeUnit

data class AppConfiguration(
    val amount: Int = 0,
    val hasBackEnd: Boolean = false,
    val projectInvitations: DynamicLinksProjectInvitationsFeatureConfigs = DynamicLinksProjectInvitationsFeatureConfigs(),
    val notificationsEnabled: Boolean = false,
    val singleDeviceProjectSkipDownloading: Boolean = true,
    val minimumExpensesCreateBeforeRequestingFeedback: Int = 0,
    val testValue: String = ""
)
data class DynamicLinksProjectInvitationsFeatureConfigs(
    val enabled: Boolean = false,
    val useBackend: Boolean = false
)

class ConfigValuesGetter(
    remoteConfig: FirebaseRemoteConfig = Firebase.remoteConfigWithDefaultSettings,
    onGotConfig: (AppConfiguration) -> Unit
){
    init {
        onGotConfig(remoteConfig.toAppConfiguration())
        remoteConfig.fetchAndActivate().addOnCompleteListener {
            onGotConfig(remoteConfig.toAppConfiguration())
        }
    }
}

private val Firebase.remoteConfigWithDefaultSettings: FirebaseRemoteConfig by lazy {
    Firebase.remoteConfig.apply {
        setConfigSettingsAsync(remoteConfigSettings {
            minimumFetchIntervalInSeconds = TimeUnit.HOURS.toSeconds(12)
        })
    }
}

private val FirebaseRemoteConfig.testConfig get() = getString("test_config")
private val FirebaseRemoteConfig.hasBackEnd get() = getBoolean("has_backend")
private val FirebaseRemoteConfig.projectInvitationsEnabled get() = getBoolean("project_invitations_dynamic_links_enabled")
private val FirebaseRemoteConfig.projectInvitationsUseBackEnd get() = hasBackEnd
private val FirebaseRemoteConfig.notificationsEnabled get() = hasBackEnd
private val FirebaseRemoteConfig.singleDeviceProjectSkipDownloading get() = getBoolean("single_device_project_skip_downloading")
private val FirebaseRemoteConfig.minimumExpensesCreateBeforeRequestingFeedback get() = getLong("minimum_expenses_create_before_requesting_feedback").toInt()
private fun FirebaseRemoteConfig.toAppConfiguration() = AppConfiguration(
    amount = this.all.size,
    hasBackEnd = this.hasBackEnd,
    testValue = this.testConfig,
    notificationsEnabled = this.notificationsEnabled,
    singleDeviceProjectSkipDownloading = this.singleDeviceProjectSkipDownloading,
    minimumExpensesCreateBeforeRequestingFeedback = this.minimumExpensesCreateBeforeRequestingFeedback,
    projectInvitations = DynamicLinksProjectInvitationsFeatureConfigs(
        enabled = this.projectInvitationsEnabled,
        useBackend = this.projectInvitationsUseBackEnd
    )
)
