package halamish.expenses.repo.dataStore

import halamish.expenses.data.*
import halamish.expenses.repo.config.AppConfiguration
import halamish.expenses.v2.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean

class V2DataStore(
  scope: CoroutineScope = appRedux.scope,
  private val eventBus: EventBus = appRedux.eventBus,
  private val timestampHolder: V2FirestoreTimestampHolder = appRedux.services.firestoreTimestampHolder,
  private val deviceId: String = appRedux.services.deviceId,
  configFlow: Flow<AppConfiguration> = appRedux.state.map { it.config },
  downloadRequests: Flow<Set<String>> = appRedux.state.map { it.downloadRequests },
  onUserInfoChanged: suspend (description: String, UserInfo?) -> Unit,
  onProjectDetailsChanged: suspend (description: String, ProjectDetails) -> Unit,
  onCategoriesChange: suspend (Map<String, ExpenseCategory>) -> Unit,
  onExpenseRulesChange: suspend (Map<String, RecurringExpenseRule>) -> Unit,
  onOneTimeExpensesChange: suspend (Map<String, OneTimeExpenseRecord>) -> Unit,
) {

  private val project = ProjectDetailsDataStore(onChanged = onProjectDetailsChanged)
  private val userInfo = UserInfoDataStore(onChanged = onUserInfoChanged)
  private val categories = ExpenseCategoriesDataStore(onChanged = onCategoriesChange)
  private val expenseRules = ExpenseRulesDataStore(onChanged = onExpenseRulesChange)
  private val oneTimeExpenses = OneTimeExpensesDataStore(onChanged = onOneTimeExpensesChange)

  private val allCollectionHolders = listOf(categories, expenseRules, oneTimeExpenses)
  private val allSingleItemHolders = listOf(project, userInfo)
  private val allHolders = allCollectionHolders + allSingleItemHolders

  private val requestedBlockingUploads = AtomicBoolean(true)
  private val requestedBlockingDownloads = AtomicBoolean(true)
  private val skipDownloadsForSingleDeviceProjects = AtomicBoolean(false)
  private val hasDownloadRequests = AtomicBoolean(false)
  private val firestoreMightHaveModeData = AtomicBoolean(false)

  init {
    eventBus.onEveryEvent<RequestDeleteLocalExpensesData>(scope) {
      Timber.d("will delete all categories, expenses, rules, and project details")
      categories.deleteLocalData(reason)
      expenseRules.deleteLocalData(reason)
      oneTimeExpenses.deleteLocalData(reason)
      project.deleteLocalData(reason)
      userInfo.deleteLocalData(reason)
      timestampHolder.clear()
      eventBus.sendResponseFor(this)
    }
  }

  init {
      // blocking everything until green light from the login validator
      setBlocked(
        uploads = true,
        downloads = true,
        localChanges = true,
        resetTimestampHolder = false
      )

      scope.launch {
        configFlow.map { it.singleDeviceProjectSkipDownloading }
          .distinctUntilChanged()
          .collect {
            skipDownloadsForSingleDeviceProjects.set(it)
            recalculateUploadDownloadBlocking()
          }
      }

    scope.launch {
      downloadRequests.map { it.isNotEmpty() }
        .distinctUntilChanged()
        .collect {
          hasDownloadRequests.set(it)
          recalculateUploadDownloadBlocking()
        }
    }
  }

  fun getCurrentProject() = project.getCurrent()

  fun setFirestoreMightHaveModeData(newValue: Boolean) {
    firestoreMightHaveModeData.set(newValue)
    recalculateUploadDownloadBlocking()
  }

  fun setBlocked(
    uploads: Boolean? = null,
    downloads: Boolean? = null,
    localChanges: Boolean? = null,
    resetTimestampHolder: Boolean = false,
  ) {

    localChanges?.let { allHolders.forEach { it.setBlockingLocalChanges(localChanges) } }
    uploads?.let { requestedBlockingUploads.set(uploads) }
    downloads?.let { requestedBlockingDownloads.set(downloads) }
    if (resetTimestampHolder) {
      timestampHolder.clear()
    }
    recalculateUploadDownloadBlocking()
  }

  private fun recalculateUploadDownloadBlocking() {
    val project = project.getCurrent()

    // upload
    val uploadAllowed = !requestedBlockingUploads.get()

    // download
    val hasMultiDevicePotential = project.devices.size > 1 || project.invitationLinks.isNotEmpty()
    val downloadAllowed =
            (!requestedBlockingDownloads.get()) &&
            (hasMultiDevicePotential || hasDownloadRequests.get() || firestoreMightHaveModeData.get() || !skipDownloadsForSingleDeviceProjects.get())

    Timber.d("recalculate: uploadAllowed($uploadAllowed), downloadAllowed($downloadAllowed), projectId(${project.id})")
    allCollectionHolders.forEach {
      it.setUploadEnabled(enabled = uploadAllowed, project.id)
      it.setDownloadEnabled(enabled = downloadAllowed, project.id)
    }
    allSingleItemHolders.forEach {
      it.setUploadEnabled(enabled = uploadAllowed)
      it.setDownloadEnabled(enabled = downloadAllowed)
    }
  }


  suspend fun uploadExpensesCollectionsToFirestore() = withContext(Dispatchers.IO) {
    coroutineScope {
      val projectId = project.getCurrent().id
      allCollectionHolders
        .map { async { it.forceUploadEverything(projectId)} }
        .map { it.await() }
    }
  }

  fun setUserFromFirestore(userInFirestore: UserInfo, reason: String) {
    userInfo.setLocalData(userInFirestore, reason)
  }
  fun setProjectFromFirestore(projectDetails: ProjectDetails, reason: String) {
    project.setLocalData(projectDetails, reason)
    allCollectionHolders.forEach { it.setProjectId(projectDetails.id) }
  }

  fun deleteAllLocalData(reason:String, createNewProject: Boolean) {
    allHolders.forEach { it.deleteLocalData(reason) }
    timestampHolder.clear()
    if (createNewProject) {
      project.setLocalData(ProjectDetails.createClean(deviceId = deviceId, creationReason = reason), reason)
    }
  }
  suspend fun createDefaultCategories() = categories.createDefaultCategories()

  suspend fun ensureProjectCreatedProperlyLocally() {
    if (categories.getCurrent().isEmpty() && expenseRules.getCurrent().isEmpty() && oneTimeExpenses.getCurrent().isEmpty()) {
      categories.createDefaultCategories()
    }
  }
}

sealed interface DataStoreDataHolder<T> {
  /**
   * @param blocking
   * if true, block any incoming requests to modify (add/delete/update) inner data.
   * if false, unblocked, submit all work that was blocked meanwhile
   */
  fun setBlockingLocalChanges(blocking: Boolean)


  fun deleteLocalData(reason: String)
}

sealed interface DataStoreCollectionHolder<T>: DataStoreDataHolder<T> {
  /** needed for uploading/downloading */
  fun setProjectId(projectId: String)
  suspend fun forceUploadEverything(projectId: String)
  fun getCurrent(): Map<String, T>
  fun setUploadEnabled(enabled: Boolean, projectId: String)
  fun setDownloadEnabled(enabled: Boolean, projectId: String)
}

sealed interface DataStoreSingleItemHolder<T>: DataStoreDataHolder<T> {
  fun getCurrent(): T
  fun setLocalData(newValue: T, reason: String)
  fun setUploadEnabled(enabled: Boolean)
  fun setDownloadEnabled(enabled: Boolean)

}