package halamish.expenses.repo.dataStore

import android.app.Application
import android.content.SharedPreferences
import androidx.annotation.StringRes
import androidx.core.content.edit
import extensions.android.fromJson
import extensions.android.toJson
import halamish.expenses.R
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.repo.CategoriesColorsProvider
import halamish.expenses.repo.PkData
import halamish.expenses.repo.dataStore.V2FirestoreTimestampHolder.UpdateTimestampReason.*
import halamish.expenses.ui.spWithName
import halamish.expenses.v2.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import org.threeten.bp.Instant
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference


private interface LocalBasedCollectionPkDataStore<T: PkData> {
  val clazz: Class<T>
  val dataFlow: StateFlow<Map<String, T>>
  fun override(pkData: T)
  fun delete(pkData: T)
  fun overrideAll(newValues: Set<T>)
}

interface CollectionDataStoreOperations<T: PkData> {
  val currentData: Map<String, T>
  suspend fun putReplacingOld(pkData: T)
  suspend fun delete(pkData: T)
  suspend fun overrideAllTo(newValues: Set<T>)
}

private interface AsyncBasedCollectionPkDataStore<T: PkData>: CollectionDataStoreOperations<T> {
  val clazz: Class<T>
  val dataFlow: StateFlow<Map<String, T>>

  fun setCollectionPath(path: String)

  fun clearLocal(reason: String)
  suspend fun forceUploadLocal()

  fun setDownloadEnabled(enabled: Boolean, path: String)
  fun setUploadEnabled(enabled: Boolean, path: String)
}

private class SpBasedCollectionPkDataStoreImpl<T : PkData>(
  override val clazz: Class<T>,
  private val sp: SharedPreferences
) : LocalBasedCollectionPkDataStore<T> {

  override val dataFlow = MutableStateFlow(loadData())

  override fun override(pkData: T) {
    if (dataFlow.value[pkData.pk()] == pkData) return
    sp.edit { putString(pkData.pk(), pkData.toJson()) }
    dataFlow.value = dataFlow.value + (pkData.pk() to pkData)
  }

  override fun delete(pkData: T) {
    if (pkData.pk() !in dataFlow.value) return
    sp.edit { remove(pkData.pk()) }
    dataFlow.value = dataFlow.value - pkData.pk()
  }

  override fun overrideAll(newValues: Set<T>) {
    if (newValues == setOf(dataFlow.value.values)) return
    sp.edit {
      clear()
      newValues.forEach { pkData ->
        putString(pkData.pk(), pkData.toJson())
      }
    }

    dataFlow.value = newValues.associateBy { it.pk() }
  }

  private fun loadData(): Map<String, T> {
    return sp.all.keys.map { sp.getString(it, null) }
      .mapNotNull { it?.fromJson(clazz) }
      .associateBy { it.pk() }
  }
}

private class FirestoreBasedCollectionPkDataStoreImpl<T : PkData>(
  private val local: LocalBasedCollectionPkDataStore<T>,
  private val scope: CoroutineScope,
  private val firestoreRepo: V2FirestoreRepo = appRedux.services.firestoreRepo,
  private val timestampHolder: V2FirestoreTimestampHolder = appRedux.services.firestoreTimestampHolder,
) : AsyncBasedCollectionPkDataStore<T> {

  private data class FirestoreActiveOperation(val subCollection: String, val job: Job)

  private var subCollectionPath = AtomicReference<String?>(null)
  private var uploadEnabled = AtomicBoolean(false)
  private var downloadEnabled = AtomicBoolean(false)
  private var activeDownload: FirestoreActiveOperation? = null

  override val clazz: Class<T> = local.clazz
  override val dataFlow: StateFlow<Map<String, T>> = local.dataFlow

  override fun setCollectionPath(path: String) {
    subCollectionPath.set(path)
    recalculateDownloads()
  }

  override fun setDownloadEnabled(enabled: Boolean, path: String) {
    downloadEnabled.set(enabled)
    subCollectionPath.set(path)
    recalculateDownloads()
  }

  override fun setUploadEnabled(enabled: Boolean, path: String) {
    uploadEnabled.set(enabled)
    subCollectionPath.set(path)
  }

  @Synchronized
  private fun recalculateDownloads() {
    val subCollection = subCollectionPath.get()
    val enabled = downloadEnabled.get()
    if (subCollection == null || !enabled) {
      activeDownload?.job?.cancel("no subCollection")
      activeDownload = null
      return
    }
    if (subCollection != activeDownload?.subCollection) {
      activeDownload?.job?.cancel("starting download from other subCollection $subCollection")
    }
    activeDownload = setupDownload(subCollection)
  }

  private fun setupDownload(collection: String): FirestoreActiveOperation {

    Timber.d("V2 firestore will start observe collection $collection class ${clazz.simpleName} for downloads")
    val job = Job()
    scope.launch(job + Dispatchers.IO) download@{
      val timestamp = timestampHolder.getStoredTimestamp(useCase = collection) ?: run {
        Timber.d("no timestamp for collection $collection class ${clazz.simpleName}, performing 1-time download")
        val data = firestoreRepo.downloadCollectionOnce(collection, clazz)
        local.overrideAll(data.values.toSet())
        val timestamp = if (data.fromServer) {
          timestampHolder.updateTimestamp(useCase = collection, INITIAL_DOWNLOAD_DONE)
        } else Instant.EPOCH
        return@run timestamp
      }
      firestoreRepo.observeDiffsInCollectionGreaterThenTimestamp(
        collection,
        timestamp.toEpochMilli(),
        clazz
      )
        .flowOn(Dispatchers.IO)
        .collect {
          handleDiffValuesFromFirestore(
            collection,
            values = it.values,
            newDeleted = it.deleted,
            fromServer = it.fromServer
          )
          // after we're in sync with firestore, it's time to upload all items we possibly missed
          uploadAllNeededItems()
        }
    }
    return FirestoreActiveOperation(collection, job)
  }

  private suspend fun uploadAllNeededItems() {
    // parallel upload everything
    coroutineScope {
      val results = mutableListOf<Deferred<Unit>>()
      dataFlow.value.values.forEach { pkData ->
        if (pkData.created == null) { // upload it
          results += async { tryUpload(pkData, null) }
        }
      }
      results.forEach { it.await() }
    }
  }


  private fun handleDiffValuesFromFirestore(
    collection: String,
    values: Collection<T>,
    newDeleted: Set<String>,
    fromServer: Boolean
  ) {
    val previous = dataFlow.value
    val newDeletedKeys = newDeleted + values.filter { it.deleted }.map { it.pk() }
    val newCreated = values.filter { !it.deleted }

    val newCollectionToStore = previous - newDeletedKeys + (newCreated.map { it.pk() to it })

    when {
      values.isEmpty() && newDeleted.isEmpty() -> {
        Timber.d("collection $collection, got emit with 0 documents from firestore, fromServer: $fromServer")
        if (fromServer) timestampHolder.updateTimestamp(useCase = collection, EMIT_WITH_0_VALUES)
        return
      }
      previous == newCollectionToStore -> {
        Timber.d("collection $collection, all documents updated already (total: ${previous.size}). got emit: ${newCreated.size} existing and ${newDeletedKeys.size} deleted, fromServer: $fromServer")
        if (fromServer) timestampHolder.updateTimestamp(useCase = collection, IDENTICAL_VALUES_RECEIVED)
        return
      }
      else -> {
        Timber.i("collection $collection, got emit: ${newCreated.size} new and ${newDeletedKeys.size} deleted (new total: ${newCollectionToStore.size}, fromServer: $fromServer). will update local")
        local.overrideAll(newCollectionToStore.values.toSet())
        if (fromServer) timestampHolder.updateTimestamp(useCase = collection, NEW_VALUES_RECEIVED)
        return
      }
    }
  }

  override suspend fun putReplacingOld(pkData: T) {
    val old = local.dataFlow.value[pkData.pk()]
    local.override(pkData)
    tryUpload(pkData, old)
  }

  override suspend fun delete(pkData: T) {
    local.delete(pkData)
    tryDelete(pkData.pk())
  }

  override suspend fun overrideAllTo(newValues: Set<T>) {
    val oldItems = local.dataFlow.value
    val keysToDelete = oldItems.keys - (newValues.map { it.pk() })
    local.overrideAll(newValues)
    coroutineScope {
      val deferred = mutableListOf<Deferred<Unit>>()
      deferred += keysToDelete.map { async { tryDelete(it) } }
      deferred += newValues.map { async { tryUpload(it, oldItems[it.pk()]) } }
      deferred.forEach { it.await() }
    }
  }

  private suspend fun tryUpload(item: T, oldItem: T?) {
    val collection = subCollectionPath.get() ?: run { Timber.w("no subCollection, won't upload $item"); return }
    if (!uploadEnabled.get()) { Timber.w("upload disabled, won't upload $item"); return }
    firestoreRepo.upload(collection, item, oldItem)
  }

  private suspend fun tryDelete(pk: String) {
    val collection = subCollectionPath.get() ?: run { Timber.w("no subCollection, won't delete $pk"); return }
    if (!uploadEnabled.get()) { Timber.w("upload disabled, won't delete $pk"); return }
    firestoreRepo.delete(collection, pk)
  }

  override val currentData: Map<String, T>
    get() = dataFlow.value

  override fun clearLocal(reason: String) {
    val old = dataFlow.value.size
    local.overrideAll(emptySet())
    if (old > 0) {
      Timber.d("${clazz.simpleName}: locally deleted $old values")
    }
  }

  override suspend fun forceUploadLocal() {
    uploadAllNeededItems()
  }
}

sealed class DataStoreCollectionHolderImpl<T: PkData>(
  onChanged: suspend (Map<String, T>) -> Unit,
  private val scope: CoroutineScope = appRedux.scope,
  private val dataStore: AsyncBasedCollectionPkDataStore<T>,
  private val copy: (T) -> T,
  )
  : DataStoreCollectionHolder<T> {

  private val updateRequests = mutableListOf<UpdateRequest>()
  private val isBlockedLocally = AtomicBoolean(true)

  init {
      scope.launch { dataStore.dataFlow.collect { onChanged(it) } }
  }

  protected inner class UpdateRequest(val description: String, val updator: suspend (dataStore: CollectionDataStoreOperations<T>) -> Unit)
  protected fun requestUpdate(description: String, updator: suspend CollectionDataStoreOperations<T>.() -> Unit) {
    updateRequests += UpdateRequest(description, updator)
    tryFlushingUpdateRequests()
  }
  protected suspend fun updateNowIgnoreLocalBlocking(updator: suspend CollectionDataStoreOperations<T>.() -> Unit) = updator(dataStore)

  private fun tryFlushingUpdateRequests() {
    scope.launch {
      if (isBlockedLocally.get()) return@launch
      val listCopy = updateRequests.map { it } // copy the list
      updateRequests.clear()
      listCopy.forEach { request -> request.updator(dataStore) }
    }
  }

  override fun getCurrent() = dataStore.dataFlow.value

  override fun setBlockingLocalChanges(blocking: Boolean) {
    isBlockedLocally.set(blocking)
    if (!blocking) tryFlushingUpdateRequests()
  }

  override fun setUploadEnabled(enabled: Boolean, projectId: String) = dataStore.setUploadEnabled(enabled, calculateSubCollectionPath(projectId))
  override fun setDownloadEnabled(enabled: Boolean, projectId: String) = dataStore.setDownloadEnabled(enabled, calculateSubCollectionPath(projectId))

  override fun deleteLocalData(reason: String) = dataStore.clearLocal(reason)

  override fun setProjectId(projectId: String) =
    dataStore.setCollectionPath(calculateSubCollectionPath(projectId))
  abstract fun calculateSubCollectionPath(projectId: String): String

  override suspend fun forceUploadEverything(projectId: String) {
    setProjectId(projectId)
    dataStore.forceUploadLocal()
  }
}




private val expenseCategoriesSpDataStore: LocalBasedCollectionPkDataStore<ExpenseCategory> by lazy { SpBasedCollectionPkDataStoreImpl(
  clazz = ExpenseCategory::class.java,
  sp = appRedux.app.spWithName("v2_categories")
) }
private val expenseCategoriesFirestoreDataStore: AsyncBasedCollectionPkDataStore<ExpenseCategory> by lazy { FirestoreBasedCollectionPkDataStoreImpl(
  local = expenseCategoriesSpDataStore,
  scope = appRedux.scope,
) }

internal class ExpenseCategoriesDataStore(
  onChanged: suspend (Map<String, ExpenseCategory>) -> Unit,
  eventBus: EventBus = appRedux.eventBus,
  scope: CoroutineScope = appRedux.scope,
  private val context: Application = appRedux.app,
  private val colorsProvider: CategoriesColorsProvider = appRedux.services.categoriesColorsProvider

  ): DataStoreCollectionHolderImpl<ExpenseCategory>(
  onChanged = onChanged,
  scope = scope,
  dataStore = expenseCategoriesFirestoreDataStore,
  copy = { it.copy() }
) {

  override fun calculateSubCollectionPath(projectId: String) =
    V2FirestoreRepo.subCollectionProjectCategories(projectId)

  init {
    eventBus.onEveryEvent<RequestCreateDefaultCategories>(scope) {
      requestUpdate("create default categories") {
        val categories = generateDefaultCategories()
        coroutineScope {
          categories.map { async { putReplacingOld(it) } }
            .map { it.await() }
        }
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }

    eventBus.onEveryEvent<RequestDeleteSingleCategory>(scope) {
      requestUpdate("delete single category $categoryId") {
        currentData[categoryId]?.let { delete(it) }
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
    eventBus.onEveryEvent<UpdateExpenseCategory>(scope) {
      requestUpdate("update category $newCategory") {
        putReplacingOld(newCategory)
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
    eventBus.onEveryEvent<OverrideExpenseCategories>(scope) {
      requestUpdate("override to ${newCategories.size} categories") {
        overrideAllTo(newCategories.toSet())
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
  }

  suspend fun createDefaultCategories() {
    updateNowIgnoreLocalBlocking {
      generateDefaultCategories().forEach { putReplacingOld(it) }
    }
  }


  private fun generateDefaultCategories(): List<ExpenseCategory> {
    fun str(@StringRes str: Int) = context.getString(str)
    val colors = colorsProvider.colorsRes.toList().shuffled().toMutableList()
    // orange looks too similar to the accent color, we allow user to use it but don't want to generate it ourselves
    colors.remove(R.color.category_orange)
    fun randomColor(): Int {
      val first = colors[0]
      colors.remove(first)
      return colorsProvider.calculateHardcodedColor(first)
    }

    // "g_" stands for "generated"
    return listOf(
      ExpenseCategory(
        id = "g_home",
        displayName = str(R.string.default_category_home_display),
        color = randomColor(),
        icon = "home-page"
      ),
      ExpenseCategory(
        id = "g_car",
        displayName = str(R.string.default_category_car_display),
        color = randomColor(),
        icon = "fiat-500"
      ),
      ExpenseCategory(
        id = "g_clothing",
        displayName = str(R.string.default_category_clothing_display),
        color = randomColor(),
        icon = "hanger"
      ),
      ExpenseCategory(
        id = "g_transportation",
        displayName = str(R.string.default_category_transportation_display),
        color = randomColor(),
        icon = "train"
      ),
      ExpenseCategory(
        id = "g_food",
        displayName = str(R.string.default_category_food_display),
        color = randomColor(),
        icon = "shopping-cart-loaded"
      ),
      ExpenseCategory(
        id = "g_fun",
        displayName = str(R.string.default_category_fun_display),
        color = randomColor(),
        icon = "dancing"
      ),
      ExpenseCategory(
        id = "g_eating_out",
        displayName = str(R.string.default_category_eating_out_display),
        color = randomColor(),
        icon = "pizza"
      )
    )
  }
}


private val expenseRulesSpDataStore: LocalBasedCollectionPkDataStore<RecurringExpenseRule> by lazy { SpBasedCollectionPkDataStoreImpl(
  clazz = RecurringExpenseRule::class.java,
  sp = appRedux.app.spWithName("v2_expense_rules")
)}

private val expenseRulesFirestoreDataStore: AsyncBasedCollectionPkDataStore<RecurringExpenseRule> by lazy { FirestoreBasedCollectionPkDataStoreImpl(
  local = expenseRulesSpDataStore,
  scope = appRedux.scope,
) }


internal class ExpenseRulesDataStore(
  onChanged: suspend (Map<String, RecurringExpenseRule>) -> Unit,
  scope: CoroutineScope = appRedux.scope,
  eventBus: EventBus = appRedux.eventBus,
  ): DataStoreCollectionHolderImpl<RecurringExpenseRule>(
  onChanged = onChanged,
  scope = scope,
  dataStore = expenseRulesFirestoreDataStore,
  copy = { it.copy() }
) {

  override fun calculateSubCollectionPath(projectId: String) =
    V2FirestoreRepo.subCollectionProjectExpenseRules(projectId)

  init {
    eventBus.onEveryEvent<UpdateExpenseRule>(scope) {
      requestUpdate("update rule ${newRule.pk()}") {
        putReplacingOld(newRule)
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }

    eventBus.onEveryEvent<OverrideExpenseRules>(scope) {
      requestUpdate("override to ${newRules.size} rules") {
        overrideAllTo(newRules.toSet())
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
    eventBus.onEveryEvent<RequestDeleteSingleExpenseRule>(scope) {
      requestUpdate("delete rule $ruleId") {
        currentData[ruleId]?.let { delete(it) }
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
  }
}



private val oneTimeExpensesSpDataStore: LocalBasedCollectionPkDataStore<OneTimeExpenseRecord> by lazy { SpBasedCollectionPkDataStoreImpl(
  clazz = OneTimeExpenseRecord::class.java,
  sp = appRedux.app.spWithName("v2_one_time_expenses")
)}

private val oneTimeExpensesFirestoreDataStore: AsyncBasedCollectionPkDataStore<OneTimeExpenseRecord> by lazy { FirestoreBasedCollectionPkDataStoreImpl(
  local = oneTimeExpensesSpDataStore,
  scope = appRedux.scope,
) }

internal class OneTimeExpensesDataStore(
  onChanged: suspend (Map<String, OneTimeExpenseRecord>) -> Unit,
  scope: CoroutineScope = appRedux.scope,
  eventBus: EventBus = appRedux.eventBus,
)  : DataStoreCollectionHolderImpl<OneTimeExpenseRecord>(
onChanged = onChanged,
scope = scope,
dataStore = oneTimeExpensesFirestoreDataStore,
copy = { it.copy() }
) {

  override fun calculateSubCollectionPath(projectId: String) =
    V2FirestoreRepo.subCollectionProjectOneTimeExpenses(projectId)

  init {
    eventBus.onEveryEvent<UpdateOneTimeExpense>(scope) {
      requestUpdate("update expense $newExpense") {
        putReplacingOld(newExpense)
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
    eventBus.onEveryEvent<OverrideOneTimeExpenses>(scope) {
      requestUpdate("override to ${newExpenses.size} expenses") {
        overrideAllTo(newExpenses.toSet())
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
    eventBus.onEveryEvent<RequestDeleteSingleOneTimeExpense>(scope) {
      requestUpdate("delete expense $expenseId") {
        currentData[expenseId]?.let { delete(it) }
        eventBus.sendResponseFor(this@onEveryEvent)
      }
    }
  }
}
