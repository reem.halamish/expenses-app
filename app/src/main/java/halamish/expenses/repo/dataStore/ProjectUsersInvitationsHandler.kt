package halamish.expenses.repo.dataStore

import com.google.firebase.firestore.FieldValue
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.repo.NewUserInProjectFcmRequest
import halamish.expenses.repo.UserRemovedFromProjectFcmRequest
import halamish.expenses.repo.dataStore.ProjectUsersInvitationsHandler.UiAction
import halamish.expenses.v2.*
import halamish.expenses.v2.V2FirestoreRepo.Companion.downloadProject
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import timber.log.Timber

interface ProjectUsersInvitationsHandler {
    suspend fun acceptInvitation(newProjectId: String)

    enum class UiAction { DO_NOTHING, GO_BACK }
    suspend fun removeUser(userIdToRemove: String): UiAction
}

class ProjectUsersInvitationsHandlerImpl(
    val appStateFlow: StateFlow<V2State>,
    val eventBus: EventBus,
    val firestoreRepo: V2FirestoreRepo,
    val timestampHolder: V2FirestoreTimestampHolder,
    val myDeviceId: String,
    ): ProjectUsersInvitationsHandler {
    override suspend fun acceptInvitation(newProjectId: String) {
        Timber.d("request to accept invitation, newProjectId:$newProjectId")
        if (newProjectId.isEmpty()) {
            Timber.e("can't change to empty project!")
            return
        }
        val appState = appStateFlow.value
        val featureConfig = appState.config.projectInvitations
        if (!featureConfig.enabled) {
            Timber.e("won't accept invitation to $newProjectId - feature disabled")
            return
        }

        if (appState.project.id == newProjectId && appState.userInfo?.projectName == newProjectId) {
            Timber.w("invitation already accepted. nothing to do")
            return
        }
        val oldProject = appState.project

        val userData = appState.user as? LoggedInUser ?: run {
            Timber.e("won't accept invitation to $newProjectId - user is not connected")
            return
        }
        val userId = userData.userId

        Timber.i("will add user into project. projectId($newProjectId), userId($userId), data($userData)")

        // 1st download - just to make sure project exists
        firestoreRepo.downloadProject(newProjectId) ?: run {
            Timber.e("won't accept invitation to $newProjectId - no project do download")
            return
        }

        firestoreRepo.editDocFields(V2FirestoreRepo.collectionUsers, userId, mapOf(
            UserInfo::projectName.name to newProjectId,
            UserInfo::edit.name to FieldValue.serverTimestamp(),
            UserInfo::lastEditBy.name to myDeviceId,
        ))

        firestoreRepo.editDocFields(V2FirestoreRepo.collectionProjects, newProjectId, mapOf(
            ((ProjectDetails::users.name) + "." + userId) to (userData.email ?: ""),
            ProjectDetails::edit.name to FieldValue.serverTimestamp(),
            ProjectDetails::lastEditBy.name to myDeviceId,
        ))

        // re-run login and wait for the new user data to show locally in app
        eventBus.send(V2UserLoginVerifier.RecheckUserValidation)
        appStateFlow.first { it.userInfo?.projectName == newProjectId }
        appStateFlow.first { it.project.id == newProjectId }
        Timber.i("accept invitation: success!")

        if (appState.config.hasBackEnd) {
            firestoreRepo.requestFcm(NewUserInProjectFcmRequest(
                projectId = newProjectId,
                deviceId = appState.deviceId,
                userData = userData
            ))
            Timber.d("did request FCM to notify new user")
        }

        // do cleanup
        val userString = if (oldProject.creator == userId) "creator-user($userId)" else "collaborator-user($userId)"
        if (oldProject.users.keys == setOf(userId) || oldProject.users.isEmpty()) {
            val modifiedOldProject = oldProject.clone().withHistoryEntry("single user $userString invited to different project($newProjectId), deleted this project")
            firestoreRepo.upload(V2FirestoreRepo.collectionProjects, modifiedOldProject)
            firestoreRepo.delete(V2FirestoreRepo.collectionProjects, oldProject.id)
        } else {
            firestoreRepo.upload(
                V2FirestoreRepo.collectionProjects,
                oldProject.copy(
                    users = oldProject.users.filter { it.key != userId },
                    devices = oldProject.devices.filter { it.key != appState.deviceId }
                    )
                    .withHistoryEntry("$userString was invited to different project($newProjectId), removing the user and their device(${appState.deviceId}) from the project"),
                oldDocument = oldProject
            )
        }
        Timber.i("did clean up old project")
    }

    override suspend fun removeUser(userIdToRemove: String): UiAction {
        val appState = appStateFlow.value
        val project = appState.project
        Timber.d("request to remove user:$userIdToRemove from project:${project.id}, project creator:${project.creator}")
        if (userIdToRemove.isEmpty()) {
            Timber.w("can't remove empty user")
            return UiAction.DO_NOTHING
        }
        val myUserId = appStateFlow.value.userInfo?.uid
        if (myUserId == null) {
            Timber.e("not connected, can't continue")
            return UiAction.DO_NOTHING
        }
        if (myUserId != userIdToRemove && myUserId != project.creator) {
            Timber.e("can't remove other user - I'm not the creator")
            return UiAction.DO_NOTHING
        }
        if (myUserId == userIdToRemove && myUserId == project.creator) {
            Timber.w("can't remove myself - I'm the owner")
            return UiAction.DO_NOTHING
        }
        if (userIdToRemove !in project.users) {
            Timber.w("nothing to do - user is not in project")
            return UiAction.DO_NOTHING
        }
        eventBus.sendAwaitingNext(UpdateProjectDetailsRunnable("remove user($userIdToRemove)") {
            copy(users = users - userIdToRemove)
                .withHistoryEntry("user($myUserId) removed user($userIdToRemove)")
        })

        if (appState.config.hasBackEnd) {
            firestoreRepo.requestFcm(UserRemovedFromProjectFcmRequest(
                projectId = project.id,
                deviceId = appState.deviceId,
                userId = userIdToRemove
            ))
            Timber.d("did request FCM to notify removed user")
        }

        if (myUserId == userIdToRemove) {
            Timber.d("removed myself as collaborator - recheck login to trigger new project creation")
//            timestampHolder.clear()
            eventBus.send(V2UserLoginVerifier.RecheckUserValidation)
            return UiAction.GO_BACK
        }
        else return UiAction.DO_NOTHING
    }
}
