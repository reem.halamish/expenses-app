package halamish.expenses.repo.dataStore

import android.app.Application
import android.content.SharedPreferences
import androidx.annotation.Keep
import halamish.expenses.repo.dataStore.V2FirestoreTimestampHolder.UpdateTimestampReason
import halamish.expenses.ui.spWithName
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import timber.log.Timber

interface V2FirestoreTimestampHolder {
  fun getStoredTimestamp(useCase: String): Instant?
  fun updateTimestamp(useCase: String, reason: UpdateTimestampReason): Instant
  fun clearTimestamp(useCase: String)
  fun clear()

  @Keep
  enum class UpdateTimestampReason { NEW_VALUES_RECEIVED, IDENTICAL_VALUES_RECEIVED, EMIT_WITH_0_VALUES, INITIAL_DOWNLOAD_DONE }
}

class V2FirestoreTimestampHolderImpl(
  app: Application,
  private val sp: SharedPreferences = app.spWithName("firestore_timestamps")
) : V2FirestoreTimestampHolder {

  private val timestamps = sp.all.keys
    .map { it to sp.getLong(it, EARLIEST_TIMESTAMP_EVER) }.toMap()
    .filterValues { it >= EARLIEST_TIMESTAMP_EVER }
    .filterValues { it >= timestampTooOld }
    .toMutableMap()

  override fun getStoredTimestamp(useCase: String): Instant? {
    return timestamps[useCase]?.toInstant
  }

  override fun updateTimestamp(useCase: String, reason: UpdateTimestampReason): Instant {
    val existing = timestamps[useCase] ?: EARLIEST_TIMESTAMP_EVER
    val timestamp = Instant.now().minus(Duration.ofHours(12)).toEpochMilli() // add some safe area
    if (timestamp < existing) return existing.toInstant
    Timber.d("will store timestamp ${timestamp.toInstant} for use-case $useCase reason $reason")
    sp.edit().putLong(useCase, timestamp).apply()
    timestamps[useCase] = timestamp
    return timestamp.toInstant
  }

  override fun clearTimestamp(useCase: String) {
    sp.edit().remove(useCase).apply()
    timestamps.remove(useCase)
  }

  override fun clear() {
    sp.edit().clear().apply()
    timestamps.clear()
  }

  companion object {
    private const val EARLIEST_TIMESTAMP_EVER = 0L
    private val timestampTooOld by lazy { Instant.now().minus(Duration.ofDays(60)).toEpochMilli() }
    private val Long.toInstant get() = Instant.ofEpochMilli(this)
  }
}