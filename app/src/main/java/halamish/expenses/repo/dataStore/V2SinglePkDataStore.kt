package halamish.expenses.repo.dataStore

import android.content.SharedPreferences
import androidx.core.content.edit
import extensions.android.fromJson
import extensions.android.toJson
import halamish.expenses.data.ProjectDetails
import halamish.expenses.data.UserInfo
import halamish.expenses.repo.PkData
import halamish.expenses.ui.spWithName
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean

data class DataWithReason<T>(val data: T, val lastChangeReason: String)

private interface LocalBasedSinglePkDataStore<T: PkData, NullableT: T?> {
  val clazz: Class<T>
  val dataFlow: StateFlow<DataWithReason<NullableT>>
  fun store(pkData: NullableT, reason: String)
  fun clear(reason: String)
}

private interface AsyncBasedSinglePkDataStore<T: PkData, NullableT: T?> {
  val dataFlow: StateFlow<DataWithReason<NullableT>>

  fun storeLocallyOnly(newDoc: NullableT, reason: String)
  fun clearLocal(reason: String)
  suspend fun forceUploadLocal()

  suspend fun store(newDoc: NullableT, reason: String)

  fun setDownloadEnabled(enabled: Boolean)
  fun setUploadEnabled(enabled: Boolean)
}

private class SpBasedSinglePkDataStoreImpl<T: PkData, NullableT: T?> (
  override val clazz: Class<T>,
  private val sp: SharedPreferences,
  private val spKey: String = clazz.name,
  private val valueCreatorIfNotStored: () -> NullableT,
        ): LocalBasedSinglePkDataStore<T, NullableT> {

  override val dataFlow: MutableStateFlow<DataWithReason<NullableT>>

  init {
    val storedInstance = sp.getString(spKey, null)?.fromJson(clazz = clazz)
    if (storedInstance != null) {
      dataFlow = MutableStateFlow(DataWithReason(storedInstance as NullableT, "loaded stored data"))
    } else {
      val newValue = valueCreatorIfNotStored()
      dataFlow = MutableStateFlow(DataWithReason(newValue, "no stored data, creator-provided data"))
      if (newValue != null) {
        handleNewData(newValue, "store creator-provided data")
      }
    }
  }

  override fun store(pkData: NullableT, reason: String) {
    handleNewData(pkData, reason)
  }

  override fun clear(reason: String) {
    handleNewData(valueCreatorIfNotStored(), reason)
  }

  private fun handleNewData(data: NullableT, reason: String) {
    sp.edit {
      if (data != null && !data.deleted)
        putString(spKey, data.toJson())
      else
        remove(spKey)
    }
    dataFlow.value = DataWithReason(data, reason)
  }
}

private class FirestoreBasedSinglePkDataStore<T: PkData, NullableT: T?>(
  private val scope: CoroutineScope,
  private val localSp: LocalBasedSinglePkDataStore<T, NullableT>,
  private val firestoreCollection: String,
  private val firestore: V2FirestoreRepo,
  private val copy: (T) -> T,

  ): AsyncBasedSinglePkDataStore<T, NullableT> {

  private val uploadEnabled = AtomicBoolean(false)
  private val downloadEnabled = AtomicBoolean(false)

  private data class ActiveDownload(val pk: String, val job: Job)
  private var currentlyActiveDownload: ActiveDownload? = null

  override val dataFlow get() = localSp.dataFlow

  override fun storeLocallyOnly(newDoc: NullableT, reason: String) = localSp.store(newDoc, reason)

  override suspend fun store(newDoc: NullableT, reason: String) {
    val oldDoc: NullableT = localSp.dataFlow.value.data
    localSp.store(newDoc, reason)

    if (uploadEnabled.get()) {
      when {
        oldDoc == null && newDoc == null -> {} // nothing to do
        oldDoc == null && newDoc != null -> { // just upload the new
          firestore.upload(firestoreCollection, newDoc)
        }
        oldDoc != null && newDoc == null -> {
          firestore.delete(firestoreCollection, oldDoc.pk())
        }
        oldDoc != null && newDoc != null -> { // both non null
          if (oldDoc.pk() != newDoc.pk()) {
            // different docs. delete old, upload new
            firestore.delete(firestoreCollection, oldDoc.pk())
            firestore.upload(firestoreCollection, copy(newDoc))
          } else {
            // same doc. upload just the delta between new & old
            firestore.upload(firestoreCollection, copy(newDoc), oldDoc)
          }
        }
      }
    }

    recalculateActiveDownload()
  }

  override fun setDownloadEnabled(enabled: Boolean) {
    downloadEnabled.set(enabled)
    recalculateActiveDownload()
  }

  @Synchronized
  private fun recalculateActiveDownload() {
    val clazz = localSp.clazz
    val active = currentlyActiveDownload
    val pkToDownload = if (downloadEnabled.get()) localSp.dataFlow.value.data?.pk() else null
    if (pkToDownload == null) {
      active?.job?.cancel("shouldn't download anymore (clazz:$clazz)")
      currentlyActiveDownload = null
      return
    }

    if (pkToDownload == active?.pk) {
      return // all good, still downloading this item
    }

    active?.job?.cancel("shouldn't download ${active.pk} anymore (clazz:$clazz), will start downloading $pkToDownload instead")
    val job = Job()
    currentlyActiveDownload = ActiveDownload(pkToDownload, job)
    scope.launch(job) {
      firestore.observeSingleDoc(firestoreCollection, pkToDownload, clazz)
        .collect { pkData: T? ->
          pkData as NullableT?
          Timber.d("got data emit (pk: ${pkData?.pk()}, clazz:${clazz.simpleName})")
          if (pkData == null)
            localSp.clear("firestore says not existing")
          else if (pkData.deleted)
            localSp.clear("firestore says deleted")
          else
            localSp.store(pkData, "new emit from firestore")
        }
    }
  }

  override fun setUploadEnabled(enabled: Boolean) {
    uploadEnabled.set(enabled)
  }

  override suspend fun forceUploadLocal() {
    val current = localSp.dataFlow.value.data
    when {
      current == null -> Unit // nothing to do
      current.deleted -> firestore.delete(firestoreCollection, current.pk())
      else -> firestore.upload(firestoreCollection, current)
    }
  }

  override fun clearLocal(reason: String) {
    localSp.clear(reason)
  }
}

sealed class DataStoreSingleItemHolderImpl<T: PkData, NullableT: T?>(
  private val dataStore: AsyncBasedSinglePkDataStore<T, NullableT>,
  onChanged: suspend (description: String, NullableT) -> Unit,
  private val copy: (T) -> T,
  private val scope: CoroutineScope = appRedux.scope,
): DataStoreSingleItemHolder<NullableT> {
  private val updateRequests = mutableListOf<UpdateRequest>()
  private val isBlockedLocally = AtomicBoolean(true)

  init {
    scope.launch { dataStore.dataFlow.collect { onChanged(it.lastChangeReason, it.data) } }
  }

  inner class UpdateRequest(val description: String, val updator: (T) -> T, val onResult: (success: Boolean, newValue: NullableT) -> Unit)
  fun requestUpdate(request: UpdateRequest) {
    updateRequests += request
    tryFlushingUpdateRequests()
  }

  private fun tryFlushingUpdateRequests() {
    scope.launch {
      if (isBlockedLocally.get()) return@launch
      val listCopy = updateRequests.map { it } // copy the list
      updateRequests.clear()
      listCopy.forEach { request ->
          val old = dataStore.dataFlow.value.data
          if (old == null) {
            request.onResult(false, old)
            return@forEach
          }

          val new = copy(copy(old).let(request.updator)) as NullableT
          if (old != new) dataStore.store(new, request.description)
          request.onResult(true, new)
        }
    }
  }

  override fun setBlockingLocalChanges(blocking: Boolean) {
    isBlockedLocally.set(blocking)
    if (!blocking) tryFlushingUpdateRequests()
  }

  override fun getCurrent(): NullableT = dataStore.dataFlow.value.data

  override fun setUploadEnabled(enabled: Boolean) = dataStore.setUploadEnabled(enabled)
  override fun setDownloadEnabled(enabled: Boolean) = dataStore.setDownloadEnabled(enabled)

  override fun deleteLocalData(reason: String) = dataStore.clearLocal(reason)
  override fun setLocalData(newValue: NullableT, reason: String) =
    dataStore.storeLocallyOnly(newValue, reason)
}

private val spFirestoreSingleDocs by lazy { appRedux.app.spWithName("firestore_single_documents") }

private val userInfoSpDataStore: LocalBasedSinglePkDataStore<UserInfo, UserInfo?> by lazy {SpBasedSinglePkDataStoreImpl(
  clazz = UserInfo::class.java,
  sp = spFirestoreSingleDocs,
  spKey = "user_info",
  valueCreatorIfNotStored = { null },
)}
private val userInfoFirestoreDataStore: AsyncBasedSinglePkDataStore<UserInfo, UserInfo?> by lazy { FirestoreBasedSinglePkDataStore(
  localSp = userInfoSpDataStore,
  scope = appRedux.scope,
  firestoreCollection = V2FirestoreRepo.collectionUsers,
  firestore = appRedux.services.firestoreRepo,
  copy = { it.copy() }
)}
internal class UserInfoDataStore(
  onChanged: suspend (description: String, UserInfo?) -> Unit,
  scope: CoroutineScope = appRedux.scope,
  private val eventBus: EventBus = appRedux.eventBus,
  )
  : DataStoreSingleItemHolderImpl<UserInfo, UserInfo?>(
  dataStore = userInfoFirestoreDataStore,
  onChanged = onChanged,
  copy = { it.copy() },
) {

  init {

    eventBus.onEveryEvent<UpdateUserInfo>(scope) {
      requestUpdate(UpdateRequest(description, updater, onResult = { success, newUser ->
        eventBus.send(DidUpdateUserInfo(success = true, original = this, newValue = newUser?.copy()))
      }))
    }
  }
}


private val projectInfoSpDataStore: LocalBasedSinglePkDataStore<ProjectDetails, ProjectDetails> by lazy { SpBasedSinglePkDataStoreImpl(
  clazz = ProjectDetails::class.java,
  sp = spFirestoreSingleDocs,
  spKey = "project_details",
  valueCreatorIfNotStored = { ProjectDetails.createClean(appRedux.services.deviceId, creationReason = "create clean project")}
) }
private val projectInfoFirestoreDataStore: AsyncBasedSinglePkDataStore<ProjectDetails, ProjectDetails> by lazy { FirestoreBasedSinglePkDataStore(
  localSp = projectInfoSpDataStore,
  scope = appRedux.scope,
  firestoreCollection = V2FirestoreRepo.collectionProjects,
  firestore = appRedux.services.firestoreRepo,
  copy = { it.clone() }
) }

internal class ProjectDetailsDataStore(
  scope: CoroutineScope = appRedux.scope,
  private val eventBus: EventBus = appRedux.eventBus,
  onChanged: suspend (description: String, ProjectDetails) -> Unit
) : DataStoreSingleItemHolderImpl<ProjectDetails, ProjectDetails>(
  dataStore = projectInfoFirestoreDataStore,
  onChanged = onChanged,
  copy = { it.clone() },
) {

  init {
    eventBus.onEveryEvent<UpdateProjectDetailsRunnable>(scope) {
      requestUpdate(UpdateRequest(description, updater, onResult = { success, newProject ->
          eventBus.send(DidUpdateProjectDetailsRunnable(success, newProject, originalEvent = this))
        }
      ))
    }
  }
}
