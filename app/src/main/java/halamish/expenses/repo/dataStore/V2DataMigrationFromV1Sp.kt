package halamish.expenses.repo.dataStore

import android.app.Application
import extensions.android.fromJson
import halamish.expenses.data.ExpenseCategory
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.repo.PkData
import halamish.expenses.ui.sp
import halamish.expenses.ui.spWithName
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class V2DataMigrationFromV1Sp(
  private val app: Application = appRedux.app,
  scope: CoroutineScope = appRedux.scope,
  eventBus: EventBus = appRedux.eventBus
) {

  private data class CollectionMeta<T : PkData>(
    val name: String,
    val cls: Class<T>,
    val firestorePath: String? = null
  )

  private fun sp(meta: CollectionMeta<*>) = app.spWithName("sp_destination_sp_dest_${meta.name}")
  private fun defaultSp() = app.sp
  private val categoriesMeta = CollectionMeta("categories", ExpenseCategory::class.java)
  private val oneTimeExpensesMeta =
    CollectionMeta("one_time_expenses", OneTimeExpenseRecord::class.java)
  private val expenseRulesMeta = CollectionMeta("expense_rules", RecurringExpenseRule::class.java)

  private val didMigrateOrDidDelete =
    defaultSp().getBoolean("did_migrateOrDelete_data_from_V1", false)

  private fun setMigratedOrDeletedDone() =
    defaultSp().edit().putBoolean("did_migrateOrDelete_data_from_V1", true).apply()

  init {
    eventBus.onEveryEvent<RequestMigrateFromV1OldSpData>(scope) {
      if (didMigrateOrDidDelete) {
        eventBus.send(MigrateFromOldDataResult())
        return@onEveryEvent
      }
      val categoriesSp = sp(categoriesMeta)
      val categories = categoriesSp.all.keys
        .map { categoriesSp.getString(it, null) }
        .map { it?.fromJson(categoriesMeta.cls) }
        .filterNotNull()

      val expenseRulesSp = sp(expenseRulesMeta)
      val expenseRules = expenseRulesSp.all.keys
        .map { expenseRulesSp.getString(it, null) }
        .map { it?.fromJson(expenseRulesMeta.cls) }
        .filterNotNull()


      val oneTimesSp = sp(oneTimeExpensesMeta)
      val oneTimes = oneTimesSp.all.keys
        .map { oneTimesSp.getString(it, null) }
        .map { it?.fromJson(oneTimeExpensesMeta.cls) }
        .filterNotNull()

      val migrationResult = MigrateFromOldDataResult(
        categories = categories.size,
        expenseRules = expenseRules.size,
        oneTimeExpenses = oneTimes.size
      )
      if (migrationResult.hasData) {
        eventBus.sendAwaitingResponse(OverrideExpenseCategories(categories))
        eventBus.sendAwaitingResponse(OverrideExpenseRules(expenseRules))
        eventBus.sendAwaitingResponse(OverrideOneTimeExpenses(oneTimes))
        Timber.i("migrated data from version 1.2.1: $migrationResult")
      }

      withContext(Dispatchers.IO) {
        logAllV1SpFilesSizes()
        deleteAllV1SpFiles()
        setMigratedOrDeletedDone()
      }

      eventBus.send(migrationResult)
    }

    eventBus.onEveryEvent<RequestDeleteV1OldSpData>(scope) {
      withContext(Dispatchers.IO) {
        logAllV1SpFilesSizes()
        deleteAllV1SpFiles()
        setMigratedOrDeletedDone()
      }
      eventBus.sendResponseFor(this)
    }
  }

  private fun logAllV1SpFilesSizes() {
    Timber.d("V1 data: ${sp(categoriesMeta).all.size} categories")
    Timber.d("V1 data: ${sp(oneTimeExpensesMeta).all.size} one-times")
    Timber.d("V1 data: ${sp(expenseRulesMeta).all.size} rules")
  }

  private fun deleteAllV1SpFiles() {
    sp(categoriesMeta).edit().clear().apply()
    sp(oneTimeExpensesMeta).edit().clear().apply()
    sp(expenseRulesMeta).edit().clear().apply()
  }
}
