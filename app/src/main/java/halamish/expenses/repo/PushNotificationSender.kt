package halamish.expenses.repo

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Intent
import android.os.Build
import androidx.annotation.Keep
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import extensions.android.mapState
import halamish.expenses.R
import halamish.expenses.data.OneTimeExpenseRecord
import halamish.expenses.data.RecurringExpenseRule
import halamish.expenses.data.repeatAmount
import halamish.expenses.repo.PushNotificationSender.NotificationBehavior.OPEN_HISTORY
import halamish.expenses.repo.PushNotificationSender.NotificationBehavior.UNKNOWN
import halamish.expenses.repo.currency.CurrencyFormatter
import halamish.expenses.ui.main.MainActivity
import halamish.expenses.v2.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow
import timber.log.Timber

class PushNotificationSender(
  scope: CoroutineScope = appRedux.scope,
  private val app: Application = appRedux.app,
  eventBus: EventBus = appRedux.eventBus,
  private val expensesData: StateFlow<ExpensesData> = appRedux.state.mapState(scope) { it.data },
  private val analytics: AnalyticsLogger = appRedux.services.analytics,
  private val currencyFormatter: () -> CurrencyFormatter = appRedux.services.currencyFormatter
) {

  init {
    eventBus.onEveryEvent<FcmPushOneTimeExpenseCreated>(scope) {
      buildExpenseNotificationChannelIfNeeded()
      prepareNotification(expense)
    }
    eventBus.onEveryEvent<FcmPushRuleCreated>(scope) {
      buildExpenseNotificationChannelIfNeeded()
      prepareNotification(rule)
    }
    eventBus.onEveryEvent<FcmPushKickedFromProject>(scope) {
      buildUserNotificationChannelIfNeeded()
      prepareNotification(ownerDescription, projectId)
    }
  }

  private fun prepareNotification(ownerDescription: String, projectId: String) {
    sendNotification(
      analyticsData = AnalyticsPushData(
        type = "removed_from_project",
        id = projectId,
      ),
      behavior = UNKNOWN,
      title = app.getString(R.string.notification_kicked_out_title),
      msg = app.getString(R.string.notification_kicked_out_msg_ps, ownerDescription),
      channelId = CHANNEL_ID_IMPORTANT_EVENTS,
      group = GROUP_IMPORTANT_EVENTS
    )
  }

  private fun buildExpenseNotificationChannelIfNeeded() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val name: CharSequence = "Expenses updates"
      val description = app.getString(R.string.notification_channel_expense)
      val importance = NotificationManager.IMPORTANCE_DEFAULT
      val channel = NotificationChannel(CHANNEL_ID_EXPENSES, name, importance)
      channel.description = description
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      val notificationManager = NotificationManagerCompat.from(app)
      if (notificationManager.notificationChannels.any { CHANNEL_ID_EXPENSES == it.id }) return
      notificationManager.createNotificationChannel(channel)
    }
  }


  private fun buildUserNotificationChannelIfNeeded() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val name: CharSequence = CHANNEL_ID_IMPORTANT_EVENTS
      val description = app.getString(R.string.notification_channel_important_events)
      val importance = NotificationManager.IMPORTANCE_HIGH
      val channel = NotificationChannel(CHANNEL_ID_IMPORTANT_EVENTS, name, importance)
      channel.description = description
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      val notificationManager = NotificationManagerCompat.from(app)
      if (notificationManager.notificationChannels.any { CHANNEL_ID_IMPORTANT_EVENTS == it.id }) return
      notificationManager.createNotificationChannel(channel)
    }
  }

  private fun getCategory(categoryId: String) = expensesData.value.categories[categoryId]

  private fun prepareNotification(expense: OneTimeExpenseRecord) {
    val title = app.getString(R.string.notification_expense_added_title)
    val msgProto = when {
      expense.comments?.takeIf { it.isNotEmpty() } != null -> {
        app.getString(R.string.notification_expense_added_msg_with_comments)
          .replace("[COMMENT]", expense.comments)
      }
      else -> { // no comments
        app.getString(R.string.notification_expense_added_msg_no_comments)
      }
    }

    val msg = msgProto
      .replace("[AMOUNT]", currencyFormatter().format(expense.amount))
      .replace(
        "[CATEGORY]",
        getCategory(expense.categoryId)?.displayName
          ?: app.getString(R.string.notification__prototype_category_default_if_absent)
      )

    Timber.i("will show notification for expense ${expense.id}")
    sendNotification(
      analyticsData = AnalyticsPushData(
        type = "expense_1time_created",
        id = expense.id,
      ),
      behavior = OPEN_HISTORY,
      title = title,
      msg = msg,
      channelId = CHANNEL_ID_EXPENSES,
      group = GROUP_NEW_EXPENSES
    )
  }

  private fun prepareNotification(rule: RecurringExpenseRule) {
    val title = app.getString(R.string.notification_rule_added_title)
    val msgProto = when {
      rule.comments?.takeIf { it.isNotEmpty() } != null -> {
        app.getString(R.string.notification_rule_added_msg_with_comments)
          .replace("[COMMENT]", rule.comments)
      }
      else -> { // no comments
        app.getString(R.string.notification_rule_added_msg_no_comments)
      }
    }

    val recurringString = when {
      rule.isForever() -> app.getString(R.string.notification_rule_added_prototype_rule_infinite)
      else -> app.getString(R.string.notification_rule_added_prototype_rule_finite)
        .replace("[TIMES]", rule.repeatAmount.toString())
    }

    val msg = msgProto
      .replace("[AMOUNT]", currencyFormatter().format(rule.amount))
      .replace(
        "[CATEGORY]",
        getCategory(rule.categoryId)?.displayName
          ?: app.getString(R.string.notification__prototype_category_default_if_absent)
      )
      .replace("[RECURRING]", recurringString)

    Timber.i("will show notification for rule ${rule.id}")
    sendNotification(
      analyticsData = AnalyticsPushData(
        type = "expense_rule_created",
        id = rule.id,
        ),
      behavior = OPEN_HISTORY,
      title = title,
      msg = msg,
      channelId = CHANNEL_ID_EXPENSES,
      group = GROUP_NEW_EXPENSES
    )
  }

  private fun sendNotification(analyticsData: AnalyticsPushData, behavior: NotificationBehavior, title: String, msg: String, channelId: String, group: String, priority: Int = NotificationCompat.PRIORITY_DEFAULT) {
    Timber.d("title: $title")
    Timber.d("msg: $msg")


    val contentIntent = PendingIntent.getActivity(
      app, 0, Intent(app, MainActivity::class.java).apply {
         flags = flags or Intent.FLAG_ACTIVITY_SINGLE_TOP
         putExtra(EXTRA_IS_INTERNAL_PUSH_NOTIFICATION, true)
         putExtra(EXTRA_PUSH_NOTIFICATION_ANALYTICS_DATA, analyticsData)
         putExtra(EXTRA_PUSH_NOTIFICATION_BEHAVIOR, behavior.name)
      }, FLAG_UPDATE_CURRENT or FLAG_IMMUTABLE
    )

    val builder = NotificationCompat.Builder(app, channelId)
      .setSmallIcon(R.drawable.ic_monetization_on_24dp) // todo based on the app flavor
      .setContentIntent(contentIntent)
      .setContentTitle(title)
      .setContentText(msg)
      .setAutoCancel(true)
      .setChannelId(channelId)
      .setGroup(group)
      .setPriority(priority)

    val notificationID = (title + msg).hashCode()

    NotificationManagerCompat.from(app).notify(notificationID, builder.build())
    analytics.pushCreated(analyticsData)
  }

  @Keep
  enum class NotificationBehavior {
    OPEN_HISTORY, DO_NOTHING, UNKNOWN
  }

  companion object {
    const val EXTRA_IS_INTERNAL_PUSH_NOTIFICATION = "halamish.expenses.EXTRA_IS_INTERNAL_PUSH_NOTIFICATION"
    const val EXTRA_PUSH_NOTIFICATION_ANALYTICS_DATA = "halamish.expenses.EXTRA_PUSH_NOTIFICATION_ANALYTICS_DATA"
    const val EXTRA_PUSH_NOTIFICATION_BEHAVIOR = "halamish.expenses.EXTRA_PUSH_NOTIFICATION_BEHAVIOR"

    private const val CHANNEL_ID_EXPENSES = "expenses"
    private const val CHANNEL_ID_IMPORTANT_EVENTS = "Important events"
    private const val GROUP_NEW_EXPENSES = "halamish.expenses.GROUP_NEW_EXPENSES"
    private const val GROUP_IMPORTANT_EVENTS = "halamish.expenses.GROUP_IMPORTANT_EVENTS"
  }
}