package halamish.expenses.repo

import com.google.firebase.crashlytics.FirebaseCrashlytics

interface CustomAppKeys {
  fun setValue(key: String, value: Boolean)
  fun setValue(key: String, value: Int)
  fun setValue(key: String, value: Long)
  fun setValue(key: String, value: Float)
  fun setValue(key: String, value: Double)
  fun setValue(key: String, value: String)
  fun setUserId(userId: String)
}

class CustomAppKeysImpl(
  private val crashlytics: FirebaseCrashlytics = FirebaseCrashlytics.getInstance()
) : CustomAppKeys {
  override fun setValue(key: String, value: Boolean) = crashlytics.setCustomKey(key, value)
  override fun setValue(key: String, value: Int) = crashlytics.setCustomKey(key, value)
  override fun setValue(key: String, value: Long) = crashlytics.setCustomKey(key, value)
  override fun setValue(key: String, value: Float) = crashlytics.setCustomKey(key, value)
  override fun setValue(key: String, value: Double) = crashlytics.setCustomKey(key, value)
  override fun setValue(key: String, value: String) = crashlytics.setCustomKey(key, value)
  override fun setUserId(userId: String) = crashlytics.setUserId(userId)
}