package halamish.expenses.repo.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import extensions.android.defaultGson
import halamish.expenses.repo.AnalyticsLogger
import halamish.expenses.repo.FirebaseAnalyticsLogger
import halamish.expenses.ui.sp
import halamish.expenses.ui.spWithName

@Module
class SingletonsModule(@ApplicationContext private val context: Context) {
  @ProcessLifeCycle
  @Provides
  @ApplicationContext
  fun context() = context

  @ProcessLifeCycle
  @Provides
  fun gson(): Gson = defaultGson


  @Provides
  fun firebaseFirestore(): FirebaseFirestore = FirebaseFirestore.getInstance()


  @Provides
  @ProcessLifeCycle
  fun analytics(@ApplicationContext context: Context): FirebaseAnalytics =
    FirebaseAnalytics.getInstance(context)


  @Provides
  @ProcessLifeCycle
  fun analyticsLogger(
    @ApplicationContext context: Context,
    firebaseAnalytics: FirebaseAnalytics
  ): AnalyticsLogger = FirebaseAnalyticsLogger(context as Application, firebaseAnalytics)

  @ProcessLifeCycle
  @DefaultSp
  @Provides
  fun DefaultSp(@ApplicationContext context: Context): SharedPreferences = context.sp

  @ProcessLifeCycle
  @FirestoreSp
  @Provides
  fun FirestoreSp(@ApplicationContext context: Context): SharedPreferences =
    context.spWithName("FireStore")

}

