package halamish.expenses.repo.dagger

import dagger.Module
import dagger.Provides
import halamish.expenses.BuildConfig
import halamish.expenses.repo.icons.CurrencySymbolServer
import halamish.expenses.repo.icons.IconsServer
import halamish.expenses.repo.icons.IpServer
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
object NetworkModule {
        @ProcessLifeCycle
        @Provides
        @JvmStatic
        fun okHttpClient(): OkHttpClient = OkHttpClient.Builder()
                .apply { if (BuildConfig.DEBUG)
                        addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                }
                .build()

                private fun retrofit(baseUrl: String, client: OkHttpClient) = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .addConverterFactory(GsonConverterFactory.create())
                .build()


        @ProcessLifeCycle
        @Provides
        @JvmStatic
        @IconsRetrofit
        fun iconsRetrofit(client: OkHttpClient): Retrofit =
                retrofit("https://search.icons8.com", client)

        @ProcessLifeCycle
        @Provides
        @JvmStatic
        fun iconsServer(@IconsRetrofit retrofit: Retrofit): IconsServer = retrofit.create(IconsServer::class.java)


        @ProcessLifeCycle
        @Provides
        @JvmStatic
        @IpRetrofit
        fun ipRetrofit(client: OkHttpClient): Retrofit = retrofit("https://api.ipify.org", client)


        @ProcessLifeCycle
        @Provides
        @JvmStatic
        fun ipServer(@IpRetrofit retrofit: Retrofit): IpServer = retrofit.create(IpServer::class.java)


        @ProcessLifeCycle
        @Provides
        @JvmStatic
        @CurrencySymbolRetrofit
        fun currencySymbolRetrofit(client: OkHttpClient): Retrofit =
                retrofit("http://www.geoplugin.net", client)


        @ProcessLifeCycle
        @Provides
        @JvmStatic
        fun currencySymbolServer(@CurrencySymbolRetrofit retrofit: Retrofit): CurrencySymbolServer = retrofit.create(CurrencySymbolServer::class.java)

}

