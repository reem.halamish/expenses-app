package halamish.expenses.repo.dagger

import dagger.Component
import halamish.expenses.repo.icons.CurrencySymbolServer
import halamish.expenses.repo.icons.IconsRepo

@Component(modules = [NetworkModule::class])
@ProcessLifeCycle
interface NetworkComponent {

    @ProcessLifeCycle
    fun iconsRepo(): IconsRepo

    @ProcessLifeCycle
    fun currencySymbolServer(): CurrencySymbolServer
}