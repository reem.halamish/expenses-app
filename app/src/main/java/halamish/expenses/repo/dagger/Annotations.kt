package halamish.expenses.repo.dagger

import javax.inject.Qualifier
import javax.inject.Scope


@Scope
@Retention
annotation class ProcessLifeCycle


@Scope
@Retention
annotation class UserSessionLifeCycle



@Qualifier
@Retention
annotation class ActivityContext

@Qualifier
@Retention
annotation class ApplicationContext

@Qualifier
@Retention
annotation class DefaultSp

@Qualifier
@Retention
annotation class FirestoreSp


@Qualifier
@Retention
annotation class FirestoreProjectNameSubCollectionPath

@Qualifier
@Retention
annotation class IconsRetrofit
@Qualifier
@Retention
annotation class IpRetrofit
@Qualifier
@Retention
annotation class CurrencySymbolRetrofit

