package halamish.expenses.repo

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import halamish.expenses.infra.ExceptionWithMetadata

import timber.log.Timber

class CrashlyticsTree(private val crashlytics: FirebaseCrashlytics = FirebaseCrashlytics.getInstance()) :
    Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.DEBUG && t == null) return
        val prioLetter = when (priority) {
            Log.DEBUG -> "D"
            Log.INFO -> "I"
            Log.VERBOSE -> "V"
            Log.WARN -> "W"
            Log.ERROR -> "E"
            else -> "?"
        }
        crashlytics.log("$prioLetter/$tag - $message")

        if (t != null) {
            if (t is ExceptionWithMetadata) {
                for ((key, value) in t.metadata) {
                    crashlytics.setCustomKey(key, value)
                }
            }
            crashlytics.recordException(t)
        }
    }
}