package extensions.android

import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import java.util.*


inline fun <K, V> Map<K, V>.nullIfEmpty(): Map<K, V>? = takeUnless { it.isEmpty() }



fun Date.toLocalDate() = Instant.ofEpochMilli(getTime()).atZone(ZoneId.systemDefault()).toLocalDate()
