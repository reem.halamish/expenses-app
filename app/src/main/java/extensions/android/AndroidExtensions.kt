package extensions.android

import android.content.res.ColorStateList
import android.os.Build
import android.os.LocaleList
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.ViewTreeObserver
import androidx.annotation.RequiresApi
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import halamish.expenses.BuildConfig
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import java.io.Serializable
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


suspend fun View.awaitLayout() {
  if (height > 0 && width > 0) return
  return suspendCancellableCoroutine { continuation ->
    val listener = object : ViewTreeObserver.OnGlobalLayoutListener {
      override fun onGlobalLayout() {
        if (height > 0 && width > 0)
          viewTreeObserver.removeOnGlobalLayoutListener(this)
        continuation.resume(Unit)
      }
    }
    viewTreeObserver.addOnGlobalLayoutListener(listener)
    continuation.invokeOnCancellation {
      viewTreeObserver.removeOnGlobalLayoutListener(listener)
    }
  }
}

val defaultGson: Gson by lazy {
  GsonBuilder()
    .apply { if (BuildConfig.DEBUG) setPrettyPrinting() }
    .create()
}

fun Serializable.toJson(gson: Gson = defaultGson): String {
  return gson.toJson(this)
}

inline fun <reified T : Serializable> String.fromJson(gson: Gson = defaultGson) = fromJson(T::class.java, gson)
fun <T : Serializable> String.fromJson(clazz: Class<T>, gson: Gson = defaultGson): T? {
  return try {
    gson.fromJson(this, clazz)
  } catch (e: Exception) {
    Timber.e(e, "can't parse $this into ${clazz.name}")
    null
  }
}

suspend fun <T> Task<T>.awaitComplete(): T {
  return suspendCoroutine { continuation ->
    addOnSuccessListener { continuation.resume(it) }
    addOnFailureListener { continuation.resumeWithException(it) }
  }
}

inline val Any.addressInMemory: Int
  get() = System.identityHashCode(this)


fun String.fromHtml(): Spanned {
  return if (Build.VERSION.SDK_INT >= 24) Html.fromHtml(
    this,
    Html.FROM_HTML_MODE_LEGACY
  ) else {
    @Suppress("DEPRECATION")
    Html.fromHtml(this)
  }
}

suspend fun ViewPropertyAnimator.await() {
  val animator = this
  return suspendCancellableCoroutine { cancellableContinuation ->
    animator.withEndAction { cancellableContinuation.resume(Unit) }
    animator.start()
    cancellableContinuation.invokeOnCancellation {
      animator.cancel()
    }
  }
}

fun LifecycleOwner.doOnceAt(event: Lifecycle.Event, block: () -> Unit) {
  val expectedEvent = event
  lifecycle.addObserver(object: LifecycleEventObserver {
    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
      if (event == expectedEvent) {
        lifecycle.removeObserver(this)
        block()
      }
    }
  })
}

@Suppress("unused")
fun colorStateListOf(vararg mapping: Pair<IntArray, Int>): ColorStateList {
  val (states, colors) = mapping.unzip()
  return ColorStateList(states.toTypedArray(), colors.toIntArray())
}


/**
 * expects a ColorInt.
 * @returns string represnation.
 * Example usage:
 * ```
 * val color = ContextCompat.getColor(context, android.R.color.black)
 * val hexString = color.toHexColorString() // "#000000"
 * ```
 */
@Suppress("unused")
object AndroidExtensions {
  fun Int.toHexColorString() = "#${Integer.toHexString(this)}"
  fun Int.toHexColorStringNoAlpha() = "#${Integer.toHexString(this and 0xFFFFFF)}"
}


@RequiresApi(Build.VERSION_CODES.N) // API 24 at least
fun LocaleList.toIterable(): Iterable<Locale> {
  val list = mutableListOf<Locale>()
  for (i in 0 until size()) {
    list += this[i]
  }
  return list
}