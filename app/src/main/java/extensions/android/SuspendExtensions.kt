package extensions.android

import halamish.expenses.logger.Logger
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn


inline fun <T, R> StateFlow<T>.mapState(scope: CoroutineScope, crossinline transform: (value: T) -> R): StateFlow<R> = map { transform(it) }.stateIn(scope, SharingStarted.Eagerly, transform(value))


fun Dispatchers.IO(logger: Logger) = IO + CoroutineExceptionHandler { _, throwable ->
  logger.e("error while running", throwable)
}
